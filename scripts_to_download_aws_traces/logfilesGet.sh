#!/bin/bash

echo "ap-south-1..."
aws configure set region ap-south-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-south-1_07052020.csv

echo "eu-west-3..."
aws configure set region eu-west-3
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/eu-west-3_07052020.csv

echo "eu-north-1..."
aws configure set region eu-north-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/eu-north-1_07052020.csv

echo "eu-west-2..."
aws configure set region eu-west-2
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/eu-west-2_07052020.csv

echo "eu-west-1..."
aws configure set region eu-west-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/eu-west-1_07052020.csv

echo "ap-northeast-2..."
aws configure set region ap-northeast-2
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-northeast-2_07052020.csv

echo "ap-northeast-1..."
aws configure set region ap-northeast-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-northeast-1_07052020.csv

echo "sa-east-1..."
aws configure set region sa-east-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/sa-east-1_07052020.csv

echo "ca-central-1..."
aws configure set region ca-central-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ca-central-1_07052020.csv

echo "ap-southeast-1..."
aws configure set region ap-southeast-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-southeast-1_07052020.csv

echo "ap-southeast-2..."
aws configure set region ap-southeast-2
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-southeast-2_07052020.csv

echo "eu-central-1..."
aws configure set region eu-central-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/eu-central-1_07052020.csv

echo "us-east-1..."
aws configure set region us-east-1
#aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-03-30T00:00:00 > ./logs/us-east-1_30032020.csv
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/us-east-1_07052020.csv

echo "us-east-2..."
aws configure set region us-east-2
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/us-east-2_07052020.csv

echo "us-west-1..."
aws configure set region us-west-1
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/us-west-1_07052020.csv

echo "us-west-2..."
aws configure set region us-west-2
aws ec2 describe-spot-price-history --start-time 2020-04-20T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/us-west-2_07052020.csv


##### I CANNOT DOWNLOAD THE DATA FOR THE FOLLOWING REGIONS!
#echo "af-south-1..."
#aws configure set region af-south-1
#aws ec2 describe-spot-price-history --start-time 2020-02-08T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/af-south-1_07052020.csv
#
#echo "ap-east-1..."
#aws configure set region ap-east-1
#aws ec2 describe-spot-price-history --start-time 2020-02-08T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-east-1_07052020.csv
#
#echo "ap-northeast-3..."
#aws configure set region ap-northeast-3
#aws ec2 describe-spot-price-history --start-time 2020-02-08T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/ap-northeast-3_07052020.csv
#
#echo "eu-south-1..."
#aws configure set region eu-south-1
#aws ec2 describe-spot-price-history --start-time 2020-02-08T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/eu-south-1_07052020.csv
#
#echo "me-south-1..."
#aws configure set region me-south-1
#aws ec2 describe-spot-price-history --start-time 2020-02-08T00:00:00 --end-time 2020-05-07T00:00:00 > ./logs/me-south-1_07052020.csv

echo "Finished downloading all the logs."


