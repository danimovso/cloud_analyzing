import numpy as np
import scipy
import scipy.cluster.hierarchy as sch
import pandas as pd
%matplotlib
import matplotlib.pyplot as plt

test=0

if test==1:

    mat = np.array([[0, 1, 2, 3, 4], [1, 0, 7, 8, 9], [2, 7, 0, 13, 14], [3, 8, 13, 0, 19], [4, 9, 14, 19, 0]])
else:
    mat = np.loadtxt(open("./all_n_instances_corr_no_static_matrix.csv", "rb"), delimiter=",")

if test==2:
    mat = mat[0:20,0:20]
if test==3:
    mat= mat[0:100,0:100]

print(mat,"\n")
n=len(mat[0])
for i in range(n):
    mat[i][i] = 0

def show(mat,title):

    fig, ax = plt.subplots()
    im = ax.imshow(mat, cmap='jet')
    ax.set_title(title)
    fig.tight_layout()
    fig.savefig(title+'.png')

def replaceColsRows(mat,strider,row,col):
    replaceOne(mat,strider,row)
    replaceOne(mat,strider+1,col)

def replaceOne(mat,strider,row): 
    # Swap the rows
    mat[[row, strider]] = mat[[strider, row]]
    # Swap the cols
    mat[:,[row, strider]] = mat[:,[strider, row]]
#    print('replacing',strider,row)
#    print (mat)


############################################################
####                  pandas Functions                  ####
############################################################
def pandas_swap_columns(df, column1, column2):
    i = list(df.columns)
    a, b = i.index(column1), i.index(column2)
    i[b], i[a] = i[a], i[b]
    df = df[i]
    return df

def pandas_swap_rows(df, row1, row2):
    rows = df.index.tolist()
    a, b = rows.index(row1), rows.index(row2)
    rows[b], rows[a] = rows[a], rows[b]
    df = df.loc[rows]
    return df

def pandas_replaceOne(df,strider,row):
    # Swap the rows
    df = swap_rows(df,strider,row)
    # Swap the cols
    df = swap_columns(df,strider,row)
    return df

def pandas_replaceColsRows(df,strider,row,col):
    df = replaceOne(df,strider,row)
    df = replaceOne(df,strider+1,col)
    return df
############################################################
####              End of pandas functions               ####
############################################################
    

def square_massage(mat,square_starts):
   #identify items which are in a higher correlation, on average, with another square than their own
   #move them to this other square.
   #this will only work as a massage, bebcause the squares need to be well defined.
   #form a matrix,, with the correlation of each vector to each square (at its current formation)
   #match each vector to the square with the highest score
   #reindex by tags
    corr = np.corrcoef(mat)
    L=len(square_starts)-1
    old_square = np.empty(n)#just to allocate it, I actually want it as -1s
    for j in range(L):
        for i in range(square_starts[j],square_starts[j+1]):
            old_square[i] = j
    
    square=old_square
    for i in range(n):
        weight = np.zeros(L)
        for j in range(L):
            weight[j] = np.mean(corr[i][square_starts[j]: square_starts[j+1]-1])
        square[i] = np.argmax(weight)
        if weight[int(old_square[i])] > 0.1*weight[int(square[i])]:
            square[i] = old_square[i]
            print("No swap: ",old_square[i], square[i], weight[int(old_square[i])], weight[int(square[i])])
        else:
            print(old_square[i], square[i], weight[int(old_square[i])], weight[int(square[i])])
    
    future=[]
    for j in range(L):
        for i in range(n):
            if (square[i]==j):
                future.append(i)
    return future

def massage(mat):
#weighted matrix
    aux=np.zeros_like(mat)
    for i in range(n):
        for j in range(n):
            aux[i][j]=np.sqrt(abs(i-j))*mat[i][j]

    row,col = np.unravel_index(np.argmax(aux,axis=None),aux.shape)
    
    val=mat[row][col]
    a=min(row,col)
    b=max(row,col)
    #print ('a b',a,b)
    #plt.show() 
    #go from a+1 to b in col, for row a. search for another place that is lower than val
    if (a+1<b):
        for i in range(a+1,b):
            if (val>mat[i][a]):
                print ('place',a,i,' is ',mat[i][a], 'and val of',a,b,'is',mat[a][b])
                replaceOne(mat,i,b)
                print ('after ',a,i,' is ',mat[i][a], 'and val of',a,b,'is',mat[a][b])
                break
            

def sortRaf(mat,raf,algo):

    strider = 0
    square_starts = []

    while strider < n-2:
        #preserve the start of the square
        square_strider=strider
        square_starts.append(strider)
        #print(mat[strider:,strider:],"\n")
        row,col = np.unravel_index(np.argmax(mat[strider:,strider:],axis=None),mat[strider:,strider:].shape)
        #print(mat[strider:,strider:])
        #print(row,col)
        #print(row+strider,col+strider,strider,"\n")
        replaceColsRows(mat,strider,row+strider,col+strider)
        strider = strider + 2

        while strider < n-1:
            #search for ind, the next index to match the existing open square
            if algo==0:
                #choose the best fitting one from the rest
                #to the last one added to the current square 
                ind = np.argmax(mat[strider-1,strider:],axis=None)
                #print("choice by last line",ind+strider,strider,"\n")
                row=strider-1
                if mat[row,ind+strider] < raf:
                    break
            else:
                #if (algo==1 or 2)
                #choose the best fit from the rest to all of the ones
                #in the current square
                row,ind = np.unravel_index(np.argmax(mat[square_strider:strider-1,strider:],axis=None),mat[square_strider:strider-1,strider:].shape)
                row=row+square_strider
                if algo==2:
                    linesum=np.sum(mat[square_strider:strider-1,strider:],axis=0)
                    #linesum should be the sum of values from square_strider to strider, for every candidate ind. its dimension should be = strider:.
                    #print ('before',ind, linesum.shape,square_strider-strider)
                    ind=np.argmax(linesum,axis=None)
                    #print ('after',ind)
                    #keep the row for the comparison criterion,
                    # it does not matter for the replacement anyhow
                    if np.average(linesum[ind])<raf:
                        break;
                    if mat[row,ind+strider] < raf:
                        break
                else:
                    if mat[row,ind+strider] < raf:
                        break

            #the algos converge here, after the possible break
            if ind != 0:
                replaceOne(mat,strider,ind+strider)
            strider = strider + 1
            
    show(mat,"correlation map, sorted")
    square_starts.append(n)#as the end of the last one
    return square_starts


def hier(mat):
#based on code from the lone nut
    X = mat #df.corr().values
    d = sch.distance.pdist(mat)   # vector of ('55' choose 2) pairwise distances
    L = sch.linkage(d, method='complete')#the list of mergers

    #code from JoernHiss
    fig=plt.figure(figsize=(25, 10))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('sample index')
    plt.ylabel('distance')
    sch.dendrogram(
        L,
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=8.,  # font size for the x axis labels
    )
    fig.savefig('dendrogram.png')
    plt.show()

    
#    ind = sch.fcluster(L, 0.5*d.max(), 'distance')
#    columns = [df.columns.tolist()[i] for i in list((np.argsort(ind)))]
#    df = df.reindex_axis(columns, axis=1)
#    plot_corr(df, size=18)
#    show(mat,"hier")

square_starts=sortRaf(mat,0.3,2)
show(mat,"sortRaf")
plt.show()

future = square_massage(mat,square_starts)
print(future)

df = pd.DataFrame(mat)
df = df.reindex(future, axis=1)
df = df.loc[future]
mat2 = df.to_numpy()
show(mat2,"square_massage")
plt.show()

#for i in range(10):#does not have to be exactly
#    massage(mat)

#plt.show() 
#show(mat,"massage")


#plt.show() 
#hier(mat)
#sortRaf(mat,0.7,1)
#sortRaf(mat,0.7,2)
#sortRaf(mat,0.3,1)
#sortRaf(mat,0.05,2)
    #print(mat,"\n")