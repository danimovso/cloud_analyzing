# cloud_analyzing

This repository stores all the scripts we used in our research "Deconstructing Alibaba Cloud's Preemptible Instance Pricing".
When using the repository, please refer to this page as the source of the scripts, so that others who read your work can find the repository and read the explanation. In addition, please read and cite the paper which describes the traces we analyzed using this repository:

@inproceedings{preemptible,
      author = {Davidow Movsowitz, Danielle and Agmon Ben-Yehuda, Orna  and Dunkelman, Orr},
      title = {Deconstructing Alibaba Cloud's Preemptible Instance Pricing},
      year = {2023},
      publisher = {Association for Computing Machinery},
      address = {New York, NY, USA},
      booktitle = {Proceedings of The 32nd ACM International Symposium on High-Performance Parallel and Distributed Computing (ACM HPDC'23)},
}

The traces that were analyzed using this repository can be found at https://crypto.cs.haifa.ac.il/~movso/traces/ .
     
We further ask that if you publish using this repository, you inform us so that we can link to your paper. Please contact Danielle Movsowitz Davidow at dani.movso (on gmail), Orna Agmon Ben-Yehuda at ladypine (on gmail) or Orr Dunkelman at orrd (at sign) cs (dot sign) haifa (dot sign) ac (dot sign) il regarding this page.
