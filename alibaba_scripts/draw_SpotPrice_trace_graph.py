import matplotlib.pyplot as plt
from matplotlib import dates
import pandas as pd, numpy as np
import argparse
from pandas.plotting import register_matplotlib_converters


register_matplotlib_converters()

tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 10,
    "font.size": 10,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 8,
    "xtick.labelsize": 8,
    "ytick.labelsize": 8
}
plt.rcParams.update(tex_fonts)


def set_size(width=229.87749, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


# 9 Instances in correlation (Fig. 2 in paper)
# instList = ['../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_r5_2xlarge__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_r5_large__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_g5_large__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_g5_large__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_g5_4xlarge__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_g5_xlarge__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_r5_xlarge__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_g5_3xlarge__optimized__clean_new_2.csv',
#            '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_r5_3xlarge__optimized__clean_new_2.csv']


# 77 correlated instances (Fig. 3.b in the paper):
# instList = ['../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_6xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_a__ecs_sn1ne_22xlarge__optimized__clean_new_2.csv', '../../alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m2_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_sn2ne_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_sn2ne_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_t5_c1m4_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_6xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_e__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_ic5_8xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5b__ecs_c6_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_b__ecs_c5_16xlarge__optimized__clean_new_2.csv', '../../alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5b__ecs_g6_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_8xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_6xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_10xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc5_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c8g1_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c4g1_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c24g1_6xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c16g1_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfc6_10xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfc6_8xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfg6_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_c6_13xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_small__optimized__clean_new_2.csv', '../../alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_n4_small__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfg6_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfc6_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_b__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfg6_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_4xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_c6_16xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_6xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_c5_6xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_8xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_large__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_c5_8xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-qingdao_full_traces_updated/cn_qingdao_c__ecs_t5_c1m2_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_t5_c1m1_2xlarge__optimized__clean_new_2.csv', '../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv']


# 52 Instances that show the August Mesa (Fig. 3.a in the paper):
instList = ['../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_medium__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_6xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g6_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_6xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_8xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_6xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_8xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g6_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c6_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_2xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_large__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_3xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c6_xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_4xlarge__optimized__clean_new_2.csv',
'../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_8xlarge__optimized__clean_new_2.csv',]


def draw_instances_in_correlation(instList, fig_num):
    """
    This function draws the Normalized Spot Price of the traces as a function of timestamp for a list of instances.
    It creature Figures 2, 3.a, 3.b from the paper.
    :param instList: The list of instances that we want to draw their traces
    :param fig_num: 1 - Create figure 2 from paper
                    2 - Create figure 3.a from paper
                    3 - Create figure 3.b from paper
    :return:
    """
    # figsize=set_size(fraction=2)
    fig = plt.figure(tight_layout=True, figsize=(5.47807, 2.4651315))
    ax = fig.add_subplot(1,1,1)
    colors = ['blue', 'orange', 'green', 'red', 'purple', 'turquoise', 'magenta', 'navy', 'salmon']
    m_list = ['.', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P',
              'h', 'H', 'D']
    print(len(instList))

    for num, inst in enumerate(instList,0):
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])

        # Create figure 2 from paper
        if fig_num == '1':
            df = df[(df.Timestamp >= pd.to_datetime('6/23/2021 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('06/24/2021  00:00:00 AM'))]

            _, name = inst.split("cn_shenzhen_")
            name, _ = name.split("__optimized__clean_new_2.csv")
            zone, name = name.split("__ecs_")
            nameZone = name + " (zone " + zone + ")"

            ax.plot(df.Timestamp, df.NormPrice, ':', color=colors[num], marker=m_list[num], label=nameZone)

        # create figure 3.a from paper
        elif fig_num == '2':
            df = df[(df.Timestamp >= pd.to_datetime('8/18/2020 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('8/26/2020  00:00:00 AM'))]

            ax.plot(df.Timestamp, df.NormPrice, '.:', markersize=2)

        # Create figure 3.b from paper
        elif fig_num == '3':
            df = df[(df.Timestamp >= pd.to_datetime('8/14/2020 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('8/20/2020 00:00:00 AM'))]

            ax.plot(df.Timestamp, df.NormPrice, '.:', markersize=2)

    ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m  %H:%M'))
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.ylabel('Normalized Spot price')
    plt.xticks(rotation = 45)
    plt.xlabel('Timestamp')
    plt.show()


def draw_trace_type():
    """
    This function creates Figure 1 in the paper.
    The function creates the figure that shows the differences between the different types of trace files.
    :return:
    """

    file1 = '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc_2.csv'
    file2 = '../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new_2.csv'

    fig = plt.figure(tight_layout=True, figsize=(5.47807, 2.4651315))
    ax = fig.add_subplot(1, 1, 1)
    df = pd.read_csv(file1)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df[(df.Timestamp >= pd.to_datetime('5/3/2020 12:00:00 PM')) & (df.Timestamp < pd.to_datetime('5/5/2020 00:00:00 AM'))]
    ax.scatter(df.Timestamp, df.SpotPrice, marker='x', color='deepskyblue', label="Original spot price trace")

    df = pd.read_csv(file2)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('MyIndex')
    df = df[(df.Timestamp >= pd.to_datetime('5/3/2020 12:00:00 PM')) & (df.Timestamp < pd.to_datetime('5/5/2020 00:00:00 AM'))]
    ax.plot(df.Timestamp, df.SpotPrice, ".:", color='black', label="Effective price trace")
    ax.scatter(df.Timestamp[df.PeakInfo48.notnull() & (df.clean==1)], df.SpotPrice[df.PeakInfo48.notnull() & (df.clean==1)], marker='o', color='red', label="Extremum prices")

    ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m  %H:%M'))
    plt.setp(ax.get_xticklabels(), rotation = 45)
    ax.set_xlabel("Timestamp")
    ax.set_ylabel("Preemptible price (\$)")
    ax.legend(loc='upper left')
    plt.show()


def draw_patterns():
    """
    The function creates Figure 13 in the paper.
    :return:
    """
    fig = plt.figure(tight_layout='True', figsize = set_size())

    ax = fig.add_subplot(2,1,1)
    #  Ceil - Floor zig-zag in decrease:
    inst = '../../alibaba_work/ap-southeast-2_full_traces_updated/ap_southeast_2a__ecs_se1ne_2xlarge__optimized__clean_new_2.csv'

    df = pd.read_csv(inst)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('MyIndex')
    df = df[(df.Timestamp >= pd.to_datetime('2019-02-07 00:00:00')) & (df.Timestamp <= pd.to_datetime('2019-02-10 00:00:00'))].copy()

    ax.plot(df.Timestamp, df.NormPrice, '.:', color='black')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == -0.014], df.NormPrice[np.round(df.PriceDelta, 3) == -0.014],
               marker='x', color='deepskyblue', label='floor')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == -0.013], df.NormPrice[np.round(df.PriceDelta, 3) == -0.013],
               s=30, facecolors='none', edgecolors='red', label='ceil')

    ax.legend(loc='lower right')
    ax.set_xlabel("Timestamp\n\n(a) From 2019.")
    ax.set_ylabel("Normalized price")
    # ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m-%y %H:%M'))
    plt.setp(ax.get_xticklabels(), rotation=45)

    ax = fig.add_subplot(2,1,2)
    # Ceil - Floor zig-zag in increase:
    inst = '../../alibaba_work\cn-beijing_full_traces_updated/cn_beijing_k__ecs_hfc6_3xlarge__optimized__clean_new_2.csv'

    df = pd.read_csv(inst)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('MyIndex')

    df = df[(df.Timestamp >= pd.to_datetime('2021-03-02 00:00:00')) & (df.Timestamp <= pd.to_datetime('2021-03-06 00:00:00'))].copy()

    ax.plot(df.Timestamp, df.NormPrice, '.:', color='black')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == 0.012], df.NormPrice[np.round(df.PriceDelta, 3) == 0.012],
               marker='x', color='deepskyblue', label='floor')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == 0.013], df.NormPrice[np.round(df.PriceDelta, 3) == 0.013],
               s=30, facecolors='none', edgecolors='red', label='ceil')

    ax.legend(loc='lower right')
    ax.set_xlabel("Timestamp\n\n(b) From 2021.")
    ax.set_ylabel("Normalized price")
    # ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m-%y %H:%M'))
    plt.setp(ax.get_xticklabels(), rotation=45)

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--function", help='The function we want to run')
    parser.add_argument("--fig_num", help='The number of the figure we want to create from the paper')

    args = parser.parse_args()

    if args.function:
        func = args.function
        if func == 'draw_instances_in_correlation':
            print("Running draw_instances_in_correlation function...")
            draw_instances_in_correlation(instList, args.fig_num)

        elif func == 'draw_trace_type':
            print("Running spotPrice_trace_for_inst function...")
            draw_trace_type()

        elif func == 'draw_patterns':
            print("Running draw_patterns function...")
            draw_patterns()


    print("Finish")
