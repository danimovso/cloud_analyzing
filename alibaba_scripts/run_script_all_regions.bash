#!/bin/bash

#ap-northeast-1 ap-south-1 ap-southeast-1 ap-southeast-2 ap-southeast-3 ap-southeast-5 cn-beijing cn-hangzhou cn-hongkong cn-huhehaote cn-qingdao cn-shanghai cn-shenzhen cn-zhangjiakou eu-central-1 eu-west-1 me-east-1 us-east-1 us-west-1
     
for region in ap-northeast-1 ap-south-1 ap-southeast-1 ap-southeast-2 ap-southeast-3 ap-southeast-5 cn-beijing cn-hangzhou cn-hongkong cn-huhehaote cn-qingdao cn-shanghai cn-shenzhen cn-zhangjiakou eu-central-1 eu-west-1 me-east-1 us-east-1 us-west-1
do
    echo "Working on $region..."
    # python3 ./cloud_analyzing_repo/alibaba_scripts/parsing_alibaba_logs.py --region $region --function 'createTracePerInstance'
    python3 ./cloud_analyzing_repo/alibaba_scripts/parsing_alibaba_logs.py --region $region --function 'cleanTraces' > "./alibaba_work/${region}_full_traces_updated/${region}_log.txt" 2> "./alibaba_work/${region}_full_traces_updated/${region}_errors.txt"&
    
    # python3 ./cloud_analyzing_repo/alibaba_scripts/old_analyze_slopes.py --region $region --function 'add_NormPriceDelta_col' > "./alibaba_work/${region}_full_traces/${region}_log.txt" 2> "./alibaba_work/${region}_full_traces/${region}_errors.txt"&
    # python3 ./scripts/old_create_ecdf_of_causation_data.py --region $region
    # python3 ./cloud_analyzing_repo/alibaba_scripts/add_tags_to_traces.py --region $region --function 'add_normalized_peaks_to_traces' > "./alibaba_work/${region}_full_traces/${region}_log.txt" 2> "./alibaba_work/${region}_full_traces/${region}_errors.txt"&
    # python3 ./scripts/feature_extract.py --function 'normPriceDelta_percentile_extract' --region $region

done

echo "Finished sending the scripts for all regions..."




###### Copy traces to the crypto server######
#mkdir ap-northeast-1_2020_traces ap-south-1_2020_traces ap-southeast-1_2020_traces ap-southeast-2_2020_traces ap-southeast-3_2020_traces ap-southeast-5_2020_traces cn-beijing_2020_traces cn-hangzhou_2020_traces cn-hongkong_2020_traces cn-huhehaote_2020_traces cn-qingdao_2020_traces cn-shanghai_2020_traces cn-shenzhen_2020_traces cn-zhangjiakou_2020_traces eu-central-1_2020_traces eu-west-1_2020_traces me-east-1_2020_traces us-east-1_2020_traces us-west-1_2020_traces
#
#
#scp -r ./ap-northeast-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/ap-northeast-1_2020_traces/ 
#scp -r ./ap-south-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/ap-south-1_2020_traces/
#scp -r ./ap-southeast-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/ap-southeast-1_2020_traces/ 
#scp -r ./ap-southeast-2_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/ap-southeast-2_2020_traces/ 
#scp -r ./ap-southeast-3_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/ap-southeast-3_2020_traces/ 
#scp -r ./ap-southeast-5_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/ap-southeast-5_2020_traces/ 
#scp -r ./cn-beijing_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-beijing_2020_traces/
#scp -r ./cn-hangzhou_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-hangzhou_2020_traces/
#scp -r ./cn-hongkong_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-hongkong_2020_traces/
#scp -r ./cn-huhehaote_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-huhehaote_2020_traces/
#scp -r ./cn-qingdao_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-qingdao_2020_traces/
#scp -r ./cn-shanghai_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-shanghai_2020_traces/
#scp -r ./cn-shenzhen_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-shenzhen_2020_traces/
#scp -r ./cn-zhangjiakou_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/cn-zhangjiakou_2020_traces/ 
#scp -r ./eu-central-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/eu-central-1_2020_traces/
#scp -r ./eu-west-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/eu-west-1_2020_traces/
#scp -r ./me-east-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/me-east-1_2020_traces/
#scp -r ./us-east-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/us-east-1_2020_traces/
#scp -r ./us-west-1_2020_traces/*.csv movso@crypto.cs.haifa.ac.il:public_html/alibaba_logs/us-west-1_2020_traces/

