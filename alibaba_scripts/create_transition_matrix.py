import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from pandas.tseries.offsets import MonthEnd, MonthBegin
import datetime

import matplotlib
from matplotlib.colors import LogNorm
from matplotlib.gridspec import GridSpec
# matplotlib.use('Agg')

# from pandas.plotting import register_matplotlib_converters
# register_matplotlib_converters()

# from statsmodels.distributions.empirical_distribution import ECDF
import argparse
from collections import Counter

# plt.style.use('grayscale')
# plt.style.use('seaborn-colorblind')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 14,
        "font.size": 14,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 12,
        "xtick.labelsize": 12,
        "ytick.labelsize": 12
    }

plt.rcParams.update(tex_fonts)

def set_size(width=229.87749, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


################################################
# Plot the matrix heatmap.                     #
# If you want to add the values to each cell   #
# set val_flag == 1.                           #
################################################
def show(mat, index_list, title, num, index, val_flag=0):
    mat = np.around(mat,4)
    index_list = np.around(index_list, 2)
    # fig, ax = plt.subplots()
    ax = fig.add_subplot(1, num, index)
    im = ax.imshow(mat, cmap='jet')

    # We want to show all ticks...
    ax.set_xticks(np.arange(len(index_list)))
    ax.set_yticks(np.arange(len(index_list)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(index_list)
    ax.set_yticklabels(index_list)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    if val_flag:
        for i in range(len(mat)):
           for j in range(len(mat)):
               text = ax.text(j, i, mat[i, j],
                              ha="center", va="center", color="w")
    ax.set_title(title)


################################################
# Plot the heatmap directly from the the       #
# dataframe and add the names of the insts     #
# as the axes of the image.                    #
################################################
def show_df(df, title,num,index):
    ax = fig.add_subplot(2, num, index)
    ax.imshow(np.around(df.values,4), cmap='jet')
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(df.columns.tolist())))
    ax.set_yticks(np.arange(len(df.columns.tolist())))
    # ... and label them with the respective list entries
    ax.set_xticklabels(df.columns)
    ax.set_yticklabels(df.columns)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.set_title(title)


def create_state_transition_mat():
    instList = ["../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv"]
    # instList = ["../../Alibaba_log_files/interesting_traces/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv"]
    # instList = ["F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc.csv"]

    # instList = "./alibaba_work/*_full_traces/*vpc.csv"
    # instList = "F:/alibaba_work/*_full_traces_updated/*vpc.csv"
    # instList = glob.glob(instList)
    total_count = len(instList)

    price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]
    mat_size = len(price_list)
    total_mat = np.zeros((4, mat_size, mat_size))

    for count, inst in enumerate(instList, 1):
        print("Processing ", inst, "(", count, "/", total_count, ")")
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        # df['NormPrice'] = df.SpotPrice / df.OriginPrice

        df = df[(df.Timestamp >= pd.to_datetime('12/1/2019 00:00:00 AM'))]
        # df = df[(df.Timestamp >= pd.to_datetime('01/1/2020 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('12/1/2020 00:00:00 AM'))]
        # df = df[(df.Timestamp >= pd.to_datetime('9/1/2019 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('12/1/2020 00:00:00 AM'))]
        # df = df[(df.Timestamp >= pd.to_datetime('12/1/2020 00:00:00 AM'))]

        if not df.empty:

            mat = np.zeros((4, mat_size, mat_size))

            df.NormPrice = np.around(df.NormPrice, 2)
            # print(df.NormPrice)

            length = df.Timestamp.count()
            if length >= 2:
                prev = df.NormPrice.iloc[0]
                curr = df.NormPrice.iloc[1]

                # 0 - increase, 1 - stay, 2 - decrease
                if curr > prev:
                    flag = 0
                elif curr < prev:
                    flag = 3
                else:
                    flag = 1

                for index in range(1, length):
                    curr = df.NormPrice.iloc[index]
                    # increase in price
                    if curr > prev:
                        row = price_list.index(prev)
                        col = price_list.index(curr)
                        # mat[flag][row][col] += 1
                        total_mat[flag][row][col] += 1
                        flag = 0
                    # decrease in price
                    elif curr < prev:
                        row = price_list.index(prev)
                        col = price_list.index(curr)
                        # mat[flag][row][col] += 1
                        total_mat[flag][row][col] += 1
                        flag = 3
                    # price stays the same
                    else:
                        if flag == 0 or flag == 1:
                            row = price_list.index(prev)
                            col = price_list.index(curr)
                            # mat[flag][row][col] += 1
                            total_mat[flag][row][col] += 1
                            flag = 1
                        elif flag == 3 or flag == 2:
                            row = price_list.index(prev)
                            col = price_list.index(curr)
                            # mat[flag][row][col] += 1
                            total_mat[flag][row][col] += 1
                            flag = 2

                    prev = curr

                # name, _ = inst.split('__vpc')
                # file = name + "_state_transition_mat_before_01-2020.npy"
                #
                # print("Creating file: ", file)
                # with open(file, 'wb') as f:
                #     np.save(f, mat)

    # for p in [0, 1, 2, 3]:
    #     sum_list = np.sum(total_mat[p], axis=1)
    #     for i in range(len(total_mat[p])):
    #         sum_row = sum_list[i]
    #         if sum_row != 0:
    #             for j in range(len(total_mat[p])):
    #                 total_mat[p][i][j] = total_mat[p][i][j] / sum_row

    # file2 = "./alibaba_work/feature_files/state_transition_matrix_all_instances_before_01-2020.npy"
    # file2 = "F:/alibaba_work/transition_matrices/state_transition_mat_all_instances_from_01-2021.npy"
    file2 = "../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge_state_transition_mat_epoch_2.npy"
    with open(file2, 'wb') as f:
        np.save(f, total_mat)


# ######################################################################### #
# This function creates a 2D transition matrix for each trace separately.   #
# The matrix created doesn't take into account the state the trace is in    #
# while calculating the matrix. It only documents the transitions from one  #
# price to the next.                                                        #
# ######################################################################### #
def create_transition_mat():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    for region in regions:
        print("Working on region ", region)
        # instList = "../../alibaba_work/" + region + "_full_traces_updated/*vpc_2.csv"
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)

        # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        # instList = ["../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc_2.csv"]
        # instList = ["../../Alibaba_log_files/interesting_traces/cn_beijing_a__ecs_e3_3xlarge__optimized__vpc.csv"]

        total_count = len(instList)

        price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]
        price_time_dict_before = {i: 0 for i in price_list}
        price_time_dict_after = {i: 0 for i in price_list}
        mat_size = len(price_list)
        total_mat_before = np.zeros((mat_size, mat_size))
        total_mat_after = np.zeros((mat_size, mat_size))

        total_time_before = 0
        total_time_after = 0

        for num, inst in enumerate(instList,1):
            print("Processing ", inst, num, "/", total_count)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('MyIndex')
            df = df[df.clean == 1].copy()

            df1 = df[(df.Timestamp < pd.to_datetime('1/1/2020 12:00:00 AM'))].copy()
            df2 = df[(df.Timestamp >= pd.to_datetime('1/1/2020 12:00:00 AM'))].copy()

            # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
            # plt.plot(df.Timestamp, df.NormPrice, '.-')

            if not df1.empty:

                # mat = np.zeros((mat_size, mat_size))
                df1.reset_index(drop=True, inplace=True)
                df1.NormPrice = np.around(df1.NormPrice, 2)

                length = df1.Timestamp.count()
                prev = df1.NormPrice.iloc[0]
                for index in range(1, length):
                    curr = df1.NormPrice.iloc[index]
                    if (df1.missing_data.iloc[index] != 0) or (curr == prev):
                        pass
                    else:
                        row = price_list.index(prev)
                        col = price_list.index(curr)

                        # mat[row][col] += 1
                        total_mat_before[row][col] += 1
                        price_time_dict_before[prev] += (df1.TimeInSeconds.iloc[index] - df1.TimeInSeconds.iloc[index-1]) / 3600
                        total_time_before += (df1.TimeInSeconds.iloc[index] - df1.TimeInSeconds.iloc[index-1]) / 3600
                    prev = curr

            if not df2.empty:

                # mat = np.zeros((mat_size, mat_size))
                df2.reset_index(drop=True, inplace=True)
                df2.NormPrice = np.around(df2.NormPrice, 2)

                length = df2.Timestamp.count()
                prev = df2.NormPrice.iloc[0]
                for index in range(1, length):
                    curr = df2.NormPrice.iloc[index]
                    if (df2.missing_data.iloc[index] != 0) or (curr == prev):
                        pass
                    else:
                        row = price_list.index(prev)
                        col = price_list.index(curr)

                        # mat[row][col] += 1
                        total_mat_after[row][col] += 1
                        price_time_dict_after[prev] += (df2.TimeInSeconds.iloc[index] - df2.TimeInSeconds.iloc[index - 1]) / 3600
                        total_time_after += (df2.TimeInSeconds.iloc[index] - df2.TimeInSeconds.iloc[index - 1]) / 3600
                    prev = curr

                # name, _ = inst.split('__vpc')
                # file = name + "_full_transition_mat_11-2020.npy"
                #
                # print("Creating file: ", file)
                # with open(file, 'wb') as f:
                #     np.save(f, mat)

        # file2 = "./cloud_analyzing_repo/alibaba_scripts/transition_matrices/transition_mat_all_instances_11-2019.npy"
        # file2 = "../../Alibaba_log_files/transition_matrices/transition_mat_all_instances_after_12-2019.npy"
        # file2 = "F:/alibaba_work/transition_matrices/" + region + "_transition_mat_epoch_2.npy"
        # print(file2)
        # with open(file2, 'wb') as f:
        #     np.save(f, total_mat)

        sum_mat = np.sum(total_mat_before)
        for i in range(len(total_mat_before)):
            for j in range(len(total_mat_before)):
                total_mat_before[i][j] = total_mat_before[i][j] / sum_mat

        fig = plt.figure(tight_layout=True)
        ax = fig.add_subplot(1,1,1)
        im = ax.imshow(total_mat_before, cmap='YlGnBu', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
        fig.colorbar(im, ax=ax, location='left', shrink=0.8)
        # plt.title("before 12-2020")

        sum_mat = np.sum(total_mat_after)
        for i in range(len(total_mat_after)):
            for j in range(len(total_mat_after)):
                total_mat_after[i][j] = total_mat_after[i][j] / sum_mat

        fig = plt.figure(tight_layout=True)
        ax = fig.add_subplot(1,1,1)
        im = ax.imshow(total_mat_after, cmap='YlGnBu', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
        fig.colorbar(im, ax=ax, location='left', shrink=0.8)
        # plt.title("after 12-2020")

        fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
        ax = fig.add_subplot(1, 1, 1)

        price_time_dict_before = {k: (v / total_time_before) for k, v in price_time_dict_before.items()}
        before = price_time_dict_before.items()
        before = sorted(before)
        ax.scatter(*zip(*before), s=80, facecolors='none', edgecolors='red', label="Before 01-2020")

        price_time_dict_after = {k: (v / total_time_after) for k, v in price_time_dict_after.items()}
        after = price_time_dict_after.items()
        after = sorted(after)
        ax.scatter(*zip(*after), marker='x', color='black', label="After 01-2020")

        ax.set_xlim(0.01,1.01)
        ax.set_xlabel("Normalized Price")
        ax.set_ylabel("Fraction of time held")
        ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)

        fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
        ax = fig.add_subplot(1, 1, 1)

        before_data = []
        for k, v in price_time_dict_before.items():
            col = price_list.index(k)
            before_data.append((v,total_mat_before[:, col].sum()))
        before = sorted(before_data)
        ax.scatter(*zip(*before), s=80, facecolors='none', edgecolors='red', label="Before 01-2020")

        after_data = []
        for k, v in price_time_dict_after.items():
            col = price_list.index(k)
            after_data.append((v, total_mat_after[:, col].sum()))
        after = sorted(after_data)
        ax.scatter(*zip(*after), marker='x', color='black', label="After 01-2020")

        ax.set_xlabel("Fraction of time held")
        ax.set_ylabel("Probability of dropping\n to price level")
        ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)



        plt.show()


def draw_time_held_in_price():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    for region in regions:
        print("Working on region ", region)
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)

        # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        # instList = ["../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc_2.csv"]
        # instList = ["../../Alibaba_log_files/interesting_traces/cn_beijing_a__ecs_e3_3xlarge__optimized__vpc.csv"]

        total_count = len(instList)

        price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]
        data = dict()
        total_time = dict()
        for num, inst in enumerate(instList,1):
            print("Processing ", inst, num, "/", total_count)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('MyIndex')
            df = df[df.clean == 1].copy()

            df.NormPrice = np.around(df.NormPrice, 2)
            # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
            # plt.plot(df.Timestamp, df.NormPrice, '.-')

            df['month_year'] = df['Timestamp'].dt.to_period('M')
            grouped = df.groupby('month_year')
            for month, group in grouped:
                # print(month, (pd.Timestamp(str(month)) + MonthBegin(0)), (pd.Timestamp(str(month)) + MonthBegin(1)))
                start = pd.to_datetime("2018-01-01 00:00:00.000")
                end_month = ((pd.Timestamp(str(month)) + MonthBegin(1) ) - start) / np.timedelta64(1, 's')
                begin_month = ((pd.Timestamp(str(month)) + MonthBegin(0)) - start) / np.timedelta64(1, 's')

                if month not in data:
                    data[month] = {i: 0 for i in price_list}
                    total_time[month] = 0

                group1 = group.copy()
                group1.reset_index(drop=True, inplace=True)

                length = group1.Timestamp.count()
                prev = group1.NormPrice.iloc[0]
                data[month][prev] += group1.TimeInSeconds.iloc[0] - begin_month
                total_time[month] += group1.TimeInSeconds.iloc[0] - begin_month
                for index in range(1, length):
                    curr = group1.NormPrice.iloc[index]
                    if (group1.missing_data.iloc[index] != 0) or (curr == prev):
                        pass
                    else:
                        curr_time = (group1.TimeInSeconds.iloc[index] - group1.TimeInSeconds.iloc[index - 1]) / 3600
                        data[month][prev] += curr_time
                        total_time[month] += curr_time
                    prev = curr
                data[month][prev] += end_month - group1.TimeInSeconds.iloc[-1]
                total_time[month] += end_month - group1.TimeInSeconds.iloc[-1]

        fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))

        f1 = 0
        f2 = 0
        for month, price_held_values in data.items():
            price_held_values = {k: (v/total_time[month]) for k, v in price_held_values.items()}
            price_held_values = price_held_values.items()
            price_held_values = sorted(price_held_values)
            # print(price_held_values)

            month_df = pd.DataFrame(price_held_values, columns =['Price', 'Fraction'])
            # month_df.set_index('Price', inplace=True)
            # print(month_df)
            cumsum_data = np.cumsum(month_df.Fraction)
            # print(cumsum_data)
            # plt.plot(month_df.Price,cumsum_data, '.-', label=month)
            # month_df.sort_index().cumsum().plot(label=month)

            if price_held_values:
                if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    # print(price_held_values)
                    if f1 == 0:
                        plt.plot(month_df.Price, cumsum_data, '.-', color='black', label="Before 01-2020")
                        # plt.scatter(*zip(*price_held_values), marker='x', color='black', label="Before 01-2020", alpha=0.5)
                        f1 = 1
                    else:
                        plt.plot(month_df.Price, cumsum_data, '.-', color='black', )
                        # plt.scatter(*zip(*price_held_values), marker='x', color='black', alpha=0.5)
                else:
                    if f2 == 0:
                        plt.plot(month_df.Price, cumsum_data, '.-', color='orange',  label="After 01-2020")
                        # plt.scatter(*zip(*price_held_values), marker='x', color='orange', label="After 01-2020", alpha=0.5)
                        f2 = 1
                    else:
                        plt.plot(month_df.Price, cumsum_data, '.-', color='orange')
                        # plt.scatter(*zip(*price_held_values), marker='x', color='orange', alpha=0.5)

        plt.xlabel("Normalized price")
        plt.ylabel("Fraction of time held")
        plt.legend()

        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    # parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    # parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
    # parser.add_argument("--slope", help='The slope we want to examine')

    args = parser.parse_args()

    # if args.region:
    #     region = args.region
    # else:
    #     region = 'cn-beijing'

    if args.function:
        func = args.function
        if func == 'create_transition_mat':
            print("Running create_transition_mat....")
            create_transition_mat()

        elif func == 'create_state_transition_mat':
            print("Running create_state_transition_mat...")
            create_state_transition_mat()

        elif func == 'draw_time_held_in_price':
            print("Running draw_time_held_in_price...")
            draw_time_held_in_price()

    print("Finished!")
