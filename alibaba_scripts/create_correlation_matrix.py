import glob
import pandas as pd


regions = ['cn-hangzhou', 'cn-hongkong']
files = []
for region in regions:
    print(region)
    instList = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
    instList = glob.glob(instList)
    files.extend(instList)

print(len(files))


# dirName = "./alibaba_work/*2020_traces/*vpc.csv"
# files = glob.glob(dirName)

# The relevant dates for the different months:
# April 2020 (TimeInSeconds): 70930800 - 73526400
# August 2020 (TimeInSeconds): 81471600 - 84153600
# July 2020 (TimeInSeconds): 78793218 - 81475200
# September 2020 (TimeInSeconds): 84150035 - 86745652
# October 2020 (TimeInSeconds): 86742024 - 89424024

# print("Calculating the correlation for April 2020")
print("creating the list of instance names...")
names = []
for f in files:
    _,_,_,name1 = f.split('/')
    name1,_ = name1.split('__vpc')
    df1 = pd.read_csv(f)
    df1['Timestamp'] = pd.to_datetime(df1['Timestamp'])
    df1 = df1.sort_values('Timestamp')

    df1 = df1[(df1.Timestamp < pd.to_datetime("12/30/2019 00:00:00 AM")) & (df1.Timestamp > pd.to_datetime("12/23/2019 00:00:00 AM"))]
    # df1 = df1[df1.TimeInSeconds < 73526400]
    # df1 = df1[df1.TimeInSeconds > 70930800]
    df1['NormPrice'] = df1.SpotPrice / df1.OriginPrice
    # df1.index = df1['Timestamp']
    if (df1.NormPrice.nunique() == 1) or (df1.Timestamp.count() == 0):
        continue
    else:
        names.append(name1)

print("finished creating the list of instance names.")
print("list of names length:"+str(len(names)))

f1= open("./alibaba_work/feature_files/corr_matrix_23_to_30_january.csv","w")
f1.write(","+str(names)+"\n")

# f2= open("./alibaba_work/feature_files/all_instances__september_2020_corr_no_static_list.csv","w")
# f2.write("Inst_A,Inst_B,Corr\n")

for file1 in files:
    print("calculating correlations for "+file1+" ...")
    _,_,_,name1 = file1.split('/')
    name1,_ = name1.split('__vpc')
    df1 = pd.read_csv(file1)
    df1['Timestamp'] = pd.to_datetime(df1['Timestamp'])
    df1 = df1.sort_values('Timestamp')
    df1 = df1[(df1.Timestamp < pd.to_datetime("12/30/2019 00:00:00 AM")) & (df1.Timestamp >= pd.to_datetime("12/23/2019 00:00:00 AM"))]
    # df1 = df1[df1.TimeInSeconds < 73526400]
    # df1 = df1[df1.TimeInSeconds > 70930800]
    df1['NormPrice'] = df1.SpotPrice/df1.OriginPrice
    df1 = df1[['Timestamp','NormPrice']].copy()
    # df1['Timestamp'] = pd.to_datetime(df1['Timestamp'])
    if (df1.NormPrice.nunique() == 1) or (df1.Timestamp.count() == 0):
        continue
    df1 = df1.sort_values('Timestamp')
    
    row = []
    for file2 in files:
        print("second", file2)
        _,_,_,name2 = file2.split('/')
        name2,_ = name2.split('__vpc')
        df2 = pd.read_csv(file2)
        df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
        df2 = df2.sort_values('Timestamp')
        df2 = df2[(df2.Timestamp < pd.to_datetime("12/30/2019 00:00:00 AM")) & (df2.Timestamp >= pd.to_datetime("12/23/2019 00:00:00 AM"))]
        # df2 = df2[df2.TimeInSeconds < 73526400]
        # df2 = df2[df2.TimeInSeconds > 70930800]
        df2['NormPrice'] = df2.SpotPrice/df2.OriginPrice
        df2 = df2[['Timestamp','NormPrice']].copy()
        # df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
        if (df2.NormPrice.nunique() == 1) or (df2.Timestamp.count() == 0):
            continue
        df2 = df2.sort_values('Timestamp')
        
        result = pd.merge(df1, df2, how='outer', on='Timestamp', suffixes=('_1', '_2'))
        corr_num = result['NormPrice_1'].corr(result['NormPrice_2'])

        row.append(float(corr_num))
        
        # write to the corr list file
        # if corr_num != 'NaN':
        #     f2.write(name1+","+name2+","+str(corr_num)+"\n")
        # else:
        #     print("Nan: ",name2)
    
    # write to the corr matrix file    
    print(str(row))
    f1.write(name1+","+str(row)+"\n")

f1.close()
# f2.close()
print("Finished creating correlation mat.")
