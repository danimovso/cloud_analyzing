import pandas as pd
import glob
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pandas.plotting import register_matplotlib_converters
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

register_matplotlib_converters()
from statsmodels.distributions.empirical_distribution import ECDF
from scipy import stats

import collections

import argparse

# plt.style.use('grayscale')
# plt.style.use('tableau-colorblind10')

tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 10,
    "font.size": 10,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 8,
    "xtick.labelsize": 8,
    "ytick.labelsize": 8
}

plt.rcParams.update(tex_fonts)


def set_size(width=395.8225, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def add_pattern_column():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean_2.csv"
    instList = glob.glob(instList)

    for num, inst in enumerate(instList, 1):
        print("processing ", inst, num)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]

        df['pattern_mark'] = np.nan

        name, csv = inst.split('__vpc__clean_2.csv')
        file = name + "__pattern_mark.csv"
        print("creating ", file)
        df.to_csv(inst, index=False)


def find_pulses():
    instList = "../../alibaba_work/*_full_traces_updated/*_clean_new_2.csv"
    instList = glob.glob(instList)
    # instList = [
    #     '../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_mn4_large__optimized__clean_new_2.csv']
    total_count = len(instList)

    all_data = []
    for num, inst in enumerate(instList, 1):
        print("processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df.reset_index(drop=True, inplace=True)

        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:', color='black')

        length = df.MyIndex.count()
        for index in range(0, length - 3):
            if df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 1] != (df['NormPrice'].iloc[index]) and \
                    df['MyIndex'].iloc[index] == df['MyIndex'].iloc[index + 1] - 1 == df['MyIndex'].iloc[index + 2] - 2 == df['MyIndex'].iloc[index + 3] - 3 and \
                    (df['missing_data'].iloc[index + 2] == 0) and (df['missing_data'].iloc[index + 3] == 0):
                all_data.append(
                    [inst, df['Timestamp'].iloc[index + 1], df['Timestamp'].iloc[index + 2],
                     df['TimeInSeconds'].iloc[index + 1], df['TimeInSeconds'].iloc[index + 2],
                     (df['TimeInSeconds'].iloc[index + 2] - df['TimeInSeconds'].iloc[index + 1]),
                     df['OriginPrice'].iloc[index], df['SpotPrice'].iloc[index],
                     df['NormPrice'].iloc[index], df['SpotPrice'].iloc[index + 1],
                     df['NormPrice'].iloc[index + 1],
                     (df['SpotPrice'].iloc[index + 1] - df['SpotPrice'].iloc[index]),
                     (df['NormPrice'].iloc[index + 1] - df['NormPrice'].iloc[index])])

    new_df = pd.DataFrame(all_data, columns=['inst', 'Timestamp1', 'Timestamp2', 'TimeInSeconds1', 'TimeInSeconds2',
                                             'TimeInterval', 'OriginPrice', 'BaseSpotPrice', 'BaseNormPrice',
                                             'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_pulse_data_by_hand.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)
    plt.show()

def find_chevrons_in_all_regions():
    # instList = "D:/alibaba_work/*_full_traces_updated/*_clean_new_2.csv"
    # instList = glob.glob(instList)
    instList = [
        'D:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_mn4_large__optimized__clean_new.csv']
    total_count = len(instList)

    all_data = []
    for num, inst in enumerate(instList, 1):
        print("processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df.reset_index(drop=True, inplace=True)

        df = df[df.FixedPeakInfo.notnull()].copy()
        df = df[df.PeakInfo12.notnull()].copy()
        df = df[df.PeakInfo24.notnull()].copy()
        df = df[df.PeakInfo48.notnull()].copy()
        df = df[df.Chevron == 1].copy()

        length = df.MyIndex.count()
        for index in range(0, length - 3):
            if df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 1] != (df['NormPrice'].iloc[index]) and \
                    df['MyIndex'].iloc[index] == df['MyIndex'].iloc[index + 1] - 1 == df['MyIndex'].iloc[
                index + 2] - 2 == df['MyIndex'].iloc[index + 3] - 3:
                all_data.append(
                    [inst, df['Timestamp'].iloc[index + 1], df['Timestamp'].iloc[index + 2],
                     df['TimeInSeconds'].iloc[index + 1], df['TimeInSeconds'].iloc[index + 2],
                     (df['TimeInSeconds'].iloc[index + 2] - df['TimeInSeconds'].iloc[index + 1]),
                     df['OriginPrice'].iloc[index], df['SpotPrice'].iloc[index],
                     df['NormPrice'].iloc[index], df['SpotPrice'].iloc[index + 1],
                     df['NormPrice'].iloc[index + 1],
                     (df['SpotPrice'].iloc[index + 1] - df['SpotPrice'].iloc[index]),
                     (df['NormPrice'].iloc[index + 1] - df['NormPrice'].iloc[index])])

    new_df = pd.DataFrame(all_data, columns=['inst', 'Timestamp1', 'Timestamp2', 'TimeInSeconds1', 'TimeInSeconds2',
                                             'TimeInterval', 'OriginPrice', 'BaseSpotPrice', 'BaseNormPrice',
                                             'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_chevron_data_12_hours.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)


def mark_chevron_patterns():
    # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    # instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = "D:/alibaba_work/*_full_traces_updated/*_clean_new_2.csv"
    instList = glob.glob(instList)
    # instList = ['D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new_2.csv']
    # instList = ['D:/alibaba_work\cn-hangzhou_full_traces_updated\cn_hangzhou_f__ecs_gn5_c4g1_xlarge__optimized__clean_new_2.csv']
    total_length = len(instList)

    all_data = []
    all_data_12 = []
    all_data_24 = []
    all_data_48 = []
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_length)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df.reset_index(drop=True, inplace=True)

        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:', color='black')

        df['Chevron'] = 0
        df['Chevron12'] = 0
        df['Chevron24'] = 0
        df['Chevron48'] = 0

        length = df.Timestamp.count()
        for i in range(0, length - 5):
            # if (df['NormPrice'].iloc[i] == df['NormPrice'].iloc[i + 1] == df['NormPrice'].iloc[i + 4] == df['NormPrice'].iloc[i + 5]) and \
            #         (df['NormPrice'].iloc[i + 1] != df['NormPrice'].iloc[i + 2] == df['NormPrice'].iloc[i + 3]) and \
            #         (df['missing_data'].iloc[i+2] == 0) and (df['missing_data'].iloc[i+4] == 0) and \
            #         (not pd.isna(df['FixedPeakInfo'].iloc[i])) and (not pd.isna(df['FixedPeakInfo'].iloc[i + 2])) and \
            #         (not pd.isna(df['FixedPeakInfo'].iloc[i+4])):
            if (df['NormPrice'].iloc[i] == df['NormPrice'].iloc[i + 1] == df['NormPrice'].iloc[i + 4] ==
                df['NormPrice'].iloc[i + 5]) and \
                    (df['NormPrice'].iloc[i + 1] != df['NormPrice'].iloc[i + 2] == df['NormPrice'].iloc[i + 3]) and \
                    (df['missing_data'].iloc[i + 2] == 0) and (df['missing_data'].iloc[i + 4] == 0):
                if (not pd.isna(df['FixedPeakInfo'].iloc[i])) and (not pd.isna(df['FixedPeakInfo'].iloc[i + 2])) and \
                        (not pd.isna(df['FixedPeakInfo'].iloc[i + 4])):
                    df.loc[np.arange(i + 1, i + 5), 'Chevron'] = 1
                    all_data.append(
                        [inst, df['Timestamp'].iloc[i + 2], df['Timestamp'].iloc[i + 3],
                         df['TimeInSeconds'].iloc[i + 2], df['TimeInSeconds'].iloc[i + 3],
                         (df['TimeInSeconds'].iloc[i + 3] - df['TimeInSeconds'].iloc[i + 2]),
                         df['OriginPrice'].iloc[i], df['SpotPrice'].iloc[i],
                         df['NormPrice'].iloc[i], df['SpotPrice'].iloc[i + 2],
                         df['NormPrice'].iloc[i + 2],
                         (df['SpotPrice'].iloc[i + 2] - df['SpotPrice'].iloc[i + 1]),
                         (df['NormPrice'].iloc[i + 2] - df['NormPrice'].iloc[i + 1])])
                if (not pd.isna(df['PeakInfo12'].iloc[i])) and (not pd.isna(df['PeakInfo12'].iloc[i + 2])) and \
                        (not pd.isna(df['PeakInfo12'].iloc[i + 4])):
                    df.loc[np.arange(i + 1, i + 5), 'Chevron12'] = 1
                    all_data_12.append(
                        [inst, df['Timestamp'].iloc[i + 2], df['Timestamp'].iloc[i + 3],
                         df['TimeInSeconds'].iloc[i + 2], df['TimeInSeconds'].iloc[i + 3],
                         (df['TimeInSeconds'].iloc[i + 3] - df['TimeInSeconds'].iloc[i + 2]),
                         df['OriginPrice'].iloc[i], df['SpotPrice'].iloc[i],
                         df['NormPrice'].iloc[i], df['SpotPrice'].iloc[i + 2],
                         df['NormPrice'].iloc[i + 2],
                         (df['SpotPrice'].iloc[i + 2] - df['SpotPrice'].iloc[i + 1]),
                         (df['NormPrice'].iloc[i + 2] - df['NormPrice'].iloc[i + 1])])
                if (not pd.isna(df['PeakInfo24'].iloc[i])) and (not pd.isna(df['PeakInfo24'].iloc[i + 2])) and \
                        (not pd.isna(df['PeakInfo24'].iloc[i + 4])):
                    df.loc[np.arange(i + 1, i + 5), 'Chevron24'] = 1
                    all_data_24.append(
                        [inst, df['Timestamp'].iloc[i + 2], df['Timestamp'].iloc[i + 3],
                         df['TimeInSeconds'].iloc[i + 2], df['TimeInSeconds'].iloc[i + 3],
                         (df['TimeInSeconds'].iloc[i + 3] - df['TimeInSeconds'].iloc[i + 2]),
                         df['OriginPrice'].iloc[i], df['SpotPrice'].iloc[i],
                         df['NormPrice'].iloc[i], df['SpotPrice'].iloc[i + 2],
                         df['NormPrice'].iloc[i + 2],
                         (df['SpotPrice'].iloc[i + 2] - df['SpotPrice'].iloc[i + 1]),
                         (df['NormPrice'].iloc[i + 2] - df['NormPrice'].iloc[i + 1])])
                if (not pd.isna(df['PeakInfo48'].iloc[i])) and (not pd.isna(df['PeakInfo48'].iloc[i + 2])) and \
                        (not pd.isna(df['PeakInfo48'].iloc[i + 4])):
                    df.loc[np.arange(i + 1, i + 5), 'Chevron48'] = 1
                    all_data_48.append(
                        [inst, df['Timestamp'].iloc[i + 2], df['Timestamp'].iloc[i + 3],
                         df['TimeInSeconds'].iloc[i + 2], df['TimeInSeconds'].iloc[i + 3],
                         (df['TimeInSeconds'].iloc[i + 3] - df['TimeInSeconds'].iloc[i + 2]),
                         df['OriginPrice'].iloc[i], df['SpotPrice'].iloc[i],
                         df['NormPrice'].iloc[i], df['SpotPrice'].iloc[i + 2],
                         df['NormPrice'].iloc[i + 2],
                         (df['SpotPrice'].iloc[i + 2] - df['SpotPrice'].iloc[i + 1]),
                         (df['NormPrice'].iloc[i + 2] - df['NormPrice'].iloc[i + 1])])

        df.to_csv(inst, index=False)
        # plt.scatter(df.Timestamp[df.Chevron == 1], df.NormPrice[df.Chevron == 1], color='red')
        # plt.scatter(df.Timestamp[df.Chevron12 == 1], df.NormPrice[df.Chevron12 == 1], color='blue')
        # plt.scatter(df.Timestamp[df.Chevron24 == 1], df.NormPrice[df.Chevron24 == 1], color='green')
        # plt.scatter(df.Timestamp[df.Chevron48 == 1], df.NormPrice[df.Chevron48 == 1], color='yellow')
        #
        # plt.show()

    new_df = pd.DataFrame(all_data, columns=['inst', 'Timestamp1', 'Timestamp2', 'TimeInSeconds1', 'TimeInSeconds2',
                                             'TimeInterval', 'OriginPrice', 'BaseSpotPrice', 'BaseNormPrice',
                                             'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_chevron_data.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)

    new_df = pd.DataFrame(all_data_12, columns=['inst', 'Timestamp1', 'Timestamp2', 'TimeInSeconds1', 'TimeInSeconds2',
                                             'TimeInterval', 'OriginPrice', 'BaseSpotPrice', 'BaseNormPrice',
                                             'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_chevron_data_12_hours.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)

    new_df = pd.DataFrame(all_data_24, columns=['inst', 'Timestamp1', 'Timestamp2', 'TimeInSeconds1', 'TimeInSeconds2',
                                                'TimeInterval', 'OriginPrice', 'BaseSpotPrice', 'BaseNormPrice',
                                                'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_chevron_data_24_hours.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)

    new_df = pd.DataFrame(all_data_48, columns=['inst', 'Timestamp1', 'Timestamp2', 'TimeInSeconds1', 'TimeInSeconds2',
                                                'TimeInterval', 'OriginPrice', 'BaseSpotPrice', 'BaseNormPrice',
                                                'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_chevron_data_48_hours.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)


def analyze_chevrons():
    file = "./alibaba_work/feature_files/all_chevron_data.csv"
    df = pd.read_csv(file)

    df['date'] = pd.to_datetime(df['date'])
    df = df.sort_values('date')

    df['origin'] = df.base_price / df.normalized_base_price
    df['frac_origin'] = df.height / df.origin
    df['frac_base'] = df.normalized_height / df.normalized_base_price

    fig = plt.figure(figsize=set_size())
    ax = fig.add_subplot(2, 1, 1)
    ax.scatter(df['date'], df['frac_base'])
    ax.set_ylabel("Fraction of the normalized base price")

    ax = fig.add_subplot(2, 1, 2)
    ax.scatter(df['date'], df['frac_origin'])
    ax.set_ylabel("Fraction of the origin price")

    plt.show()


def count_static_traces():
    instList = "D:/alibaba_work/*_full_traces_updated/*vpc_2.csv"
    instList = glob.glob(instList)

    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    total_count = len(instList)

    count_vpc = 0
    inst_dict = dict()
    for num, inst in enumerate(instList, 1):
        # print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)

        if df.SpotPrice.nunique() <= 1:
            # inst, _ = inst.split("__vpc_2")
            # if inst in inst_dict.keys():
            #     inst_dict[inst] += 1
            # else:
            #     inst_dict[inst] = 1
            count_vpc += 1

    print(count_vpc, "/", total_count)

    instList = "F:/alibaba_work/*_full_traces_updated/*clean_new.csv"
    instList = glob.glob(instList)

    count_square = 0
    for num, inst in enumerate(instList, 1):
        # print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)

        if df.SpotPrice.nunique() <= 1:
            # inst, _ = inst.split("clean_new")
            # if inst in inst_dict.keys():
            #     inst_dict[inst] += 1
            # else:
            #     inst_dict[inst] = 1
            count_square += 1

    print(count_square, "/", total_count)

    # instList = "F:/alibaba_work/*_full_traces_updated/*vpc__clean_2.csv"
    # instList = glob.glob(instList)
    #
    # count_clean_2 = 0
    # for num, inst in enumerate(instList, 1):
    #     # print("Processing ", inst, num, "/", total_count)
    #     df = pd.read_csv(inst)
    #
    #     if df.SpotPrice.nunique() == 1:
    #         if inst in inst_dict.keys():
    #             inst_dict[inst] += 1
    #         else:
    #             inst_dict[inst] = 1
    #         count_clean_2 += 1
    #
    # print(count_clean_2, "/", total_count)

    # print([k for (k, v) in inst_dict.items() if v == 1])
    # print(inst_dict)
    # inst_dict = dict(sorted(inst_dict.items(), key=lambda item: item[1]))


def find_inst_with_norm_price_greater_than_1():
    instList = "../../alibaba_work/*_full_traces_updated/*vpc_2.csv"
    instList = glob.glob(instList)
    total_count = len(instList)

    max_val = 0
    max_inst = ''
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df = df[(df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM"))]
        if df.SpotPrice.nunique() > max_val:
            max_val = df.SpotPrice.nunique()
            max_inst = inst

        # if df.SpotPrice.nunique() > 20:
        #     print(inst)
        # if df.NormPrice.max() > 1:
        #     print(inst)
    print(max_inst, max_val)
    print("finished processing")


def calc_pattern_probability():
    # instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = "../../alibaba_work/*_full_traces_updated/*clean_new_2.csv"
    instList = glob.glob(instList)
    # instList = ["../../alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_i2g_4xlarge__optimized__clean_new_2.csv"]
    # instList = ['../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new_2.csv']
    # instList = ["../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1b__ecs_t5_lc1m2_large__optimized__clean_new_2.csv"]
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_e4_small__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    # info_dict = dict()

    # Matrix based on height
    # height_list = [round(num, 2) for num in np.arange(-1.0, 1.01, 0.01)]
    # mat_size = len(height_list)
    # mat = np.zeros((mat_size, mat_size))

    # Matrix based on steps
    step_list = [num for num in np.arange(-50, 35, 1)]
    # step_list_dec = [num for num in np.arange(-50, 1, 1)]
    # step_list_inc = [num for num in np.arange(0, 51, 1)]
    # mat_size = len(step_list_dec)
    mat_size = len(step_list)
    mat = np.zeros((mat_size, mat_size))
    # mat_up = np.zeros((mat_size, mat_size))
    # mat_down = np.zeros((mat_size, mat_size))

    # jump_data = []
    # tmp = []
    # print(step_list)
    return_pat = 0
    all_pat = 0
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')

        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:k')
        # plt.scatter(df.Timestamp[df.PeakInfo48.notna()], df.NormPrice[df.PeakInfo48.notna()], marker='o', color='red')

        df = df[df.clean == 1].copy()
        # df = df.sort_values('Timestamp')
        # plt.plot(df.Timestamp, df.NormPrice, '.--b')

        if df.SpotPrice.nunique() > 1:
            # df.NormPrice = np.around(df.NormPrice, 2)

            df = df[df.Timestamp < pd.to_datetime('1/1/2020 00:00:00 AM')].copy()
            # df = df[(df.Timestamp >= pd.to_datetime('3/1/2020 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('4/1/2020 00:00:00 AM'))].copy()
            df.reset_index(drop=True, inplace=True)

            if not df.empty:
                peaks_index_list = df.index[df.PeakInfo48.notna()].tolist()
                # print(peaks_index_list)
                if len(peaks_index_list) >= 3:
                    prev = peaks_index_list[0]
                    for i, curr in enumerate(peaks_index_list[1:-1], 1):
                        next = peaks_index_list[i + 1]
                        if (df['missing_data'].iloc[curr] == 0) and (df['missing_data'].iloc[next] == 0) \
                                and (df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next]):
                            # if df['missing_data'].iloc[curr] == 0:

                            if df['NormPrice'].iloc[prev] < df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[next]:
                                prev_steps = curr - prev
                                next_steps = curr - next
                                row = step_list.index(prev_steps)
                                col = step_list.index(next_steps)
                                mat[row][col] += 1
                                # row = step_list_inc.index(prev_steps)
                                # col = step_list_dec.index(next_steps)
                                # mat_up[row][col] += 1
                            elif df['NormPrice'].iloc[prev] > df['NormPrice'].iloc[curr] < df['NormPrice'].iloc[next]:
                                prev_steps = prev - curr
                                next_steps = next - curr
                                row = step_list.index(prev_steps)
                                col = step_list.index(next_steps)
                                mat[row][col] += 1
                                # row = step_list_dec.index(prev_steps)
                                # col = step_list_inc.index(next_steps)
                                # mat_down[row][col] += 1
                            elif df['NormPrice'].iloc[prev] < df['NormPrice'].iloc[curr] < df['NormPrice'].iloc[next]:
                                prev_steps = curr - prev
                                next_steps = next - curr
                                row = step_list.index(prev_steps)
                                col = step_list.index(next_steps)
                                # print(df['Timestamp'].iloc[prev], df['Timestamp'].iloc[curr], df['Timestamp'].iloc[next])
                                # print(prev_steps,next_steps)
                                # print(row,col)
                                mat[row][col] += 1
                            elif df['NormPrice'].iloc[prev] > df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[next]:
                                # print(inst)
                                prev_steps = prev - curr
                                next_steps = curr - next
                                row = step_list.index(prev_steps)
                                col = step_list.index(next_steps)
                                # print(df['Timestamp'].iloc[prev], df['Timestamp'].iloc[curr],df['Timestamp'].iloc[next])
                                # print(prev_steps,next_steps)
                                mat[row][col] += 1

                            # row = step_list.index(prev_steps)
                            # col = step_list.index(next_steps)
                            # mat[row][col] += 1

                            if df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next]:
                                return_pat += 1
                            all_pat += 1

                        prev = curr

    print(return_pat, "/", all_pat, "(", (return_pat / all_pat), ")")

    sum_mat = np.sum(mat)
    for i in range(len(mat)):
        for j in range(len(mat)):
            mat[i][j] = mat[i][j] / sum_mat

    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1.5))
    ax = fig.add_subplot(1, 1, 1)
    pcm = ax.imshow(mat, cmap='YlGnBu', norm=LogNorm())
    ax.xaxis.set_major_locator(MultipleLocator(10))
    ax.xaxis.set_major_formatter('{x:.0f}')
    ax.yaxis.set_major_locator(MultipleLocator(10))
    ax.yaxis.set_major_formatter('{x:.0f}')
    # ax.set_xticks(np.arange(mat_size))
    # ax.set_yticks(np.arange(mat_size))
    ax.set_xticklabels([num for num in np.arange(-60, 35, 10)])
    ax.set_yticklabels([num for num in np.arange(-60, 35, 10)])
    # ax.set_xticklabels(np.arange(-60,60,10))
    # ax.set_yticklabels(np.arange(-60,60,10))
    fig.colorbar(pcm, ax=ax, location='right', shrink=0.8)

    # sum_mat = np.sum(mat_up) + np.sum(mat_down)
    # for i in range(mat_size):
    #     for j in range(mat_size):
    #         mat_up[i][j] = mat_up[i][j] / sum_mat
    #         mat_down[i][j] = mat_down[i][j] / sum_mat
    #
    # print(np.sum(mat_up))
    # print(np.sum(mat_down))

    # # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    # # ax = fig.add_subplot(1, 2, 1)
    # fig, axs = plt.subplots(2, 1, constrained_layout=True, figsize=set_size(fraction=2))
    # pcm = axs[0].imshow(mat_up, cmap='YlGnBu', norm=LogNorm())
    # axs[0].xaxis.set_major_locator(MultipleLocator(10))
    # axs[0].xaxis.set_major_formatter('{x:.0f}')
    # axs[0].yaxis.set_major_locator(MultipleLocator(10))
    # axs[0].yaxis.set_major_formatter('{x:.0f}')
    # # axs[0].set_title('Increase jump')
    # # axs[0].set_xticks(np.arange(mat_size))
    # # axs[0].set_yticks(np.arange(mat_size))
    # axs[0].set_xticklabels([-60,-50,-40,-30,-20,-10,0])
    # axs[0].set_yticklabels([0,0,10,20,30,40,50])
    # # plt.setp(axs[0].get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    #
    # pcm = axs[1].imshow(mat_down, cmap='YlGnBu', norm=LogNorm())
    # axs[1].xaxis.set_major_locator(MultipleLocator(10))
    # axs[1].xaxis.set_major_formatter('{x:.0f}')
    # axs[1].yaxis.set_major_locator(MultipleLocator(10))
    # axs[1].yaxis.set_major_formatter('{x:.0f}')
    # # axs[1].set_title('Decrease jump')
    # # axs[1].set_xticks(np.arange(mat_size))
    # # axs[1].set_yticks(np.arange(mat_size))
    # axs[1].set_xticklabels([0,0,10,20,30,40,50])
    # axs[1].set_yticklabels([-60,-50,-40,-30,-20,-10,-0])
    # # plt.setp(axs[1].get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    # fig.colorbar(pcm, ax=axs, location='right', shrink=0.8)
    # # plt.title("Before Jun. 2020")

    # print(step_list_dec[0],step_list_dec[20],step_list_dec[40])
    # # Create ECDF and PDF for the matrices.
    # # This didn't show what we expected it to show...
    # vals = []
    # for i in range(len(mat)):
    #     for j in range(len(mat)):
    #         # print(step_list[j], step_list[i])
    #         if mat[i][j] > 0:
    #             vals.append((step_list[j] / step_list[i]) * mat[i][j])
    #
    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
    #
    # # print(vals)
    # vals.sort()
    # values, bins, _ = plt.hist(vals, 100, density=True, log=True, label="Histogram")
    # # bin_centers = 0.5 * (bins[1:] + bins[:-1])
    # # pdf = stats.norm.pdf(x=bin_centers, loc=0, scale=1)  # Compute probability density function
    # # plt.plot(bin_centers, pdf, label="PDF", color='black')
    #
    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
    # # print(vals)
    # ecdf = ECDF(vals)
    # y = ecdf(vals)
    # plt.title("Before Jun. 2020")
    # plt.step(vals, y, color='black', linestyle='--', where='post')

    plt.show()


def create_jump_transition_mat():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]
    mat_size = len(price_list)
    mat = np.zeros((mat_size, mat_size))
    # print(step_list)
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df.NormPrice = np.around(df.NormPrice, 2)

        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:k')
        # plt.scatter(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], marker='o',
        #             color='red')

        df = df[df.Timestamp >= pd.to_datetime('1/1/2020 00:00:00 AM')].copy()
        # df = df[(df.Timestamp >= pd.to_datetime('9/1/2019 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('1/1/2020 00:00:00 AM'))].copy()
        df.reset_index(drop=True, inplace=True)

        if not df.empty:
            peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
            # print(peaks_index_list)
            if len(peaks_index_list) >= 2:
                prev = peaks_index_list[0]
                for i, curr in enumerate(peaks_index_list[1:], 1):
                    row = price_list.index(df['NormPrice'].iloc[prev])
                    col = price_list.index(df['NormPrice'].iloc[curr])

                    mat[row][col] += 1

                    prev = curr

    sum_mat = np.sum(mat)
    for i in range(len(mat)):
        for j in range(len(mat)):
            mat[i][j] = mat[i][j] / sum_mat

    print(np.sum(mat))

    fig = plt.figure(tight_layout=True, figsize=set_size())
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(mat, cmap='binary', norm=LogNorm())
    ax.set_xticks(np.arange(mat_size))
    ax.set_yticks(np.arange(mat_size))
    # ... and label them with the respective list entries
    ax.set_xticklabels(price_list)
    ax.set_yticklabels(price_list)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    fig.colorbar(im, location='right', shrink=0.8)
    plt.title("After January 2020")

    plt.show()


def calc_steps_per_height():
    # instList = "D:/alibaba_work/*_full_traces_updated/*clean_new_2.csv"
    instList = "../../alibaba_work/*_full_traces_updated/*clean_new_2.csv"
    instList = glob.glob(instList)
    # instList = ['../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_e4_small__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    # inc_data = dict()
    # dec_data = dict()
    inc_data_before = []
    dec_data_before = []
    inc_data_after = []
    dec_data_after = []
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')

        df.NormPrice = np.around(df.NormPrice, 2)

        df_before = df[(df.Timestamp < pd.to_datetime('1/1/2020 00:00:00 AM'))]
        df_before.reset_index(drop=True, inplace=True)

        if not df_before.empty:
            peaks_index_list = df_before.index[df_before.PeakInfo48.notna()].tolist()
            if len(peaks_index_list) >= 3:
                prev = peaks_index_list[0]
                for i, curr in enumerate(peaks_index_list[1:], 1):
                    if df_before['missing_data'].iloc[curr] == 0:

                        if df_before['NormPrice'].iloc[curr] > df_before['NormPrice'].iloc[prev]:
                            steps = (curr - prev + 1) / 2
                            height = df_before['NormPrice'].iloc[curr] - df_before['NormPrice'].iloc[prev]

                            inc_data_before.append((height, steps))
                        elif df_before['NormPrice'].iloc[curr] < df_before['NormPrice'].iloc[prev]:
                            steps = (curr - prev + 1) / 2
                            height = df_before['NormPrice'].iloc[prev] - df_before['NormPrice'].iloc[curr]

                            dec_data_before.append((height, steps))

                    prev = curr

        df_after = df[(df.Timestamp >= pd.to_datetime('1/1/2020 00:00:00 AM'))]
        df_after.reset_index(drop=True, inplace=True)

        if not df_after.empty:
            peaks_index_list = df_after.index[df_after.PeakInfo48.notna()].tolist()
            if len(peaks_index_list) >= 3:
                prev = peaks_index_list[0]
                for i, curr in enumerate(peaks_index_list[1:], 1):
                    if df_after['missing_data'].iloc[curr] == 0:

                        if df_after['NormPrice'].iloc[curr] > df_after['NormPrice'].iloc[prev]:
                            steps = (curr - prev + 1) / 2
                            height = df_after['NormPrice'].iloc[curr] - df_after['NormPrice'].iloc[prev]

                            inc_data_after.append((height, steps))
                        elif df_after['NormPrice'].iloc[curr] < df_after['NormPrice'].iloc[prev]:
                            steps = (curr - prev + 1) / 2
                            height = df_after['NormPrice'].iloc[prev] - df_after['NormPrice'].iloc[curr]

                            dec_data_after.append((height, steps))

                    prev = curr

    # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
    fig, axs = plt.subplots(2, 2, constrained_layout=True, figsize=set_size(fraction=1.25))
    axs[0, 0].scatter(*zip(*dec_data_before), marker='.', color='black', alpha=0.01)
    axs[0, 0].set_xlabel("Jump height \n\n (a) Decrease jumps before 01-2020.")
    axs[0, 0].set_ylabel("Step No.")
    axs[0, 0].set_xlim(0,1)
    axs[0, 0].set_ylim(0,50)

    axs[0, 1].scatter(*zip(*dec_data_after), marker='.', color='black', alpha=0.01)
    axs[0, 1].set_xlabel("Jump height \n\n (b) Decrease jumps after 01-2020.")
    axs[0, 1].set_ylabel("Step No.")
    axs[0, 1].set_xlim(0, 1)
    axs[0, 1].set_ylim(0, 50)

    axs[1, 0].scatter(*zip(*inc_data_before), marker='.', color='black', alpha=0.01)
    axs[1, 0].set_xlabel("Jump height \n\n (c) Increase jumps before 01-2020.")
    axs[1, 0].set_ylabel("Step No.")
    axs[1, 0].set_xlim(0, 1)
    axs[1, 0].set_ylim(0, 35)

    axs[1, 1].scatter(*zip(*inc_data_after), marker='.', color='black', alpha=0.01)
    axs[1, 1].set_xlabel("Jump height \n\n (d) Increase jumps after 01-2020.")
    axs[1, 1].set_ylabel("Step No.")
    axs[1, 1].set_xlim(0, 1)
    axs[1, 1].set_ylim(0, 35)

    plt.show()


def draw_jump_height_length_step():
    instList = "../../alibaba_work/*_full_traces_updated/*clean_new_2.csv"
    instList = glob.glob(instList)
    # instList = ['../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new_2.csv']
    # instList = ['../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_i__ecs_gn6e_c12g1_6xlarge__optimized__clean_new_2.csv']
    # instList = ['../../alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square_2.csv']
    total_count = len(instList)

    inc_data = dict()
    dec_data = dict()
    inc_data_test = []
    dec_data_test = []
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')

        # df = df[df.clean == 1].copy()
        # df = df[df.missing_data == 0].copy()
        # df = df.sort_values('Timestamp')

        # df.NormPrice = np.around(df.NormPrice, 2)

        df = df[(df.Timestamp >= pd.to_datetime('1/1/2020 00:00:00 AM'))].copy()

        # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
        # plt.plot(df.Timestamp, df.NormPrice, '.:k')
        # plt.scatter(df.Timestamp[df.PeakInfo48.notna()], df.NormPrice[df.PeakInfo48.notna()], marker='o', color='red')

        df.reset_index(drop=True, inplace=True)

        if not df.empty:
            peaks_index_list = df.index[df.PeakInfo48.notna()].tolist()
            # print(peaks_index_list)
            if len(peaks_index_list) >= 3:
                prev = peaks_index_list[0]
                for i, curr in enumerate(peaks_index_list[1:], 1):
                    # if df['missing_data'].iloc[curr] == 0 and (df['TimeInSeconds'].iloc[curr] - df['TimeInSeconds'].iloc[prev] <= 86400):
                    if df['missing_data'].iloc[curr] == 0:

                        if df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[prev]:
                            steps = (curr - prev + 1) / 2
                            height = np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)
                            length = (df['TimeInSeconds'].iloc[curr] - df['TimeInSeconds'].iloc[prev]) / 3600
                            # print(prev, df['Timestamp'].iloc[prev], curr, df['Timestamp'].iloc[curr], steps, length,height)

                            # if length > 50:
                            #     print(inst)

                            # if (height, steps) in inc_data.keys():
                            #     inc_data[(height, steps)] += 1
                            # else:
                            #     inc_data[(height, steps)] = 1
                            inc_data_test.append((height, length, steps))
                        elif df['NormPrice'].iloc[curr] < df['NormPrice'].iloc[prev]:
                            steps = (curr - prev + 1) / 2
                            height = np.around(df['NormPrice'].iloc[prev] - df['NormPrice'].iloc[curr], 3)
                            length = (df['TimeInSeconds'].iloc[curr] - df['TimeInSeconds'].iloc[prev]) / 3600
                            # print(prev, df['Timestamp'].iloc[prev], curr, df['Timestamp'].iloc[curr], steps, length, height)

                            # if length > 50:
                            #     print(inst)

                            # if (height, steps) in dec_data.keys():
                            #     dec_data[(height, steps)] += 1
                            # else:
                            #     dec_data[(height, steps)] = 1
                            dec_data_test.append((height, length, steps))

                    prev = curr

    cm = plt.cm.get_cmap('RdYlBu')

    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    axs = fig.add_subplot()

    height, length, steps = zip(*inc_data_test)
    # axs.scatter(*zip(*inc_data_test), marker='.', alpha=0.01)
    sc = axs.scatter(height, steps, marker='+', c=length, cmap=cm, alpha=0.1)

    plt.title("Increase jumps")
    # axs.yaxis.set_major_locator(MultipleLocator(10))
    # axs.yaxis.set_major_formatter('{x:.0f}')
    axs.set_xlabel("Jump height")
    # axs.set_ylabel("Jump length")
    axs.set_ylabel("Step No.")
    axs.set_xlim(0, 1)
    # axs.set_ylim(0, 50)
    # axs.set_zlim(0, 50)
    plt.colorbar(sc)
    # plt.legend()

    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    axs = fig.add_subplot()

    height, length, steps = zip(*dec_data_test)
    sc = axs.scatter(height, steps, marker='+', c=length, cmap=cm, alpha=0.1)
    # axs.scatter(*zip(*dec_data_test), marker='.', alpha=0.01)
    plt.title("Decrease jumps")
    # axs.yaxis.set_major_locator(MultipleLocator(10))
    # axs.yaxis.set_major_formatter('{x:.0f}')
    axs.set_xlabel("Jump height")
    # axs.set_ylabel("Jump length")
    axs.set_ylabel("Step No.")
    axs.set_xlim(0, 1)
    # axs.set_ylim(0, 50)
    # axs.set_zlim(0, 50)
    # plt.legend()
    plt.colorbar(sc)

    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--function", help='The function we want to run')

    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    # regions = ['ap-northeast-1']

    args = parser.parse_args()

    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'

    if args.function:
        func = args.function
        if func == 'find_chevrons_in_all_regions':
            print("Running find_chevrons_in_all_regions function...")
            find_chevrons_in_all_regions()
        elif func == 'add_pattern_column':
            print("Running add_pattern_column function...")
            add_pattern_column()
        elif func == 'mark_chevron_patterns':
            print("Running mark_chevron_patterns function...")
            mark_chevron_patterns()
        elif func == 'analyze_chevrons':
            print("Running analyze_chevrons function...")
            analyze_chevrons()
        elif func == 'count_static_traces':
            print("Running count_static_traces...")
            count_static_traces()

        elif func == 'find_inst_with_norm_price_greater_than_1':
            print("Running find_inst_with_norm_price_greater_than_1...")
            find_inst_with_norm_price_greater_than_1()

        elif func == 'calc_pattern_probability':
            print("Running calc_pattern_probability...")
            calc_pattern_probability()

        elif func == 'create_jump_transition_mat':
            print("Running create_jump_transition_mat...")
            create_jump_transition_mat()

        elif func == 'calc_steps_per_height':
            print("Running calc_steps_per_height...")
            calc_steps_per_height()

        elif func == 'draw_jump_height_length_step':
            print("Running draw_jump_height_length_step...")
            draw_jump_height_length_step()

        elif func == 'find_pulses':
            print("Running find_pulses...")
            find_pulses()

    print("Finish")
