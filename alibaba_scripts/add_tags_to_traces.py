# import os.path
# from os import path
import glob
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import pandas as pd, numpy as np, seaborn as sns, scipy
# from contextlib import closing
import argparse
from statsmodels.distributions.empirical_distribution import ECDF
# from collections import Counter


##########################################################
#       Adding additional information to traces      #####
##########################################################

def add_important_cols():
    # instList = "./"+region+"_2020_traces/*clean.csv"
    # instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*_clean_2.csv"
    instList = glob.glob(instList)
    print("here")
    for inst in instList:
        print("processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df = df[df.columns.drop(
            ['month_year', 'TimeDelta', 'PriceDelta', 'NormPriceDelta', 'FixedNormPriceDelta', 'min', 'max', 'half_min',
             'hal_max', 'PeakInfo', 'BadFixedNormPriceDelta1', 'BadFixedNormPriceDelta2', 'BadFixedNormPriceDelta3',
             'BadFixedNormPriceDelta4'])]

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df['month_year'] = df['Timestamp'].dt.to_period('M')
        df['month_year'] = df['month_year'].dt.strftime('%Y-%m')

        df['TimeDelta'] = df['TimeInSeconds'].diff()
        df['PriceDelta'] = df['SpotPrice'].diff()

        df['NormPrice'] = (df.SpotPrice / df.OriginPrice)
        df['NormPriceDelta'] = (df.PriceDelta / df.OriginPrice)

        df.to_csv(inst, index=False)


##########################################################
#           Adding the peaks data to the traces      #####
##########################################################
def add_peaks_to_traces():
    # instList = "./"+region+"_traces/*__optimized__vpc_updated.csv"
    # instList = "./alibaba_work/" + region + "_full_traces/*_clean.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*_square_2.csv"
    instList = glob.glob(instList)
    # instList = [
    #     'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square.csv']
    total_count = len(instList)
    
    for num, inst in enumerate(instList, 1):
        print(inst, num, "/", total_count)
        df = pd.read_csv(inst)
        # df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        # df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        # df.reset_index(drop=True,inplace=True)
        # plt.plot(df.Timestamp, df.NormPrice, ".:", color='Black')

        df['min'] = df.NormPrice[(df.NormPrice.shift(1) > df.NormPrice) & (df.NormPrice.shift(-1) > df.NormPrice)]
        df['max'] = df.NormPrice[(df.NormPrice.shift(1) < df.NormPrice) & (df.NormPrice.shift(-1) < df.NormPrice)]

        df['half_min'] = df.NormPrice[((df.NormPrice.shift(1) == df.NormPrice) & (df.NormPrice.shift(-1) > df.NormPrice)) |
                                      ((df.NormPrice.shift(1) > df.NormPrice) & (df.NormPrice.shift(-1) == df.NormPrice))]
        df['half_max'] = df.NormPrice[((df.NormPrice.shift(1) == df.NormPrice) & (df.NormPrice.shift(-1) < df.NormPrice)) |
                                      ((df.NormPrice.shift(1) < df.NormPrice) & (df.NormPrice.shift(-1) == df.NormPrice))]

        length = df.Timestamp.count()
        info_list = []
        for index in range(length):
            if index == 0:
                info_list.append(df.NormPrice.iloc[index])
            elif index == 1 and df.NormPrice.iloc[index] == df.NormPrice.iloc[index-1]:
                info_list.append(df.NormPrice.iloc[index])
            elif index == length-1:
                info_list.append(df.NormPrice.iloc[index])
            elif index == length-2 and df.NormPrice.iloc[index] == df.NormPrice.iloc[index+1]:
                info_list.append(index)
            else:
                if pd.isna(df['min'].iloc[index]) and pd.isna(df['max'].iloc[index]) and pd.isna(df['half_min'].iloc[index]) and pd.isna(df['half_max'].iloc[index]):
                    #print("no value")
                    info_list.append(np.nan)
                elif pd.notna(df['min'].iloc[index]):
                    info_list.append(df['min'].iloc[index])
                    #print("min ", df['min'].iloc[index])
                elif pd.notna(df['max'].iloc[index]):
                    info_list.append(df['max'].iloc[index])
                    #print("max ", df['max'].iloc[index])
                elif pd.notna(df['half_min'].iloc[index]) and pd.notna(df['half_min'].iloc[index + 1]):
                    info_list.append(df['half_min'].iloc[index])
                elif pd.notna(df['half_min'].iloc[index]) and pd.notna(df['half_min'].iloc[index - 1]):
                    info_list.append(df['half_min'].iloc[index])
                elif pd.notna(df['half_max'].iloc[index]) and pd.notna(df['half_max'].iloc[index + 1]):
                    info_list.append(df['half_max'].iloc[index])
                elif pd.notna(df['half_max'].iloc[index]) and pd.notna(df['half_max'].iloc[index - 1]):
                    info_list.append(df['half_max'].iloc[index])
                    #print("half_max ", df['half_max'].iloc[index])
                else:
                    info_list.append(np.nan)

        df['FixedPeakInfo'] = info_list

        # plt.scatter(df.Timestamp[pd.notna(df.FixedPeakInfo)], df.NormPrice[pd.notna(df.FixedPeakInfo)], color='red')

        # plt.show()

        # name,_ = inst.split("__optimized__vpc_updated.csv")
        # name = name + "_with_tags.csv"
        # df.to_csv(name)
        # df.reset_index(inplace=True)
        df.to_csv(inst, index=False)
        # print("updated file: ",inst)
        # print("-------------------------------------------")


def add_whole_jumps_to_traces(region):
    # instList = "./"+region+"_traces/*__optimized__vpc_updated.csv"
    # instList = "./alibaba_work/" + region + "_full_traces/*_clean.csv"
    instList = "./alibaba_work/*_full_traces/*_vpc.csv"
    instList = glob.glob(instList)

    for inst in instList:
        print(inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df.drop_duplicates(subset="Timestamp", keep='first', inplace=True)

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)

        length = df.Timestamp.count()
        # print(length)
        info_list = []
        index = 0
        while index < length:
            if pd.isna(df.PeakInfo.iloc[index]):
                info_list.append(np.nan)
                index += 1
            if (index < length-2) and pd.notna(df.half_max.iloc[index]) and pd.notna(df.half_min.iloc[index + 1]):
                info_list.extend([np.nan, np.nan])
                index += 2
            elif (index < length-2) and pd.notna(df.half_min.iloc[index]) and pd.notna(df.half_max.iloc[index + 1]):
                info_list.extend([np.nan, np.nan])
                index += 2
            elif pd.notna(df.PeakInfo.iloc[index]):
                info_list.append(df.NormPrice.iloc[index])
                index += 1

        # print(index)
        #
        # while len(info_list) < length:
        #     info_list.append(df.NormPrice.iloc[index])
        #     index += 1

        # print(len(df.NormPrice.tolist()))
        # print(len(info_list))
        # print("==================")
        df['NormJumps'] = info_list
        df.to_csv(inst, index=False)
        print("updated file: ", inst)


# #############################################################################
#                 Adding the description column the traces               #####
#             The description column tags is the sample represents       #####
#                 an increase/decrease/const in the SpotPrice            #####
# #############################################################################
def add_description_col(region):
    # instList = "./"+region+"_2020_traces/*_vpc.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*__vpc__clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square.csv']

    total_count = len(instList)
    
    for num, inst in enumerate(instList, 1):
        print("processing ",inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        # df.reset_index(drop=True,inplace=True)

        length = df.Timestamp.count()
        info_list = [0]
        for index in range(1, length):
            if df.SpotPrice.iloc[index] > df.SpotPrice.iloc[index - 1]:
                info_list.append(1)
            elif df.SpotPrice.iloc[index] < df.SpotPrice.iloc[index - 1]:
                info_list.append(-1)
            else:
                info_list.append(0)

        df['Description'] = info_list

        # print(df.head(50))
        df.to_csv(inst, index=False)


# #############################################################################
#                 Add 'DescriptionNoKnee' column.                    #####
#      This column marks decrease/increase/const without 'knees'     #####
# #############################################################################
def add_description_without_knee_col(region):
    # instList = "./"+region+"_traces/*_vpc.csv"
    instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)

        length = df.Timestamp.count()
        if length > 3:
            info_list = []
            for index in range(length):
                if not pd.isna(df['half_min'].iloc[index-1]) and not pd.isna(df['half_max'].iloc[index]):
                    info_list.append('decrease')
                elif not pd.isna(df['half_min'].iloc[index-2]) and (df.Description.iloc[index-1] == 'const') and \
                     not pd.isna(df['half_max'].iloc[index]):
                    info_list.append('decrease')
                elif not pd.isna(df['half_min'].iloc[index-1]) and (df.Description.iloc[index] == 'const') and \
                     (index+1 < length) and not pd.isna(df['half_max'].iloc[index+1]):
                    info_list.append('decrease')
                elif not pd.isna(df['half_max'].iloc[index-1]) and not pd.isna(df['half_min'].iloc[index]):
                    info_list.append('increase')
                elif not pd.isna(df['half_max'].iloc[index-2]) and (df.Description.iloc[index-1] == 'const') and \
                     not pd.isna(df['half_min'].iloc[index]):
                    info_list.append('increase')
                elif not pd.isna(df['half_max'].iloc[index-1]) and (df.Description.iloc[index] == 'const') and \
                     (index+1 < length) and not pd.isna(df['half_min'].iloc[index+1]):
                    info_list.append('increase')
                else:
                    info_list.append(df.Description.iloc[index])
                    
            df['DescriptionNoKnee'] = info_list

            df.to_csv(inst)


# #########################################################
#   Add normalized peak data to the traces                #
# #########################################################
def add_normalized_peaks_to_traces(region):
    # instList = "./"+region+"_2020_traces/*vpc.csv"
    instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        df = pd.read_csv(inst)

        df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)
        
        df['NormPrice'] = df.SpotPrice / df.OriginPrice
        
        df['NormMin'] = df.NormPrice[(df.NormPrice.shift(1) > df.NormPrice) & (df.NormPrice.shift(-1) > df.NormPrice)]
        df['NormMax'] = df.NormPrice[(df.NormPrice.shift(1) < df.NormPrice) & (df.NormPrice.shift(-1) < df.NormPrice)]
        
        df['half_NormMin'] = df.NormPrice[((df.NormPrice.shift(1) == df.NormPrice) & (df.NormPrice.shift(-1) > df.NormPrice)) |
                                      ((df.NormPrice.shift(1) > df.NormPrice) & (df.NormPrice.shift(-1) == df.NormPrice))]
        df['half_NormMax'] = df.NormPrice[((df.NormPrice.shift(1) == df.NormPrice) & (df.NormPrice.shift(-1) < df.NormPrice)) |
                                      ((df.NormPrice.shift(1) < df.NormPrice) & (df.NormPrice.shift(-1) == df.NormPrice))]
        
        length = df.Timestamp.count()
        info_list = []
        for index in range(length):
            if index == 0:
                info_list.append(df.NormPrice.iloc[index])
                #print("first ", df.NormPrice.iloc[index])
            elif index == length-1:
                info_list.append(df.NormPrice.iloc[index])
                #print("last ", df.NormPrice.iloc[index])
            else:
                if pd.isna(df['NormMin'].iloc[index]) and pd.isna(df['NormMax'].iloc[index]) and pd.isna(df['half_NormMin'].iloc[index]) and pd.isna(df['half_NormMax'].iloc[index]):
                    #print("no value")
                    info_list.append(np.nan)
                if not pd.isna(df['NormMin'].iloc[index]):
                    info_list.append(df['NormMin'].iloc[index])
                    #print("NormMin ", df['NormMin'].iloc[index])
                elif not pd.isna(df['NormMax'].iloc[index]):
                    info_list.append(df['NormMax'].iloc[index])
                    #print("NormMax ", df['NormMax'].iloc[index])
                elif not pd.isna(df['half_NormMin'].iloc[index]):
                    info_list.append(df['half_NormMin'].iloc[index])
                    #print("half_NormMin ", df['half_NormMin'].iloc[index])
                elif not pd.isna(df['half_NormMax'].iloc[index]):
                    info_list.append(df['half_NormMax'].iloc[index])
                    #print("half_NormMax ", df['half_NormMax'].iloc[index])
        
        df['NormPeakInfo'] = info_list

        #name,_ = inst.split("__vpc.csv")
        #name = name + "_normalized_tags.csv"
        df.to_csv(inst)
        print("created file: ",inst)
        print("-------------------------------------------")            


##############################################################################
#####  draw the most common decrease slope value per trace per week      #####
##############################################################################
def draw_find_epoch(region):
    # instList = "./"+region+"_traces/*_with_tags.csv"
    instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df[df['Description'] == 'decrease']
        
        if df.empty != True:
            slope_list = []
            date_list = []
            grouped = df.groupby(pd.Grouper(key='Timestamp',freq='W'))
            for name,group in grouped:
                if not group.Slope.value_counts().dropna().empty:
                    date_list.append(str(name))
                    slope_list.append(group.Slope.value_counts().idxmax())
                    #print(name,",", group.slope.value_counts().idxmax())
                    #print(group.slope.value_counts().idxmax(), group.slope.value_counts().max(), group.slope.count())
                    #print("--------------------------------------------------")
            #print(slope_list)
            #print(date_list)

            df2 = pd.DataFrame(list(zip(date_list, slope_list)), columns =['Timestamp', 'Slope']) 
            df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
            
            plt.scatter(df2.Timestamp,df2.Slope)
    
    plt.ylim(-3,0.5)
    plt.show()

##############################################################################
#####  Find the week where the most common decrease slope value changes. #####
#####  We where checking doe the change that happened during the months  #####
#####  August-September 2019.                                            #####   
##############################################################################
def find_epoch(region):
    # instList = "./"+region+"_traces/*_with_tags.csv"
    instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"
    instList = glob.glob(instList)
    
    dates = ['2019-07-07 00:00:00','2019-07-14 00:00:00','2019-07-21 00:00:00','2019-07-28 00:00:00','2019-08-04 00:00:00','2019-08-11 00:00:00','2019-08-18 00:00:00','2019-08-25 00:00:00',
             '2019-09-01 00:00:00','2019-09-08 00:00:00','2019-09-15 00:00:00','2019-09-22 00:00:00','2019-09-29 00:00:00','2019-10-06 00:00:00','2019-10-13 00:00:00','2019-10-20 00:00:00','2019-10-27 00:00:00']
    
    slope_list = []
    date_list = []
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df[df['Description'] == 'increase']
        
        if df.empty != True:
            grouped = df.groupby(pd.Grouper(key='Timestamp',freq='W'))
            for name,group in grouped:
                if str(name) in dates:
                    if not group.Slope.value_counts().dropna().empty:
                        date_list.append(str(name))
                        slope_list.append(group.Slope.value_counts().idxmax())
                    

    df2 = pd.DataFrame(list(zip(date_list, slope_list)), columns =['Timestamp', 'Slope'])
    
    fig = plt.figure(figsize=(15, 12))
    
    df3 = df2.groupby(['Timestamp']).var()
    #print(df3)
    plt.scatter(df3.index,df3.Slope, label = 'Variance')
    
    df3 = df2.groupby(['Timestamp']).mean()
    #print(df3)
    plt.scatter(df3.index,df3.Slope, label = 'Mean')
    
    plt.xticks(rotation=45)
    plt.legend()
    plt.title(region)
    name = "./graphs/epoch_search/"+region+"_increase_epoch_search.png"
    plt.savefig(name)
    #plt.show()


##################################################################################
#####  Calculate the ratio of 'decreasing' samples VS 'increasing' samples   #####
##################################################################################            
def decrease_vs_increase_count(regions):
    ratio_list = []
    no_inc_num = 0
    no_dec_num = 0
    const_num = 0
    num_traces = 0

    fig = plt.figure(figsize=(15, 12))
    
    for region in regions:
        print("proccessing ",region)
        instList = "./"+region+"_traces/*_with_tags.csv"
        instList = glob.glob(instList)
        
        inc_list = []
        dec_list = []
        
        for inst in instList:
            num_traces = num_traces + 1
            print("proccessing ",inst)
            df = pd.read_csv(inst)
            
            length = df.Timestamp.count()
            if length > 3:
                inc_count = df.DescriptionNoKnee[df.DescriptionNoKnee == 'increase'].count()
                dec_count = df.DescriptionNoKnee[df.DescriptionNoKnee == 'decrease'].count()
                
                
                #if (inc_count != 0) and (dec_count != 0):
                #    ratio = inc_count/dec_count
                #    ratio_list.append(ratio)
                #elif (inc_count == 0) and (dec_count == 0):
                #    const_num = const_num + 1
                #elif (inc_count == 0) and (dec_count != 0):
                #    no_inc_num = no_inc_num + 1
                #else:
                #    no_dec_num = no_dec_num + 1
                #ratio_list.append(1e77)
                #print("inc:", inc_count, "dec: ",dec_count)
                #print("nan")
                if (inc_count != 0) or (dec_count != 0):
                    inc_list.append(inc_count)
                    dec_list.append(dec_count)
        
        plt.scatter(dec_list, inc_list)
    
    #print("Number of all traces:", num_traces)
    #print("Number of constant traces:", const_num)
    #print("Number of traces that have no increase points:", no_inc_num)
    #print("Number of traces that have no decrease points:", no_dec_num)
    #
    #ratio_list.sort()
    #ecdf = ECDF(ratio_list)
    #y = ecdf(ratio_list)
    #
    #plt.step(ratio_list,y,label=region)
    
    plt.xlabel("Number of 'decreasing' points")
    plt.ylabel("Number of 'increasing' points")
    plt.title("Ratio increase/decrease points")
    title = "./graphs/epoch_search/ratio_increase_decrease.png"
    plt.savefig(title)
    #plt.show()
    
    
def draw_slope_ecdf_by_epoch(region):
    
    # instList = "./"+region+"_traces/*_with_tags.csv"
    instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"
    instList = glob.glob(instList)
    
    slope_list = []
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        
        length = df.Timestamp.count()
        if length > 3:
            df = df[df.TimeInSeconds < 52531200]  # 52531200 is the TimeInSeconds for "9/1/2019 12:00:00 AM"
            df = df[df.DescriptionNoKnee == 'decrease']
            
            if df.empty != True:
                slope_list.extend(df.Slope.to_numpy())
                #print(slope_list)
    
    
    slope_list.sort()
    ecdf = ECDF(slope_list)
    y = ecdf(slope_list)

    plt.step(slope_list, y)
    
    title = "First epoch (before 01-09-2019): Decreasing slop ECDF ("+region+")"
    plt.title(title)
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    #parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
    parser.add_argument("--slope", help='The slope we want to examine')
  
    args = parser.parse_args()
    
    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'
        
    if args.function:
        func = args.function
        if func == 'add_peaks_to_traces':
            print("Adding peaks to all traces....")
            add_peaks_to_traces()
            print("Finished adding preaks to all traces")

        elif func == 'add_whole_jumps_to_traces':
            print("Running add_whole_jumps_to_traces function...")
            add_whole_jumps_to_traces(region)
            print("Finished")
            
        elif func == 'add_normalized_peaks_to_traces':
            print("Adding normalized peaks to all traces....")
            add_normalized_peaks_to_traces(region)
            print("Finished adding normalized preaks to all traces")
        
        elif func == 'add_description_col':
            print("Adding description column to all traces....")
            add_description_col(region)
            print("Adding descriptionNoKnee column to all traces....")
            add_description_without_knee_col(region)
            print("Finished adding description column to all traces")
        
        elif func == 'find_epoch':
            print("Running find_epoch function...")
            find_epoch(region)
            
        elif func == 'draw_slope_ecdf_by_epoch':
            print("Running draw_slope_ecdf_by_epoch function...")
            draw_slope_ecdf_by_epoch(region)

        elif func == 'decrease_vs_increase_count':
            print("Running decrease_vs_increase_count function...")
            regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
                    'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
                    'eu-west-1','me-east-1','us-east-1','us-west-1']
            decrease_vs_increase_count(regions)

        elif func == 'add_important_cols':
            print("Running add_important_cols function...")
            add_important_cols()
    
    print("Finish")

