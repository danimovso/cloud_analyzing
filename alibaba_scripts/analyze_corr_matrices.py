import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd, numpy as np
# import networkx as nx
import itertools

# plt.style.use('seaborn-colorblind')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 14,
        "font.size": 14,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 12,
        "xtick.labelsize": 12,
        "ytick.labelsize": 12
    }

plt.rcParams.update(tex_fonts)


def set_size(width=395.8225, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def calc_squares(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    min_red = max(min_red, 0.01)
    for i in range(1, size - 1):
        if (abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] * mat[i - 1][i] > 0 and mat[i][i + 1] > min_red):
            temp.append(i)  # continue the square
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)  # why does this repeat in both cases?
                temp = [i]  # start a new square

    if temp:  # i think this case can happen regardless
        temp.append(size - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([size - 1])

    # print(squares)
    return squares


def get_insts_in_squares(df, squares, file_name):
    # f = open(file_name, "w")
    # f.write(str(df.columns.tolist()) + "\n")
    # f.write("==================================================\n")
    # f.write("==================================================\n")
    all_squares_info = []
    for index, s in enumerate(squares):
        # f.write("Square number: " + str(index) + " ,Len square: " + str(len(s)) + " ,Row numbers: [" + str(
        #     s[0]) + "," + str(s[-1]) + "]\n")
        inst_list = []
        for i in s:
            name = df.columns.tolist()[i]
            # reg, _, _ = name.split("__")
            # reg = reg.replace('_', '-')
            # reg = reg[:-1]
            # if reg[-1] == '-':
            #     reg = reg[:-1]
            # inst = "F:/alibaba_work/" + reg + "_full_traces_updated/" + name + "__vpc__clean__square.csv"
            # inst_list.append(inst)
            inst_list.append(name)
        all_squares_info.append(inst_list)
        # f.write(str(inst_list) + "\n")
        # rows = [list(df.index.values)[i] for i in s]
        # print(rows)
        # f.write("==================================================\n")
        # f.write(rows)

    # f.close()
    return all_squares_info


def get_info_per_square(df, squares):
    for index, s in enumerate(squares):
        name_set = set()
        region_set = set()
        family_set = set()
        for i in s:
            name = df.columns.tolist()[i]
            name_set.add(name)

            reg, _, _ = name.split("__")
            reg = reg.replace('_', '-')
            region_set.add(reg)

            _, family = name.split("ecs")
            family, _ = family.split("__")
            family = family[1:]
            family_set.add(family)

        print(name_set)
        print(region_set)
        print(family_set)
        print("============================")


# l = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg5_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_hfr6_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_ic5_8xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_ic5_6xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_c5_6xlarge__optimized__vpc__clean__square.csv']
#
# print(list(itertools.combinations(l, 2)))

# order_files = ["../../Alibaba_log_files/Correlation_tables/best_all_instances_august_2020.txt"]
# crop_files = ["../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_corr_weighted_red_square.csv"]
# square_files = ["../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_squares_list.txt"]

order_files = ["../../Alibaba_log_files/Correlation_tables/cropped_august_2020_red_squares.txt",
               "../../Alibaba_log_files/Correlation_tables/cropped_july_2020_red_squares.txt",
               "../../Alibaba_log_files/Correlation_tables/cropped_september_2020_red_squares.txt",
               "../../Alibaba_log_files/Correlation_tables/cropped_october_2020_red_squares.txt"]
#
crop_files = ["../../Alibaba_log_files/Correlation_tables/cropped_august_2020_corr_weighted.csv",
              "../../Alibaba_log_files/Correlation_tables/cropped_july_2020_corr_weighted.csv",
              "../../Alibaba_log_files/Correlation_tables/cropped_september_2020_corr_weighted.csv",
              "../../Alibaba_log_files/Correlation_tables/cropped_october_2020_corr_weighted.csv"]
#
square_files = ["../../Alibaba_log_files/Correlation_tables/cropped_august_2020_squares_list.txt",
                "../../Alibaba_log_files/Correlation_tables/cropped_july_2020_squares_list.txt",
                "../../Alibaba_log_files/Correlation_tables/cropped_september_2020_squares_list.txt",
                "../../Alibaba_log_files/Correlation_tables/cropped_october_2020_squares_list.txt"]

month_list = ["July 2020", "August 2020", "September 2020", "October 2020"]
colors = ['blue', 'red', 'green', 'black']

my_file = open("../../Alibaba_log_files/Correlation_tables/cropped_august_2020_red_squares.txt", "r")
order_list = my_file.read()
order_list = order_list.split(",")
my_file.close()

df = pd.read_csv("../../Alibaba_log_files/Correlation_tables/cropped_august_2020_corr_weighted.csv")
df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
df.set_index("inst", inplace=True)

df = df.reindex(order_list, axis=1)
df = df.reindex(order_list, axis=0)

squares = calc_squares(df.values, len(df.values), 0.95)
squares_names = get_insts_in_squares(df, squares, "temp_name")
p_dict = dict()
for s in squares_names:
    tmp_list = list(itertools.combinations(s, 2))
    for row, col in tmp_list:
        if (row, col) in p_dict.keys():
            p_dict[(row, col)] += 0
        elif (col, row) in p_dict.keys():
            p_dict[(col, row)] += 0
        else:
            p_dict[(row, col)] = 0

tot_mat = np.zeros((992, 992))
tot_mat_2 = np.zeros((992, 992))
df_tot = pd.DataFrame(data=tot_mat_2, index=order_list, columns=order_list)

fig = plt.figure(tight_layout=True,figsize=set_size(fraction=1))
all_edges = dict()
for num, mat_f, order_f, square_f in zip([0,1,2,3], crop_files, order_files, square_files):
    my_file = open(order_f, "r")
    order_list = my_file.read()
    order_list = order_list.split(",")
    my_file.close()

    df = pd.read_csv(mat_f)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    df.set_index("inst", inplace=True)

    df = df.reindex(order_list, axis=1)
    df = df.reindex(order_list, axis=0)

    squares = calc_squares(df.values, len(df.values), 0.95)
    squares_names = get_insts_in_squares(df, squares, square_f)
    for s in squares_names:
        tmp_list = list(itertools.combinations(s, 2))
        for row, col in tmp_list:
            if (row, col) in p_dict.keys():
                p_dict[(row, col)] += 1
            elif (col, row) in p_dict.keys():
                p_dict[(col, row)] += 1

for row, col in p_dict.keys():
    df_tot.at[row,col] = p_dict[(row, col)]
    df_tot.at[col, row] = p_dict[(row, col)]

# c = mpl.colors.ListedColormap(['blue', 'navy', 'lightblue', 'grey'])
c = mpl.colors.ListedColormap(['whitesmoke', 'silver', 'dimgray', 'black'])
n = mpl.colors.Normalize(vmin=1,vmax=4)

ax = fig.add_subplot(1, 1, 1)
df_tot = df_tot.replace(0, np.nan)
print(df_tot.index)
# im = ax.imshow(df_tot.values, cmap='seismic')
im = ax.imshow(df_tot.values, cmap=c)

plt.colorbar(im, ticks=range(5))

plt.show()
