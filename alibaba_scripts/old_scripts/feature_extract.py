import os.path
from os import path
import glob
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
# register_matplotlib_converters()
import pandas as pd, numpy as np, seaborn as sns, scipy
from contextlib import closing
import argparse
from statsmodels.distributions.empirical_distribution import ECDF

import time
import sklearn

from sklearn import decomposition
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE

from sklearn.cluster import DBSCAN
from sklearn import metrics

from scipy.cluster.hierarchy import dendrogram
from sklearn.cluster import AgglomerativeClustering

import scipy
import scipy.cluster.hierarchy as sch

import itertools


def normPriceDelta_percentile_extract(region):
    instList = "./"+region+"_2020_traces/*clean.csv"
    instList = glob.glob(instList)
    
    file = "./feature_files/"+region+"_decrease_normPriceDelta_percent.csv"
    print(file)
    f1= open(file,"w")
    f1.write("inst,5,15,25,35,45,55,65,75,85,95\n")
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df = df[df.NormPriceDelta < 0.0]

        if df.empty != True:
            df = df.sort_values('NormPriceDelta')
            data = df['NormPriceDelta'].dropna().to_numpy()
            ecdf = ECDF(data)
            y = ecdf(data)

            out = np.percentile(data, [5,15,25,35,45,55,65,75,85,95])
            out_str = ','.join(map(str, out))

            str1 = inst+","+out_str+"\n"
            f1.write(str1)
    f1.close()
    
def normPriceDelta_percentile_extract_all():
    regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
                    'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
                    'eu-west-1','me-east-1','us-east-1','us-west-1']
    #regions = ['eu-west-1','me-east-1','us-east-1']
                    
    file = "./feature_files/all_regions_increase_normPriceDelta_percent_features.csv"
    print(file)
    f1= open(file,"w")
    f1.write("inst,5,15,25,35,45,55,65,75,85,95\n")
    for region in regions:
        instList = "./"+region+"_2020_traces/*clean.csv"
        instList = glob.glob(instList)
    
        for inst in instList:
            print("proccessing ",inst)
            df = pd.read_csv(inst)
            df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')

            df = df[df.NormPriceDelta > 0.0]

            if df.empty != True:
                df = df.sort_values('NormPriceDelta')
                data = df['NormPriceDelta'].dropna().to_numpy()
                ecdf = ECDF(data)
                y = ecdf(data)

                out = np.percentile(data, [5,15,25,35,45,55,65,75,85,95])
                out_str = ','.join(map(str, out))

                str1 = inst+","+out_str+"\n"
                f1.write(str1)
    f1.close()
    
def merge_feature_files():
    f1 = "./feature_files/all_regions_decrease_normPriceDelta_percent_features.csv"
    f2 = "./feature_files/all_regions_increase_normPriceDelta_percent_features.csv"
    
    df1 = pd.read_csv(f1)
    df2 = pd.read_csv(f2)
    
    result = pd.merge(df1, df2, how='outer', on='inst', suffixes=('_dec', '_inc'))
    
    result[["region", "inst_type", "crap1","crap2","crap3"]] = result["inst"].str.split("__", expand = True)
    result.drop(columns=["crap1","crap2","crap3", "region"],inplace=True)
    
    d = {name: i for (i,name) in enumerate(result['inst_type'])}
    result['color'] = result['inst_type'].map(d)
    
    result.fillna(value=0,inplace=True)
    
    result.to_csv("./feature_files/all_regions_normPriceDelta_percent_features.csv")
    
    
def arrange_trace_for_clustering(file):
    print("Processing "+file+" ...") 
    df = pd.read_csv(file)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('Timestamp')

    #df['TimeModulo3600'] = df['TimeInSeconds'] % 3600
    
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    df = df[df.columns.drop(['OriginPrice','SpotPrice','TimeInSeconds','Timestamp','NormPrice','PriceDelta','TimeDelta'])]
    
    df['1_prev'] = df.NormPriceDelta.shift(periods=1, fill_value=0)
    df['2_prev'] = df.NormPriceDelta.shift(periods=2, fill_value=0)
    df['3_prev'] = df.NormPriceDelta.shift(periods=3, fill_value=0)
    df['4_prev'] = df.NormPriceDelta.shift(periods=4, fill_value=0)
    df['5_prev'] = df.NormPriceDelta.shift(periods=5, fill_value=0)
    df['6_prev'] = df.NormPriceDelta.shift(periods=6, fill_value=0)
    df['7_prev'] = df.NormPriceDelta.shift(periods=7, fill_value=0)
    df['8_prev'] = df.NormPriceDelta.shift(periods=8, fill_value=0)
    df['9_prev'] = df.NormPriceDelta.shift(periods=9, fill_value=0)
    df['10_prev'] = df.NormPriceDelta.shift(periods=10, fill_value=0)
    #df['prev_TimeDelta'] = df.TimeDelta.shift(periods=1, fill_value=0)
    
    name,_ = file.split('__optimized')
    name = name+"_feature_extract.csv"
    
    df.to_csv(name,index=False)


def run_pca(file):
    print("Processing "+file+" ...") 
    df = pd.read_csv(file)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    df = df.fillna(0)
    #df.drop(columns=['inst'],inplace=True)
    
    fig = plt.figure(figsize=(12, 12))
    ax = fig.add_subplot(1, 2, 1)
    
    X = df.values

    #plt.cla()
    pca = decomposition.PCA(n_components=2)
    pca.fit(X)
    X = pca.transform(X) 
   
    ## Checking the results of the PCA:
    ax.scatter(X[:, 0], X[:, 1])
    ax.set_title("With data from prev row")
    
    ax = fig.add_subplot(1, 2, 2)
    
    df.drop(columns=['prev_NormPriceDelta','prev_TimeDelta'],inplace=True)
    
    X = df.values

    #plt.cla()
    pca = decomposition.PCA(n_components=2)
    pca.fit(X)
    X = pca.transform(X) 
   
    ## Checking the results of the PCA:
    ax.scatter(X[:, 0], X[:, 1])
    ax.set_title("Without data from prev row")
    
    plt.show()


def run_TSNE(file):
    print("Processing "+file+" ...") 
    df = pd.read_csv(file)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    df = df.fillna(0)
    df.drop(columns=['inst','type','size','color_by_full_inst_type'],inplace=True)
    
    #fig = plt.figure(figsize=(12, 12))
    #ax = fig.add_subplot(1, 2, 1)
    
    X = df.values
    
    time_start = time.time()
    tsne = TSNE(n_components=2, verbose=1, perplexity=30, n_iter=300)
    tsne_results = tsne.fit_transform(X)
    print('t-SNE done! Time elapsed: {} seconds'.format(time.time()-time_start))

    estimators = [('k_means_2', KMeans(n_clusters=2)),
                  ('k_means_3', KMeans(n_clusters=3)),
                  ('k_means_4', KMeans(n_clusters=4)),
                  ('k_means_5', KMeans(n_clusters=5)),
                  ('k_means_6', KMeans(n_clusters=6)),
                  ('k_means_7', KMeans(n_clusters=7)),
                  ('k_means_8', KMeans(n_clusters=8)),
                  ('k_means_9', KMeans(n_clusters=9))]
                  
    
    
    fig = plt.figure(figsize=(15, 15))
    fignum = 1
    titles = ['2 clusters', '3 clusters', '4 clusters', '5 clusters','6 clusters', '7 clusters', '8 clusters', '9 clusters']
    for name, est in estimators:
        ax = fig.add_subplot(2, 4, fignum)
        est.fit(X)
        labels = est.labels_
         
        ax.scatter(tsne_results[:,0], tsne_results[:,1], c=labels.astype(np.float), edgecolor='k')
    
        ax.set_title(titles[fignum - 1])
        ax.dist = 12
        fignum = fignum + 1
    
    plt.savefig("./feature_files/test_tsne.png")
    
    ## Checking the results of the t-SNE:
    #plt.scatter(tsne_results[:,0], tsne_results[:,1])
    ##ax.set_title("With data from prev row")
    #
    #plt.savefig("./feature_files/test_tsne.png")
    
    
def run_TSNE_for_inst(inst):
    print("Processing "+inst+" ...") 
    df = pd.read_csv(inst)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    
    X = df.values
    
    time_start = time.time()
    tsne = TSNE(n_components=2, verbose=1, perplexity=30, n_iter=300)
    tsne_results = tsne.fit_transform(X)
    print('t-SNE done! Time elapsed: {} seconds'.format(time.time()-time_start))

    estimators = [('k_means_2', KMeans(n_clusters=2)),
                  ('k_means_3', KMeans(n_clusters=3)),
                  ('k_means_4', KMeans(n_clusters=4)),
                  ('k_means_5', KMeans(n_clusters=5)),
                  ('k_means_6', KMeans(n_clusters=6)),
                  ('k_means_7', KMeans(n_clusters=7)),
                  ('k_means_8', KMeans(n_clusters=8)),
                  ('k_means_9', KMeans(n_clusters=9))]
                  
    
    
    fig = plt.figure(figsize=(15, 15))
    fignum = 1
    titles = ['2 clusters', '3 clusters', '4 clusters', '5 clusters','6 clusters', '7 clusters', '8 clusters', '9 clusters']
    for name, est in estimators:
        ax = fig.add_subplot(2, 4, fignum)
        est.fit(X)
        labels = est.labels_
         
        ax.scatter(tsne_results[:,0], tsne_results[:,1], c=labels.astype(np.float), edgecolor='k')
    
        ax.set_title(titles[fignum - 1])
        ax.dist = 12
        fignum = fignum + 1
    
    #plt.savefig("./feature_files/tsne_for_cn_beijing_inst.png")
    plt.show()
    
    ## Checking the results of the t-SNE:
    #plt.scatter(tsne_results[:,0], tsne_results[:,1])
    ##ax.set_title("With data from prev row")
    #
    #plt.savefig("./feature_files/test_tsne.png")    



def run_Kmeans(file):
    print("Processing "+file+" ...") 
    df = pd.read_csv(file)
    
    df2 = df.drop(columns=['inst','type','size','color_by_full_inst_type'])
    X = df2.values
    
    plt.cla()
    pca = decomposition.PCA(n_components=2)
    X_less_d = pca.fit_transform(X)
    
    ## Color by the type of inst instead of by Kmeans
    #cm = plt.cm.get_cmap('RdYlBu')
    #sc = plt.scatter(X_less_d[:, 0], X_less_d[:, 1], c=df.color , cmap=cm, edgecolor='k')
    #
    #plt.colorbar(sc)
    #plt.show()
      
    ## Using Kmeans on the PCA
    estimators = [('k_means_2', KMeans(n_clusters=2)),
                  ('k_means_3', KMeans(n_clusters=3)),
                  ('k_means_4', KMeans(n_clusters=4)),
                  ('k_means_5', KMeans(n_clusters=5)),
                  ('k_means_6', KMeans(n_clusters=6)),
                  ('k_means_7', KMeans(n_clusters=7)),
                  ('k_means_8', KMeans(n_clusters=8)),
                  ('k_means_9', KMeans(n_clusters=9))]
                  
    
    
    fig = plt.figure(figsize=(15, 15))
    fignum = 1
    titles = ['2 clusters', '3 clusters', '4 clusters', '5 clusters','6 clusters', '7 clusters', '8 clusters', '9 clusters']
    for name, est in estimators:
        ax = fig.add_subplot(2, 4, fignum)
        est.fit(X)
        labels = est.labels_
         
        ax.scatter(X_less_d[:, 0], X_less_d[:, 1], c=labels.astype(np.float), edgecolor='k')
    
        ax.set_title(titles[fignum - 1])
        ax.dist = 12
        fignum = fignum + 1
    
    plt.show()
    #plt.savefig("./feature_files/test_kmeans_PCA.png")

def run_Kmeans_for_inst(inst):
    print("Processing "+inst+" ...") 
    df = pd.read_csv(inst)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    
    X = df.values
    
    pca = decomposition.PCA(n_components=2)
    X_less_d = pca.fit_transform(X)
    
    print(pca.components_)
    
    # number of components
    n_pcs= pca.components_.shape[0]

    # get the index of the most important feature on EACH component i.e. largest absolute value
    # using LIST COMPREHENSION HERE
    most_important = [np.abs(pca.components_[i]).argmax() for i in range(n_pcs)]

    initial_feature_names = ['NormPriceDelta','1_prev','2_prev','3_prev','4_prev','5_prev','6_prev','7_prev','8_prev','9_prev','10_prev']

    # get the names
    most_important_names = [initial_feature_names[most_important[i]] for i in range(n_pcs)]

    # using LIST COMPREHENSION HERE AGAIN
    dic = {'PC{}'.format(i+1): most_important_names[i] for i in range(n_pcs)}

    # build the dataframe
    #print(pd.DataFrame(sorted(dic.items())))
    
    estimators = [('k_means_2', KMeans(n_clusters=2)),
                  ('k_means_3', KMeans(n_clusters=3)),
                  ('k_means_4', KMeans(n_clusters=4)),
                  ('k_means_5', KMeans(n_clusters=5)),
                  ('k_means_6', KMeans(n_clusters=6)),
                  ('k_means_7', KMeans(n_clusters=7)),
                  ('k_means_8', KMeans(n_clusters=8)),
                  ('k_means_9', KMeans(n_clusters=9))]
    
    fig = plt.figure(figsize=(12, 12))
    fignum = 1
    titles = ['2 clusters', '3 clusters', '4 clusters', '5 clusters','6 clusters', '7 clusters', '8 clusters', '9 clusters']
    for name, est in estimators:
        ax = fig.add_subplot(2, 4, fignum)
        est.fit(X)
        labels = est.labels_
         
        ax.scatter(X_less_d[:, 0], X_less_d[:, 1], c=labels.astype(np.float), edgecolor='k')
        #ax.scatter(df['2_prev'], df['5_prev'], c=labels.astype(np.float), edgecolor='k')
    
        ax.set_title(titles[fignum - 1])
        ax.dist = 12
        fignum = fignum + 1

    plt.show()



def run_DBSCAN(file):
    print("Processing "+file+" ...") 
    df = pd.read_csv(file)
    df.drop(columns=['inst','type','size','color_by_full_inst_type'],inplace=True)
    X = df.values

    time_start = time.time()
    tsne = TSNE(n_components=2, verbose=1, perplexity=30, n_iter=300)
    X_less_d = tsne.fit_transform(X)
    
    #pca = decomposition.PCA(n_components=2)
    #pca.fit(X)
    #X_less_d = pca.transform(X) 
    
    fig = plt.figure(figsize=(12, 12))
    
    # #############################################################################
    # Compute DBSCAN
    db = DBSCAN(eps=0.3, min_samples=10).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    print("Silhouette Coefficient: %0.3f"
          % metrics.silhouette_score(X, labels))

    # #############################################################################
    # Plot result
    ax = fig.add_subplot(1, 1, 1)
    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = X_less_d[class_member_mask & core_samples_mask]
        ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=14)

        xy = X_less_d[class_member_mask & ~core_samples_mask]
        ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=6)

    ax.set_title('Estimated number of clusters: %d' % n_clusters_)
    ax.dist = 12
    
    plt.show()


def cluster_corr_mat(matrix):
    
    #data = np.loadtxt(open(matrix,"rb"), delimiter=',')
    print("Processing "+matrix+" ...") 
    df = pd.read_csv(matrix,low_memory=False)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    print(df)
    print("==========================================================================")
    df2 = df.drop(columns=['inst'])
    data = df2.values
    data = data.astype(np.float64)
    print(data)
    print("==========================================================================")
    np.nan_to_num(data)
    print(data)
    F = 10
    data = F - (np.where(data > 0.0, data*F, -data))
    #z = np.zeros_like(data)
    #data = np.maximum(data,np.zeros_like(data),z)
    #data = 1 - data
    # print(np.where(np.isnan(data)))

    #data is a matrix of m samples with n features each
    corr_mat = sklearn.metrics.pairwise.pairwise_distances(data, metric="correlation")
    
    clustering = DBSCAN(eps=0.1, min_samples=10, metric='precomputed')
    clustering.fit(corr_mat)
    
    core_samples_mask = np.zeros_like(clustering.labels_, dtype=bool)
    core_samples_mask[clustering.core_sample_indices_] = True
    labels = clustering.labels_
    
    df['labels'] = labels
    
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    
    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    print("Silhouette Coefficient: %0.3f"% metrics.silhouette_score(corr_mat, labels))
    
    df.to_csv('./alibaba_work/feature_files/corr_matrix_05_to_13_january_DBScan_with_labels.csv')
    
    ## #############################################################################
    ## Plot result
    #ax = fig.add_subplot(1, 1, 1)
    ## Black removed and is used for noise instead.
    #unique_labels = set(labels)
    #colors = [plt.cm.Spectral(each)
    #          for each in np.linspace(0, 1, len(unique_labels))]
    
    #for k, col in zip(unique_labels, colors):
    #    if k == -1:
    #        # Black used for noise.
    #        col = [0, 0, 0, 1]
    #
    #    class_member_mask = (labels == k)
    #
    #    xy = X_less_d[class_member_mask & core_samples_mask]
    #    ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
    #             markeredgecolor='k', markersize=14)
    #
    #    xy = X_less_d[class_member_mask & ~core_samples_mask]
    #    ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
    #             markeredgecolor='k', markersize=6)
    #
    #ax.set_title('Estimated number of clusters: %d' % n_clusters_)
    #ax.dist = 12
    #
    #plt.show()


def cluster_red_squares(matrix):
    print("Processing "+matrix+" ...") 
    df = pd.read_csv(matrix)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    #df = df[df.labels > 1]
    #df = df.drop(columns=['inst','labels'])
    df = df.drop(columns=['inst'])
    data = df.values
    F = 10
    data = F - (np.where(data > 0.0, data*F, -data))
    
    time_start = time.time()
    tsne = TSNE(n_components=2, verbose=1, perplexity=30, n_iter=300)
    X_less_tsne = tsne.fit_transform(data)
    
    pca = decomposition.PCA(n_components=2)
    pca.fit(data)
    X_less_pca = pca.transform(data) 
    

    #data is a matrix of m samples with n features each
    corr_mat = sklearn.metrics.pairwise.pairwise_distances(data, metric="correlation")
    
    clustering = DBSCAN(eps=0.1, min_samples=30, metric='precomputed')
    clustering.fit(corr_mat)
    
    core_samples_mask = np.zeros_like(clustering.labels_, dtype=bool)
    core_samples_mask[clustering.core_sample_indices_] = True
    labels = clustering.labels_
    
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    
    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)
    print("Silhouette Coefficient: %0.3f"% metrics.silhouette_score(corr_mat, labels))
    
    ## #############################################################################
    ## Plot result
    fig = plt.figure(figsize=(12, 12))
    ax = fig.add_subplot(1, 2, 1)
    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]
    
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]
    
        class_member_mask = (labels == k)
    
        xy = X_less_tsne[class_member_mask & core_samples_mask]
        ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=14)
    
        xy = X_less_tsne[class_member_mask & ~core_samples_mask]
        ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=6)
    
    ax.set_title('TSNE reduction - estimated number of clusters: %d' % n_clusters_)
    ax.dist = 12

    ax = fig.add_subplot(1, 2, 2)
    # Black removed and is used for noise instead.

    
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]
    
        class_member_mask = (labels == k)
    
        xy = X_less_pca[class_member_mask & core_samples_mask]
        ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=14)
    
        xy = X_less_pca[class_member_mask & ~core_samples_mask]
        ax.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                 markeredgecolor='k', markersize=6)
    
    ax.set_title('PCA reduction - estimated number of clusters: %d' % n_clusters_)
    ax.dist = 12

    
    plt.show()



def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_, counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)


def create_dendrogram(matrix):

    print("Processing "+matrix+" ...") 
    df = pd.read_csv(matrix)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    print(df)
    #df = df[df.labels > 1]
    df2 = df.drop(columns=['inst'])
    #df = df.drop(columns=['inst'])
    data = df2.values
    F = 10
    data = F - (np.where(data > 0.0, data*F, -data))
    #z = np.zeros_like(data)
    #data = np.maximum(data,np.zeros_like(data),z)
    #data = 1 - data
    
    corr_mat = sklearn.metrics.pairwise.pairwise_distances(data, metric="correlation")
    model = AgglomerativeClustering(distance_threshold=0.2, n_clusters=None, affinity="precomputed", linkage='complete', compute_full_tree=True)

    model = model.fit(corr_mat)
    
    df['labels'] = model.labels_
    df.to_csv('./alibaba_work/feature_files/cropped_July_2020_corr_with_dendrogram_labels.csv')
    # plt.figure(figsize=(25, 10))
    # plt.title('Hierarchical Clustering Dendrogram')
    # # plot the top three levels of the dendrogram
    # plot_dendrogram(model)#, truncate_mode='level', p=6)
    # plt.xlabel("Number of points in node (or index of point if no parenthesis).")
    # plt.savefig('./alibaba_work/feature_files/all_instances_weighted_august_2020_corr_3_dendrogram_threshold_02_F10.png')
    # #plt.show()
    
def create_dendrogram_2(matrix):

    print("Processing "+matrix+" ...") 
    df = pd.read_csv(matrix)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    #df = df[df.labels > 1]
    df = df.drop(columns=['inst'])
    #df = df.drop(columns=['inst'])
    data = df.values
    z = np.zeros_like(data)
    data = np.maximum(data,np.zeros_like(data),z)
    data = 1 - data
    
    #based on code from the lone nut
    d = sch.distance.pdist(data)   # vector of ('55' choose 2) pairwise distances
    L = sch.linkage(d, method='complete',optimal_ordering=True)#the list of mergers
    print(L)

    #code from JoernHiss
    fig=plt.figure(figsize=(25, 10))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('sample index')
    plt.ylabel('distance')
    sch.dendrogram(
        L,
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=8.,  # font size for the x axis labels
    )
    #fig.savefig('dendrogram.png')
    plt.show()


def show(mat,title):

    fig, ax = plt.subplots()
    im = ax.imshow(mat, cmap='jet')
    ax.set_title(title)
    fig.tight_layout()
    plt.show() 

def sort_squares(matrix, threshold = 0.4):
    df = pd.read_csv(matrix)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    df.set_index('inst',inplace=True)
    mat = df.values
    mat = np.around(mat,1)
    
    fig = plt.figure(figsize=(25, 10))
    ax = fig.add_subplot(1, 2, 1)
    ax.imshow(mat, cmap='jet')
    ax.set_title("before")
    
    #print(mat)
    #print("========================================")

    #mat = np.around(mat,1)
    #print(mat)
    size = len(mat)
    #print(size)

    squares = []
    temp = [0]

    for i in range(1, size):
        #print(i,":",mat[i][i])
        if i < size-1:
            #print(temp)
            #print(i,",",(i+1))
            if (abs(mat[i][i+1] - mat[i-1][i]) < threshold and mat[i][i+1] > 0.5):
                #print("appending: ",i)
                #print("(",i,",",(i+1),") ",mat[i][i+1],"-",mat[i-1][i],"=",abs(mat[i][i+1] - mat[i-1][i]))
                temp.append(i)
            else:
                if (mat[i-1][i] > 0.5):
                    temp.append(i)
                    squares.append(temp)
                    temp = []
                else:
                    if temp:
                        squares.append(temp)
                    temp = [i]
        

    if temp:
        #print("here")
        temp.append(size-1)
        squares.append(temp)
    else:
        squares.append([size-1])
    #print(squares)

    max_daig = np.trace(mat, offset=1)
    print("before: ",max_daig)
    max_order = list(itertools.chain.from_iterable(squares))
    print(max_order)
    
    per_list = list(itertools.permutations(squares))
    print(2)
    #print(per_list)
    for per in per_list:
        print(per)
        col_order = list(itertools.chain.from_iterable(per))
        mat = mat[col_order][:,col_order]
        sum_daig = np.trace(mat, offset=1)
        #print(np.trace(mat, offset=0))
        #print(sum_daig)
        if max_daig < sum_daig:
            max_daig = sum_daig
            max_order = col_order
        #print(per)
        #print("========================")
        #print(list(itertools.chain.from_iterable(per)))
        #print("========================")
        #print("========================")
    
    print("after: ",max_daig)
    mat = mat[max_order][:,max_order]
    ax = fig.add_subplot(1, 2, 2)
    ax.imshow(mat, cmap='jet')
    ax.set_title("after")
    
    plt.tight_layout()
    plt.show()
    print("finished")
    


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name')
    parser.add_argument("--function", help='The function we want to run')
    parser.add_argument("--file", help='The file we want to use')
    parser.add_argument("--inst", help='The inst file we want to use') 
    parser.add_argument("--matrix", help='The correlation matrix file we want to use')   
  
    args = parser.parse_args()
    
    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'
        
    if args.file:
        file = args.file
    else:
        file = './feature_files/all_regions_normPriceDelta_percent_features.csv'
        
    if args.inst:
        inst = args.inst
    else:
        inst = './feature_files/cn_beijing_c__ecs_c5_2xlarge_feature_extract.csv'
    
    if args.matrix:
        matrix = args.matrix
    else:
        matrix = './feature_files/all_instances_corr_no_static_matrix.csv'

    
    if args.function:
        func = args.function
        if func == 'normPriceDelta_percentile_extract':
            print("Running normPriceDelta_percentile_extract function...")
            normPriceDelta_percentile_extract(region)
        
        elif func == 'normPriceDelta_percentile_extract_all':
            print("Running normPriceDelta_percentile_extract_all function...")
            normPriceDelta_percentile_extract_all()
        
        elif func == 'run_pca':
            print("Running run_pca function...")
            run_pca(file)
        
        elif func == 'run_TSNE':
            print("Running run_TSNE function...")
            run_TSNE(file)
            
        elif func == 'run_Kmeans':
            print("Running run_Kmeans function...")
            run_Kmeans(file)
            
        elif func == 'run_DBSCAN':
            print("Running run_DBSCAN function...")
            run_DBSCAN(file)
        
        elif func == 'arrange_trace_for_clustering':
            print("Running arrange_trace_for_clustering function...")
            arrange_trace_for_clustering(file)
            
        elif func == 'run_Kmeans_for_inst':
            print("Running run_Kmeans_for_inst function...")
            run_Kmeans_for_inst(inst)
            
        elif func == 'merge_feature_files':
            print("Running merge_feature_files function...")
            merge_feature_files()
            
        elif func == 'run_TSNE_for_inst':
            print("Running run_TSNE_for_inst function...")
            run_TSNE_for_inst(inst)
            
        elif func == 'cluster_corr_mat':
            print("Running cluster_corr_mat function...")
            cluster_corr_mat(matrix)
            
        elif func == 'cluster_red_squares':
            print("Running cluster_red_squares function...")
            cluster_red_squares(matrix)
            
        elif func == 'create_dendrogram':
            print("Running create_dendrogram function...")
            create_dendrogram(matrix)
        
        elif func == 'create_dendrogram_2':
            print("Running create_dendrogram_2 function...")
            create_dendrogram_2(matrix)
            
        elif func == 'sort_squares':
            print("Running sort_squares function...")
            sort_squares(matrix, 0.4)