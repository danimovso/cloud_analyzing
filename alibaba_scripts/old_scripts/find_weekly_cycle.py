import glob
import matplotlib.pyplot as plt
import argparse
import matplotlib
# matplotlib.use('Agg')
import pandas as pd, numpy as np
from scipy.stats import f_oneway
from scipy.stats import kruskal
from scipy import stats

import random

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from statistics import mean, variance

from statsmodels.distributions.empirical_distribution import ECDF


# width=229.87749
def set_size(width=441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


fig = plt.figure(figsize=set_size(fraction=2))
# fig = plt.figure()
# plt.style.use('grayscale')

tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 12,
    "font.size": 12,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 10,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10
}

plt.rcParams.update(tex_fonts)


# ########################################################################### #
#   This function calculates the total number of missing days in the traces   #
# ########################################################################### #
def find_missing_dates():
    instList = "./alibaba_work/*_full_traces_updated/*vpc.csv"
    instList = glob.glob(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc.csv',
    #             './alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc.csv']

    date_list = []
    for inst in instList:
        print(inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        date_list.extend(df.Timestamp)
        # print("before: ", len(date_list))
        date_list = list(set(date_list))
        # print("after: ", len(date_list))

    df_dates = pd.DataFrame(date_list, columns=['Timestamp'])
    df_dates['Timestamp'] = pd.to_datetime(df_dates['Timestamp'])
    df_dates = df_dates.sort_values('Timestamp')
    df_dates = df_dates.set_index('Timestamp')

    missing_dates = pd.date_range(start=pd.to_datetime("11/13/2018 00:00:00 AM"), end=pd.to_datetime("7/14/2021 00:00:00 AM"), freq='D').difference(df_dates.index).tolist()
    print("Total missing days: ", len(missing_dates))


# ########################################################################### #
#   This function calculates the number of traces we have prior to 01.2020    #
#   and post 01.2020                                                          #
# ########################################################################### #
def count_number_of_traces():
    instList = "./alibaba_work/*_full_traces_updated/*clean.csv"
    instList = glob.glob(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
                # './alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc.csv']

    both_list = []
    before = 0
    after = 0
    both = 0
    total = 0
    for inst in instList:
        print(inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df_before = df[df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")]
        df_after = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]

        if not df_before.empty:
            before += 1
        if not df_after.empty:
            after += 1
        if not df_before.empty and not df_after.empty:
            both_list.append(inst)
            both += 1

        total += 1

    print("total: ", total, " before: ", before, " after: ", after, " both: ", both)

    both_arr = np.array(both_list)
    # print(both_arr)
    file = "./alibaba_work/feature_files/list_of_insts_appearing_in_full_period.npy"
    with open(file, 'wb') as f:
        np.save(f, both_arr)


def add_info_for_weekly_cycle_and_index():
    instList = "F:/alibaba_work/*_full_traces_updated/*__clean_new.csv"
    instList = glob.glob(instList)

    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__clean_new.csv']
    total_count = len(instList)
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df.sort_values('MyIndex')
        df = df[df.clean == 1].copy()

        df['fixed_missing_data'] = df.missing_data
        df.loc[df.missing_data.shift(-1) != 0, 'missing_data'] = 3

        df = df[['OriginPrice', 'SpotPrice', 'NormPrice', 'Timestamp', 'missing_data', 'fixed_missing_data']]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)
        df = df.set_index('Timestamp')

        # print(df[['OriginPrice', 'SpotPrice', 'NormPrice', 'missing_data']])
        # print(df.index[0], df.index[-1])

        missing_dates = pd.date_range(start=df.index[0], end=df.index[-1], freq='1H').difference(df.index).tolist()
        # missing_dates = pd.date_range(start=pd.to_datetime("11/13/2018 00:00:00 AM"), end=pd.to_datetime("7/14/2021 00:00:00 AM"), freq='D').difference(df.index).tolist()

        df2 = pd.DataFrame(index=missing_dates, columns=['OriginPrice', 'SpotPrice', 'NormPrice', 'missing_data'])
        df = df.append(df2)
        df.index.name = 'Timestamp'
        df.reset_index(inplace=True)
        # print(df.head(5))
        # df = df.sort_index()
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df.ffill()

        start = pd.to_datetime("2018-01-01 00:00:00.000")
        df['TmpDiff'] = df['Timestamp'] - start
        df['seconds'] = df['TmpDiff'] / np.timedelta64(1, 's')
        # df['secondsDiff'] = df['seconds'].diff()

        df['TimeModulo3600'] = df['seconds'] % 3600
        df = df[df.TimeModulo3600 == 0]

        df = df[['Timestamp', 'OriginPrice', 'SpotPrice', 'NormPrice', 'missing_data', 'fixed_missing_data']]

        # print(df)

        name, _ = inst.split('__clean_new.csv')
        name = name + "__resample_hour.csv"
        df.to_csv(name, index=False)
        # print("Created ", name, "(", i, "/", total_count, ")")


def calc_daily_norm_price():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions,1):
        print(region)
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        instList = glob.glob(instList)

        # instList = ['../../Alibaba_log_files/interesting_traces/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']

        total_count = len(instList)

        for index, inst in enumerate(instList, 1):
            print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')

            # df = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]
            df['day'] = df['Timestamp'].dt.to_period('D')

            info_list = []
            grouped = df.groupby('day')
            for name, group in grouped:
                # print(name)
                # print(group)
                end_day = name.to_timestamp(how="end")
                length = group.Timestamp.count()
                norm_price = 0
                spot_price = 0
                if length == 1:
                    norm_price = group.NormPrice.iloc[0] * 24
                    spot_price = group.SpotPrice.iloc[0] * 24
                else:
                    for i in range(0, length):
                        np = group.NormPrice.iloc[i]
                        sp = group.SpotPrice.iloc[i]
                        if i+1 != length:
                            t = pd.Timedelta(group.Timestamp.iloc[i+1] - group.Timestamp.iloc[i]).seconds / 3600.0
                        else:
                            # print(end_day, group.Timestamp.iloc[i])
                            t = pd.Timedelta(end_day - group.Timestamp.iloc[i]).seconds / 3600.0
                        norm_price += np*t
                        spot_price += sp*t
                # print(price/24)
                info_list.append((name, norm_price/24, spot_price/24))
                #     if index == 0:
                #         info_list.append(df.NormPrice.iloc[index])

            res = pd.DataFrame(info_list, columns=['Date', 'EffectiveNormPrice', 'EffectiveSpotPrice'])
            # print(res)
            name, _ = inst.split('__resample.csv')
            name = name + "__daily_price.csv"
            # print(name)
            res.to_csv(name, index=False)


def weekly_cycle():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['cn-beijing']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions,1):
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        instList = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
        instList = glob.glob(instList)

        # instList = ['../../Alibaba_log_files/interesting_traces/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']

        total_count = len(instList)

        # m_list = ['.', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p']

        days_1 = {}
        days_2 = {}
        all_count_1 = 0
        all_count_2 = 0
        static_count_1 = 0
        static_count_2 = 0
        for index, inst in enumerate(instList, 1):
            # print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df['week_day'] = df.Timestamp.dt.dayofweek

            df_1 = df[df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]

            # if not df_1.empty and not df_2.empty:

            if df_1.SpotPrice.count() > 1:
                all_count_1 += 1
                grouped_1 = df_1.groupby('week_day')
                for name, group in grouped_1:
                    if name in days_1:
                        days_1[name].append(group.NormPrice.mean())
                    else:
                        days_1[name] = [group.NormPrice.mean()]
            else:
                static_count_1 += 1

            if df_2.SpotPrice.count() > 1:
                all_count_2 += 1
                grouped_2 = df_2.groupby('week_day')
                for name, group in grouped_2:
                    if name in days_2:
                        days_2[name].append(group.NormPrice.mean())
                    else:
                        days_2[name] = [group.NormPrice.mean()]
            else:
                static_count_2 += 1

        # print(region, "epoch 1: ", static_count_1, "/", all_count_1)
        # print(region, "epoch 2: ", static_count_2, "/", all_count_2)

        ax = fig.add_subplot(5, 4, num)
        ax.set_title(region)

        means_list = []
        # vars_list = []
        # print("region, day, mean, var")
        days_1 = dict(sorted(days_1.items()))
        for day in days_1.keys():
            # print(day, len(days_1[day]))
            means_list.append(mean(days_1[day]))
            # vars_list.append(variance(days_1[day]))
            # print(region, day, mean(days_1[day]))

        norm_mean = mean(means_list)
        norm_mean_list = [(x - norm_mean) for x in means_list]
        # print(zip(days_1.keys(),norm_mean_list))

        # ax.plot(days_1.keys(), norm_mean_list, '*:', label=str(region))
        # ax.legend(loc='upper center', ncol=1, fancybox=True, shadow=True)
        ax.plot(days_1.keys(), norm_mean_list, '.-', label="Epoch 1")

        means_list = []
        # # vars_list = []
        # # print("region, day, mean, var")
        days_2 = dict(sorted(days_2.items()))
        for day in days_2.keys():
            # print(day, len(days_2[day]))
            means_list.append(mean(days_2[day]))
            # vars_list.append(variance(days_2[day]))
            # print(region, day, mean(days_2[day]))

        norm_mean = mean(means_list)
        norm_mean_list = [(x - norm_mean) for x in means_list]
        # print(zip(days_2.keys(), norm_mean_list))

        # ax = fig.add_subplot(5, 4, num)
        # ax.set_title(region)
        # ax.boxplot(days_2.values(), showfliers=True)
        # ax.set_xticklabels(days_2.keys(), rotation=90, fontsize=8)
        # ax.plot(days_2.keys(), norm_mean_list, '*:', label=str(region))
        # ax.legend(loc='upper center', ncol=1, fancybox=True, shadow=True)
        ax.plot(days_2.keys(), norm_mean_list, '*:', label="Epoch 2")

        ax.set_ylim(-0.025, 0.025)
        ax.legend(loc='upper center', ncol=2, fancybox=True, shadow=True)

        stat, p = f_oneway(days_1[0], days_1[1], days_1[2], days_1[3], days_1[4], days_1[5], days_1[6])
        print('Epoch 1 - ANOVA: stat=%.3f, p=%.3f' % (stat, p))
        stat, p = f_oneway(days_2[0], days_2[1], days_2[2], days_2[3], days_2[4], days_2[5], days_2[6])
        print('Epoch 2 - ANOVA: stat=%.3f, p=%.3f' % (stat, p))
        # stat, p = kruskal(days_2[0], days_2[1], days_2[2], days_2[3], days_2[4], days_2[5], days_2[6])
        # print('kruskal: stat=%.3f, p=%.3f' % (stat, p))
        # if p > 0.05:
        #     print('Probably the same distribution')
        # else:
        #     print('Probably different distributions')

    # plt.xlabel("Day number")
    # plt.ylabel("Normalized mean daily price")
    # plt.legend(loc='upper center', ncol=5, fancybox=True, shadow=True)

    plt.tight_layout()
    # plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_per_region_sampled_prices.png"
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_hist.png"
    plt.savefig(file, bbox_inches="tight")


def weekly_cycle_2():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions,1):
        print(region)
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*__daily_price.csv"
        instList = glob.glob(instList)

        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__daily_price.csv']

        total_count = len(instList)

        days_2 = {}
        for index, inst in enumerate(instList, 1):
            # print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Date'] = pd.to_datetime(df['Date'])
            df = df.sort_values('Date')
            df['week_day'] = df.Date.dt.dayofweek

            # df_1 = df[df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Date > pd.to_datetime("1/1/2020 00:00:00 AM")]

            # if not df_1.empty and not df_2.empty:

            # if df_1.SpotPrice.count() > 1:
            #     all_count_1 += 1
            #     grouped_1 = df_1.groupby('week_day')
            #     for name, group in grouped_1:
            #         if name in days_1:
            #             days_1[name].append(group.NormPrice.mean())
            #         else:
            #             days_1[name] = [group.NormPrice.mean()]
            # else:
            #     static_count_1 += 1
            # 'EffectiveNormPrice', 'EffectiveSpotPrice'
            if len(df_2.index) > 1:
                grouped_2 = df_2.groupby('week_day')
                for name, group in grouped_2:
                    if name in days_2:
                        days_2[name].append(group.EffectiveSpotPrice.mean())
                    else:
                        days_2[name] = [group.EffectiveSpotPrice.mean()]

        # print(region, "epoch 1: ", static_count_1, "/", all_count_1)
        # print(region, "epoch 2: ", static_count_2, "/", all_count_2)

        # means_list = []
        # # vars_list = []
        # # print("region, day, mean, var")
        # days_1 = dict(sorted(days_1.items()))
        # for day in days_1.keys():
        #     # print(day, len(days_1[day]))
        #     means_list.append(mean(days_1[day]))
        #     # vars_list.append(variance(days_1[day]))
        #     # print(region, day, mean(days_1[day]))
        #
        # norm_mean = mean(means_list)
        # norm_mean_list = [(x - norm_mean) for x in means_list]
        # # print(zip(days_1.keys(),norm_mean_list))
        #
        # ax = fig.add_subplot(5, 4, num)
        # ax.plot(days_1.keys(), norm_mean_list, '*:', label=str(region))
        # ax.legend(loc='upper center', ncol=1, fancybox=True, shadow=True)
        # # plt.plot(days_1.keys(), norm_mean_list, '.-', label="Epoch 1")

        means_list = []
        # # vars_list = []
        # # print("region, day, mean, var")
        days_2 = dict(sorted(days_2.items()))
        for day in days_2.keys():
            # print(day, len(days_2[day]))
            means_list.append(mean(days_2[day]))
            # vars_list.append(variance(days_2[day]))
            # print(region, day, mean(days_2[day]))

        norm_mean = mean(means_list)
        norm_mean_list = [(x - norm_mean) for x in means_list]
        # print(zip(days_2.keys(), norm_mean_list))

        ax = fig.add_subplot(5, 4, num)
        ax.set_title(region)
        # ax.boxplot(days_2.values(), showfliers=True)
        ax.set_xticklabels(days_2.keys(), rotation=90, fontsize=10)
        ax.plot(days_2.keys(), norm_mean_list, '*:', label=str(region))
        ax.set_ylim(-0.03, 0.03)
        # ax.legend(loc='upper center', ncol=1, fancybox=True, shadow=True)
        # ax.set_ylim(-0.006, 0.006)
        # plt.plot(days_2.keys(), norm_mean_list, '*:', label="Epoch 2")


    # plt.xlabel("Day number")
    # plt.ylabel("Normalized mean daily price")
    # plt.legend(loc='upper center', ncol=5, fancybox=True, shadow=True)

    plt.tight_layout()
    # plt.show()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_effective_price_per_region_epoch_2.png"
    plt.savefig(file, bbox_inches="tight")


def weekly_cycle_3():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=2))

    m_list = ['.', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p']
    all_means = {}
    for num, region in enumerate(regions,0):
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*__daily_price.csv"
        instList = glob.glob(instList)

        # instList = ['../../Alibaba_log_files/interesting_traces/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']

        total_count = len(instList)
        print(total_count)

        # days_1 = {}
        days_2 = {}
        for index, inst in enumerate(instList, 1):
            # print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Date'] = pd.to_datetime(df['Date'])
            df = df.sort_values('Date')
            df['week_day'] = df.Date.dt.dayofweek

            # df_1 = df[df.Date >= pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Date < pd.to_datetime("1/1/2020 00:00:00 AM")]

            # df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df = df.sort_values('Timestamp')
            # df['week_day'] = df.Timestamp.dt.dayofweek
            #
            # df_1 = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]
            # df_2 = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]

            # if not df_1.empty and not df_2.empty:

            # if df_1.SpotPrice.count() > 1:
            #     grouped_1 = df_1.groupby('week_day')
            #     for name, group in grouped_1:
            #         if name in days_1:
            #             days_1[name].append(group.NormPrice.mean())
            #         else:
            #             days_1[name] = [group.NormPrice.mean()]

            # if df_1.DailyMean.count() > 1:
            #     grouped_1 = df_1.groupby('week_day')
            #     for name, group in grouped_1:
            #         if name in days_1:
            #             days_1[name].append(group.DailyMean.mean())
            #         else:
            #             days_1[name] = [group.DailyMean.mean()]

            if len(df_2.index) > 1:
                grouped_2 = df_2.groupby('week_day')
                for name, group in grouped_2:
                    if name in days_2:
                        days_2[name].append(group.EffectiveNormPrice.mean())
                    else:
                        days_2[name] = [group.EffectiveNormPrice.mean()]

        means_list = []
        days_2 = dict(sorted(days_2.items()))
        for day in days_2.keys():
            means_list.append(mean(days_2[day]))
            if day in all_means:
                all_means[day].extend(days_2[day])
            else:
                all_means[day] = days_2[day]

        norm_mean = mean(means_list)
        norm_mean_list = [(x - norm_mean) for x in means_list]
        # norm_max = max(means_list)
        # norm_max_list = [(x/norm_max) for x in means_list]

        plt.scatter(days_2.keys(), norm_mean_list, marker=m_list[num], label=str(region))
        # ax.legend(loc='upper center', ncol=1, fancybox=True, shadow=True)

        # means_list = []
        # days_2 = dict(sorted(days_2.items()))
        # for day in days_2.keys():
        #     means_list.append(mean(days_2[day]))
        #
        # ply.plot(days_2.keys(), mean_list, '*:', label=str(region))
        # # ax.legend(loc='upper center', ncol=1, fancybox=True, shadow=True)
        #
        # ax.set_ylim(-0.025, 0.025)
        # ax.legend(loc='upper center', ncol=2, fancybox=True, shadow=True)

    # print(all_means)
    all_means_list = []
    all_means = dict(sorted(all_means.items()))
    for day in all_means.keys():
        all_means_list.append(mean(all_means[day]))

    norm_mean = mean(all_means_list)
    norm_mean_list = [(x - norm_mean) for x in all_means_list]
    # norm_max = max(all_means_list)
    # norm_max_list = [(x / norm_max) for x in all_means_list]
    #
    plt.plot(all_means.keys(), norm_mean_list, '.-', label="general mean")
    plt.xlabel("Day of week")
    plt.ylabel("Normalized mean daily price")
    # plt.legend(loc='upper center', ncol=5, fancybox=True, shadow=True)
    plt.legend(loc='center', bbox_to_anchor=(0,1.02,1,0.2), ncol=6, fancybox=True, shadow=True)

    plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_per_region_sampled_prices.png"
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_epoch_2_effective_spot_price.png"
    # plt.savefig(file, bbox_inches="tight")


def check_weekly_cycle():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing',
               'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1',
               'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions,1):
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        instList = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
        instList = glob.glob(instList)

        # total_count = len(instList)

        days_1 = {}
        days_2 = {}
        for index, inst in enumerate(instList, 1):
            # print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df['week_day'] = df.Timestamp.dt.dayofweek

            df_1 = df[df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]

            if df_1.SpotPrice.count() > 1:
                grouped_1 = df_1.groupby('week_day')
                for name, group in grouped_1:
                    if name in days_1:
                        days_1[name].append(group.NormPrice.mean())
                    else:
                        days_1[name] = [group.NormPrice.mean()]

            if df_2.SpotPrice.count() > 1:
                grouped_2 = df_2.groupby('week_day')
                for name, group in grouped_2:
                    if name in days_2:
                        days_2[name].append(group.NormPrice.mean())
                    else:
                        days_2[name] = [group.NormPrice.mean()]

        ax = fig.add_subplot(5, 4, num)
        ax.set_title(region)

        data = []
        for day in range(7):
            s = []
            for i in range(10000):
                s.append(random.choices(days_1[day], k=100))
            s = np.mean(np.asarray(s), axis=1)

            data.append(s)

        stat, p = f_oneway(data[0], data[1], data[2], data[3], data[4], data[5], data[6])
        print('Epoch 1 - ANOVA: stat=%f, p=%f' % (stat, p))

        means_list = []
        for day in range(7):
            means_list.append(mean(data[day]))

        norm_mean = mean(means_list)
        norm_mean_list = [(x - norm_mean) for x in means_list]

        ax.set_xticklabels(range(7), rotation=90, fontsize=8)
        ax.plot(range(7), norm_mean_list, '*:', label="epoch 1")
        # ax.set_ylim(-0.03, 0.03)

        data = []   # Array of arrays - each array contains the resampled data for a specific day.
        for day in range(7):

            s = []  # The array of the sampled data
            # Create 10000 samples for each day.
            # Each sample in s is the mean of 100 randomly picked values. These values are chosen from our original
            # data.
            for i in range(10000):
                s.append(random.choices(days_2[day], k=100))
            s = np.mean(np.asarray(s), axis=1)  # s is an array of samples with a normally distributed population.

            data.append(s)

        # Running the ANOVA test
        stat, p = f_oneway(data[0], data[1], data[2], data[3], data[4], data[5], data[6])
        print('Epoch 2 - ANOVA: stat=%f, p=%f' % (stat, p))

        means_list = []
        for day in range(7):
            means_list.append(mean(data[day]))

        norm_mean = mean(means_list)
        norm_mean_list = [(x - norm_mean) for x in means_list]

        ax.set_xticklabels(range(7), rotation=90, fontsize=8)
        ax.plot(range(7), norm_mean_list, 'o-', label="epoch 2")
        ax.legend(loc='upper center', ncol=2, fancybox=True, shadow=True)

    plt.tight_layout()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_hist_test.png"
    plt.savefig(file, bbox_inches="tight")


def weekly_cycle_ecdf():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing',
               'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1',
               'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['ap-northeast-1', 'cn-beijing', 'eu-west-1', 'me-east-1', 'us-east-1']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions,1):
        print(region)
        instList = "./alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
        instList = glob.glob(instList)

        # instList = ['../../Alibaba_log_files/interesting_traces/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']

        total_count = len(instList)

        # days_1 = {}
        days_2 = {}
        for index, inst in enumerate(instList, 1):
            # print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df['week_day'] = df.Timestamp.dt.dayofweek

            # df_1 = df[df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]

            # if df_1.SpotPrice.count() > 1:
            #     grouped_1 = df_1.groupby('week_day')
            #     for name, group in grouped_1:
            #         if name in days_1:
            #             days_1[name].append(group.NormPrice.mean())
            #         else:
            #             days_1[name] = [group.NormPrice.mean()]

            if df_2.SpotPrice.count() > 1:
                grouped_2 = df_2.groupby('week_day')
                for name, group in grouped_2:
                    if name in days_2:
                        days_2[name].append(group.NormPrice.mean())
                    else:
                        days_2[name] = [group.NormPrice.mean()]

        ax = fig.add_subplot(5, 4, num)
        ax.set_title(region)
        for day, values in days_2.items():
            # values.sort()
            if values:
                values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                # if month.to_timestamp(freq ='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):

                ax.step(values, y, '-', where='post', label=day)

        ax.legend()
        # ax.set_ylim(-0.025, 0.025)
        # ax.legend(loc='upper center', ncol=2, fancybox=True, shadow=True)

    plt.tight_layout()
    # plt.show()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/weekly_cycle_per_region_ecdf.png"
    plt.savefig(file, bbox_inches="tight")


def effective_price():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions,1):
        print(region)
        instList = "./alibaba_work/" + region + "_full_traces_updated/*__daily_price.csv"
        instList = glob.glob(instList)

        total_count = len(instList)

        data_1 = []
        data_2 = []
        for index, inst in enumerate(instList, 1):
            # print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Date'] = pd.to_datetime(df['Date'])
            df = df.sort_values('Date')
            # df['week_day'] = df.Date.dt.dayofweek

            df_1 = df[df.Date < pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Date >= pd.to_datetime("1/1/2020 00:00:00 AM")]

            # if not df_1.empty and not df_2.empty:

            if df_1.Date.count() >= 1:
                data_1.extend(df_1.EffectiveNormPrice)

            if df_2.Date.count() >= 1:
                data_2.extend(df_2.EffectiveNormPrice)

        ax = fig.add_subplot(5, 4, num)
        ax.set_title(region)

        data_1.sort()
        ecdf = ECDF(data_1)
        y = ecdf(data_1)

        ax.step(data_1, y, '-', where='post', label="Epoch 1")

        data_2.sort()
        ecdf = ECDF(data_2)
        y = ecdf(data_2)

        ax.step(data_2, y, '-', where='post', label="Epoch 2")

        # ax.set_ylim(-0.25, 0.15)
        ax.legend(loc='upper center', ncol=2, fancybox=True, shadow=True)

    plt.tight_layout()
    # plt.show()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/effective_price_per_region_ecdf.png"
    plt.savefig(file, bbox_inches="tight")

def daily_cycle():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['ap-northeast-1', 'cn-beijing', 'eu-west-1', 'me-east-1', 'us-east-1']

    regions = ['*']

    for region in regions:
        print(region)
        instList = "./alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        instList = glob.glob(instList)

        # instList = ['../../Alibaba_log_files/interesting_traces/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']

        total_count = len(instList)

        # m_list = ['.', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p']

        days_1 = {}
        days_2 = {}
        # all_count_1 = 0
        # all_count_2 = 0
        # static_count_1 = 0
        # static_count_2 = 0
        for index, inst in enumerate(instList):
            print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            # df['week_day'] = df.Timestamp.dt.dayofweek
            # df['time_of_day'] = ((df.Timestamp.dt.hour * 3600) + (
            #         df.Timestamp.dt.minute * 60) + df.Timestamp.dt.second) / 3600
            df['time_of_day'] = ((df.Timestamp.dt.hour * 3600) + (df.Timestamp.dt.minute * 60) ) / 3600

            df['time_of_week'] = df.Timestamp.dt.dayofweek
            df['time_of_week'] = df.time_of_week + (df.time_of_day / 24)

            df_1 = df[df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")]
            df_2 = df[df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")]

            if df_1.SpotPrice.count() > 1:
                # all_count_1 += 1
                grouped_1 = df_1.groupby('time_of_week')
                for name, group in grouped_1:
                    if name in days_1:
                        days_1[name].append(group.NormPrice.mean())
                    else:
                        days_1[name] = [group.NormPrice.mean()]
            # else:
            #     static_count_1 += 1

            if df_2.SpotPrice.count() > 1:
                # all_count_2 += 1
                grouped_2 = df_2.groupby('time_of_week')
                for name, group in grouped_2:
                    if name in days_2:
                        days_2[name].append(group.NormPrice.mean())
                    else:
                        days_2[name] = [group.NormPrice.mean()]
            # else:
            #     static_count_2 += 1

        # print(region, "epoch 1: ", static_count_1, "/", all_count_1)
        # print(region, "epoch 2: ", static_count_2, "/", all_count_2)

        means_list_1 = []
        # vars_list = []
        # print("region, day, mean, var")
        days_1 = dict(sorted(days_1.items()))
        for day in days_1.keys():
            # print(day, len(days_1[day]))
            means_list_1.append(mean(days_1[day]))
            # vars_list.append(variance(days_1[day]))
            # print(region, day, mean(days_1[day]))

        norm_mean = mean(means_list_1)
        norm_mean_list = [(x - norm_mean) for x in means_list_1]
        # print(zip(days_1.keys(),norm_mean_list))

        ax = fig.add_subplot(2, 1, 1)
        ax.set_title("Epoch 1")
        ax.plot(days_1.keys(), means_list_1, '.-', label=str(region))
        # plt.plot(days_1.keys(), norm_mean_list, '.-', label="Epoch 1")
        ax.set_xlabel("Time of week")
        ax.set_ylabel("Normalized price mean")

        means_list_2 = []
        # vars_list = []
        # print("region, day, mean, var")
        days_2 = dict(sorted(days_2.items()))
        for day in days_2.keys():
            # print(day, len(days_2[day]))
            means_list_2.append(mean(days_2[day]))
            # vars_list.append(variance(days_2[day]))
            # print(region, day, mean(days_2[day]))

        norm_mean = mean(means_list_2)
        norm_mean_list = [(x - norm_mean) for x in means_list_2]
        # print(zip(days_2.keys(), norm_mean_list))

        ax = fig.add_subplot(2, 1, 2)
        ax.set_title("Epoch 2")
        ax.plot(days_2.keys(), means_list_2, '*:', label=str(region))
        # plt.plot(days_2.keys(), norm_mean_list, '*:', label="Epoch 2")
        ax.set_xlabel("Time of week")
        ax.set_ylabel("Normalized price mean")

    # plt.xlabel("Time of day")
    # plt.ylabel("Normalized mean price")
    # plt.legend(loc='upper center', ncol=5, fancybox=True, shadow=True)

    plt.tight_layout()
    # plt.show()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/daily_cycle_whole_week_without_static.png"
    plt.savefig(file, bbox_inches="tight")


def calc_time_of_week_list():
    missing_dates = pd.date_range(start=pd.to_datetime("11/13/2018 00:00:00 AM"),
                                  end=pd.to_datetime("7/14/2021 00:00:00 AM"), freq='5T').tolist()
    df = pd.DataFrame(missing_dates, columns=['Timestamp'])
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('Timestamp')

    df['year_num'] = df.Timestamp.dt.year - 2018
    # df['week_num'] = (df.Timestamp.dt.isocalendar().week + (df.year_num * 52))
    df['week_num'] = (df.Timestamp.dt.week + (df.year_num * 52))
    df.loc[df['week_num'] == 1, 'week_num'] = 53
    df.loc[df['week_num'] == 209, 'week_num'] = 158

    df['time_of_day'] = ((df.Timestamp.dt.hour * 3600) + (df.Timestamp.dt.minute * 60) + df.Timestamp.dt.second) / 3600

    df['time_of_week'] = df.Timestamp.dt.dayofweek
    df['time_of_week'] = df.time_of_week + (df.time_of_day / 24)

    # print(sorted(df.time_of_week.unique()))

    time_of_week = [round(num, 6) for num in sorted(df.time_of_week.unique())]
    week_number = [x for x in df.week_num.unique()]
    return time_of_week, week_number


def daily_cycle_2():
    time_of_week, week_number = calc_time_of_week_list()
    print(len(week_number), len(time_of_week))
    week_count = len(week_number)
    time_count = len(time_of_week)

    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    regions = ['ap-northeast-1']

    # regions = ['*']

    for region in regions:
        instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__resample.csv']
        # print(region)
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__resample.csv"
        # instList = glob.glob(instList)

        price_mat = np.zeros((week_count, time_count))
        count_mat = np.zeros((week_count, time_count))

        total_count = len(instList)
        for index, inst in enumerate(instList):
            print("Processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df = df.set_index('Timestamp')

            missing_dates = pd.date_range(start=df.index[0], end=df.index[-1], freq='5T').difference(df.index).tolist()

            df2 = pd.DataFrame(index=missing_dates, columns=['OriginPrice', 'SpotPrice', 'NormPrice'])
            df = df.append(df2)
            df = df.sort_index()
            df = df.ffill()

            df = df.resample("5T").interpolate()
            df.index.name = 'Timestamp'
            df = df.reset_index()

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')

            df['year_num'] = df.Timestamp.dt.year - 2018
            # df['week_num'] = (df.Timestamp.dt.isocalendar().week + (df.year_num * 52))
            df['week_num'] = (df.Timestamp.dt.week + (df.year_num * 52))
            df.loc[df['week_num'] == 1, 'week_num'] = 53
            df.loc[df['week_num'] == 209, 'week_num'] = 158

            df['time_of_day'] = ((df.Timestamp.dt.hour * 3600) + (df.Timestamp.dt.minute * 60) + df.Timestamp.dt.second) / 3600

            df['time_of_week'] = df.Timestamp.dt.dayofweek
            df['time_of_week'] = df.time_of_week + (df.time_of_day / 24)

            for index, row in df.iterrows():
                # print(row['Timestamp'], row['year_num'], row['week_num'], row['time_of_week'])
                price_mat[week_number.index(row['week_num'])][time_of_week.index(round(row['time_of_week'], 6))] += row.NormPrice
                count_mat[week_number.index(row['week_num'])][time_of_week.index(round(row['time_of_week'], 6))] += 1

        total_mat = np.zeros((week_count, time_count))
        for row in range(week_count):
            for col in range(time_count):
                if price_mat[row][col] != 0:
                    total_mat[row][col] = price_mat[row][col] / count_mat[row][col]

        file2 = "./alibaba_work/feature_files/" + region + "_time_of_week_cycle_test.npy"
        with open(file2, 'wb') as f:
            np.save(f, total_mat)


def norm_price_per_time_of_day_week():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['cn-beijing']

    for region in regions:
        print(region)
        instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        instList = glob.glob(instList)

        for inst in instList:
            print(inst)
            df = pd.read_csv(inst)
            df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')

            # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

            df['year_num'] = df.Timestamp.dt.year - 2018
            # df['week_num'] = (df.Timestamp.dt.isocalendar().week + (df.year_num * 52))
            df['week_num'] = (df.Timestamp.dt.week + (df.year_num * 52))
            df['day_num'] = df.Timestamp.dt.dayofyear + (df.year_num * 365)

            df['week_day'] = df.Timestamp.dt.dayofweek

            df['time_of_day'] = ((df.Timestamp.dt.hour * 3600) + (
                        df.Timestamp.dt.minute * 60) + df.Timestamp.dt.second) / 3600

            df['time_of_week'] = df.Timestamp.dt.dayofweek
            df['time_of_week'] = df.time_of_week + (df.time_of_day / 24)
            # print(df[['Timestamp','year_num','week_num','time_of_week','day_num','time_of_day']])

            # print(df.head(20))
            # df2 = df[['NormPrice', 'Timestamp', 'week_day']].copy()
            # print(df2)
            # df2['Timestamp2'] = df2.Timestamp.dt.round("T")
            # r = pd.date_range(start='2018-11-13', end='2021-01-01', freq='1min')
            # df2 = df2.set_index('Timestamp2').reindex(r).ffill()
            # df2.reset_index(inplace=True)

            ax = fig.add_subplot(2, 1, 1)
            ax.set_title("Normalized price per time in week")
            ax.scatter(df.time_of_week, df.week_num, c=df.NormPrice)
            ax.set_xlabel("Time of week")
            ax.set_ylabel("Week number")

            ax = fig.add_subplot(2, 1, 2)
            ax.set_title("Normalized price per time in day")
            ax.scatter(df.time_of_day, df.day_num, c=df.NormPrice)
            ax.set_xticks(np.arange(0, 25, 1))
            ax.set_xlabel("Time of day")
            ax.set_ylabel("Day number")

    plt.tight_layout()
    plt.show()


def find_minimal_price_in_epoch():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing',
               'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1',
               'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    for region in regions:
        # print(region)
        instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        instList = glob.glob(instList)

        min_1 = 0.1
        min_2 = 0.1
        for inst in instList:
            # print(inst)
            df = pd.read_csv(inst)
            df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')

            df = df[df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM")]

            curr_min_1 = df.NormPrice[df.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM")].min()
            curr_min_2 = df.NormPrice[df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")].min()

            if curr_min_1 < min_1:
                min_1 = curr_min_1

            if curr_min_2 < min_2:
                min_2 = curr_min_2

        print(region,": ", min_1, min_2)





if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--function", help='The function we want to run')

    args = parser.parse_args()

    if args.function:
        func = args.function
        if func == 'add_info_for_weekly_cycle_and_index':
            print("Running add_info_for_weekly_cycle_and_index function...")
            add_info_for_weekly_cycle_and_index()

        elif func == 'weekly_cycle':
            print("Running weekly_cycle function...")
            weekly_cycle()

        elif func == 'weekly_cycle_2':
            print("Running weekly_cycle_2 function...")
            weekly_cycle_2()

        elif func == 'weekly_cycle_3':
            print("Running weekly_cycle_3 function...")
            weekly_cycle_3()

        elif func == 'check_weekly_cycle':
            print("Running check_weekly_cycle function...")
            check_weekly_cycle()

        elif func == 'calc_daily_norm_price':
            print("Running calc_daily_norm_price function...")
            calc_daily_norm_price()

        elif func == 'norm_price_per_time_of_day_week':
            print("Running norm_price_per_time_of_day_week function...")
            norm_price_per_time_of_day_week()

        elif func == 'find_minimal_price_in_epoch':
            print("Running find_minimal_price_in_epoch function...")
            find_minimal_price_in_epoch()

        elif func == 'daily_cycle':
            print("Running daily_cycle function...")
            daily_cycle()

        elif func == 'daily_cycle_2':
            print("Running daily_cycle_2 function...")
            daily_cycle_2()

        elif func == 'find_missing_dates':
            print("Running find_missing_dates")
            find_missing_dates()

        elif func == 'count_number_of_traces':
            print("Running count_number_of_traces")
            count_number_of_traces()

        elif func == 'calc_time_of_week_list':
            print("Running calc_time_of_week_list function ...")
            calc_time_of_week_list()

        elif func == 'weekly_cycle_ecdf':
            print("Running weekly_cycle_ecdf function...")
            weekly_cycle_ecdf()

        elif func == 'effective_price':
            print("Running effective_price function...")
            effective_price()

    print("Finished!")