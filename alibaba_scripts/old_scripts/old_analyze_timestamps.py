# import os.path
# from os import path
import glob
import matplotlib.pyplot as plt

import matplotlib
# matplotlib.use('Agg')

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import pandas as pd, numpy as np, seaborn as sns, scipy
# from contextlib import closing
import argparse
from statsmodels.distributions.empirical_distribution import ECDF
from scipy import stats
# from collections import Counter

# plt.style.use('seaborn-colorblind')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 12,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10
    }

plt.rcParams.update(tex_fonts)


def set_size(width=441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def fix_time_intervals_data():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    # fig = plt.figure(figsize=set_size(fraction=3))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        # instList = [
        #     'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        # instList = ['F:/alibaba_work\cn-hangzhou_full_traces_updated\cn_hangzhou_i__ecs_e3_3xlarge__optimized__vpc__clean.csv']
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean.csv"
        instList = glob.glob(instList)
        total_length = len(instList)

        # data = dict()
        for index, inst in enumerate(instList, 1):
            print(inst, index, "/", total_length)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            new_df = []
            length = df.Timestamp.count()
            print(length)
            i = 0
            while i < length - 1:
                new_df.append(
                    [df.OriginPrice.iloc[i], df.SpotPrice.iloc[i], df.NormPrice.iloc[i], df.TimeInSeconds.iloc[i],
                     df.Timestamp.iloc[i]])
                new_df.append([df.OriginPrice.iloc[i], df.SpotPrice.iloc[i], df.NormPrice.iloc[i],
                               df.TimeInSeconds.iloc[i + 1], df.Timestamp.iloc[i + 1]])
                i += 1

            new_df.append(
                [df.OriginPrice.iloc[-1], df.SpotPrice.iloc[-1], df.NormPrice.iloc[-1], df.TimeInSeconds.iloc[-1],
                 df.Timestamp.iloc[-1]])
            df2 = pd.DataFrame(new_df, columns=['OriginPrice', 'SpotPrice', 'NormPrice', 'TimeInSeconds', 'Timestamp'])
            df2.reset_index(drop=True, inplace=True)

            row_list = []
            length = df2.Timestamp.count()
            i = 1
            while i < length-1:
                if df2.SpotPrice.iloc[i - 1] == df2.SpotPrice.iloc[i] == df2.SpotPrice.iloc[i + 1]:
                    row_list.append(i)
                i += 1

            df3 = df2.drop(row_list)
            # print(df3)

            name, csv = inst.split('.csv')
            file = name + "__square_2.csv"
            print("creating ", file)
            # print(df3)
            df3.to_csv(file, index=False)

            # plt.plot(df.Timestamp, df.NormPrice, '.:', label='origin')
            # plt.plot(df3.Timestamp, df3.NormPrice, 'x:', label='fixed')

    # plt.legend()
    # plt.show()
    # plt.savefig("./cloud_analyzing_repo/alibaba_scripts/checking_time_interval_fix.png", bbox_inches="tight")


def keep_only_slopes():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "./alibaba_work/" + region + "_full_traces_updated/*clean.csv"
        instList = glob.glob(instList)
        total_length = len(instList)

        for index, inst in enumerate(instList, 1):
            print(inst, index, "/", total_length)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            new_df = [[df.OriginPrice.iloc[0], df.SpotPrice.iloc[0], df.NormPrice.iloc[0], df.TimeInSeconds.iloc[0], df.Timestamp.iloc[0]]]
            length = df.Timestamp.count()
            i = 1
            while i < length:
                if df.NormPrice.iloc[i] != df.NormPrice.iloc[i - 1]:
                    new_df.append([df.OriginPrice.iloc[i], df.SpotPrice.iloc[i], df.NormPrice.iloc[i], df.TimeInSeconds.iloc[i], df.Timestamp.iloc[i]])
                i += 1
            new_df.append([df.OriginPrice.iloc[-1], df.SpotPrice.iloc[-1], df.NormPrice.iloc[-1], df.TimeInSeconds.iloc[-1], df.Timestamp.iloc[-1]])

            df2 = pd.DataFrame(new_df, columns=['OriginPrice', 'SpotPrice', 'NormPrice', 'TimeInSeconds', 'Timestamp'])
            df2 = df2.sort_values('Timestamp')
            df2.reset_index(drop=True, inplace=True)

            df2['NormPriceDelta'] = df2.NormPrice.diff()
            df2['TimeDelta'] = df2['TimeInSeconds'].diff()
            df2['TimeDeltaHours'] = df2.TimeDelta / 3600

            name, csv = inst.split('.csv')
            file = name + "__only_slopes.csv"
            print("creating ", file)
            df2.to_csv(file, index=False)


def draw_time_delta_ecdf_per_epoch():
    fig = plt.figure(tight_layout=True, figsize=set_size())
    # regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
    #                 'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
    #                 'eu-west-1','me-east-1','us-east-1','us-west-1']
    # regions = ['eu-west-1','me-east-1','us-east-1']
    regions = ['*']

    for region in regions:
        # instList = "./"+region+"_2020_traces/*clean.csv"
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*__clean_2.csv"
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__vpc.csv"
        instList = glob.glob(instList)

        total_length = len(instList)

        time_delta_epoch_1 = []
        time_delta_epoch_2 = []
        time_delta_epoch_3 = []
        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_length)
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df.loc[0, 'TimeDelta'] = 0
            # df = df[df['TimeDelta'] == 0].copy()
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True,inplace=True)

            # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

            df['TimeDelta'] = df['TimeInSeconds'].diff()
            df['TimeDeltaHours'] = df.TimeDelta / 3600

            # df = df[df['NormPriceDelta'] < 0.0].copy()
            # df = df[df.FixedNormPriceDelta == 0.03].copy()
            df = df[(df.FixedNormPriceDelta != -0.02) & (df.FixedNormPriceDelta != 0.03)].copy()

            time_delta_epoch_1.extend(df.TimeDeltaHours[df.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM")].dropna().tolist())
            time_delta_epoch_2.extend(df.TimeDeltaHours[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())
            time_delta_epoch_3.extend(df.TimeDeltaHours[(df.Timestamp > pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())

        print("Finished processing all instances. Starting to draw...")
        print("Decrease time intervals:")

        time_delta_epoch_1.sort()
        ecdf = ECDF(time_delta_epoch_1)
        y = ecdf(time_delta_epoch_1)
        y1 = y

        print("epoch 1 " + str(np.interp([0.05,0.06,0.07,0.08,0.09], time_delta_epoch_1, y)))

        plt.step(time_delta_epoch_1, y, '-', color='black', where='post', label="Epoch 1")

        time_delta_epoch_2.sort()
        ecdf = ECDF(time_delta_epoch_2)
        y = ecdf(time_delta_epoch_2)
        y2 = y

        print("epoch 2 " + str(np.interp([1.0,1.01], time_delta_epoch_2, y)))

        plt.step(time_delta_epoch_2, y, '-.', color='red', where='post', label="Epoch 2")

        time_delta_epoch_3.sort()
        ecdf = ECDF(time_delta_epoch_3)
        y = ecdf(time_delta_epoch_3)
        y3 = y

        print("epoch 3 " + str(np.interp([0.24,0.25,0.26], time_delta_epoch_3, y)))

        plt.step(time_delta_epoch_3, y, '--', color='blue', where='post', label="Epoch 3")

        # print("1 vs 2:", stats.ks_2samp(y1, y2))
        # print("1 vs 3:", stats.ks_2samp(y1, y3))
        # print("2 vs 3:", stats.ks_2samp(y2, y3))

    # plt.legend()
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
    plt.xlabel("Time between consecutive price changes [h]")
    plt.ylabel("Probability")
    plt.xlim(0.0, 6.0)
    plt.show()

    # plt.savefig("./cloud_analyzing_repo/alibaba_scripts/graphs/time_intervals_between_price_changes_decrease.png",
    #             bbox_inches="tight")


def draw_time_delta_ecdf_per_region():
    
    fig = plt.figure(figsize=set_size())
    # regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
    #                 'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
    #                 'eu-west-1','me-east-1','us-east-1','us-west-1']
    # regions = ['eu-west-1','me-east-1','us-east-1']
    regions = ['*']

    for region in regions:
        # instList = "./"+region+"_2020_traces/*clean.csv"
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__clean.csv"
        # instList = "./alibaba_work/" + region + "_full_traces_updated/*__vpc.csv"
        instList = "./alibaba_work/" + region + "_full_traces_updated/*__only_slopes.csv"
        instList = glob.glob(instList)

        total_length = len(instList)

        time_delta_epoch_dec_1 = []
        time_delta_epoch_dec_2 = []
        time_delta_epoch_dec_3 = []

        time_delta_epoch_inc_1 = []
        time_delta_epoch_inc_2 = []
        time_delta_epoch_inc_3 = []
        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_length)
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            # df.reset_index(drop=True,inplace=True)

            # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

            # df['TimeDelta'] = df['TimeInSeconds'].diff()
            # df['TimeDeltaHours'] = df.TimeDelta / 3600

            df_dec = df[df['NormPriceDelta'] < 0.0].copy()

            time_delta_epoch_dec_1.extend(df_dec.TimeDeltaHours[df_dec.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM")].dropna().tolist())
            time_delta_epoch_dec_2.extend(df_dec.TimeDeltaHours[(df_dec.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df_dec.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())
            time_delta_epoch_dec_3.extend(df_dec.TimeDeltaHours[(df_dec.Timestamp > pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())

            df_inc = df[df['NormPriceDelta'] > 0.0].copy()

            time_delta_epoch_inc_1.extend(df_inc.TimeDeltaHours[df_inc.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM")].dropna().tolist())
            time_delta_epoch_inc_2.extend(df_inc.TimeDeltaHours[(df_inc.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df_inc.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())
            time_delta_epoch_inc_3.extend(df_inc.TimeDeltaHours[(df_inc.Timestamp > pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())

        print("Finished processing all instances. Starting to draw...")
        print("Decrease time intervals:")

        time_delta_epoch_dec_1.sort()
        ecdf = ECDF(time_delta_epoch_dec_1)
        y = ecdf(time_delta_epoch_dec_1)

        # print("epoch 1 " + str(np.interp([0.09], time_delta_epoch_1, y)))

        plt.step(time_delta_epoch_dec_1, y, color='black', linestyle='-', where='post', label="Decrease epoch 1")

        time_delta_epoch_inc_1.sort()
        ecdf = ECDF(time_delta_epoch_inc_1)
        y = ecdf(time_delta_epoch_inc_1)

        # print("epoch 1 " + str(np.interp([0.09], time_delta_epoch_1, y)))

        plt.step(time_delta_epoch_inc_1, y, color='silver', linestyle='--', where='post', label="Increase epoch 1")

        time_delta_epoch_dec_2.sort()
        ecdf = ECDF(time_delta_epoch_dec_2)
        y = ecdf(time_delta_epoch_dec_2)

        # print("epoch 2 " + str(np.interp([1.0], time_delta_epoch_dec_2, y)))

        plt.step(time_delta_epoch_dec_2, y, color='blue', linestyle='-', where='post', label="Decrease epoch 2")

        time_delta_epoch_inc_2.sort()
        ecdf = ECDF(time_delta_epoch_inc_2)
        y = ecdf(time_delta_epoch_inc_2)

        # print("epoch 2 " + str(np.interp([1.0], time_delta_epoch_dec_2, y)))

        plt.step(time_delta_epoch_inc_2, y, color='lightblue', linestyle='--', where='post', label="Increase epoch 2")

        time_delta_epoch_dec_3.sort()
        ecdf = ECDF(time_delta_epoch_dec_3)
        y = ecdf(time_delta_epoch_dec_3)

        # print("epoch 3 " + str(np.interp([0.26], time_delta_epoch_3, y)))

        plt.step(time_delta_epoch_dec_3, y, color='red', linestyle='-', where='post', label="Decrease epoch 3")

        time_delta_epoch_inc_3.sort()
        ecdf = ECDF(time_delta_epoch_inc_3)
        y = ecdf(time_delta_epoch_inc_3)

        # print("epoch 3 " + str(np.interp([0.26], time_delta_epoch_inc_3, y)))

        plt.step(time_delta_epoch_inc_3, y, color='lightsalmon', linestyle='-', where='post', label="Increase epoch 3")

    plt.legend()
    plt.xlabel("Time between consecutive price changes [h]")
    plt.ylabel("Probability")
    plt.xlim(0.0, 2.0)
    # plt.show()

    plt.savefig("./cloud_analyzing_repo/alibaba_scripts/graphs/time_intervals_between_price_changes_increase_vs_decrease_per_epoch.png", bbox_inches = "tight")


def draw_time_delta_ecdf_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = glob.glob(instList)
        total_length = len(instList)

        data = dict()
        for index, inst in enumerate(instList, 1):
            print(inst, index, "/", total_length)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df.loc[0, 'TimeDelta'] = 0
            # df = df[df['TimeDelta'] == 0].copy()
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            # df = df[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))]
            # df = df[(df.Timestamp < pd.to_datetime("2/1/2019 00:00:00 AM"))]
            df['month_year'] = df['Timestamp'].dt.to_period('M')

            grouped = df.groupby('month_year')
            for month, group in grouped:
                group1 = group.copy()
                group1['TimeDelta'] = group1['TimeInSeconds'].diff()
                group1['TimeDeltaHours'] = group1.TimeDelta / 3600
                if month in data:
                    data[month].extend(group1.TimeDeltaHours.dropna().tolist())
                    # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    data[month] = group1.TimeDeltaHours.dropna().tolist()
                    # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        data = dict(sorted(data.items()))

        # ax = fig.add_subplot(4, 5, num)
        ax = fig.add_subplot(1, 1, num)
        # ax.set_title(region)

        f1 = 0
        f2 = 0
        f3 = 0
        # m_list = ['.', ',', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P', 'h', 'H', 'D']
        for month, values in data.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                # ax.step(values, y, '-.', where='post', label=month)
                if month.to_timestamp(freq ='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        ax.step(values, y, '--', color='black', where='post', label="Before 09-2019")
                        f1 = 1
                    else:
                        ax.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq ='M') < pd.to_datetime("12/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        ax.step(values, y, '-.', color='red', where='post', label="09-2019 until 12-2020")
                        f2 = 1
                    else:
                        ax.step(values, y, '-.', color='red', where='post')
                else:
                    if f3 == 0:
                        ax.step(values, y, ':', color='blue', where='post', label="After 12-2020")
                        f3 = 1
                    else:
                        ax.step(values, y, ':', color='blue', where='post')

        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
        # ax.legend(ncol=3, fancybox=True, shadow=True)
        ax.set_xlabel("Time between consecutive price changes [h]")
        ax.set_ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        # ax.set_xlim(0.0, 1.0)
        # ax.set_ylim(0.0, 0.25)

    plt.show()
    # plt.tight_layout()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/time_delta_ecdf_by_month_clean_traces_zoom-in.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_slope_time_delta_ecdf_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    fig = plt.figure(figsize=set_size(fraction=3))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "./alibaba_work/" + region + "_full_traces_updated/*__only_slopes.csv"
        instList = glob.glob(instList)
        total_length = len(instList)

        data_inc = dict()
        data_dec = dict()
        for index, inst in enumerate(instList, 1):
            print(inst, index, "/", total_length)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            # df = df[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))]
            # df = df[(df.Timestamp < pd.to_datetime("2/1/2019 00:00:00 AM"))]
            df['month_year'] = df['Timestamp'].dt.to_period('M')

            grouped = df.groupby('month_year')
            for month, group in grouped:
                group1 = group.copy()
                group1['GroupTimeDelta'] = group1['TimeInSeconds'].diff()
                group1['GroupTimeDeltaHours'] = group1.GroupTimeDelta / 3600

                group_inc = group1[group1.NormPriceDelta > 0.0].copy()
                if month in data_inc:
                    data_inc[month].extend(group_inc.GroupTimeDeltaHours.dropna().tolist())
                else:
                    data_inc[month] = group_inc.GroupTimeDeltaHours.dropna().tolist()

                group_dec = group1[group1.NormPriceDelta < 0.0].copy()
                if month in data_dec:
                    data_dec[month].extend(group_dec.GroupTimeDeltaHours.dropna().tolist())
                else:
                    data_dec[month] = group_dec.GroupTimeDeltaHours.dropna().tolist()

        data_inc = dict(sorted(data_inc.items()))
        data_dec = dict(sorted(data_dec.items()))

        # ax = fig.add_subplot(4, 5, num)
        ax = fig.add_subplot(1, 2, 1)
        ax.set_title("increase")

        f1 = 0
        f2 = 0
        f3 = 0
        # m_list = ['.', ',', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P', 'h', 'H', 'D']
        for month, values in data_inc.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                # ax.step(values, y, '-.', where='post', label=month)
                if month.to_timestamp(freq ='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        ax.step(values, y, '--', color='black', where='post', label="Before 09-2019")
                        f1 = 1
                    else:
                        ax.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq ='M') < pd.to_datetime("12/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        ax.step(values, y, '-.', color='red', where='post', label="09-2019 until 12-2020")
                        f2 = 1
                    else:
                        ax.step(values, y, '-.', color='red', where='post')
                else:
                    if f3 == 0:
                        ax.step(values, y, ':', color='blue', where='post', label="After 12-2020")
                        f3 = 1
                    else:
                        ax.step(values, y, ':', color='blue', where='post')

        ax.legend(ncol=3, fancybox=True, shadow=True)
        ax.set_xlabel("Time Difference (hours)")
        ax.set_ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        ax.set_xlim(0.0, 2.0)
        # ax.set_ylim(0.0, 0.25)

        ax = fig.add_subplot(1, 2, 2)
        ax.set_title("decrease")

        f1 = 0
        f2 = 0
        f3 = 0
        # m_list = ['.', ',', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P', 'h', 'H', 'D']
        for month, values in data_dec.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                # ax.step(values, y, '-.', where='post', label=month)
                if month.to_timestamp(freq='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        ax.step(values, y, '--', color='black', where='post', label="Before 09-2019")
                        f1 = 1
                    else:
                        ax.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime(
                        "12/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        ax.step(values, y, '-.', color='red', where='post', label="09-2019 until 12-2020")
                        f2 = 1
                    else:
                        ax.step(values, y, '-.', color='red', where='post')
                else:
                    if f3 == 0:
                        ax.step(values, y, ':', color='blue', where='post', label="After 12-2020")
                        f3 = 1
                    else:
                        ax.step(values, y, ':', color='blue', where='post')

        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
        ax.legend(ncol=3, fancybox=True, shadow=True)
        ax.set_xlabel("Time Difference (hours)")
        ax.set_ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        ax.set_xlim(0.0, 2.0)
        # ax.set_ylim(0.0, 0.25)

    plt.tight_layout()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/time_delta_ecdf_by_slope_by_month.png"
    plt.savefig(file, bbox_inches="tight")


def timeDelta_boxplot_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    regions = ['*']

    for region in regions:
        print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc.csv']
        instList = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
        instList = glob.glob(instList)

        data = dict()
        for inst in instList:
            # print(inst)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            df = df[df.Timestamp > pd.to_datetime("1/1/2019 00:00:00 AM")]

            df['TimeDelta'] = df['TimeInSeconds'].diff()
            df['TimeDeltaHours'] = df.TimeDelta / 3600

            df['month_year'] = df['Timestamp'].dt.to_period('M')

            grouped = df.groupby('month_year')
            for name, group in grouped:
                if name in data:
                    data[name].extend(group.TimeDeltaHours.dropna().tolist())
                else:
                    data[name] = group.TimeDeltaHours.dropna().tolist()

        data = dict(sorted(data.items()))

        fig = plt.figure(figsize=set_size(fraction=2))

        # m_list = ['.', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p']

        for month, values in data.items():
            values.sort()
            ecdf = ECDF(values)
            y = ecdf(values)
            plt.step(values, y, where='post', label=month)

        # plt.legend(loc='upper center', ncol=9, fancybox=True, shadow=True)
        # ax.set_xlabel("Normalized steps")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0, 5)

        # fig, ax = plt.subplots(figsize=set_size(fraction=2))
        # ax.boxplot(data.values(), showfliers=False)
        # ax.set_xticklabels(data.keys(), rotation=90)
        # # ax.set_ylim(-0.15, 0.15)
        # # print(data.keys())

        file = "./cloud_analyzing_repo/alibaba_scripts/graphs/time_intervals_by_month.png"
        plt.savefig(file, bbox_inches="tight")



def draw_TimeDelta_ecdf_per_trace(region):
    instList = "./"+region+"_2020_traces/*clean.csv"
    instList = glob.glob(instList)

    for inst in instList:
        #print("proccessing ",inst)
        df = pd.read_csv(inst)    
        df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)
        
        length = df.Timestamp.count()
        if length > 3:          
            df = df.sort_values('TimeDelta')
            data = df['TimeDelta']
            ecdf = ECDF(data)
            y = ecdf(data)
        
            plt.step(data, y, where='post', label=inst)
    
    title = "Time Delta ECDF ("+region+")"
    plt.title(title)
    #plt.xlim(-3,0.5)
    plt.show()
    

def draw_TimeInSecondsModulo3600_per_Timestamp_per_trace(region):
    # instList = "./"+region+"_2020_traces/*clean.csv"
    instList = "./alibaba_work/" + region + "_full_traces/*__vpc.csv"
    instList = glob.glob(instList)

    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)

        df['TimeModolo3600'] = df['TimeInSeconds'] % 3600

        plt.scatter(df.Timestamp, df.TimeModolo3600)
        
    
    title = "TimeInSeconds modulo 3600 per Timestamp for each inst in "+ region
    plt.title(title)
    plt.show()
    
def draw_Time_Modulo_3600_with_NormDeltaPrice_per_trace(region):
    instList = "./"+region+"_2020_traces/*clean.csv"
    instList = glob.glob(instList)
    print(instList)
    
    cm = plt.cm.get_cmap('RdYlBu')
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)    
        df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)
        
        df['TimeModolo3600'] = df['TimeInSeconds'] % 3600
        
        sc = plt.scatter(df.Timestamp, df.TimeModolo3600, c=df.NormPriceDelta, s=35, cmap=cm)
        
    plt.colorbar(sc)
    plt.show()


def draw_TimeInSecondsModulo3600_per_Timestamp_all_regions():
    fig = plt.figure(figsize=(15, 10))
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']

    for region in regions:
        # instList = "./"+region+"_full_traces/*vpc.csv"
        instList = "./alibaba_work/" + region + "_full_traces/*__clean.csv"
        instList = glob.glob(instList)
        # instList = ["./alibaba_work/cn-beijing_full_traces/cn_beijing_k__ecs_t6_c4m1_large__optimized__vpc.csv"]

        timestamp_list = []
        modulo_list = []
        for inst in instList:
            # print("proccessing ", inst)
            df = pd.read_csv(inst)
            df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df = df[df.Timestamp >= "1/1/2020 00:00:00 AM"]
            df['Timestamp'] = df.Timestamp.dt.round("T")
            df = df.sort_values('Timestamp')

            start = pd.to_datetime("2018-01-01 00:00:00.000")
            df['TmpDiff'] = df['Timestamp'] - start
            df['seconds'] = df['TmpDiff'].dt.total_seconds()

            df['FixedTimeModolo3600'] = df['seconds'] % 3600

            # if df.FixedTimeModolo3600.count() > 100:
            #     print(inst)
            timestamp_list.extend(df.Timestamp.tolist())
            modulo_list.extend(df.FixedTimeModolo3600.tolist())

            # plt.scatter(df.Timestamp, df.FixedTimeModolo3600)
        plt.scatter(timestamp_list,modulo_list)

    # title = "TimeInSeconds modulo 3600 per Timestamp for each inst in " + region
    plt.title("All regions 'clean' data")
    # plt.legend()
    plt.ylabel("TimeInSeconds % 3600")
    plt.xlabel("Timestamp")
    plt.show()


def num_changes_full_hour_vs_part_hour():
    # fig = plt.figure(figsize=set_size(width=469.75499))
    fig = plt.figure(tight_layout=True,figsize=set_size())

    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']

    # for region in regions:
        # instList = "./"+region+"_full_traces/*vpc.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*clean_2.csv"
    # instList = "./alibaba_work/*_full_traces_updated/*__vpc.csv"
    instList = glob.glob(instList)

    all_data_list = []
    # instList = ["./alibaba_work/cn-beijing_full_traces/cn_beijing_k__ecs_t6_c4m1_large__optimized__vpc.csv",
    #             "./alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_ic5_large__optimized__vpc__clean.csv"]
    # instList = ["./alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_ic5_large__optimized__vpc__clean.csv"]
    # instList = ["./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_n4_4xlarge__optimized__vpc__clean2.csv"]
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean2.csv']

    for num, inst in enumerate(instList,1):
        print("proccessing ", inst, num)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df['Timestamp'] = df.Timestamp.dt.round("T")
        df = df.sort_values('Timestamp')
        # df.loc[0, 'TimeDelta'] = 0
        # df = df[df['TimeDelta'] == 0]
        # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

        df['month_year'] = df['Timestamp'].dt.to_period('M')

        start = pd.to_datetime("2018-01-01 00:00:00.000")
        df['TmpDiff'] = df['Timestamp'] - start
        df['seconds'] = df['TmpDiff'] / np.timedelta64(1,'s')
        # df['secondsDiff'] = df['seconds'].diff()

        df['TimeModulo3600'] = df['seconds'] % 3600

        grouped = df.groupby('month_year')
        for name, group in grouped:
            all_samples_count = group.TimeModulo3600.count()
            full_hour_samples_count = group.TimeModulo3600[group.TimeModulo3600 == 0.0].count()
            all_data_list.append((str(name), full_hour_samples_count, all_samples_count))

        # print(all_data_list)
    d = {x: [0, 0] for x, _, _ in all_data_list}
    for name, full_count, all_count in all_data_list:
        d[name][0] += full_count
        d[name][1] += all_count

    print(d)
    month_year = []
    # value = []
    print("Month, Percent")
    for key, item in d.items():
        print(key, (item[0] / item[1]))
        # print(item)
        month_year.append((key, (item[0] / item[1])))
        # value.append(item[0]/item[1])

    month_year = sorted(month_year, key=lambda x: x[0])
    print(month_year)
    zip(*month_year)
    plt.scatter(*zip(*month_year))
    # plt.scatter(month_year,value)
    # Output = list(map(tuple, d.items()))
    # print(Output)
    # zip(*Output)

    # plt.scatter(*zip(*Output))
    plt.xlabel(r'Year - Month')
    plt.ylabel(r'Fraction of price changes at round hours')
    plt.xticks(rotation=45)
    plt.yticks()

    # plt.tight_layout()
    # plt.savefig("./cloud_analyzing_repo/alibaba_scripts/graphs/percent_price_changes_per_round_hour_clean_traces.png", bbox_inches = "tight")
    plt.show()


def cleanTraces(region):
    # for region in regions:
    # dirName = "./alibaba_work/" + region + "_full_traces/*.csv"
    dirName = "./alibaba_work/*_full_traces_updated/*_vpc.csv"
    # print(dirName)

    files = glob.glob(dirName)

    num = 0
    for file in files:
        num += 1
        # print("Processing ",file)
        df = pd.read_csv(file)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df['Timestamp'] = df.Timestamp.dt.round("T")
        df = df.sort_values('Timestamp')

        prev_price = df.SpotPrice.iloc[0]
        prev_index = 0

        row_list = [0]
        for index, row in df.iterrows():
            if row.SpotPrice == prev_price:
                if not (prev_index in row_list):
                    row_list.append(prev_index)
                row_list.append(index)
                prev_price = row.SpotPrice
            prev_index = index
        row_list.append(index)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--instType", help='The inst type for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
    
  
    args = parser.parse_args()
    
    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'
    
    if args.instType:
        instType = args.instType
    else:
        instType = 'r6'    
    
    if args.function:
        func = args.function
        if func == 'draw_time_delta_ecdf_per_region':
            print('Running draw_time_delta_ecdf_per_region function...')
            draw_time_delta_ecdf_per_region()
            
        elif func == 'draw_TimeDelta_ecdf_per_trace':
            print('Running draw_TimeDelta_ecdf_per_trace function...')
            draw_TimeDelta_ecdf_per_trace(region)

        elif func == 'draw_time_delta_ecdf_per_month':
            print('Running draw_time_delta_ecdf_per_month function...')
            draw_time_delta_ecdf_per_month()
            
        elif func == 'draw_TimeInSecondsModulo3600_per_Timestamp_per_trace':
            print('Running draw_TimeInSecondsModulo3600_per_Timestamp_per_trace function...')
            draw_TimeInSecondsModulo3600_per_Timestamp_per_trace(region)
            
        elif func == 'draw_Time_Modulo_3600_with_NormDeltaPrice_per_trace':
            print('Running draw_Time_Modulo_3600_with_NormDeltaPrice_per_trace function...')
            draw_Time_Modulo_3600_with_NormDeltaPrice_per_trace(region)

        elif func == 'draw_TimeInSecondsModulo3600_per_Timestamp_all_regions':
            print("Running draw_TimeInSecondsModulo3600_per_Timestamp_all_regions function...")
            draw_TimeInSecondsModulo3600_per_Timestamp_all_regions()

        elif func == 'draw_fraction_of_price_changes_at_round_hours':
            print("Running draw_fraction_of_price_changes_at_round_hours function...")
            num_changes_full_hour_vs_part_hour()

        elif func == 'timeDelta_boxplot_per_month':
            print("Running timeDelta_boxplot_per_month function...")
            timeDelta_boxplot_per_month()

        elif func == 'fix_time_intervals_data':
            print("Running fix_time_intervals_data function...")
            fix_time_intervals_data()

        elif func == 'keep_only_slopes':
            print("Running keep_only_slopes function...")
            keep_only_slopes()

        elif func == 'draw_slope_time_delta_ecdf_per_month':
            print("Running draw_slope_time_delta_ecdf_per_month...")
            draw_slope_time_delta_ecdf_per_month()

        elif func == 'draw_time_delta_ecdf_per_epoch':
            print("Running draw_time_delta_ecdf_per_epoch...")
            draw_time_delta_ecdf_per_epoch()