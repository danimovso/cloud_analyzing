import os.path
from os import path
import glob
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import pandas as pd, numpy as np, seaborn as sns, scipy
from contextlib import closing
import argparse
from statsmodels.distributions.empirical_distribution import ECDF
from collections import Counter


################# Begin helper functions ################

def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier

def round_down(n, decimals=0):
    multiplier = 10 ** decimals
    return np.floor(n * multiplier) / multiplier

################# End helper functions ##################



##########################################################
#####       Adding the peaks data to the traces      #####
##########################################################
def add_peaks_to_traces(region):
    instList = "./"+region+"_traces/*__optimized__vpc_updated.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        df = pd.read_csv(inst)

        df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)
        
        df['min'] = df.SpotPrice[(df.SpotPrice.shift(1) > df.SpotPrice) & (df.SpotPrice.shift(-1) > df.SpotPrice)]
        df['max'] = df.SpotPrice[(df.SpotPrice.shift(1) < df.SpotPrice) & (df.SpotPrice.shift(-1) < df.SpotPrice)]
        
        df['half_min'] = df.SpotPrice[((df.SpotPrice.shift(1) == df.SpotPrice) & (df.SpotPrice.shift(-1) > df.SpotPrice)) |
                                      ((df.SpotPrice.shift(1) > df.SpotPrice) & (df.SpotPrice.shift(-1) == df.SpotPrice))]
        df['half_max'] = df.SpotPrice[((df.SpotPrice.shift(1) == df.SpotPrice) & (df.SpotPrice.shift(-1) < df.SpotPrice)) |
                                      ((df.SpotPrice.shift(1) < df.SpotPrice) & (df.SpotPrice.shift(-1) == df.SpotPrice))]
        
        length = df.Timestamp.count()
        info_list = []
        for index in range(length):
            if index == 0:
                info_list.append(df.SpotPrice.iloc[index])
                #print("first ", df.SpotPrice.iloc[index])
            elif index == length-1:
                info_list.append(df.SpotPrice.iloc[index])
                #print("last ", df.SpotPrice.iloc[index])
            else:
                if pd.isna(df['min'].iloc[index]) and pd.isna(df['max'].iloc[index]) and pd.isna(df['half_min'].iloc[index]) and pd.isna(df['half_max'].iloc[index]):
                    #print("no value")
                    info_list.append(np.nan)
                if not pd.isna(df['min'].iloc[index]):
                    info_list.append(df['min'].iloc[index])
                    #print("min ", df['min'].iloc[index])
                elif not pd.isna(df['max'].iloc[index]):
                    info_list.append(df['max'].iloc[index])
                    #print("max ", df['max'].iloc[index])
                elif not pd.isna(df['half_min'].iloc[index]):
                    info_list.append(df['half_min'].iloc[index])
                    #print("half_min ", df['half_min'].iloc[index])
                elif not pd.isna(df['half_max'].iloc[index]):
                    info_list.append(df['half_max'].iloc[index])
                    #print("half_max ", df['half_max'].iloc[index])
        
        df['info'] = info_list

        name,_ = inst.split("__optimized__vpc_updated.csv")
        name = name + "_with_tags.csv"
        df.to_csv(name)
        print("created file: ",name)
        print("-------------------------------------------")
        

##############################################################################
#####             Adding the description column the traces               #####
#####         The description column tags is the sample represents       ##### 
#####             an increase/decrease/const in the SpotPrice            #####
##############################################################################
def add_description_col(region):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)

        length = df.Timestamp.count()
        info_list = []
        price = df['info'].iloc[0]
        for index in range(length):
            if pd.isna(df['info'].iloc[index]):
                if df.SpotPrice.iloc[index] < price:
                    info_list.append('decrease')
                elif df.SpotPrice.iloc[index] > price:
                    info_list.append('increase')
                else:
                    info_list.append('const')
            else:
                if df['info'].iloc[index] < price:
                    info_list.append('decrease')
                elif df['info'].iloc[index] > price:
                    info_list.append('increase')
                else:
                    info_list.append('const')
                price = df['info'].iloc[index]

        df['Description'] = info_list

        df['TimeInHours'] = df['TimeInSeconds']/3600
        df['Slope'] = df['SpotPrice'].diff() / (df['TimeInHours'].diff())

        df.to_csv(inst)

##############################################################################
#####                 Add 'DescriptionNoKnee' column.                    #####
#####      This column marks decrease/increase/const without 'knees'     #####
##############################################################################
def add_description_without_knee_col(region):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True,inplace=True)

        length = df.Timestamp.count()
        if length > 3:
            info_list = []
            for index in range(length):
                if not pd.isna(df['half_min'].iloc[index-1]) and not pd.isna(df['half_max'].iloc[index]):
                    info_list.append('decrease')
                elif not pd.isna(df['half_min'].iloc[index-2]) and (df.Description.iloc[index-1] == 'const') and \
                     not pd.isna(df['half_max'].iloc[index]):
                    info_list.append('decrease')
                elif not pd.isna(df['half_min'].iloc[index-1]) and (df.Description.iloc[index] == 'const') and \
                     (index+1 < length) and not pd.isna(df['half_max'].iloc[index+1]):
                    info_list.append('decrease')
                elif not pd.isna(df['half_max'].iloc[index-1]) and not pd.isna(df['half_min'].iloc[index]):
                    info_list.append('increase')
                elif not pd.isna(df['half_max'].iloc[index-2]) and (df.Description.iloc[index-1] == 'const') and \
                     not pd.isna(df['half_min'].iloc[index]):
                    info_list.append('increase')
                elif not pd.isna(df['half_max'].iloc[index-1]) and (df.Description.iloc[index] == 'const') and \
                     (index+1 < length) and not pd.isna(df['half_min'].iloc[index+1]):
                    info_list.append('increase')
                else:
                    info_list.append(df.Description.iloc[index])
                    
            df['DescriptionNoKnee'] = info_list

            df.to_csv(inst) 

##############################################################################
#####                 Add 'TimeDelta' and 'PriceDelta' columns.          #####
##############################################################################
def add_time_and_price_delta_cols(region):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        
        df['TimeDelta'] = df['TimeInSeconds'].diff()
        df['PriceDelta'] = df['SpotPrice'].diff()
        
        df.to_csv(inst)

##############################################################################
#####                 Add Normalized price and delta columns.            #####
##############################################################################
def add_normalized_price_and_delta_cols(region, epsilon1, epsilon2):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df = df[df.columns.drop(['FracOPC','RoundFOPC','FracToCompare','FixedNormPriceDelta','DecRoundFOPC','DecFracToCompare','DecFixedNormPriceDelta','IncFracOPC','IncRoundFOPC','IncFracToCompare','IncFixedNormPriceDelta'])]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        
        df['OPCents'] = (df.OriginPrice * 100)  # OPCents - Origin Price in Cents
        df['NormPrice'] = df.SpotPrice / df.OriginPrice
        df['NormPriceDelta'] = (df.PriceDelta / df.OriginPrice)
        
        # Fix the values for the decreasing prices
        df['DecFracOPC'] = (df.OPCents * epsilon1)    # DecFracOPC - Decrease Fraction of Origin Price in Cents
        
        # The following lines use round() which rounds the number to the closese decimal value
        df['DecRoundFOPC1'] = round(df.DecFracOPC,1)             # Round Fraction of Origin Price to tens of Cents
        df['DecFracToCompare1'] = (df.DecRoundFOPC1/df.OPCents)  # uses the value from the regular round().
        df['DecFixedNormPriceDelta1'] = (df.PriceDelta / df.OriginPrice)
        
        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare1) < 0.00001),'DecFixedNormPriceDelta1'] = epsilon1
        
        # The following lines use round_up() which rounds up the number
        df['DecRoundFOPC2'] = round_up(df.DecFracOPC,1)          # Round Fraction of Origin Price to tens of Cents
        df['DecFracToCompare2'] = (df.DecRoundFOPC2/df.OPCents)  # uses the value from the regular round_up()
        df['DecFixedNormPriceDelta2'] = (df.PriceDelta / df.OriginPrice)
        
        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare2) < 0.00001),'DecFixedNormPriceDelta2'] = epsilon1
        
        # The following lines use round_down() which rounds down the number
        df['DecRoundFOPC3'] = round_down(df.DecFracOPC,1)          # Round Fraction of Origin Price to tens of Cents
        df['DecFracToCompare3'] = (df.DecRoundFOPC3/df.OPCents)  # uses the value from the regular round_down()
        df['DecFixedNormPriceDelta3'] = (df.PriceDelta / df.OriginPrice)
        
        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare3) < 0.00001),'DecFixedNormPriceDelta3'] = epsilon1
        
        # Fix the values for the increasing prices
        df['IncFracOPC'] = (df.OPCents * epsilon2)    
        df['IncRoundFOPC'] = round(df.IncFracOPC,1)   
        df['IncFracToCompare'] = (df.IncRoundFOPC/df.OPCents)
        df['IncFixedNormPriceDelta'] = (df.PriceDelta / df.OriginPrice)
        
        df.loc[(abs(df.NormPriceDelta - df.IncFracToCompare) < 0.00001),'IncFixedNormPriceDelta'] = epsilon2
        
        df.to_csv(inst)



##############################################################################
#####  draw the most common decrease slope value per trace per week      #####
##############################################################################
def draw_find_epoch(region):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df[df['Description'] == 'decrease']
        
        if df.empty != True:
            slope_list = []
            date_list = []
            grouped = df.groupby(pd.Grouper(key='Timestamp',freq='W'))
            for name,group in grouped:
                if not group.Slope.value_counts().dropna().empty:
                    date_list.append(str(name))
                    slope_list.append(group.Slope.value_counts().idxmax())
                    #print(name,",", group.slope.value_counts().idxmax())
                    #print(group.slope.value_counts().idxmax(), group.slope.value_counts().max(), group.slope.count())
                    #print("--------------------------------------------------")
            #print(slope_list)
            #print(date_list)

            df2 = pd.DataFrame(list(zip(date_list, slope_list)), columns =['Timestamp', 'Slope']) 
            df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
            
            plt.scatter(df2.Timestamp,df2.Slope)
    
    plt.ylim(-3,0.5)
    plt.show()

##############################################################################
#####  Find the week where the most common decrease slope value changes  #####
##############################################################################
def find_epoch(region):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    dates = ['2019-07-07 00:00:00','2019-07-14 00:00:00','2019-07-21 00:00:00','2019-07-28 00:00:00','2019-08-04 00:00:00','2019-08-11 00:00:00','2019-08-18 00:00:00','2019-08-25 00:00:00',
             '2019-09-01 00:00:00','2019-09-08 00:00:00','2019-09-15 00:00:00','2019-09-22 00:00:00','2019-09-29 00:00:00','2019-10-06 00:00:00','2019-10-13 00:00:00','2019-10-20 00:00:00','2019-10-27 00:00:00']
    
    slope_list = []
    date_list = []
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df[df['Description'] == 'increase']
        
        if df.empty != True:
            grouped = df.groupby(pd.Grouper(key='Timestamp',freq='W'))
            for name,group in grouped:
                if str(name) in dates:
                    if not group.Slope.value_counts().dropna().empty:
                        date_list.append(str(name))
                        slope_list.append(group.Slope.value_counts().idxmax())
                    

    df2 = pd.DataFrame(list(zip(date_list, slope_list)), columns =['Timestamp', 'Slope'])
    
    fig = plt.figure(figsize=(15, 12))
    
    df3 = df2.groupby(['Timestamp']).var()
    #print(df3)
    plt.scatter(df3.index,df3.Slope, label = 'Variance')
    
    df3 = df2.groupby(['Timestamp']).mean()
    #print(df3)
    plt.scatter(df3.index,df3.Slope, label = 'Mean')
    
    plt.xticks(rotation=45)
    plt.legend()
    plt.title(region)
    name = "./graphs/epoch_search/"+region+"_increase_epoch_search.png"
    plt.savefig(name)
    #plt.show()


##################################################################################
#####  Calculate the ratio of 'decreasing' samples VS 'increasing' samples   #####
##################################################################################            
def decrease_vs_increase_count(regions):
    ratio_list = []
    no_inc_num = 0
    no_dec_num = 0
    const_num = 0
    num_traces = 0

    fig = plt.figure(figsize=(15, 12))
    
    for region in regions:
        print("proccessing ",region)
        instList = "./"+region+"_traces/*_with_tags.csv"
        instList = glob.glob(instList)
        
        inc_list = []
        dec_list = []
        
        for inst in instList:
            num_traces = num_traces + 1
            print("proccessing ",inst)
            df = pd.read_csv(inst)
            
            length = df.Timestamp.count()
            if length > 3:
                inc_count = df.DescriptionNoKnee[df.DescriptionNoKnee == 'increase'].count()
                dec_count = df.DescriptionNoKnee[df.DescriptionNoKnee == 'decrease'].count()
                
                
                #if (inc_count != 0) and (dec_count != 0):
                #    ratio = inc_count/dec_count
                #    ratio_list.append(ratio)
                #elif (inc_count == 0) and (dec_count == 0):
                #    const_num = const_num + 1
                #elif (inc_count == 0) and (dec_count != 0):
                #    no_inc_num = no_inc_num + 1
                #else:
                #    no_dec_num = no_dec_num + 1
                #ratio_list.append(1e77)
                #print("inc:", inc_count, "dec: ",dec_count)
                #print("nan")
                if (inc_count != 0) or (dec_count != 0):
                    inc_list.append(inc_count)
                    dec_list.append(dec_count)
        
        plt.scatter(dec_list, inc_list)
    
    #print("Number of all traces:", num_traces)
    #print("Number of constant traces:", const_num)
    #print("Number of traces that have no increase points:", no_inc_num)
    #print("Number of traces that have no decrease points:", no_dec_num)
    #
    #ratio_list.sort()
    #ecdf = ECDF(ratio_list)
    #y = ecdf(ratio_list)
    #
    #plt.step(ratio_list,y,label=region)
    
    plt.xlabel("Number of 'decreasing' points")
    plt.ylabel("Number of 'increasing' points")
    plt.title("Ratio increase/decrease points")
    title = "./graphs/epoch_search/ratio_increase_decrease.png"
    plt.savefig(title)
    #plt.show()
    
    
def draw_slope_ecdf_by_epoch(region):
    
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    slope_list = []
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        
        length = df.Timestamp.count()
        if length > 3:
            df = df[df.TimeInSeconds < 52531200]  # 52531200 is the TimeInSeconds for "9/1/2019 12:00:00 AM"
            df = df[df.DescriptionNoKnee == 'decrease']
            
            if df.empty != True:
                slope_list.extend(df.Slope.to_numpy())
                #print(slope_list)
    
    
    slope_list.sort()
    ecdf = ECDF(slope_list)
    y = ecdf(slope_list)

    plt.step(slope_list, y)
    
    title = "First epoch (before 01-09-2019): Decreasing slop ECDF ("+region+")"
    plt.title(title)
    #plt.show()

 
def draw_PriceDelta_ecdf_merged_for_region(region,slope):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    print(len(instList))
    
    norm_priceDelta_list = []
    fixednorm_priceDelta_list1 = []
    fixednorm_priceDelta_list2 = []
    fixednorm_priceDelta_list3 = []
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        
        length = df.Timestamp.count()
        if length > 3:
            df = df[df.TimeInSeconds < 52531200]  # 52531200 is the TimeInSeconds for "9/1/2019 12:00:00 AM"
            df = df[df.Description == slope]
            
            if df.empty != True:
                norm_priceDelta_list.extend(df.NormPriceDelta.to_numpy())
                if slope == 'decrease':
                    #print(Counter(df.DecFixedNormPriceDelta.to_numpy()))
                    #print("======================================================================================")
                    fixednorm_priceDelta_list1.extend(df.DecFixedNormPriceDelta1.to_numpy())
                    fixednorm_priceDelta_list2.extend(df.DecFixedNormPriceDelta2.to_numpy())
                    fixednorm_priceDelta_list3.extend(df.DecFixedNormPriceDelta3.to_numpy())
                else:
                    fixednorm_priceDelta_list1.extend(df.IncFixedNormPriceDelta.to_numpy())
    
    #print("Before fixing error:")
    #print(Counter(norm_priceDelta_list))
    #print("===================================================================")
    #print("Before fixing error:")
    #print(Counter(fixednorm_priceDelta_list1))
    
    
    fig = plt.figure(figsize=(15, 10))
    
    fixednorm_priceDelta_list1.sort()
    ecdf = ECDF(fixednorm_priceDelta_list1)
    y = ecdf(fixednorm_priceDelta_list1)

    plt.step(fixednorm_priceDelta_list1, y, 'o-', c='blue', where='post', label='Fixed Normalized Price Delta (round closest)')
    
    fixednorm_priceDelta_list2.sort()
    ecdf = ECDF(fixednorm_priceDelta_list2)
    y = ecdf(fixednorm_priceDelta_list2)

    plt.step(fixednorm_priceDelta_list2, y, '^-', c='green', where='post', label='Fixed Normalized Price Delta (round up)')
    
    fixednorm_priceDelta_list3.sort()
    ecdf = ECDF(fixednorm_priceDelta_list3)
    y = ecdf(fixednorm_priceDelta_list3)

    plt.step(fixednorm_priceDelta_list3, y, 'v-', c='red', where='post', label='Fixed Normalized Price Delta (round down)')
    
    #print(Counter(norm_priceDelta_list))
    norm_priceDelta_list.sort()
    ecdf = ECDF(norm_priceDelta_list)
    y = ecdf(norm_priceDelta_list)

    plt.step(norm_priceDelta_list, y, c='black', where='post', label='Normalized Price Delta')

    
    title = slope+" Price delta ECDF ("+region+")"
    plt.title(title)
    plt.legend()
    plt.xlim(-0.03,0.0)
    #plt.show()
    file = "./graphs/epoch_search/fixed_ecdf_decrease_norm_price/First_epoch_decrease_price_delta_ecdf_"+region+".png"
    plt.savefig(file)

def draw_PriceDelta_ecdf_per_trace(region):
    instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    
    for inst in instList:
        print("proccessing ",inst)
        df = pd.read_csv(inst)
        
        length = df.Timestamp.count()
        if length > 3:
            df = df[df.TimeInSeconds < 52531200]  # 52531200 is the TimeInSeconds for "9/1/2019 12:00:00 AM"
            df = df[df.Description == 'decrease']
            
            if df.empty != True:
                df = df.sort_values('PriceDelta')
                data = df['PriceDelta']
                ecdf = ECDF(data)
                y = ecdf(data)

                plt.step(data, y)
    
    title = "Price delta ECDF ("+region+")"
    plt.title(title)
    plt.xlim(-3,0.5)
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    #parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
  
    args = parser.parse_args()
    
    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'
        
    if args.function:
        func = args.function
        if func == 'add_peaks_to_traces':
            print("Adding peaks to all traces....")
            add_peaks_to_traces(region)
            print("Finished adding preaks to all traces")
        
        elif func == 'add_description_col':
            print("Adding description column to all traces....")
            add_description_col(region)
            print("Adding descriptionNoKnee column to all traces....")
            add_description_without_knee_col(region)
            print("Finished adding description column to all traces")
        
        elif func == 'find_epoch':
            print("Running find_epoch function...")
            find_epoch(region)
            
        elif func == 'draw_slope_ecdf_by_epoch':
            print("Running draw_slope_ecdf_by_epoch function...")
            draw_slope_ecdf_by_epoch(region)

        elif func == 'add_time_and_price_delta_cols':
            print("Running add_time_and_price_delta_cols function...")
            add_time_and_price_delta_cols(region)
        
        elif func == 'draw_PriceDelta_ecdf_per_trace':
            print("Running draw_PriceDelta_ecdf_per_trace function...")
            draw_PriceDelta_ecdf_per_trace(region)
        
        elif func == 'draw_PriceDelta_ecdf_merged_for_region':
            print("Running draw_PriceDelta_ecdf_merged_for_region function...")
            draw_PriceDelta_ecdf_merged_for_region(region,'decrease')
        
        elif func == 'add_normalized_price_and_delta_cols':
            print("Running add_normalized_price_and_delta_cols function...")
            add_normalized_price_and_delta_cols(region, -0.02, 0.03)
            
        elif func == 'decrease_vs_increase_count':
            print("Running decrease_vs_increase_count function...")
            regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
                    'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
                    'eu-west-1','me-east-1','us-east-1','us-west-1']
            decrease_vs_increase_count(regions)
    
    print("Finish")

