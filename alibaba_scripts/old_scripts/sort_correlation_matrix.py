import pandas as pd
import numpy as np
# %matplotlib
import matplotlib.pyplot as plt
import itertools

# file = "../../Alibaba_log_files/Correlation_tables/all_inst_July_weighted_val_matrix.csv"
# file3 = "../../Alibaba_log_files/Correlation_tables/best_all_instances_July_2020_correlation_matrix.txt"

# file = "../../Alibaba_log_files/Correlation_tables/best_all_instances_weighted_august_2020_corr.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_weighted_august_2020_corr_with_dendrogram_labels_threshold_02_F10_ordered.csv"
# file3 = "../../Alibaba_log_files/Correlation_tables/best_ordered_august_mat_after_using_w_algorithem.txt"

# file = "../../Alibaba_log_files/Correlation_tables/all_instances_September_2020_corr_matrix_ordered.csv"
# file3 = "../../Alibaba_log_files/Correlation_tables/best_all_instances_September_2020_correlation_matrix.txt"

file = "../../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_corr_weighted_corr_matrix.csv"
file3 = "../../Alibaba_log_files/Correlation_tables/best_all_instances_august_2020.txt"


# file2 = "../../Alibaba_log_files/Correlation_tables/first_and_last_activity_non_static_insts_2.csv"

# file = "../../Alibaba_log_files/Correlation_tables/cropped_august_2020_corr_weighted_agglomerative_ordered.csv"
# file3 = "../../Alibaba_log_files/Correlation_tables/cropped_august_2020_red_squares_best_order.txt"



# plt.style.use('grayscale')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 12,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10
    }

plt.rcParams.update(tex_fonts)


def set_size(width=441.01772 , fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


################################################
# Plot the matrix heatmap.                     #
# If you want to add the values to each cell   #
# set val_flag == 1.                           #
################################################
def show(mat, title, num, index, val_flag=0):
    mat = np.around(mat, 1)
    # fig, ax = plt.subplots()
    ax = fig.add_subplot(1, num, index)
    im = ax.imshow(mat, cmap='jet')
    if val_flag:
        for i in range(len(mat)):
            for j in range(len(mat)):
                text = ax.text(j, i, mat[i, j],
                               ha="center", va="center", color="w")
    # ax.set_title(title)
    # im.set_clim(0, 1.0)
    plt.colorbar(im)


################################################
# Plot the heatmap directly from the the       #
# dataframe and add the names of the insts     #
# as the axes of the image.                    #
################################################
def show_df(df, title, num, index):
    ax = fig.add_subplot(1, num, index)
    ax.imshow(df.values, cmap='jet')
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(df.columns.tolist())))
    ax.set_yticks(np.arange(len(df.index.values)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(df.columns)
    ax.set_yticklabels(df.index.values)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.set_title(title)


def calc_squares(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    min_red = max(min_red, 0.01)
    for i in range(1, size - 1):
        if (abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] * mat[i - 1][i] > 0 and mat[i][
            i + 1] > min_red):
            #       if (abs(mat[i][i+1]) >min_red and mat[i][i+1] > min_red):
            temp.append(i)  # continue the square
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)  # why does this repeat in both cases?
                temp = [i]  # start a new square

    if temp:  # i think this case can happen regardless
        temp.append(size - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([size - 1])

    # print(squares)
    return squares


def get_insts_in_squares(df, squares, file_name):
    f = open(file_name, "w")
    f.write(str(df.columns.tolist()) + "\n")
    f.write("==================================================\n")
    f.write("==================================================\n")
    for index, s in enumerate(squares):
        f.write("Square number: " + str(index) + " ,Len square: " + str(len(s)) + " ,Row numbers: [" + str(
            s[0]) + "," + str(s[-1]) + "]\n")
        inst_list = []
        for i in s:
            name = df.columns.tolist()[i]
            reg, _, _ = name.split("__")
            reg = reg.replace('_', '-')
            reg = reg[:-1]
            if reg[-1] == '-':
                reg = reg[:-1]
            inst = "./alibaba_work/" + reg + "_2020_traces/" + name + "__vpc__clean.csv"
            inst_list.append(inst)
        f.write(str(inst_list) + "\n")
        # rows = [list(df.index.values)[i] for i in s]
        # print(rows)
        f.write("==================================================\n")
        # f.write(rows)

    f.close()


def calc_daig(mat):
    return np.trace(mat, offset=1)


def calc_red_distance(mat):
    val = 0.0
    size = len(mat)
    for i in range(size):
        for j in range(i + 1, size):
            if (mat[i][j] > 0.0):
                val += mat[i][j] * (j - i)  # j >i always
    return val / size


def sort_matrix_by_activity(df_1, df_2):
    # Merge the two files to keep only the instances that were not static for the entire period
    # (and the appear in both files).
    result = pd.merge(df_1, df_2, how='inner', on='inst')

    # Keep in df2 only the information of the instances that appear in both files
    df3 = result[['inst', 'start_timestamp', 'first_activity', 'last_activity', 'last_timestamp']].copy()

    # Clean and rearrange the correlation matrix to make sure that it only has the information kept in both files
    result.set_index("inst", inplace=True)
    result = result.reindex(df3.inst.tolist(), axis=1)

    # View the correlation matrix before we rearrange it by time.
    show(result.values, "Best ordered red squares", 3, 1)

    # Get the order of the instances based on the last activity time
    # (we will arrange the rows of the matrix by this data).
    df3['last_activity'] = pd.to_datetime(df3['last_activity'])
    df3 = df3.sort_values('last_activity')
    rowList = df3.inst.tolist()

    result = result.reindex(rowList, axis=1)
    result = result.reindex(rowList, axis=0)
    show(result.values, "Order by last activity", 3, 2)

    # Get the order of the instances based on the first activity time
    # (we will arrange the cols of the matrix by this data).
    df3['first_activity'] = pd.to_datetime(df3['first_activity'])
    df3 = df3.sort_values('first_activity')
    colList = df3.inst.tolist()

    # Rearrange the correlation matrix based on the first and last activity time.
    result = result.reindex(colList, axis=1)
    result = result.reindex(rowList, axis=0)

    # Draw the rearranges correlation matrix
    show(result.values, "Order by first (columns) and last (rows) activity", 3, 3)


def sort_matrix_by_hand(df):
    df.set_index("inst", inplace=True)
    mat = df.values

    # show(mat, 'Before: ' + str(calc_daig(df.values)) + " " + str(calc_red_distance(df.values)), 2, 1)

    size = len(mat)
    col_order = np.arange(0, size).tolist()

    tmp_list = col_order[154:207]
    del col_order[154:207]
    col_order[715:715] = tmp_list

    tmp_list = col_order[3756:4341]
    del col_order[3756:4341]
    col_order[3374:3374] = tmp_list

    # tmp_list = col_order[1717:2312]
    # del col_order[1717:2312]
    # col_order[4431:4431] = tmp_list

    columns = [df.columns.tolist()[i] for i in col_order]
    rows = [list(df.index.values)[i] for i in col_order]
    df = df.reindex(columns, axis=1)
    df = df.reindex(rows, axis=0)

    show(df.values, 'After: ' + str(calc_daig(df.values)) + " " + str(calc_red_distance(df.values)), 1, 1)

    # col_order = list(itertools.chain.from_iterable(squares))
    # mat = mat[col_order][:, col_order]
    # return col_order, squares, mat
    #
    # tmp_list = squares[max_index_c2:max_index_c3 + 1]
    # del squares[max_index_c2:max_index_c3 + 1]
    # squares[i1 + 1:i1 + 1] = tmp_list


# Read the correlation matrix and remove unnamed columns
df1 = pd.read_csv(file)
df1 = df1[df1.columns.drop(list(df1.filter(regex='Unnamed:')))]
df1 = df1.dropna()
print(df1.head(1))

# Read the first and last activity csv and remove unnamed columns
# df2 = pd.read_csv(file2)
# df2 = df2[df2.columns.drop(list(df2.filter(regex='Unnamed:')))]
# df2 = df2.dropna()
# print(df2.head(5))

my_file = open(file3, "r")
content = my_file.read()
content_list = content.split(",")
# print(content_list)
my_file.close()
# print(content_list)

fig = plt.figure(figsize=set_size())

df1.set_index("inst", inplace=True)
show(df1.values, "Before", 1, 1)
# show_df(df1, "best", 1, 1)
print(len(df1.index))
df1 = df1.reindex(content_list, axis=1)
df1 = df1.reindex(content_list, axis=0)
print(len(df1.index))
#
fig = plt.figure(figsize=set_size())
show(df1.values, "after", 1, 1)

# to_delete = [content_list[x] for x in range(1407,3332)]
# df1 = df1.drop(to_delete, axis=1)
# df1 = df1.drop(to_delete, axis=0)
# show(df1.values, "last", 3, 3)
#
df1.to_csv("../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_corr_weighted_red_square.csv")

# sort_matrix_by_hand(df1)
#
# squares = calc_squares(df1.values, len(df1.values), 0.95)
# file_name = "../../Alibaba_log_files/Correlation_tables/all_inst_July_2020_best_mat_map_machines_to_red_square.txt"
# get_insts_in_squares(df1, squares, file_name)

# sort_matrix_by_activity(df1, df2)
# sort_matrix_by_hand(df1)

fig.tight_layout()
plt.show()
