import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
import bisect
from numpy import linalg as LA

# plt.style.use('grayscale')
# plt.style.use('seaborn-colorblind')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 12,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10
    }

plt.rcParams.update(tex_fonts)

def set_size(width=441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


# ****************************************************** #
#   This function draws a matrix from a given df         #
# ****************************************************** #
def show_df(df, title, row, col, index):
    ax = fig.add_subplot(row, col, index)
    im = ax.imshow(np.around(df.values, 4), cmap='turbo')
    # # We want to show all ticks...
    # ax.set_xticks(np.arange(len(df.columns.tolist())))
    # ax.set_yticks(np.arange(len(df.columns.tolist())))
    # # ... and label them with the respective list entries
    # ax.set_xticklabels(df.columns)
    # ax.set_yticklabels(df.columns)
    # # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    ax.set_title(title)
    # im.set_clim(0, 1.0)
    plt.colorbar(im, shrink=0.5)


# ****************************************************** #
#   This function draws the 4 transition matrices        #
#   that are in mat                                      #
# ****************************************************** #
def show_transition_matrices(mat, t, price_list):
    df2 = pd.DataFrame(mat[0], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Increase"), 2, 2, 1)
    # show_df(df2, "Increase", 2, 2, 1)

    df2 = pd.DataFrame(mat[1], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Increase - stay"), 2, 2, 2)
    # show_df(df2, "Increase - stay", 2, 2, 2)

    df2 = pd.DataFrame(mat[2], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Decrease - stay"), 2, 2, 3)
    # show_df(df2, "Decrease - stay", 2, 2, 3)

    df2 = pd.DataFrame(mat[3], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Decrease"), 2, 2, 4)
    # show_df(df2, "Decrease", 2, 2, 4)


# ****************************************************** #
#   This function draws the 4 transition matrices        #
#   that are in mat                                      #
# ****************************************************** #
def show_transition_matrices_thesis(mat):
    # ax = fig.add_subplot(2, 2, 1)
    im = axs[0,0].imshow(np.around(mat[0], 4), cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01])
    axs[0,0].set_title("Increase")

    # ax = fig.add_subplot(2, 2, 2)
    im = axs[0,1].imshow(np.around(mat[1], 4), cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01])
    axs[0,1].set_title("Increase - stay")

    # ax = fig.add_subplot(2, 2, 3)
    im = axs[1,0].imshow(np.around(mat[2], 4), cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01])
    axs[1,0].set_title("Decrease - stay")

    # ax = fig.add_subplot(2, 2, 4)
    im = axs[1,1].imshow(np.around(mat[3], 4), cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01])
    axs[1,1].set_title("Decrease")

    # im.set_clim(0, 1.0)
    fig.colorbar(im, ax=axs[:, 1], location='right', shrink=0.8)


# ****************************************************** #
#   This function draws the trace defined by df          #
# ****************************************************** #
# def show_trace(df, title):
#     df['Timestamp'] = pd.to_datetime(df['Timestamp'])
#     df = df.sort_values('Timestamp')
#     df = df[(df.Timestamp >= pd.to_datetime('9/1/2019 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('11/1/2020 00:00:00 AM'))]
#
#     plt.plot(df.Timestamp, df.NormPrice, '.:')
#     plt.title(title)


# ****************************************************** #
#   This function randomly chooses a value from a list   #
#   with given weights                                   #
# ****************************************************** #
def choice(prices, weights, flag=1):
    if flag:
        assert len(prices) == len(weights)
        cdf_vals = np.cumsum(weights)
        x = random.random()
        idx = bisect.bisect_left(cdf_vals, x)
        if idx == len(prices):
            idx -= 1
        return prices[idx]
    else:
        next_price = random.choices(prices, weights=weights, k=1)
        # print(next_price[0])
        return next_price[0]


# ****************************************************** #
#   This function normalizes all 4 matrices together     #
# ****************************************************** #
# def normalize_transition_mat(mat):
#     for p in [0, 1, 2, 3]:
#         sum_mat = np.sum(mat[p])
#         for i in range(len(mat[p])):
#             for j in range(len(mat[p])):
#                 mat[p][i][j] = mat[p][i][j] / sum_mat
#
#     return mat


# ****************************************************** #
#   This function normalizes the rows of the given       #
#   matrix. Each cell is normalized by the sum of its    #
#   row.                                                 #
# ****************************************************** #
def normalize_transition_mat_rows(mat):
    all_sums = []
    for p in [0, 1, 2, 3]:
        sum_list = np.sum(mat[p], axis=1)
        all_sums.append(sum_list)
        for i in range(len(mat[p])):
            sum_row = sum_list[i]
            if sum_row != 0:
                for j in range(len(mat[p])):
                    mat[p][i][j] = mat[p][i][j] / sum_row

    return mat, all_sums


# def calc_folded_pdfs(mat, all_sums):
#     size_mat = len(mat[0])
#     inc_data = []
#     dec_data = []
#     inc_s_data = []
#     dec_s_data = []
#
#     for p in [0, 1, 2, 3]:
#         for row in range(0, size_mat):
#             for col in range(0, size_mat):
#                 if p == 0:
#                     inc_data.append(((col - row) / 100, mat[p][row][col], row))
#                 elif p == 1:
#                     inc_s_data.append(((col - row) / 100, mat[p][row][col], row))
#                 elif p == 2:
#                     dec_s_data.append(((col - row) / 100, mat[p][row][col], row))
#                 else:
#                     dec_data.append(((col - row) / 100, mat[p][row][col], row))
#
#     d = {x: [0.0, 0.0] for x, _, _ in inc_data}
#     for price, val, row in inc_data:
#         d[price][0] += (val * all_sums[0][row])
#         d[price][1] += all_sums[0][row]
#         # d[price][0] += val
#         # d[price][1] += 1
#
#     new_d = {x: 0.0 for x in d.keys()}
#     for price in d.keys():
#         # print(d[price][0])
#         if d[price][0] != 0.0:
#             new_d[price] = (d[price][0] / d[price][1])
#             # print(new_d[price])
#
#     # inc_data = new_d.items()
#     inc_data = list(map(tuple, new_d.items()))
#     inc_data = sorted(inc_data, key=lambda x: x[0])
#
#     d = {x: [0, 0] for x, _, _ in inc_s_data}
#     for price, val, row in inc_s_data:
#         d[price][0] += (val * all_sums[1][row])
#         d[price][1] += all_sums[1][row]
#         # d[price][0] += val
#         # d[price][1] += 1
#
#     new_d = {x: 0 for x in d.keys()}
#     for price in d.keys():
#         if d[price][0] == 0.0:
#             new_d[price] = 0.0
#         else:
#             new_d[price] = (d[price][0] / d[price][1])
#
#     inc_s_data = list(map(tuple, new_d.items()))
#     inc_s_data = sorted(inc_s_data, key=lambda x: x[0])
#
#     d = {x: [0, 0] for x, _, _ in dec_s_data}
#     for price, val, row in dec_s_data:
#         d[price][0] += (val * all_sums[2][row])
#         d[price][1] += all_sums[2][row]
#         # d[price][0] += val
#         # d[price][1] += 1
#
#     new_d = {x: 0 for x in d.keys()}
#     for price in d.keys():
#         if d[price][0] == 0.0:
#             new_d[price] = 0.0
#         else:
#             new_d[price] = (d[price][0] / d[price][1])
#
#     dec_s_data = list(map(tuple, new_d.items()))
#     dec_s_data = sorted(dec_s_data, key=lambda x: x[0])
#
#     d = {x: [0, 0] for x, _, _ in dec_data}
#     for price, val, row in dec_data:
#         d[price][0] += (val * all_sums[3][row])
#         d[price][1] += all_sums[3][row]
#         # d[price][0] += val
#         # d[price][1] += 1
#
#     new_d = {x: 0 for x in d.keys()}
#     for price in d.keys():
#         if d[price][0] == 0.0:
#             new_d[price] = 0.0
#         else:
#             new_d[price] = (d[price][0] / d[price][1])
#
#     dec_data = list(map(tuple, new_d.items()))
#     dec_data = sorted(dec_data, key=lambda x: x[0])
#
#     return inc_data, inc_s_data, dec_s_data, dec_data
#
#
# def calc_folded_cdfs(inc_data, inc_s_data, dec_s_data, dec_data):
#     ax = fig.add_subplot(2, 2, 1)
#
#     inc_prices, inc_weights = zip(*inc_data)
#     cdf = np.cumsum(inc_weights)
#     # print(cdf)
#
#     # plotting PDF and CDF
#     ax.plot(inc_prices, inc_weights, color="red", label="PDF")
#     ax.plot(inc_prices, cdf, label="CDF")
#     ax.legend()
#     ax.set_title("Increase")
#
#     ax = fig.add_subplot(2, 2, 2)
#
#     inc_s_prices, inc_s_weights = zip(*inc_s_data)
#     cdf = np.cumsum(inc_s_weights)
#
#     # plotting PDF and CDF
#     ax.plot(inc_s_prices, inc_s_weights, color="red", label="PDF")
#     ax.plot(inc_s_prices, cdf, label="CDF")
#     ax.legend()
#     ax.set_title("Increase - Stay")
#
#     ax = fig.add_subplot(2, 2, 3)
#
#     dec_s_prices, dec_s_weights = zip(*dec_s_data)
#     cdf = np.cumsum(dec_s_weights)
#
#     # plotting PDF and CDF
#     ax.plot(dec_s_prices, dec_s_weights, color="red", label="PDF")
#     ax.plot(dec_s_prices, cdf, label="CDF")
#     ax.legend()
#     ax.set_title("Decrease - Stay")
#
#     ax = fig.add_subplot(2, 2, 4)
#
#     dec_prices, dec_weights = zip(*dec_data)
#     cdf = np.cumsum(dec_weights)
#
#     # plotting PDF and CDF
#     ax.plot(dec_prices, dec_weights, color="red", label="PDF")
#     ax.plot(dec_prices, cdf, label="CDF")
#     ax.legend()
#     ax.set_title("Decrease")


# ****************************************************** #
#   This function calculates the transition matrices     #
#   created by the given df                              #
# ****************************************************** #
def cal_generated_trace_transition_mat(df, price_list):
    df = df.sort_values('Timestamp')
    df.NormPrice = np.around(df.NormPrice, 2)

    mat_size = len(price_list)
    mat = np.zeros((4, mat_size, mat_size))

    prev = df.NormPrice.iloc[0]
    curr = df.NormPrice.iloc[1]

    # 0 - increase, 1 - stay, 2 - decrease
    if curr > prev:
        state = 0
    elif curr < prev:
        state = 3
    else:
        state = 1

    length = df.Timestamp.count()
    for index in range(1, length-1):
        curr = df.NormPrice.iloc[index]

        # increase in price
        if curr > prev:
            row = price_list.index(prev)
            col = price_list.index(curr)
            mat[state][row][col] += 1
            state = 0
            # decrease in price
        elif curr < prev:
            row = price_list.index(prev)
            col = price_list.index(curr)
            mat[state][row][col] += 1
            state = 3
        # price stays the same
        else:
            if state == 0 or state == 1:
                row = price_list.index(prev)
                col = price_list.index(curr)
                mat[state][row][col] += 1
                state = 1
            elif state == 3 or state == 2:
                row = price_list.index(prev)
                col = price_list.index(curr)
                mat[state][row][col] += 1
                state = 2

        prev = curr

    return mat
    # for p in [0, 1, 2, 3]:
    #     sum_list = np.sum(mat[p], axis=1)
    #     for i in range(len(mat[p])):
    #         sum_row = sum_list[i]
    #         if sum_row != 0:
    #             for j in range(len(mat[p])):
    #                 mat[p][i][j] = mat[p][i][j] / sum_row
    #
    #
    # show_transition_matrices(mat, "Generated: ", price_list)


# ****************************************************** #
#   This function calculates the summary transition      #
#   mat from the original mat given to it                #
# ****************************************************** #
# def calc_summary_transition_mat(mat):
#     mat_size = len(mat[0])
#     summary_mat = np.zeros((4, mat_size, mat_size))
#
#     mat_sum = np.sum(mat)
#     print(mat_sum, (mat_sum / (mat_size * mat_size * 4)))
#     for p in [0, 1, 2, 3]:
#     # for p in [0]:
#         summary_mat[p] = np.full_like(mat[p], (mat_sum / (mat_size * mat_size * 4)))
#
#         diags = []
#         # for i in [-16, -2, 0, 3]:
#         for i in range(-mat_size + 1, mat_size):
#             d = np.diagonal(mat[p], offset=i)
#             diags.append((i, d.mean(), len(d)))
#
#         diags = sorted(diags,key=lambda x: x[1], reverse=True)
#         print(diags)
#
#         # fixed = []
#         # tot_len = 0
#         # for offset, diag_sum, length in diags:
#         #     fixed.append(offset)
#         #     tot_len += length
#         #     for i in range(mat_size):
#         #         if mat_size > i + offset >= 0:
#         #             summary_mat[p][i, i + offset] += diag_sum / length
#         #
#         #     dec_val = diag_sum / ((mat_size * mat_size) - tot_len)
#         #     for i in range(mat_size):
#         #         for j in range(mat_size):
#         #             if j - i not in fixed:
#         #                 summary_mat[p][i, j] -= dec_val
#         #
#         # print(summary_mat[p])
#
#     return summary_mat
#
#     # rows = [price_list.index(x) for x in [0.1, 0.18, 0.26, 0.32, 0.36, 0.42, 0.46, 0.58, 0.8, 1.0]]
#     # cols = [price_list.index(x) for x in [0.1, 0.18, 0.26, 0.32, 0.36, 0.42, 0.46, 0.58, 0.8, 1.0]]
#     # diags = [-16, -2, 0, 3]
#     #
#     # # Fill generates increase matrix #
#     # for p in [0, 1, 2, 3]:
#     #     for col in cols:
#     #         summary_mat[p][:, col] = mat[p][:, col]
#     #
#     #     for row in rows:
#     #         summary_mat[p][row, :] = mat[p][row, :]
#     #
#     #     for i in range(mat_size):
#     #         for j in diags:
#     #             if mat_size > i + j >= 0:
#     #                 summary_mat[p][i, i + j] = mat[p][i, i + j]
#     #
#     # return normalize_transition_mat_rows(summary_mat)
#
#
# def create_mask_mat(mat, price_list):
#     mat_size = len(mat[0])
#     summary_mask = np.zeros((4, mat_size, mat_size))
#
#     floor_prices = [0.1, 0.18, 0.26, 0.32, 0.36, 0.46, 0.58]
#     # cols = [price_list.index(x) for x in [0.1, 0.18, 0.26, 0.32, 0.36, 0.42, 0.46, 0.58, 0.8, 1.0]]
#     # diags = [-16, -2, 0, 3]
#
#     for p in [0, 1, 2, 3]:
#         diags = []
#         for i in [-16, -3, -2, 0, 3]:
#             d = np.diagonal(mat[p], offset=i)
#             diags.append((i, d.mean()))
#
#         # print(p, diags)
#
#         for offset, diag_mean in diags:
#             for i in range(mat_size):
#                 if mat_size > i + offset >= 0:
#                     # summary_mat[p][i, i + offset] += diag_mean
#                     if mat[p][i, i + offset] != 0:
#                         summary_mask[p][i, i + offset] = mat[p][i, i + offset]
#
#         for col in floor_prices:
#             for row in [round(num, 2) for num in np.arange(col-0.03, col+0.16, 0.03)]:
#                 if mat[p][price_list.index(row), price_list.index(col)] != 0:
#                     summary_mask[p][price_list.index(row), price_list.index(col)] = mat[p][price_list.index(row), price_list.index(col)]
#
#     summary_mask, all_sums = normalize_transition_mat_rows(summary_mask)
#
#     offset_list = [-16, -15, -12, -9, -6, -3, -2, 0, 3]
#
#     for p in [0, 1, 2, 3]:
#         diags2 = []
#         # for i in offset_list:
#         for i in range(-mat_size + 1, mat_size):
#             d = np.diagonal(mat[p], offset=i)
#             diags2.append((i, d.mean()))
#
#         diags2 = sorted(diags2, key=lambda x: x[1], reverse=True)
#         print(diags2)
#
#
#     # inc_off_list = [(-16, 0.02), (-15, 0.005), (-12, 0.01), (-9, 0.015), (-6, 0.03), (-3, 0.06), (-2, 0.01), (0, 0.2), (3, 0.65)]
#     # sum = 0
#     # for _, x in inc_off_list:
#     #     sum += x
#     # print("inc: ", sum)
#     # inc_s_off_list = [(-16, 0.005), (-15, 0.001), (-12, 0.001), (-9, 0.003), (-6, 0.002), (-3, 0.001), (-2, 0.001), (0, 0.9), (3, 0.05)]
#     # sum = 0
#     # for _, x in inc_s_off_list:
#     #     sum += x
#     # print("inc_s : ", sum)
#     # dec_s_off_list = [(-16, 0.003), (-15, 0.0001), (-12, 0.0001), (-9, 0.0011), (-6, 0.0001), (-3, 0.001), (-2, 0.01), (0, 0.9), (3, 0.02)]
#     # sum = 0
#     # for _, x in dec_s_off_list:
#     #     sum += x
#     # print("dec_s : ", sum)
#     # dec_off_list = [(-16, 0.15), (-15, 0.003), (-12, 0.003), (-9, 0.006), (-6, 0.006), (-3, 0.01), (-2, 0.2), (0, 0.4), (3, 0.2)]
#     # sum = 0
#     # for _, x in dec_off_list:
#     #     sum += x
#     # print("dec : ", sum)
#
#     return summary_mask


# ****************************************************** #
#   This function randomly chooses the starting point    #
#   of a generated trace                                 #
# ****************************************************** #
def calc_start_vals():
    start_prices, start_weights = zip(
        *[(1.0, 7478), (0.1, 1248), (0.18, 752), (0.46, 577), (0.26, 505), (0.09, 374), (0.32, 367), (0.84, 201),
          (0.11, 193), (0.58, 141), (0.12, 129), (0.19, 63), (0.2, 52), (0.17, 51), (0.85, 42), (0.98, 42), (0.16, 36),
          (0.27, 31), (0.13, 25), (0.5, 24), (0.07, 22), (0.33, 20), (0.8, 19), (0.3, 19), (0.28, 17), (0.6, 15),
          (0.56, 15), (0.47, 14), (0.36, 14), (0.21, 12), (0.22, 10), (0.25, 9), (0.24, 9), (0.86, 9), (0.68, 8),
          (0.59, 7), (0.29, 6), (0.83, 6), (0.49, 6), (0.08, 6), (0.52, 5), (0.38, 5), (0.65, 5), (0.44, 4), (0.35, 4),
          (0.42, 4), (0.94, 4), (0.87, 4), (0.76, 3), (0.61, 3), (0.34, 3), (0.71, 3), (0.41, 3), (0.72, 3), (0.99, 2),
          (0.74, 2), (0.64, 2), (0.88, 2), (0.48, 2), (0.37, 2), (0.54, 2), (0.57, 2), (0.97, 2), (0.92, 2), (0.96, 2),
          (0.73, 2), (0.9, 1), (0.62, 1), (0.7, 1), (0.82, 1), (0.55, 1), (0.15, 1), (0.78, 1), (0.79, 1), (0.77, 1),
          (0.63, 1), (0.23, 1), (0.95, 1), (0.05, 1), (0.14, 1), (0.67, 1), (0.4, 1)])
    start_weights = np.array(start_weights)
    weights_sum = start_weights.sum()
    start_weights = [x / weights_sum for x in start_weights]
    start_price = random.choices(start_prices, weights=start_weights, k=1)

    start_state = random.choice(['inc', 'inc_s', 'dec_s', 'dec'])
    return start_price[0], start_state[0]


# ****************************************************** #
#   This function generates a fake trace and names it    #
#   using the given 'name'                               #
# ****************************************************** #
# def simulator(name):
#     df_gen = pd.DataFrame({'Timestamp': pd.date_range(start=pd.to_datetime("1/1/2020 00:00:00 AM"),
#                                                       end=pd.to_datetime("7/1/2021 00:00:00 AM"), freq='h')})
#     start_price, start_state = calc_start_vals()
#     normPrice = [start_price]
#     state = start_state
#
#     size_trace = df_gen.Timestamp.count()
#     for i in range(size_trace - 1):
#         prev_price = normPrice[i]
#
#         if state == 'inc':
#             weights = mat[0][price_list.index(prev_price)]
#             prices = [x - prev_price for x in price_list]
#         elif state == 'inc_s':
#             weights = mat[1][price_list.index(prev_price)]
#             prices = [x - prev_price for x in price_list]
#         elif state == 'dec_s':
#             weights = mat[2][price_list.index(prev_price)]
#             prices = [x - prev_price for x in price_list]
#         else:  # state == 'dec'
#             weights = mat[3][price_list.index(prev_price)]
#             prices = [x - prev_price for x in price_list]
#
#         # new_price = choice(price_list, weights, 1)
#         num = choice(prices, weights, 0)
#         new_price = prev_price + num
#         while new_price > 1.0 or new_price <= 0.04:
#             if new_price > 1.0:
#                 new_price = 1.0
#             elif new_price <= 0.04:
#                 num = choice(prices, weights, 0)
#                 new_price = prev_price + num
#         normPrice.append(round(new_price, 2))
#
#         # set next state
#         if new_price > prev_price:
#             state = 'inc'
#         elif new_price == prev_price:
#             if state == 'inc' or state == 'inc_s':
#                 state = 'inc_s'
#             elif state == 'dec' or state == 'dec_s':
#                 state = 'dec_s'
#         else:
#             state = 'dec'
#
#     df_gen['NormPrice'] = normPrice
#
#     name = "./alibaba_work/generated_traces_epoch_2/trace_" + name + ".csv"
#     print(name)
#     # df_gen.to_csv(name, index=False)
#
#     return(df_gen)


def merge_transition_mat(mat, title, price_list):
    mat_size = len(mat[0])
    full_mat = np.zeros((mat_size, mat_size))

    for p in [0, 1, 2, 3]:
        full_mat += mat[p]

    # tot_sum = np.sum(full_mat)
    # d = np.diagonal(full_mat, offset=0)
    # diag_sum = d.sum()
    # print(tot_sum, diag_sum, diag_sum/tot_sum)

    sum_list = np.sum(full_mat, axis=1)
    for i in range(mat_size):
        sum_row = sum_list[i]
        if sum_row != 0:
            for j in range(mat_size):
                full_mat[i][j] = full_mat[i][j] / sum_row

    diags = []
    for i in range(-mat_size + 1, mat_size):
        d = np.diagonal(full_mat, offset=i)
        diags.append((i, d.mean(), len(d)))

    diags = sorted(diags, key=lambda x: x[1], reverse=True)
    # print(diags)

    df = pd.DataFrame(full_mat, columns=price_list)
    df = df.replace(0, np.nan)
    show_df(df, title, 1, 1, 1)

    return full_mat


def create_trace_sequence(mat_size, price_list):
    trace = [0.0] * 560 + [0.03] * 294 + [-0.16] * 38 + [0.02] * 23 + [-0.03] * 20 + [0.04] * 11 + [0.01] * 11 + [-0.02] * 11 + [-0.06] * 8 + [-0.09] * 8 + [-0.12] * 8 + [-0.15] * 8
    # print(trace)
    random.shuffle(trace)
    # print(trace)
    # trace = [0.03, 0.0, 0.0, 0.03, 0.03, 0.0, 0.03, -0.02, -0.02, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, -0.02, 0.03, -0.02, 0.0, 0.0, -0.02, 0.0, 0.0, 0.03, 0.03, 0.03, 0.0, 0.03, 0.03, 0.0, 0.0, -0.02, -0.02, 0.0, -0.02, -0.02, -0.02, -0.02, -0.02, 0.0, 0.03, 0.0, -0.02, 0.0, 0.0, 0.0, 0.0, 0.0, -0.02, 0.0, 0.03, 0.03, 0.0, 0.03, -0.02, -0.02, 0.0, 0.03, 0.03, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, -0.02, 0.0, 0.03, 0.0, 0.0, 0.0, -0.02, 0.03, 0.0, -0.02, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, 0.03, 0.03, -0.02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    # trace = [0.0, 0.03, 0.0, -0.16, 0.0, 0.0, 0.0, 0.03, 0.03, 0.03, -0.16, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.03, -0.02, 0.03, 0.03, 0.0, 0.03, 0.03, -0.02, -0.03, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, 0.03, 0.03, 0.03, 0.03, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, -0.03, 0.0, 0.0, 0.0, 0.0, 0.0, -0.16, 0.03, -0.02, 0.03, 0.0, 0.0, 0.0, -0.16, 0.0, 0.0, 0.0, 0.0, -0.16, 0.0, 0.0, 0.03, 0.03, 0.0, 0.03, 0.0, -0.16, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.02, -0.02, 0.0, -0.16, 0.0, 0.03, 0.0, -0.16, 0.03, 0.0, 0.0, -0.16, 0.0, 0.0, -0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, -0.03, 0.03, -0.16, 0.0, 0.03, -0.03, 0.0, -0.03, 0.0, 0.03, -0.02, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, -0.02, 0.0, -0.16, 0.03, 0.0, 0.0, 0.0, -0.02, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, 0.03, 0.0, 0.0, 0.0, 0.0, -0.03, 0.0, 0.0, -0.16, 0.0, 0.03, 0.0, 0.03, -0.16, -0.03, 0.0, 0.03, 0.03, 0.03, 0.0, 0.0, 0.0, 0.03, 0.03, 0.03, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.16, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, -0.03, 0.0, 0.0, 0.0, 0.03, -0.16, 0.0, 0.03, 0.0, 0.0, 0.03, -0.16, -0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.0, 0.03, -0.16, 0.0, 0.03, 0.0, 0.03, 0.0, 0.03, -0.02, 0.0, 0.03, -0.16, 0.03, 0.0, 0.0, -0.16, 0.0, 0.0, 0.0, -0.16, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, 0.0, -0.02, 0.0, 0.0, 0.03, 0.0, 0.03, -0.16, -0.02, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, -0.16, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, -0.16, 0.03, 0.03, 0.03, -0.03, -0.02, -0.03, 0.03, 0.0, 0.0, 0.0, -0.03, 0.0, 0.0, 0.03, 0.0, -0.16, 0.0, -0.16, 0.03, 0.0, 0.0, -0.16, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, -0.16, 0.03, 0.0, -0.16, 0.0, 0.0, 0.0, 0.03, -0.16, 0.0, -0.16, 0.0, 0.0, -0.16, 0.0, 0.03, 0.03, 0.0, 0.03, -0.16, 0.0, 0.0, 0.0, 0.0, 0.0, -0.02, -0.03, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, -0.03, 0.0, 0.03, 0.03, 0.0, 0.0, 0.03, 0.03, -0.02, 0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, -0.03, 0.0, 0.0, 0.03, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, 0.0, 0.0, -0.02, 0.0, -0.16, -0.02, 0.03, -0.16, 0.03, 0.0, 0.0, 0.03, 0.0, -0.03, -0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.0, 0.03, -0.16, 0.0, -0.02, 0.03, 0.0, 0.03, -0.16, 0.0, 0.03, 0.0, -0.03, 0.0, 0.0, 0.0, 0.0, 0.03, -0.03, 0.03, 0.0, 0.0, 0.03, 0.03, 0.0, -0.16, 0.0, -0.16, 0.03, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, 0.03, 0.03, 0.0, 0.0, 0.03, -0.03, 0.03, 0.0, 0.0, -0.02, 0.0, -0.16, -0.02, 0.0, -0.03, -0.16, 0.0, 0.0, 0.03, 0.03, 0.03, 0.0, 0.0, 0.0, -0.02, 0.0, 0.0, 0.0, 0.03, 0.0, -0.03, 0.0, -0.16, 0.0, 0.0, 0.0, 0.0, -0.16, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, -0.03, 0.03, -0.16, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.03, 0.0, -0.02, -0.02, -0.02, 0.0, 0.03, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, 0.03, 0.0, -0.03, 0.0, 0.03, 0.03, 0.0, 0.0, 0.03, 0.03, 0.0, -0.03, 0.03, 0.0, 0.0, 0.0, 0.0, -0.16, 0.0, 0.0, 0.03, 0.0, 0.03, 0.0, 0.0, -0.16, 0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.03, 0.03, -0.03, 0.03, 0.03, 0.0, 0.0, 0.03, -0.16, -0.02, -0.02, 0.0, 0.03, 0.0, 0.03, -0.16, 0.0, 0.03, 0.0, -0.02, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, -0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, -0.03, 0.0, 0.0, 0.0, 0.0, 0.03, -0.02, -0.03, 0.0, 0.03, 0.0, 0.03, -0.16, 0.03, -0.02, 0.0, -0.02, -0.16, 0.0, 0.03, 0.03, 0.0, -0.16, 0.0, -0.16, 0.0, -0.16, 0.0, -0.16, 0.0, 0.03, -0.03, 0.0, 0.0, 0.03, 0.03, 0.0, -0.03, 0.0, 0.0, 0.03, -0.16, 0.0, 0.03, -0.02, -0.16, 0.0, -0.03, 0.0, 0.03, 0.0, 0.03, 0.03, -0.02, -0.16, 0.03, 0.03, 0.03, 0.0, -0.16, 0.0, 0.0, -0.16, 0.0, 0.03, 0.0, -0.03, -0.03, 0.0, -0.16, -0.16, 0.03, 0.0, -0.16, 0.0, 0.03, 0.0, -0.03, 0.03, 0.0, -0.16, -0.03, 0.0, -0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.03, -0.16, -0.03, 0.0, -0.03, 0.03, 0.0, -0.16, -0.16, 0.0, -0.16, 0.0, 0.03, 0.0, 0.03, -0.03, 0.0, 0.03, 0.0, 0.0, -0.02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, -0.03, 0.0, 0.03, 0.0, 0.03, 0.0, -0.03, 0.03, 0.0, -0.16, -0.02, 0.03, 0.03, 0.03, -0.16, 0.03, 0.0, 0.03, -0.16, -0.03, 0.0, 0.03, -0.16, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, 0.0, -0.16, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, -0.02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.16, -0.16, 0.0, 0.03, 0.0, 0.0, -0.02, 0.0, 0.0, 0.03, -0.02, 0.03, 0.03, -0.16, 0.0, 0.03, 0.0, -0.02, 0.03, 0.03, 0.03, 0.03, -0.16, 0.03, 0.0, -0.02, 0.03, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.03, -0.02, 0.0, 0.03, -0.16, 0.03, -0.02, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, -0.16, -0.16, -0.02, -0.16, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.03, -0.16, 0.03, -0.02, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.03, 0.03, -0.02, 0.03, -0.03, 0.03, 0.0, -0.16, 0.0, 0.03, 0.03, -0.16, 0.0, 0.0, 0.0, 0.0, -0.16, 0.0, 0.0, 0.0, 0.03, -0.16, 0.03, 0.03, 0.03, -0.16, 0.0, -0.16, 0.03, 0.0, 0.03, 0.03, 0.03, -0.16, 0.0, -0.03, -0.16, 0.0, 0.03, 0.03, -0.02, 0.0, -0.02, -0.16, 0.03, 0.0, 0.03, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, -0.16, 0.0, -0.03, -0.02, 0.0, 0.03, 0.0, -0.16, 0.03, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, -0.02, 0.0, 0.03, -0.16, -0.03, 0.0, 0.0, 0.03, 0.03, 0.0, 0.03, 0.0, 0.03, 0.0, 0.0, -0.16, 0.0, 0.03, -0.16, 0.03, -0.02, 0.03, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, 0.0, 0.0, 0.03, 0.0, 0.03, 0.03, -0.16, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.03, 0.03, 0.0, 0.03, 0.0, -0.02, 0.0, 0.0, 0.03, 0.0, 0.0, 0.03, -0.16, 0.0, 0.0, 0.03, -0.16, 0.0, 0.03, -0.16, 0.03, -0.03, 0.03, 0.0, 0.0, 0.03, 0.0, 0.03, 0.03, 0.0, -0.02, 0.0, 0.03, 0.0, -0.16, 0.0, -0.16, 0.03, 0.0, 0.03, 0.0, 0.0]
    # # print(len(trace))
    trace = trace * 10
    # print(trace)
    start, _ = calc_start_vals()
    # print(start)
    price_trace = [start]
    time = [0]
    for index, step in enumerate(trace, 1):
        curr_price = round(price_trace[-1]+step, 2)
        if curr_price > 1.0:
            price_trace.append(1.0)
        elif curr_price < 0.04:
            price_trace.append(price_trace[-1])
        else:
            price_trace.append(curr_price)
        time.append(index)

    # plt.plot(time, price_trace)
    df = pd.DataFrame(list(zip(time, price_trace)), columns=['Timestamp', 'NormPrice'])

    return df


def create_artificial_transition_mat(mat_size, price_list):

    total_mat = np.zeros((4, mat_size, mat_size))

    for i in range(10):
        df = create_trace_sequence(mat_size, price_list)
        # plt.plot(df.Timestamp, df.NormPrice, ".-")
        total_mat += cal_generated_trace_transition_mat(create_trace_sequence(mat_size, price_list), price_list)

    merged_mat = merge_transition_mat(total_mat, "created", price_list)
    # total_mat, _ = normalize_transition_mat_rows(total_mat)
    # show_transition_matrices(total_mat, "Testing: ", price_list)

    # return total_mat
    return merged_mat


def calc_matrix_norm(original, artificial, mat_size):

    temp = (original-artificial)
    print(temp)
    print(LA.norm(temp, 2))
    # diff_mat = np.zeros((4, mat_size, mat_size))
    #
    # for p in [0, 1, 2, 3]:
    #     diff_mat[p] = original[p] - artificial[p]
    #
    #     print(LA.norm(diff_mat[p], 2))



# fig = plt.figure()
# # inst = "../../Alibaba_log_files/interesting_traces/cn_zhangjiakou_c__ecs_hfc6_8xlarge__optimized__vpc.csv"
# inst = "../../Alibaba_log_files/interesting_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv"
#
# df = pd.read_csv(inst)
# show_trace(df, inst)

price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]

# file = "../../Alibaba_log_files/feature_files/state_transition_matrix_all_instances_from_01-2020.npy"
# file = "../../Alibaba_log_files/feature_files/state_transition_matrix_all_instances_correct_epoch_2.npy"
# file = "../../Alibaba_log_files/interesting_traces/cn_zhangjiakou_c__ecs_hfc6_8xlarge_state_transition_mat_epoch_2.npy"
# file = "../../Alibaba_log_files/interesting_traces/cn_beijing_g__ecs_hfr6_2xlarge_state_transition_mat_epoch_2.npy"
# file = "../../Alibaba_log_files/transition_matrices/state_transition_mat_all_instances_epoch_1.npy"
file = "../../../Alibaba_log_files/transition_matrices/state_transition_mat_all_instances_from_01-2021.npy"
with open(file, 'rb') as f:
    mat = np.load(f)


# ########### Draw the original transition matrices ########### #
fig, axs = plt.subplots(2, 2, constrained_layout=True, figsize=set_size(fraction=3))
# fig.suptitle('First Epoch')
# mat = normalize_transition_mat(mat)
mat, all_sums = normalize_transition_mat_rows(mat)
show_transition_matrices_thesis(mat)

# fig = plt.figure()
# mat[mat < 0.05] = 0
# show_transition_matrices(mat, "test", price_list)

# print(list(zip(np.sum(mat, axis=(0,2)), np.sum(mat, axis=(0,1)))))

# fig = plt.figure()
# inc_data, inc_s_data, dec_s_data, dec_data = calc_folded_pdfs(mat, all_sums)
# calc_folded_cdfs(inc_data, inc_s_data, dec_s_data, dec_data)

# ######## Generate transition matrices ######## #
# sum_mat = calc_summary_transition_mat(mat)
# mask_mat = create_mask_mat(mat, price_list)
#
# fig = plt.figure()
# original = merge_transition_mat(mat, "original", price_list)
# show_transition_matrices(mask_mat, "Summary", price_list)

# fig = plt.figure()
# create_trace_sequence(len(mat[0]), price_list)
# artificial = create_artificial_transition_mat(len(mat[0]), price_list)


# calc_matrix_norm(original, artificial, len(mat[0]))
# df_gen = simulator("1")
#
# fig = plt.figure()
# show_trace(df_gen, "Generated trace")


# fig = plt.figure()
# cal_generated_trace_transition_mat(df_gen, price_list)

plt.show()


