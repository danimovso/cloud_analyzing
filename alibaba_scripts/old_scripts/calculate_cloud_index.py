import glob
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import pandas as pd, numpy as np

# plt.style.use('grayscale')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 10,
        "font.size": 10,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 8,
        "xtick.labelsize": 8,
        "ytick.labelsize": 8
    }

plt.rcParams.update(tex_fonts)


def set_size(width = 469.75499 , fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


# regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
#            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen', 'cn-zhangjiakou',
#            'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

regions = ['ap-south-1', 'ap-southeast-1', 'cn-beijing', 'cn-hangzhou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1']

fig, axs = plt.subplots(len(regions))
fig.suptitle('Cloud Index Per Region')

# regions = ['*']

for num, region in enumerate(regions, start=0):
    instList = "./alibaba_work/"+region+"_full_traces/*__resample.csv"
    instList = glob.glob(instList)
    # print(len(instList))
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
    # instList = ['../../Alibaba_log_files/Correlation_tables/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc.csv']

    num_insts = len(instList)
    all_data_dict = {}
    for inst in instList:
        print("processing ",inst)
        df = pd.read_csv(inst)

        # df['NormPrice'] = df.SpotPrice / df.OriginPrice
        #
        # df = df[['OriginPrice', 'SpotPrice', 'NormPrice', 'Timestamp']]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        #
        # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]
        # df = df.set_index('Timestamp')
        #
        # if not df.empty:
        #     missing_dates = pd.date_range(start=df.index[0], end=df.index[-1], freq='D').difference(df.index).tolist()
        #     df2 = pd.DataFrame(index=missing_dates, columns=['OriginPrice', 'SpotPrice', 'NormPrice'])
        #     df = df.append(df2)
        #     df = df.sort_index()
        #     df = df.ffill()

        df['date'] = df.Timestamp.dt.date
        grouped = df.groupby('date')
        for name, group in grouped:
            # print("HERE!!!!")
            if name in all_data_dict:
                # print("here")
                all_data_dict[name][0] += group.NormPrice.mean()
                all_data_dict[name][1] += 1
            else:
                all_data_dict[name] = [0,0]
                all_data_dict[name][0] = group.NormPrice.mean()
                all_data_dict[name][1] = 1

    # print(all_data_dict)
    # d = {x: [0,0] for x, _ in all_data_list}
    # print(d)
    # for time, normPrice in all_data_list:
    #     d[time][0] += normPrice
    #     d[time][1] += 1

    cloud_index = []
    for key, item in all_data_dict.items():
        # print(key)
    #     print(item)
    #     cloud_index.append((key, (item / num_insts)))
        cloud_index.append((key, (item[0] / item[1]), item[1]))

    cloud_index = sorted(cloud_index)
    date_list, price_list, num_insts_list = zip(*cloud_index)
#     zip(*cloud_index)
#     plt.plot(*zip(*cloud_index), label=region)
#     plt.scatter(date_list, price_list, c=num_insts_list, label=region)
#     plt.scatter(date_list, num_insts_list, c=num_insts_list)
    axs[num].scatter(date_list, price_list, c=num_insts_list, label=region)
#     # axs[num].plot(*zip(*cloud_index), label=region)
    axs[num].legend()
#
plt.ylabel("Index level")
# plt.ylabel("Number of instances")
# plt.colorbar()
# plt.legend()
plt.show()