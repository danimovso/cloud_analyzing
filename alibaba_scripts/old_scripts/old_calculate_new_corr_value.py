import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob
import argparse
import os.path
from os import path

epsilon = 0.02

def create_all_regions_corr_data_file(region_list):
    
    newFile = "./all_regions_causation_summary.csv"
    print("creating ",newFile," ...")
    f1 = open(newFile,"w")
    f1.write("Original_inst,Compared_inst,TotalNumberOfEventes,Original_ECDF,Causation_ECDF,Diff\n")

    for region in region_list:
        regionDir = "./"+region+"_ecdf_files/*"
        instDirList = glob.glob(regionDir)
                
        for instDir in instDirList:
            print(instDir)
            if path.isdir(instDir):
                _,inst = instDir.split("_files/")
                
                instECDF = "./"+region+"_traces/"+inst+"__ecdf.csv"
                df1 = pd.read_csv(instECDF)
                
                firstVal1 = df1["DiffTimeBucket"].iloc[0]
                if (firstVal1 <= epsilon):
                    df_below1 = df1[df1.DiffTimeBucket <= epsilon]
                    x1 = df_below1["DiffTimeBucket"].iloc[-1]
                    y1 = df_below1["ecdfVal"].iloc[-1]
                else:
                    x1 = 0.0
                    y1 = 0
                
                ecdfVal1 = y1
                
                instDirName = instDir+"/*_VS_*.csv"
                causationList = glob.glob(instDirName)
                for file in causationList:
                    _,_,_,fileName = file.split("/")
                    fileName,_ = fileName.split("__ecdf.csv")
                    inst1,inst2 = fileName.split("_VS_")
                    
                    df2 = pd.read_csv(file)
                    num_events = df2.shape[0]
                    if not df2.empty:
                        print(file)
                        
                        firstVal2 = df2["DiffTimeBucket"].iloc[0]
                        if (firstVal2 <= epsilon):
                            df_below2 = df2[df2.DiffTimeBucket <= epsilon]
                            x2 = df_below2["DiffTimeBucket"].iloc[-1]
                            y2 = df_below2["ecdfVal"].iloc[-1]
                        else:
                            x2 = 0.0
                            y2 = 0
                        
                        ecdfVal2 = y2
                            
                        f1.write(str(inst1)+","+str(inst2)+","+str(num_events)+","+str(ecdfVal1)+","+str(ecdfVal2)+","+str(ecdfVal2-ecdfVal1)+"\n")
    
    f1.close()

def create_merged_corr_data_file(region):
    regionDir = "./"+region+"_ecdf_files/*"
    instDirList = glob.glob(regionDir)
    
    newFile = "./"+region+"_ecdf_files/"+region+"__causation_summary.csv"
    print("creating ",newFile," ...")
    f1 = open(newFile,"w")
    f1.write("Original_inst,Compared_inst,TotalNumberOfEventes,Original_ECDF,Causation_ECDF,Diff\n")
    
    for instDir in instDirList:
        print(instDir)
        if path.isdir(instDir):
            _,inst = instDir.split("_files/")
            
            instECDF = "./"+region+"_traces/"+inst+"__ecdf.csv"
            df1 = pd.read_csv(instECDF)
            
            firstVal1 = df1["DiffTimeBucket"].iloc[0]
            if (firstVal1 <= epsilon):
                df_below1 = df1[df1.DiffTimeBucket <= epsilon]
                x1 = df_below1["DiffTimeBucket"].iloc[-1]
                y1 = df_below1["ecdfVal"].iloc[-1]
            else:
                x1 = 0.0
                y1 = 0
            
            ecdfVal1 = y1
            
            instDirName = instDir+"/*_VS_*.csv"
            causationList = glob.glob(instDirName)
            for file in causationList:
                _,_,_,fileName = file.split("/")
                fileName,_ = fileName.split("__ecdf.csv")
                inst1,inst2 = fileName.split("_VS_")
                
                df2 = pd.read_csv(file)
                num_events = df2.shape[0]
                if not df2.empty:
                    print(file)
                    
                    firstVal2 = df2["DiffTimeBucket"].iloc[0]
                    if (firstVal2 <= epsilon):
                        df_below2 = df2[df2.DiffTimeBucket <= epsilon]
                        x2 = df_below2["DiffTimeBucket"].iloc[-1]
                        y2 = df_below2["ecdfVal"].iloc[-1]
                    else:
                        x2 = 0.0
                        y2 = 0
                    
                    ecdfVal2 = y2
                        
                    f1.write(str(inst1)+","+str(inst2)+","+str(num_events)+","+str(ecdfVal1)+","+str(ecdfVal2)+","+str(ecdfVal2-ecdfVal1)+"\n")
    
    f1.close()

def create_corr_data(region):
    regionDir = "./"+region+"_ecdf_files/*"
    instDirList = glob.glob(regionDir)
    
    for instDir in instDirList:
        print(instDir)
        if path.isdir(instDir):
            _,inst = instDir.split("_files/")
            
            newFile = "./"+region+"_ecdf_files/"+inst+"/"+inst+"__causation_without_first_entry.csv"
            print("creating ",newFile," ...")
            f1 = open(newFile,"w")
            f1.write("Original_inst,Compared_inst,TotalNumberOfEventes,Original_ECDF,Causation_ECDF,Diff\n")
            
            instECDF = "./"+region+"_traces/"+inst+"__ecdf.csv"
            df1 = pd.read_csv(instECDF)
            
            firstVal1 = df1["DiffTimeBucket"].iloc[0]
            if (firstVal1 <= epsilon):
                df_below1 = df1[df1.DiffTimeBucket <= epsilon]
                x1 = df_below1["DiffTimeBucket"].iloc[-1]
                y1 = df_below1["ecdfVal"].iloc[-1]
            else:
                x1 = 0.0
                y1 = 0
            
            ecdfVal1 = y1
            
            instDirName = instDir+"/*_VS_*.csv"
            causationList = glob.glob(instDirName)
            for file in causationList:
                _,_,_,fileName = file.split("/")
                fileName,_ = fileName.split("__ecdf.csv")
                inst1,inst2 = fileName.split("_VS_")
                
                df2 = pd.read_csv(file)
                num_events = df2.shape[0]
                if not df2.empty:
                    print(file)
                    
                    firstVal2 = df2["DiffTimeBucket"].iloc[0]
                    if (firstVal2 <= epsilon):
                        df_below2 = df2[df2.DiffTimeBucket <= epsilon]
                        x2 = df_below2["DiffTimeBucket"].iloc[-1]
                        y2 = df_below2["ecdfVal"].iloc[-1]
                    else:
                        x2 = 0.0
                        y2 = 0
                    
                    ecdfVal2 = y2
                        
                    f1.write(str(inst1)+","+str(inst2)+","+str(num_events)+","+str(ecdfVal1)+","+str(ecdfVal2)+","+str(ecdfVal2-ecdfVal1)+"\n")
            f1.close()
            
          
def create_corr_data_specific(region,inst):
    
    newFile = "./"+region+"_ecdf_files/"+inst+"/causation.txt"
    ##f1 = open(newFile,"w")
        
    instECDF = "./"+region+"_traces/"+inst+"__ecdf.csv"
    ##f1.write(instECDF+"\n")
    df1 = pd.read_csv(instECDF)
    ##df1 = df1[df1.DiffTimeBucket <= epsilon]
    #print(df1)
    firstVal1 = df1["DiffTimeBucket"].iloc[0]
    if (firstVal1 <= epsilon):
        df_below1 = df1[df1.DiffTimeBucket <= epsilon]
        x1 = df_below1["DiffTimeBucket"].iloc[-1]
        y1 = df_below1["ecdfVal"].iloc[-1]
    else:
        x1 = 0.0
        y1 = 0
    
    ecdfVal1 = y1
    #print(ecdfVal1)
    
    ##f1.write(str(inst)+","+str(ecdfVal1)+"\n")
    
    dirName = "./"+region+"_ecdf_files/"+inst+"/*_VS_*.csv"
    causationList = glob.glob(dirName)
    #causationList = ["./us-east-1_ecdf_files/us_east_1a__ecs_e3_large/us_east_1a__ecs_e3_large_VS_cn_shenzhen_b__ecs_e4_small__ecdf.csv"]
    ##print(causationList)
    
    for file in causationList:
        #file = "./us-east-1_ecdf_files/us_east_1a__ecs_e3_large/us_east_1a__ecs_e3_large_VS_cn_shenzhen_b__ecs_e4_small__ecdf.csv"
        df2 = pd.read_csv(file)
        ##df_sort = df2.loc[(df2['DiffTimeBucket']-epsilon).abs().argsort()[:2]]
        print("number of rows: ",df2.shape[0])
        
        firstVal2 = df2["DiffTimeBucket"].iloc[0]
        if (firstVal2 <= epsilon):
            df_below2 = df2[df2.DiffTimeBucket <= epsilon]
            x2 = df_below2["DiffTimeBucket"].iloc[-1]
            y2 = df_below2["ecdfVal"].iloc[-1]
        else:
            x2 = 0.0
            y2 = 0
        
        ecdfVal2 = y2
        print(ecdfVal2)
        
        ##if not df2.empty:
        ##    ecdfVal2 = df2["ecdfVal"].iloc[-1]
        ##    _,_,_,fileName = file.split("/")
        ##    fileName,_ = fileName.split(".csv")
        ##    inst1,inst2 = fileName.split("_VS_")
        ##    f1.write(str(inst1)+","+str(inst2)+","+str(ecdfVal2)+","+str(ecdfVal2-ecdfVal1)+"\n")
    ##f1.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region inst for which we want to calculate the new ECDFs')
    parser.add_argument("--inst", help='The inst inst for which we want to calculate the ECDF graph')
  
    args = parser.parse_args()
    
    region_list = ["ap-northeast-1", "ap-south-1", "ap-southeast-1", "ap-southeast-2", "ap-southeast-3", "ap-southeast-5", "cn-beijing", "cn-hangzhou", "cn-hongkong", "cn-huhehaote", "cn-qingdao", "cn-shanghai", "cn-shenzhen", "cn-zhangjiakou", "eu-central-1", "eu-west-1", "me-east-1", "us-east-1", "us-west-1"]
    
    if args.inst:
        inst = args.inst
        create_corr_data_specific(region,inst)
    elif args.region:
        region = args.region
        create_merged_corr_data_file(region)
    else:
        create_all_regions_corr_data_file(region_list)
        
    print("Finish")