import networkx as nx
import glob
import pandas as pd
import matplotlib.pyplot as plt
import collections


from itertools import count

#dirName = "./*_traces/*vpc__clean.csv"
#list2 = glob.glob(dirName)
#
#allInsts = []
#for inst in list2:
#    if (len(open(inst).readlines(  )) > 3):
#        allInsts.append(inst)
#        
#
#print("------------------------------------------------")
#print(len(allInsts))
#print("------------------------------------------------")
#print(allInsts)

#fig = plt.figure(figsize=(20, 20))
#df = pd.read_csv("./us-east-1_ecdf_files/us_east_1a__ecs_e3_large/us_east_1a__ecs_e3_large__causation_without_first_entry.csv")
#df = pd.read_csv("./us-east-1_ecdf_files/us-east-1__causation_summary.csv")
df = pd.read_csv("./all_regions_causation_summary.csv")
print("Number of lines: ",df.Diff.count())
df = df[df.Diff > 0]
print("Number of lines: ",df.Diff.count())
df = df[df.TotalNumberOfEventes > 1]
print("Number of lines: ",df.Diff.count())

G = nx.from_pandas_edgelist(df,source="Original_inst",target="Compared_inst", edge_attr="Diff")

#nx.draw(G, pos=nx.spring_layout(G), with_labels=True)
#plt.show()
#plt.savefig("./corr_graph.png")

################################################
# drawing nodes and edges separately so we can #
# capture collection for colobar               #
################################################
#pos = nx.spring_layout(G)
#ec = nx.draw_networkx_edges(G, pos, alpha=0.2)
#nc = nx.draw_networkx_nodes(G, pos, nodelist=nodes, node_color=colors, 
#                            with_labels=False, node_size=100, cmap=plt.cm.jet)

#plt.show()

################################################
#########  Filter the graph by degree  #########
################################################
i = 760

f1 = open("causation_graph_node_and_degree.txt","w")
f1.write(str([(k, v) for k, v in sorted(G.degree(), key=lambda x: x[1], reverse=True)]))
f1.close()

outdeg = G.degree()
to_keep = [n for n,d in G.degree() if d>=i]
H = G.subgraph(to_keep)

d = dict(G.degree)
#print(len(d))
h = dict(H.degree)
#print(h)
#print(max(h)[1])

#fig = plt.figure(figsize=(18, 18))
str = "Node degree above: " + str(i)
plt.title(str)
nx.draw(H, pos=nx.spring_layout(H),node_size=20, with_labels=True)
#nx.draw(H, pos=nx.spring_layout(H), with_labels=False)

#pos=nx.spring_layout(H)
#nx.draw_networkx_nodes(H, pos, node_size=20, with_labels=True)
#nx.draw_networkx_edges(H, pos, alpha=0.4)
plt.show()

####################################################
#               Degree Rank                        #
# Random graph from given degree sequence.         #
# Draw degree rank plot and graph with matplotlib. #
####################################################

#degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
##print("Degree sequence", degree_sequence)
#dmax = max(degree_sequence)
#
#plt.loglog(degree_sequence, 'b-', marker='o')
##plt.plot(degree_sequence, 'b-', marker='o')
#plt.title("Degree rank plot")
#plt.ylabel("degree")
#plt.xlabel("rank")
#
## draw graph in inset
#plt.axes([0.45, 0.45, 0.45, 0.45])
#Gcc = G.subgraph(sorted(nx.connected_components(G), key=len, reverse=True)[0])
#pos = nx.spring_layout(Gcc)
#plt.axis('off')
#nx.draw_networkx_nodes(Gcc, pos, node_size=20)
#nx.draw_networkx_edges(Gcc, pos, alpha=0.4)
#
#plt.show()


####################################################
#               Degree histogram                   #
# Draw degree histogram with matplotlib.           #
####################################################

#degree_sequence = sorted([d for n, d in G.degree()], reverse=True)  # degree sequence
## print "Degree sequence", degree_sequence
#degreeCount = collections.Counter(degree_sequence)
#deg, cnt = zip(*degreeCount.items())
#
#fig, ax = plt.subplots()
#plt.bar(deg, cnt, width=0.80, color='b')
#
#plt.title("Degree Histogram")
#plt.ylabel("Count")
#plt.xlabel("Degree")
#ax.set_xticks([d + 0.4 for d in deg])
#ax.set_xticklabels(deg,rotation='vertical')
#
## draw graph in inset
#plt.axes([0.4, 0.4, 0.5, 0.5])
#Gcc = G.subgraph(sorted(nx.connected_components(G), key=len, reverse=True)[0])
#pos = nx.spring_layout(G)
#plt.axis('off')
#nx.draw_networkx_nodes(G, pos, node_size=20)
#nx.draw_networkx_edges(G, pos, alpha=0.4)
#
#plt.show()

print("finished!")