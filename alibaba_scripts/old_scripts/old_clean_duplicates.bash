#!/bin/bash

FILES=` ls ./*_traces/*__vpc_updated.csv`
#FILES=` ls ./cn-beijing_traces/*ting.txt`

for f in $FILES
do
    echo "Processing $f file..."
    name=$(echo $f | cut -d"." -f2)
    newName=".${name}_noDup.csv"
	echo "$newName"
    
    wc -l $f
    cat $f |sort |uniq >> $newName
    wc -l $newName
done
	
