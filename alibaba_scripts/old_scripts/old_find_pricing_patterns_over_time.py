import pandas as pd
import glob
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pandas.plotting import register_matplotlib_converters

import matplotlib

# matplotlib.use('Agg')

register_matplotlib_converters()
from statsmodels.distributions.empirical_distribution import ECDF

import argparse

# plt.style.use('grayscale')
# plt.style.use('tableau-colorblind10')

tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 12,
    "font.size": 12,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 10,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10
}

plt.rcParams.update(tex_fonts)


def set_size(width=441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def add_pattern_column():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean_2.csv"
    instList = glob.glob(instList)

    for num, inst in enumerate(instList, 1):
        print("processing ", inst, num)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]

        df['pattern_mark'] = np.nan

        name, csv = inst.split('__vpc__clean_2.csv')
        file = name + "__pattern_mark.csv"
        print("creating ", file)
        df.to_csv(inst, index=False)


def find_chevrons_in_all_regions():
    # timeInSeconds_patterns = []
    # timestamp_patters = []
    #
    # price_increase_list = []
    # price_decrease_list = []

    all_data = []

    # instList = "./alibaba_work/" + region + "_2020_traces/*__clean.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*__square_2.csv"
    instList = glob.glob(instList)
    # instList = [
    #     'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_b__ecs_gn5_c4g1_xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square_2.csv']
    total_count = len(instList)

    for num, inst in enumerate(instList, 1):
        print("processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        # df = df[df.Timestamp >= "1/1/2020 00:00:00 AM"]

        df.reset_index(drop=True, inplace=True)
        # print(df)
        length = df.Timestamp.count()
        for index in range(1, length - 4):
            if df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index+3] and \
                    df['NormPrice'].iloc[index+1] != (df['NormPrice'].iloc[index]) and \
                    df['TimeInSeconds'].iloc[index+2] - df['TimeInSeconds'].iloc[index + 1] < 172800 and \
                    ((df['TimeInSeconds'].iloc[index] - df['TimeInSeconds'].iloc[index-1] >= 172800 and df['TimeInSeconds'].iloc[index+4] - df['TimeInSeconds'].iloc[index+3] >= 172800) or \
                     (not pd.isna(df['FixedPeakInfo'].iloc[index]) and not pd.isna(df['FixedPeakInfo'].iloc[index+3]))):
                all_data.append(
                    [inst, df['Timestamp'].iloc[index], df['Timestamp'].iloc[index+1], df['Timestamp'].iloc[index+2],
                     df['Timestamp'].iloc[index+3],
                     df['TimeInSeconds'].iloc[index], df['TimeInSeconds'].iloc[index+1], df['TimeInSeconds'].iloc[index+2],
                     df['TimeInSeconds'].iloc[index+3],
                     (df['TimeInSeconds'].iloc[index+2] - df['TimeInSeconds'].iloc[index+1]),
                     df['OriginPrice'].iloc[index], df['SpotPrice'].iloc[index],
                     df['NormPrice'].iloc[index], df['SpotPrice'].iloc[index+1],
                     df['NormPrice'].iloc[index+1],
                     (df['SpotPrice'].iloc[index+1] - df['SpotPrice'].iloc[index]),
                     (df['NormPrice'].iloc[index+1] - df['NormPrice'].iloc[index])])

    new_df = pd.DataFrame(all_data, columns=['inst', 'Timestamp1', 'Timestamp2', 'Timestamp3', 'Timestamp4',
                                             'TimeInSeconds1',
                                             'TimeInSeconds2', 'TimeInSeconds3', 'TimeInSeconds4',
                                             'TimeInterval', 'OriginPrice',
                                             'BaseSpotPrice',
                                             'BaseNormPrice', 'PeakSpotPrice', 'PeakNormPrice', 'Height',
                                             'NormHeight'])

    # print(new_df.Timestamp1.head(30))
    file = "../../Alibaba_log_files/all_chevron_data_by_hand.csv"
    print("saving the data in ", file)
    new_df.to_csv(file, index=False)


def mark_chevron_patterns():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = [
    #     'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv']
    total_length = len(instList)

    all_data = []
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_length)
        df = pd.read_csv(inst)
        # df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')

        peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
        # print(peaks_index_list)
        if peaks_index_list:
            prev = peaks_index_list[0]
            chevron_index_list = []
            for i, curr in enumerate(peaks_index_list[1:-2], 1):
                if curr not in chevron_index_list:
                    # Set the next relevant peak value
                    if df['NormPrice'].iloc[curr] == df['NormPrice'].iloc[peaks_index_list[i + 1]]:
                        next = peaks_index_list[i + 2]
                        curr_2 = peaks_index_list[i + 1]
                    else:
                        next = peaks_index_list[i + 1]
                        curr_2 = curr

                    # print(df['NormPrice'].iloc[prev], df['NormPrice'].iloc[next], curr , prev)
                    if df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next] and (curr - prev) == 1 and (next - curr_2) == 1:
                        chevron_index_list.append(curr_2)
                        # if df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[prev]:
                        all_data.append([inst, df['Timestamp'].iloc[prev], df['Timestamp'].iloc[curr], df['Timestamp'].iloc[curr_2],
                                         df['Timestamp'].iloc[next],
                                         df['TimeInSeconds'].iloc[prev], df['TimeInSeconds'].iloc[curr], df['TimeInSeconds'].iloc[curr_2],
                                         df['TimeInSeconds'].iloc[next],
                                         (df['TimeInSeconds'].iloc[next] - df['TimeInSeconds'].iloc[prev]),
                                         df['OriginPrice'].iloc[curr], df['SpotPrice'].iloc[prev],
                                         df['NormPrice'].iloc[prev], df['SpotPrice'].iloc[curr],
                                         df['NormPrice'].iloc[curr],
                                         (df['SpotPrice'].iloc[curr] - df['SpotPrice'].iloc[prev]),
                                         (df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev])])
                prev = curr

    new_df = pd.DataFrame(all_data, columns=['inst', 'Timestamp1', 'Timestamp2', 'Timestamp3', 'Timestamp4', 'TimeInSeconds1',
                                             'TimeInSeconds2', 'TimeInSeconds3', 'TimeInSeconds4', 'TimeInterval', 'OriginPrice',
                                             'BaseSpotPrice',
                                             'BaseNormPrice', 'PeakSpotPrice', 'PeakNormPrice', 'Height', 'NormHeight'])
    # print(new_df.count())
    new_df.to_csv("../../Alibaba_log_files/all_chevron_data_from_square_2.csv", index=False)

    # plt.plot(df.Timestamp, df.NormPrice, '.:k', label='Spot')
    # plt.scatter(df.Timestamp[df.pattern_mark == 1], df.NormPrice[df.pattern_mark == 1], label='Pattern 1')
    # plt.scatter(df.Timestamp[df.pattern_mark == 2], df.NormPrice[df.pattern_mark == 2], label='Pattern 2')
    # plt.scatter(df.Timestamp[df.pattern_mark == 3], df.NormPrice[df.pattern_mark == 3], label='Pattern 3')
    # plt.scatter(df.Timestamp[df.pattern_mark == 4], df.NormPrice[df.pattern_mark == 4], label='Pattern 4')
    # plt.scatter(df.Timestamp[df.pattern_mark == 5], df.NormPrice[df.pattern_mark == 5], label='Pattern 5')
    # plt.scatter(df.Timestamp[df.pattern_mark == 6], df.NormPrice[df.pattern_mark == 6], label='Pattern 6')
    # plt.scatter(df.Timestamp[df.pattern_mark == 7], df.NormPrice[df.pattern_mark == 7], label='Pattern 7')
    # plt.scatter(df.Timestamp[df.pattern_mark == 10], df.NormPrice[df.pattern_mark == 10], label='Pattern 10')

    # plt.legend()
    # plt.show()


def mark_slope_pattern():
    # instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    # instList = glob.glob(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']

    for inst in instList:
        print("Processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

        df['slope_mark'] = np.nan

        peaks_index_list = df.index[df.NormJumps.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for curr in peaks_index_list[1:]:
                if curr - prev >= 10:
                    common_delta = df.FixedNormPriceDelta.iloc[np.arange(prev + 1, curr, 1)].mode().values
                    # Mark increase in price
                    if common_delta[0] == 0.03:
                        flag = 1
                        for i in np.arange(prev + 1, curr, 1):
                            if df.FixedNormPriceDelta.iloc[i] == 0:
                                # static for less than 24 hours
                                if df.TimeDelta.iloc[i] < 86400 and flag != 3:
                                    flag = 2
                                else:
                                    flag = 3
                        df.loc[np.arange(prev, curr + 1, 1), 'slope_mark'] = flag

                    # Mark decrease in price
                    elif common_delta[0] == -0.02:
                        flag = 11
                        for i in np.arange(prev + 1, curr, 1):
                            if df.FixedNormPriceDelta.iloc[i] == 0:
                                # static for less than 24 hours
                                if df.TimeDelta.iloc[i] < 86400 and flag != 12:
                                    flag = 12
                                else:
                                    flag = 13
                        df.loc[np.arange(prev, curr + 1, 1), 'slope_mark'] = flag
                prev = curr

        # df.to_csv(inst, index=False)

        plt.plot(df.Timestamp, df.NormPrice, '.:k', label='Norm_Price')
        plt.scatter(df.Timestamp[df.slope_mark == 1], df.NormPrice[df.slope_mark == 1], label='Pattern 1')
        plt.scatter(df.Timestamp[df.slope_mark == 2], df.NormPrice[df.slope_mark == 2], label='Pattern 2')
        plt.scatter(df.Timestamp[df.slope_mark == 3], df.NormPrice[df.slope_mark == 3], label='Pattern 3')
        plt.scatter(df.Timestamp[df.slope_mark == 11], df.NormPrice[df.slope_mark == 11], label='Pattern 11')
        plt.scatter(df.Timestamp[df.slope_mark == 12], df.NormPrice[df.slope_mark == 12], label='Pattern 12')
        plt.scatter(df.Timestamp[df.slope_mark == 13], df.NormPrice[df.slope_mark == 13], label='Pattern 13')

    plt.legend()
    plt.show()


def mark_static_pattern():
    # instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    # instList = glob.glob(instList)

    instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_sn2ne_2xlarge__optimized__vpc__clean2.csv',
                './alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean2.csv',
                './alibaba_work/cn-beijing_full_traces/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean2.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean2.csv']

    for inst in instList:
        print("Processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        length = df.TimeInSeconds.count()

        index = 0
        while index < (length - 8):
            if df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 8] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[
                index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8]:
                df.loc[[index], 'pattern_mark'] = 23
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7,
                        index + 8], 'pattern_mark'] = -23
                index += 8
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[
                index + 5] > df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7]:
                df.loc[[index], 'pattern_mark'] = 23
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7], 'pattern_mark'] = -23
                index += 7
            else:
                index += 1

        # prev_price = df.NormPrice.iloc[0]
        for i in range(1, length):
            if (df.NormPrice.iloc[i] == df.NormPrice.iloc[i - 1]) and pd.isna(df.pattern_mark.iloc[i]) and pd.isna(
                    df.pattern_mark.iloc[i - 1]):
                df.loc[[i - 1, i], 'pattern_mark'] = 0

        # print(df.head(50))

        # df.to_csv(inst, index=False)

        plt.plot(df.Timestamp, df.NormPrice, '.:', label=inst)
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 21], df.NormPrice[abs(df.pattern_mark) == 21],
                    label='Pattern 21')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 22], df.NormPrice[abs(df.pattern_mark) == 22],
                    label='Pattern 22')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 23], df.NormPrice[abs(df.pattern_mark) == 23],
                    label='Pattern 23')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 0], df.NormPrice[abs(df.pattern_mark) == 0], label='Pattern 0')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 1], df.NormPrice[abs(df.pattern_mark) == 1], label='Pattern 1')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 2], df.NormPrice[abs(df.pattern_mark) == 2], label='Pattern 2')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 3], df.NormPrice[abs(df.pattern_mark) == 3], label='Pattern 3')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 4], df.NormPrice[abs(df.pattern_mark) == 4], label='Pattern 4')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 5], df.NormPrice[abs(df.pattern_mark) == 5], label='Pattern 5')

        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 6], df.NormPrice[abs(df.pattern_mark) == 6], label='Pattern 6')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 7], df.NormPrice[abs(df.pattern_mark) == 7], label='Pattern 7')

        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 11], df.NormPrice[abs(df.pattern_mark) == 11],
                    label='Pattern 11')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 14], df.NormPrice[abs(df.pattern_mark) == 14],
                    label='Pattern 14')

    # plt.legend()
    plt.show()


def mark_first_epoch_patterns():
    # instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    # instList = "F:/alibaba_work/*_full_traces_updated/*clean_2.csv"
    # instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean_2.csv']
    instList = [
        'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    # f = open("./alibaba_work/feature_files/pattern_summary_all_traces.csv", "w")
    # f.write("inst,pat_1,pat_2,pat_3,pat_4,pat_5,pat_6,pat_7,pat_11,pat_14\n")

    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        # df = df[(df.Timestamp > pd.to_datetime("8/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("8/5/2019 00:00:00 AM"))]
        # df.reset_index(drop=True, inplace=True)

        # df['TimeInHours'] = df.TimeInSeconds / 3600

        df['pattern_mark'] = np.nan

        length = df.TimeInSeconds.count()
        print(length)

        # pat_1 = pat_2 = pat_3 = pat_4 = pat_5 = pat_6 = pat_7 = pat_11 = pat_14 = 0
        index = 0
        while index < (length - 14):
            # 5 up 8 down
            if not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 13]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 13] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[
                index + 7] > df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9] > df['NormPrice'].iloc[
                index + 10] > df['NormPrice'].iloc[index + 11] > df['NormPrice'].iloc[index + 12] > \
                    df['NormPrice'].iloc[index + 13]:
                df.loc[[index], 'pattern_mark'] = 15
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8, index + 9,
                     index + 10, index + 11, index + 12, index + 13], 'pattern_mark'] = -15
                index += 13
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 12]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 12] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[
                index + 7] > df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9] > df['NormPrice'].iloc[
                index + 10] > df['NormPrice'].iloc[index + 11] > df['NormPrice'].iloc[index + 12]:
                df.loc[[index], 'pattern_mark'] = 15
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8, index + 9,
                     index + 10, index + 11, index + 12], 'pattern_mark'] = -15
                index += 12

            # 4 up 6 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 11]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 11] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] == df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[
                index + 7] > df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9] > df['NormPrice'].iloc[
                index + 10] > df['NormPrice'].iloc[index + 11]:
                df.loc[[index], 'pattern_mark'] = 14
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8, index + 9,
                     index + 10, index + 11], 'pattern_mark'] = -14
                index += 11
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 10]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 10] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[
                index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[
                index + 9] > df['NormPrice'].iloc[index + 10]:
                df.loc[[index], 'pattern_mark'] = 14
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8, index + 9,
                     index + 10], 'pattern_mark'] = -14
                index += 10

            # 3 up 4 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 8]) and \
                     df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 8] and \
                     df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                     df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 4] and \
                     df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[
                         index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8]:
                df.loc[[index], 'pattern_mark'] = 13
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7,
                        index + 8], 'pattern_mark'] = -13
                index += 8
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 7]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[
                index + 5] > df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7]:
                df.loc[[index], 'pattern_mark'] = 13
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7], 'pattern_mark'] = -13
                index += 7

            # 2 up 3 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 6]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] == \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[
                index + 5] > df['NormPrice'].iloc[index + 6]:
                df.loc[[index], 'pattern_mark'] = 12
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6], 'pattern_mark'] = -12
                # pat_14 += 1
                index += 6
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 5]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] and \
                    df['NormPrice'].iloc[index + 2] > df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[
                index + 4] > df['NormPrice'].iloc[index + 5]:
                df.loc[[index], 'pattern_mark'] = 12
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5], 'pattern_mark'] = -12
                # pat_14 += 1
                index += 5

            else:
                index += 1

        peaks_index_list = df.index[df.PeakInfo.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for i, curr in enumerate(peaks_index_list[1:-2], 1):
                # print(i)
                next_val = peaks_index_list[i + 2]
                # print(prev, df['NormPrice'].iloc[prev], curr, df['NormPrice'].iloc[curr], next_val, df['NormPrice'].iloc[next_val])
                # print(peaks_index_list[i:])
                if curr - prev >= 7 and next_val - curr >= 7 and \
                        df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next_val] and df.TimeDelta.iloc[
                    next_val] < 172800:
                    # print("in here!")
                    common_delta_inc = df.FixedNormPriceDelta.iloc[np.arange(prev + 1, curr, 1)].mode().values
                    common_delta_dec = df.FixedNormPriceDelta.iloc[np.arange(curr + 1, next_val, 1)].mode().values
                    if common_delta_inc[0] == 0.03 and common_delta_dec[0] == -0.02:
                        df.loc[[prev], 'pattern_mark'] = 16
                        df.loc[np.arange(prev + 1, next_val + 1, 1), 'pattern_mark'] = -16

                prev = curr

        # Mark pattern of one jump in price and then a gradual and long price decrease
        peaks_index_list = df.index[df.PeakInfo.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for i, curr in enumerate(peaks_index_list[1:-1], 1):
                next_val = peaks_index_list[i + 1]
                if df["pattern_mark"].iloc[curr] == np.nan and curr - prev == 1 and next_val - curr > 7 and \
                        df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next_val] and df.TimeDelta.iloc[
                    next_val] < 172800:
                    common_delta_dec = df.FixedNormPriceDelta.iloc[np.arange(curr + 1, next_val, 1)].mode().values
                    if common_delta_dec[0] == -0.02:
                        df.loc[[prev], 'pattern_mark'] = 17
                        df.loc[np.arange(prev + 1, next_val + 1, 1), 'pattern_mark'] = -17

                prev = curr

        # df.to_csv(inst, index=False)

        plt.plot(df.Timestamp, df.NormPrice, '.:k')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 12], df.NormPrice[abs(df.pattern_mark) == 12],
                    label='Pattern 12')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 13], df.NormPrice[abs(df.pattern_mark) == 13],
                    label='Pattern 13')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 14], df.NormPrice[abs(df.pattern_mark) == 14],
                    label='Pattern 14')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 15], df.NormPrice[abs(df.pattern_mark) == 15],
                    label='Pattern 15')
        plt.scatter(df.Timestamp[abs(df.pattern_mark) == 16], df.NormPrice[abs(df.pattern_mark) == 16],
                    label='Pattern 16')

    plt.legend()
    plt.show()


def mark_second_epoch_patterns():
    # instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*clean.csv"
    instList = glob.glob(instList)
    total_count = len(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean2.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean2.csv']

    # f = open("./alibaba_work/feature_files/pattern_summary_all_traces.csv", "w")
    # f.write("inst,pat_1,pat_2,pat_3,pat_4,pat_5,pat_6,pat_7,pat_11,pat_14\n")

    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        # df = df[(df.Timestamp > pd.to_datetime("8/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("8/5/2019 00:00:00 AM"))]
        # df.reset_index(drop=True, inplace=True)

        # df['TimeInHours'] = df.TimeInSeconds / 3600

        # df['pattern_mark'] = np.nan

        length = df.TimeInSeconds.count()
        print(length)
        # pat_1 = pat_2 = pat_3 = pat_4 = pat_5 = pat_6 = pat_7 = pat_11 = pat_14 = 0
        index = 0
        while index < (length - 10):

            # 7 up 3 down
            if not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 10]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 10] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < \
                    df['NormPrice'].iloc[index + 6] < df['NormPrice'].iloc[index + 7] == df['NormPrice'].iloc[
                index + 8] and \
                    df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9] > df['NormPrice'].iloc[
                index + 10]:
                df.loc[[index], 'pattern_mark'] = 27
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8,
                        index + 9, index + 10], 'pattern_mark'] = -27
                # pat_7 += 1
                index += 10
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 9]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 9] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < \
                    df['NormPrice'].iloc[index + 6] < df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9]:
                df.loc[[index], 'pattern_mark'] = 27
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8,
                        index + 9], 'pattern_mark'] = -27
                # pat_7 += 1
                index += 9

            # 6 up 3 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 9]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 9] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < \
                    df['NormPrice'].iloc[index + 6] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9]:
                df.loc[[index], 'pattern_mark'] = 26
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7,
                        index + 8, index + 9], 'pattern_mark'] = -26
                # pat_6 += 1
                index += 9
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 8]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 8] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < \
                    df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8]:
                df.loc[[index], 'pattern_mark'] = 26
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7,
                        index + 8], 'pattern_mark'] = -26
                # pat_6 += 1
                index += 8

            # 5 up 1 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 7]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7]:
                df.loc[[index], 'pattern_mark'] = 25
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7], 'pattern_mark'] = -25
                # pat_5 += 1
                index += 7
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 6]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6]:
                df.loc[[index], 'pattern_mark'] = 25
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6], 'pattern_mark'] = -25
                # pat_5 += 1
                index += 6

            # 4 up 1 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 6]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] == df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6]:
                df.loc[[index], 'pattern_mark'] = 24
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6], 'pattern_mark'] = -24
                # pat_4 += 1
                index += 6
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 5]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5]:
                df.loc[[index], 'pattern_mark'] = 24
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5], 'pattern_mark'] = -24
                # pat_4 += 1
                index += 5

            # 3 up 1 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 5]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5]:
                df.loc[[index], 'pattern_mark'] = 23
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5], 'pattern_mark'] = -23
                # pat_3 += 1
                index += 5
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 4]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4]:
                df.loc[[index], 'pattern_mark'] = 23
                df.loc[[index + 1, index + 2, index + 3, index + 4], 'pattern_mark'] = -23
                # pat_3 += 1
                index += 4

            # 2 up 1 down
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 4]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] == \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4]:
                df.loc[[index], 'pattern_mark'] = 22
                df.loc[[index + 1, index + 2, index + 3, index + 4], 'pattern_mark'] = -22
                # pat_2 += 1
                index += 4
            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 3]) and \
                    df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] and \
                    df['NormPrice'].iloc[index + 2] > df['NormPrice'].iloc[index + 3]:
                df.loc[[index], 'pattern_mark'] = 22
                df.loc[[index + 1, index + 2, index + 3], 'pattern_mark'] = -22
                # pat_2 += 1
                index += 3

            elif not pd.isna(df.PeakInfo.iloc[index]) and not pd.isna(df.PeakInfo.iloc[index + 2]) and \
                    df['TimeInSeconds'].iloc[index] == (df['TimeInSeconds'].iloc[index + 1] - 3600) and \
                    df['TimeInSeconds'].iloc[index] == (df['TimeInSeconds'].iloc[index + 2] - 7200) and \
                    df['NormPrice'].iloc[index] == (df['NormPrice'].iloc[index + 2]):
                if df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1]:
                    df.loc[[index], 'pattern_mark'] = 1
                    df.loc[[index + 1, index + 2], 'pattern_mark'] = -1
                elif df['NormPrice'].iloc[index] > df['NormPrice'].iloc[index + 1]:
                    df.loc[[index], 'pattern_mark'] = 2
                    df.loc[[index + 1, index + 2], 'pattern_mark'] = -2
                # pat_1 += 1
                index += 2

            else:
                index += 1

        peaks_index_list = df.index[df.PeakInfo.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for i, curr in enumerate(peaks_index_list[1:-2], 1):
                next_val = peaks_index_list[i + 2]
                if pd.isna(df["pattern_mark"].iloc[curr]) and curr - prev > 7 and next_val - curr < 7 and \
                        df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next_val] and df.TimeDelta.iloc[
                    next_val] < 172800:
                    # print("in here!")
                    common_delta_inc = df.FixedNormPriceDelta.iloc[np.arange(prev + 1, curr, 1)].mode().values
                    # common_delta_dec = df.FixedNormPriceDelta.iloc[np.arange(curr + 1, next_val, 1)].mode().values
                    if common_delta_inc[0] == 0.03:  # and common_delta_dec[0] == -0.02:
                        df.loc[[prev], 'pattern_mark'] = 28
                        df.loc[np.arange(prev + 1, next_val + 1, 1), 'pattern_mark'] = -28

                prev = curr

        for i in range(1, length):
            if (df.NormPrice.iloc[i] == df.NormPrice.iloc[i - 1]) and pd.isna(df.pattern_mark.iloc[i]) and pd.isna(
                    df.pattern_mark.iloc[i - 1]):
                df.loc[[i - 1, i], 'pattern_mark'] = 0

        df.to_csv(inst, index=False)

    #     plt.plot(df.Timestamp, df.NormPrice, '.:k')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 0], df.NormPrice[abs(df.pattern_mark) == 0],
    #                 label='Pattern 0')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 1], df.NormPrice[abs(df.pattern_mark) == 1],
    #                 label='Pattern 1')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 2], df.NormPrice[abs(df.pattern_mark) == 2],
    #                 label='Pattern 2')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 22], df.NormPrice[abs(df.pattern_mark) == 22],
    #                 label='Pattern 22')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 23], df.NormPrice[abs(df.pattern_mark) == 23],
    #                 label='Pattern 23')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 24], df.NormPrice[abs(df.pattern_mark) == 24],
    #                 label='Pattern 24')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 25], df.NormPrice[abs(df.pattern_mark) == 25],
    #                 label='Pattern 25')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 26], df.NormPrice[abs(df.pattern_mark) == 26],
    #                 label='Pattern 26')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 27], df.NormPrice[abs(df.pattern_mark) == 27],
    #                 label='Pattern 27')
    #     plt.scatter(df.Timestamp[abs(df.pattern_mark) == 28], df.NormPrice[abs(df.pattern_mark) == 28],
    #                 label='Pattern 28')
    #
    # plt.legend()
    # plt.show()


def mark_seven_boom_pattern():
    instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    instList = glob.glob(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']

    # f = open("./alibaba_work/feature_files/pattern_summary_all_traces.csv", "w")
    # f.write("inst,pat_1,pat_2,pat_3,pat_4,pat_5,pat_6,pat_7,pat_11,pat_14\n")

    for inst in instList:
        print("Processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]
        # df['pattern_mark'] = np.nan

        length = df.TimeInSeconds.count()
        print(length)

        # pat_1 = pat_2 = pat_3 = pat_4 = pat_5 = pat_6 = pat_7 = pat_11 = pat_14 = 0
        index = 0
        while index < (length - 10):

            if df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 10] and \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 8] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] == \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < df['NormPrice'].iloc[index + 6] < df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8] == df['NormPrice'].iloc[
                index + 9] > df['NormPrice'].iloc[index + 10]:
                df.loc[[index], 'pattern_mark'] = 14
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8,
                        index + 9, index + 10], 'pattern_mark'] = -14
                # pat_14 += 1
                index += 10

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] == \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5] == df['NormPrice'].iloc[
                index + 6] > df['NormPrice'].iloc[index + 7]:
                df.loc[[index], 'pattern_mark'] = 11
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7], 'pattern_mark'] = -11
                # pat_11 += 1
                index += 7

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 10] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < df['NormPrice'].iloc[index + 6] < df['NormPrice'].iloc[index + 7] == df['NormPrice'].iloc[
                index + 8] and \
                    df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9] > df['NormPrice'].iloc[
                index + 10]:
                df.loc[[index], 'pattern_mark'] = 7
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7, index + 8, index + 9,
                     index + 10], 'pattern_mark'] = -7
                # pat_7 += 1
                index += 10

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 8] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8]:
                df.loc[[index], 'pattern_mark'] = 6
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7,
                        index + 8], 'pattern_mark'] = -6
                # pat_6 += 1
                index += 8


            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7]:
                df.loc[[index], 'pattern_mark'] = 5
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6, index + 7], 'pattern_mark'] = -5
                # pat_5 += 1
                index += 7

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6]:
                df.loc[[index], 'pattern_mark'] = 5
                df.loc[
                    [index + 1, index + 2, index + 3, index + 4, index + 5, index + 6], 'pattern_mark'] = -5
                # pat_5 += 1
                index += 6

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] == df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6]:
                df.loc[[index], 'pattern_mark'] = 4
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5, index + 6], 'pattern_mark'] = -4
                # pat_4 += 1
                index += 6

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5]:
                df.loc[[index], 'pattern_mark'] = 4
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5], 'pattern_mark'] = -4

                # pat_4 += 1
                index += 5

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5]:
                df.loc[[index], 'pattern_mark'] = 3
                df.loc[[index + 1, index + 2, index + 3, index + 4, index + 5], 'pattern_mark'] = -3
                # pat_3 += 1
                index += 5

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4]:
                df.loc[[index], 'pattern_mark'] = 3
                df.loc[[index + 1, index + 2, index + 3, index + 4], 'pattern_mark'] = -3
                # pat_3 += 1
                index += 4

            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] == \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4]:
                df.loc[[index], 'pattern_mark'] = 2
                df.loc[[index + 1, index + 2, index + 3, index + 4], 'pattern_mark'] = -2
                # pat_2 += 1
                index += 4

            elif df['TimeInSeconds'].iloc[index] == (df['TimeInSeconds'].iloc[index + 1] - 3600) and \
                    df['TimeInSeconds'].iloc[index] == (df['TimeInSeconds'].iloc[index + 2] - 7200) and \
                    df['NormPrice'].iloc[index] == (df['NormPrice'].iloc[index + 2]):
                df.loc[[index], 'pattern_mark'] = 1
                df.loc[[index + 1, index + 2], 'pattern_mark'] = -1
                # pat_1 += 1
                index += 2

            else:
                index += 1

        # f.write(str(inst) + "," + str(pat_1) + "," + str(pat_2) + "," + str(pat_3) + "," + str(pat_4) + "," + str(
        #     pat_5) + "," + str(pat_6) + "," + str(pat_7) + "," + str(pat_11) + "," + str(pat_14) + "\n")

        df.to_csv(inst, index=False)

        # print(inst, ": ", set(drop_slope))

        # plt.plot(df.Timestamp, df.NormPrice, '.:k', label='Spot')
        # plt.scatter(df.Timestamp[df.NormJumps.notnull()], df.NormPrice[df.NormJumps.notnull()], label='Jumps')
        # plt.scatter(df.Timestamp[df.PeakInfo.notnull()], df.NormPrice[df.PeakInfo.notnull()], label='Peaks')
        # plt.scatter(df.Timestamp[df.pattern_mark == 1], df.NormPrice[df.pattern_mark == 1], label='Pattern 1')
        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 2], df.NormPrice[abs(df.pattern_mark) == 2], label='Pattern 2')
        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 3], df.NormPrice[abs(df.pattern_mark) == 3], label='Pattern 3')
        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 4], df.NormPrice[abs(df.pattern_mark) == 4], label='Pattern 4')
        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 5], df.NormPrice[abs(df.pattern_mark) == 5], label='Pattern 5')

        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 6], df.NormPrice[abs(df.pattern_mark) == 6], label='Pattern 6')
        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 7], df.NormPrice[abs(df.pattern_mark) == 7], label='Pattern 7')

        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 11], df.NormPrice[abs(df.pattern_mark) == 11],
        #             label='Pattern 11')
        # plt.scatter(df.Timestamp[abs(df.pattern_mark) == 14], df.NormPrice[abs(df.pattern_mark) == 14],
        #             label='Pattern 14')

    # f.close()
    # plt.legend()
    # plt.show()


def count_patterns_per_month():
    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/ap-southeast-3_full_traces_updated/ap_southeast_3a__ecs_c6_4xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean2.csv']
    total_count = len(instList)

    all_data_list = []

    pattern_list = [1001.02, 1001.03, 1002.03, 1001.1, 1045.89, 1045.9, 2001.06, 2003.06, 3001.07, 3001.09, 3001.08,
                    3004.08, 4001.12, 4006.12, 5001.13, 5001.15, 5001.14, 5007.14, 7002.2, 14003.42, 14021.42, 18027.54,
                    18004.54, 1]
    pattern_label = ["(1, 1, 0.02)", "(1, 1, 0.03)", "(1, 2, 0.03)", "(1, 1, 0.1)", "(1, 45, 0.89)", "(1, 45, 0.9)",
                     "(2, 1, 0.06)", "(2, 3, 0.06)", "(3, 1, 0.07)", "(3, 1, 0.09)", "(3, 1, 0.08)", "(3, 4, 0.08)",
                     "(4, 1, 0.12)", "(4, 6, 0.12)", "(5, 1, 0.13)", "(5, 1, 0.15)", "(5, 1, 0.14)", "(5, 7, 0.14)",
                     "(7, 2, 0.2)", "(14, 3, 0.42)", "(14, 21, 0.42)", "(18, 27, 0.54)", "(18, 4, 0.54)", "Other"]

    colors = ['blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'lime', 'lightblue', 'salmon',
              'lightgreen', 'magenta', 'yellow', 'plum', 'navy',
              'aqua', 'turquoise', 'violet', 'tan', 'coral', 'gold', 'ivory', 'silver', 'white']

    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

        df['month_year'] = df['Timestamp'].dt.to_period('M')
        df['month_year'] = df['month_year'].dt.strftime('%Y-%m')

        df['pattern_mark_4'] = df.pattern_mark_4.fillna(0)
        # print(df.pattern_mark_3[~abs(df['pattern_mark_3']).isin(pattern_list)].count())
        # print(df.pattern_mark_3.head(40))

        grouped = df.groupby('month_year')
        for name, group in grouped:

            pat_count_list = []
            for pat in pattern_list:
                pat_count_list.append(group.pattern_mark_4[abs(group.pattern_mark_4) == pat].count())

            # pat_count_list.append(group.pattern_mark_3[~abs(group['pattern_mark_3']).isin(pattern_list)].count())

            # pat_count_list.append(group.Timestamp.count())
            # all_data_list.append(tuple(pat_count_list))
            all_data_list.append((str(name), pat_count_list, group.Timestamp.count()))

        # print(all_data_list)

    d = {x: [0] * (len(pattern_list) + 1) for x, *y in all_data_list}
    for name, pat_count_list, price_change_total in all_data_list:
        for i, pat in enumerate(pat_count_list):
            d[name][i] += pat
        d[name][-1] += price_change_total

    hatches = ['//', '\\', '||', '--', '+', 'x', 'o', 'O', '.', '*', 'xx', 'oo', 'OO', '..', '**', '\\/...', '+o', 'x*',
               '/o', 'O.', '|*', 'O|', '++', '++..', '////', '\\\\']

    bar_width = 0.5
    #
    y_offset = np.zeros(len(d.keys()))
    #
    for i, color in enumerate(pattern_list):
        month_year = []
        for key, item in d.items():
            # print(key)
            # print(item)
            month_year.append((key, (item[i] / item[-1])))

        month_year = sorted(month_year, key=lambda x: x[0])
        year, vals = zip(*month_year)
        print(year)
        print(vals)
        #   , color=colors[i]
        plt.bar(year, vals, bar_width, bottom=y_offset, color=colors[i], hatch=hatches[i], label=pattern_label[i])
        # plt.bar(year, vals, bar_width, bottom=y_offset, label=pattern_list[i])
        y_offset = y_offset + vals
    #
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), title="(Up, Down, Height)")
    plt.xlabel("Month - Year")
    plt.xticks(rotation=90)
    plt.ylabel("Fraction of pattern occurrences")
    plt.show()


def create_ecdf_first_last_activity():
    file = "../../Alibaba_log_files/Correlation_tables/first_and_last_activity_non_static_insts.csv"
    df = pd.read_csv(file)
    df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
    df = df.dropna()

    # lastActivity_list = df.last_activity_date.tolist()
    # lastActivity_count = [lastActivity_list.count(x) for x in set(lastActivity_list)]
    # lastActivity_count.sort()
    # ecdf = ECDF(lastActivity_count)
    # y = ecdf(lastActivity_count)
    #
    # plt.step(lastActivity_count, y, c='red', where='post', label='last activity')
    #
    # firstActivity_list = df.first_activity_date.tolist()
    # firstActivity_count = [firstActivity_list.count(x) for x in set(firstActivity_list)]
    # firstActivity_count.sort()
    # ecdf = ECDF(firstActivity_count)
    # y = ecdf(firstActivity_count)
    #
    # plt.step(firstActivity_count, y, c='black', where='post', label='first activity')

    df['last_activity_date'] = pd.to_datetime(df['last_activity_date'])
    df['last_month_activity'] = df.last_activity_date.dt.to_period('M')

    last_month_activity_list = df.last_month_activity.tolist()
    last_month_activity_count = [last_month_activity_list.count(x) for x in set(last_month_activity_list)]
    last_month_activity_count.sort()
    ecdf = ECDF(last_month_activity_count)
    y = ecdf(last_month_activity_count)

    plt.step(last_month_activity_count, y, c='black', where='post', label='last month activity')

    plt.title("ECDF of number of instances - start & stop activity")
    plt.legend()


def find_instances_dropped_between_months():
    df1 = pd.read_csv("../../Alibaba_log_files/Correlation_tables/best_all_instances_weighted_august_2020_corr.csv")
    df1 = df1[['inst']]
    # df1.set_index("inst", inplace=True)
    df2 = pd.read_csv("../../Alibaba_log_files/Correlation_tables/all_instances_September_2020_corr_matrix_ordered.csv")
    df2 = df2[['inst']]
    # df2.set_index("inst", inplace=True)

    result = pd.merge(df1, df2, on="inst", how="outer", indicator=True, suffixes=('_august', '_september'))
    result.to_csv("../../Alibaba_log_files/Correlation_tables/instances_dropped_in_september.csv")


def find_artificial_patterns():
    fig = plt.figure(figsize=set_size())
    count = 0
    all_count = 0
    inst_list = []
    base_list = []
    diff_price_list = []
    spot_list = []
    diff_spot_list = []
    # base_list_dec = []
    # diff_price_list_dec = []

    instList = "F:/alibaba_work/*_full_traces_updated/*__clean.csv"
    instList = glob.glob(instList)
    # print("Num instances: ", len(instList))
    all_count += len(instList)

    # instList = ['./alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_ic5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn1ne_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_g6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn1ne_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn1ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn1ne_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_g5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_r5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn2ne_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_se1ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_g6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_ic5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_ic5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_g6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r5_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_ic5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_ic5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_ic5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_c6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_se1ne_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_ic5_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_ic5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn2ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_se1ne_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_sn2ne_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_r6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_r6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_r6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_c5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_g5_large__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_g__ecs_ic5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_g5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_g5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_r5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_r5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_a__ecs_sn2_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_ic5_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_ic5_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_b__ecs_ic5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_r6_large__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_a__ecs_sn2_large__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_a__ecs_sn2_medium__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_sn1ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c6_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_sn2ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_sn2_medium__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_sn1ne_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1ne_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_sn1_medium__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1ne_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_c__ecs_c6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1ne_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-zhangjiakou_2020_traces/cn_zhangjiakou_a__ecs_se1ne_3xlarge__optimized__vpc__clean.csv']

    pattern_list = [[pd.to_datetime('2020-07-22 02:00:00'), pd.to_datetime('2020-07-22 03:00:00'),
                     pd.to_datetime('2020-07-22 04:00:00')]]
    # pattern_list = [[pd.to_datetime('2020-08-28 14:00:00'), pd.to_datetime('2020-08-28 15:00:00'),
    #                  pd.to_datetime('2020-08-28 16:00:00')]]

    for inst in instList:
        # print("processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df[(df.Timestamp >= pd.to_datetime('2020-07-22 00:00:00')) & (df.Timestamp < pd.to_datetime('2020-07-22 06:00:00'))]
        # df = df[(df.Timestamp >= pd.to_datetime('2020-08-28 12:00:00')) & (
        #             df.Timestamp < pd.to_datetime('2020-08-28 18:00:00'))]
        df.reset_index(drop=True, inplace=True)

        # plt.plot(df.Timestamp, df.NormPrice, c='black')

        pattern_index_list = []
        for pat in pattern_list:
            index_list = df.index[df.Timestamp.isin(pat)].tolist()
            # print(index_list)
            pattern_index_list.append(index_list)
            print(index_list)
            if len(index_list) == 3:
                if (index_list[0] != 0 and index_list[2] != df.index[-1] and df.NormPrice.loc[index_list[0] - 1] ==
                    df.NormPrice.loc[index_list[0]] ==
                    df.NormPrice.loc[index_list[2]] == df.NormPrice.loc[index_list[2] + 1]) \
                        or df.NormPrice.loc[index_list[0]] == df.NormPrice.loc[index_list[2]]:
                    count += 1
                    inst_list.append(inst)
                    spot_list.append(df.SpotPrice.loc[index_list[0]])
                    base_list.append(df.NormPrice.loc[index_list[0]])
                    diff_spot_list.append(df.SpotPrice.loc[index_list[1]] - df.SpotPrice.loc[index_list[0]])
                    diff_price_list.append(df.NormPrice.loc[index_list[1]] - df.NormPrice.loc[index_list[0]])

                    # if df.NormPrice.loc[index_list[1]] > df.NormPrice.loc[index_list[0]]:
                    #     # spot_list.append(df.SpotPrice.loc[index_list[0]])
                    #     base_list.append(df.NormPrice.loc[index_list[0]])
                    #     # diff_spot_list.append(df.SpotPrice.loc[index_list[1]] - df.SpotPrice.loc[index_list[0]])
                    #     diff_price_list.append(df.NormPrice.loc[index_list[1]] - df.NormPrice.loc[index_list[0]])
                    #
                    #
                    #     # spot_price_cents = df.NormPrice.loc[index_list[0]] * 100
                    #     # # print("spot price cents: ", spot_price_cents)
                    #     # fixed_peak_cents = spot_price_cents * 1.25
                    #     # # print("125% spot base price: ", fixed_peak_cents)
                    #     # fixed_peak_cents = min(fixed_peak_cents, (df.OriginPrice.loc[index_list[1]] * 100))
                    #     # # print("min with Origin price: ", fixed_peak_cents)
                    #     # fixed_peak_cents = round(fixed_peak_cents, 1)
                    #     # # print("rounded: ", fixed_peak_cents)
                    #     # fixed_percent = (fixed_peak_cents - spot_price_cents) / spot_price_cents
                    #     # # print("fixed percent: ", fixed_percent)
                    #     # base_list.append(fixed_percent)
                    #     # percent = ((df.NormPrice.loc[index_list[1]] * 100) - spot_price_cents) / spot_price_cents
                    #     # diff_price_list.append(percent)
                    #     # print("original percent: ", percent)
                    #     # print("==============================")
                    #
                    # else:
                    #     base_list.append(df.NormPrice.loc[index_list[0]])
                    #     diff_price_list.append(df.NormPrice.loc[index_list[0]] - df.NormPrice.loc[index_list[1]])
                    #     # plt.plot(df.Timestamp, df.NormPrice, '.-')

    plt.scatter(base_list, diff_price_list, color="blue")

    results = pd.DataFrame(list(zip(inst_list, base_list, diff_price_list, spot_list, diff_spot_list)),
                           columns=['inst', 'normalized_base_price', 'normalized_height', 'base_price', 'height'])
    print(results)
    results.to_csv("../../Alibaba_log_files/july_chevron_data_check.csv", index=False)

    # plt.scatter(base_list, diff_price_list, color='red', label='Normalized price')
    # z = np.polyfit(base_list, diff_price_list, 1)
    # p = np.poly1d(z)
    # plt.plot(base_list, p(base_list), "r--")
    # the line equation:
    # print("y=%.6fx+(%.6f)" % (z[0], z[1]))
    #
    # plt.scatter(spot_list, diff_spot_list, color='blue', label='Spot price')
    # z = np.polyfit(spot_list, diff_spot_list, 1)
    # p = np.poly1d(z)
    # plt.plot(spot_list, p(spot_list), "b--")
    # the line equation:
    # print("y=%.6fx+(%.6f)" % (z[0], z[1]))
    #
    # plt.scatter(base_list_dec, diff_price_list_dec, label = 'Down')
    # plt.title("28/08/2020 Chevron height as a function of the base price")
    plt.ylabel("Chevron height (normalized)")
    plt.xlabel("Chevron base price (normalized)")
    # plt.legend()

    # plt.title("Normalized spot price chevron - 28/08/2020")
    # plt.xlabel("Timestamp")
    # plt.ylabel("Normalized price")
    plt.show()
    # print(all_count)
    # print(count)


def time_held_in_pattern():
    instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    instList = glob.glob(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean2.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean2.csv']

    # f = open("./alibaba_work/feature_files/pattern_summary_all_traces.csv", "w")
    # f.write("inst,pat_1,pat_2,pat_3,pat_4,pat_5,pat_6,pat_7,pat_11,pat_14\n")
    pat_2 = []
    pat_3 = []
    pat_4 = []
    pat_5 = []
    pat_6 = []
    pat_7 = []
    pat_23 = []

    for inst in instList:
        print("Processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df['TimeInHours'] = df.TimeInSeconds / 3600

        length = df.TimeInSeconds.count()
        print(length)

        index = 0
        while index < (length - 10):
            # Pattern 23 - 3 increase 4 decrease
            if df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 8] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[
                index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8]:
                pat_23.append(df.TimeInHours.iloc[index + 8] - df.TimeInHours.iloc[index])
                index += 8

            # Pattern 23 - 3 increase 4 decrease (no holding price in peak)
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[
                index + 5] > df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7]:
                pat_23.append(df.TimeInHours.iloc[index + 8] - df.TimeInHours.iloc[index])
                index += 7

            # Pattern 7 - 7 increase keep price 2 decrease
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 10] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < \
                    df['NormPrice'].iloc[index + 6] < df['NormPrice'].iloc[index + 7] == df['NormPrice'].iloc[
                index + 8] and \
                    df['NormPrice'].iloc[index + 8] > df['NormPrice'].iloc[index + 9] > df['NormPrice'].iloc[
                index + 10]:
                pat_7.append(df.TimeInHours.iloc[index + 10] - df.TimeInHours.iloc[index])
                index += 10

            # Pattern 6 - 6 increase keep price 2 decrease (no holding price in peak)
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 8] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] < df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7] > df['NormPrice'].iloc[index + 8]:
                pat_6.append(df.TimeInHours.iloc[index + 8] - df.TimeInHours.iloc[index])
                index += 8

            # Pattern 5 - 5 increase keep price 1 decrease
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 7] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index + 6] > df['NormPrice'].iloc[index + 7]:
                pat_5.append(df.TimeInHours.iloc[index + 7] - df.TimeInHours.iloc[index])
                index += 7

            # Pattern 5 - 5 increase keep price 1 decrease (no holding price in peak)
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] < df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6]:
                pat_5.append(df.TimeInHours.iloc[index + 6] - df.TimeInHours.iloc[index])
                index += 6

            # Pattern 4 - 4 increase keep price 1 decrease
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 6] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] == df['NormPrice'].iloc[
                index + 5] and \
                    df['NormPrice'].iloc[index + 5] > df['NormPrice'].iloc[index + 6]:
                pat_4.append(df.TimeInHours.iloc[index + 6] - df.TimeInHours.iloc[index])
                if (df.TimeInHours.iloc[index + 6] - df.TimeInHours.iloc[index]) == 233.0:
                    print("pat_4: ", df.Timestamp.iloc[index], df.Timestamp.iloc[index + 6])
                index += 6

            # Pattern 4 - 4 increase keep price 1 decrease (no holding price in peak)
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] < df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5]:
                pat_4.append(df.TimeInHours.iloc[index + 5] - df.TimeInHours.iloc[index])
                if (df.TimeInHours.iloc[index + 5] - df.TimeInHours.iloc[index]) == 233.0:
                    print("pat_4: ", df.Timestamp.iloc[index], df.Timestamp.iloc[index + 5])
                index += 5

            # Pattern 3 - 3 increase keep price 1 decrease
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 5] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index + 4] > df['NormPrice'].iloc[index + 5]:
                pat_3.append(df.TimeInHours.iloc[index + 5] - df.TimeInHours.iloc[index])
                index += 5

            # Pattern 3 - 3 increase keep price 1 decrease (no holding price in peak)
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] < \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4]:
                pat_3.append(df.TimeInHours.iloc[index + 4] - df.TimeInHours.iloc[index])
                index += 4

            # Pattern 2 - 2 increase keep price 1 decrease
            elif df['NormPrice'].iloc[index] == df['NormPrice'].iloc[index + 4] and \
                    df['NormPrice'].iloc[index] < df['NormPrice'].iloc[index + 1] < df['NormPrice'].iloc[index + 2] == \
                    df['NormPrice'].iloc[index + 3] and \
                    df['NormPrice'].iloc[index + 3] > df['NormPrice'].iloc[index + 4]:
                pat_2.append(df.TimeInHours.iloc[index + 4] - df.TimeInHours.iloc[index])
                if (df.TimeInHours.iloc[index + 4] - df.TimeInHours.iloc[index]) == 44.0:
                    print("pat_2: ", df.Timestamp.iloc[index], df.Timestamp.iloc[index + 4])
                index += 4

            else:
                index += 1

    for index, l in zip(['pat_2', 'pat_3', 'pat_4', 'pat_5', 'pat_6', 'pat_7', 'pat_23'],
                        [pat_2, pat_3, pat_4, pat_5, pat_6, pat_7, pat_23]):
        print(index, ": ")
        print(set(l))
        print("==================================================================")

        # plt.plot(df.Timestamp,df.NormPrice,".-")
        # plt.show()

        # f.write(str(inst) + "," + str(pat_1) + "," + str(pat_2) + "," + str(pat_3) + "," + str(pat_4) + "," + str(
        #     pat_5) + "," + str(pat_6) + "," + str(pat_7) + "," + str(pat_11) + "," + str(pat_14) + "\n")


def analyze_chevrons():
    file = "./alibaba_work/feature_files/all_chevron_data.csv"
    df = pd.read_csv(file)

    df['date'] = pd.to_datetime(df['date'])
    df = df.sort_values('date')

    df['origin'] = df.base_price / df.normalized_base_price
    df['frac_origin'] = df.height / df.origin
    df['frac_base'] = df.normalized_height / df.normalized_base_price

    fig = plt.figure(figsize=set_size())
    ax = fig.add_subplot(2, 1, 1)
    ax.scatter(df['date'], df['frac_base'])
    ax.set_ylabel("Fraction of the normalized base price")

    ax = fig.add_subplot(2, 1, 2)
    ax.scatter(df['date'], df['frac_origin'])
    ax.set_ylabel("Fraction of the origin price")

    plt.show()


def find_band_width():
    # fig = plt.figure(figsize=set_size())

    # pattern_list = [0, 1, 2, 12, 13, 14, 15, 16, 22, 23, 24, 25, 26, 27, 28]

    instList = "./alibaba_work/*_full_traces/*__clean2.csv"
    instList = glob.glob(instList)
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean2.csv']

    # instList = ['./alibaba_work/us-west-1_full_traces/us_west_1b__ecs_t5_c1m4_2xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/us-east-1_full_traces/us_east_1b__ecs_hfr6_xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/eu-central-1_full_traces/eu_central_1a__ecs_sn2ne_4xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-zhangjiakou_full_traces/cn_zhangjiakou_c__ecs_hfr6_2xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-shenzhen_full_traces/cn_shenzhen_e__ecs_v5_c1m2_large__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-shanghai_full_traces/cn_shanghai_g__ecs_hfr6_xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-qingdao_full_traces/cn_qingdao_c__ecs_hfc5_2xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-hongkong_full_traces/cn_hongkong_c__ecs_t5_c1m4_2xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-huhehaote_full_traces/cn_huhehaote_a__ecs_c5_2xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/ap-southeast-2_full_traces/ap_southeast_2b__ecs_c5_2xlarge__optimized__vpc__clean2.csv']

    # instList = ['./alibaba_work/ap-southeast-5_full_traces/ap_southeast_5b__ecs_c5_xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-beijing_full_traces/cn_beijing_b__ecs_sn2_medium__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-beijing_full_traces/cn_beijing_d__ecs_sn2_large__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-huhehaote_full_traces/cn_huhehaote_a__ecs_ic5_2xlarge__optimized__vpc__clean2.csv']

    # instList = ['./alibaba_work/us-east-1_full_traces/us_east_1b__ecs_hfr6_xlarge__optimized__vpc__clean2.csv',
    #             './alibaba_work/cn-beijing_full_traces/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean2.csv']

    print("Start processing...")
    # base_price_list = []
    # origin_price_list = []
    # percent_from_base_list = []
    # percent_from_origin_list = []
    pat_13_list = []
    pat_14_list = []
    pat_15_list = []
    pat_23_list = []
    pat_24_list = []
    pat_25_list = []

    for inst in instList:
        # print("Processing ", inst)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        # df = df[(df.Timestamp >= pd.to_datetime('3/1/2020 12:00:00 AM')) & (df.Timestamp < pd.to_datetime('5/1/2020  12:00:00 AM'))]
        if not df.empty:
            # df['tmp'] = abs(df.pattern_mark)
            df = df[df.NormJumps.notnull()]
            df.reset_index(drop=True, inplace=True)
            df['height'] = df.NormJumps.diff()
            df['percent_from_base'] = df.height.shift(-1) / df.NormPrice
            df['percent_from_origin'] = df.height.shift(-1) / df.OriginPrice
            # print(df.head(20))

            for i in range(df.NormPrice.count()):
                if df.pattern_mark.iloc[i] == 13:
                    pat_13_list.append((df.SpotPrice.iloc[i] / df.OriginPrice.iloc[i]))
                elif df.pattern_mark.iloc[i] == 14:
                    pat_14_list.append((df.SpotPrice.iloc[i] / df.OriginPrice.iloc[i]))
                elif df.pattern_mark.iloc[i] == 15:
                    pat_15_list.append((df.SpotPrice.iloc[i] / df.OriginPrice.iloc[i]))
                elif df.pattern_mark.iloc[i] == 23:
                    pat_23_list.append((df.SpotPrice.iloc[i] / df.OriginPrice.iloc[i]))
                elif df.pattern_mark.iloc[i] == 24:
                    pat_24_list.append((df.SpotPrice.iloc[i] / df.OriginPrice.iloc[i]))
                elif df.pattern_mark.iloc[i] == 25:
                    pat_25_list.append((df.SpotPrice.iloc[i] / df.OriginPrice.iloc[i]))
                    # base_price_list.append(df.NormPrice.iloc[i])
                    # base_price_list.append(df.OriginPrice.iloc[i])
                    # origin_price_list.append(df.OriginPrice.iloc[i])
                    # percent_from_base_list.append(df.percent_from_base.iloc[i])
                    # percent_from_base_list.append(df.SpotPrice.iloc[i])
                    # percent_from_origin_list.append(df.percent_from_origin.iloc[i])
            # df['percent_base']

            # most_common = df.half_min.mode().values
            # print(inst)
            # print(most_common[0])
            # if len(most_common) != 0:
            #     if most_common[0] == 0.1818181818181818:
            #         print(inst)

        # plt.plot(df.Timestamp, df.NormPrice, '.-', label=inst)
        # df['peak_diff'] = df.PeakInfo.diff()
        # print(df.peak_diff.value_counts())
    # print(list(zip(base_price_list, percent_from_base_list)))
    # plt.scatter(base_price_list, percent_from_base_list, color='black', label='from base')
    # plt.scatter(base_price_list, percent_from_base_list)
    # plt.scatter(origin_price_list, percent_from_origin_list, color ='red', label='from origin')
    # df.NormPrice.hist()

    # plt.title("Pattern 25")
    # plt.xlabel("Origin Price")
    # plt.ylabel("Spot Price")
    # plt.xlim(0.0,2.0)
    # plt.ylim(0.0,2.0)

    pat_13_list.sort()
    ecdf = ECDF(pat_13_list)
    y = ecdf(pat_13_list)

    plt.step(pat_13_list, y, where='post', label='13')

    pat_14_list.sort()
    ecdf = ECDF(pat_14_list)
    y = ecdf(pat_14_list)

    plt.step(pat_14_list, y, where='post', label='14')

    pat_15_list.sort()
    ecdf = ECDF(pat_15_list)
    y = ecdf(pat_15_list)

    plt.step(pat_15_list, y, where='post', label='15')

    pat_23_list.sort()
    ecdf = ECDF(pat_23_list)
    y = ecdf(pat_23_list)

    plt.step(pat_23_list, y, where='post', label='23')

    pat_24_list.sort()
    ecdf = ECDF(pat_24_list)
    y = ecdf(pat_24_list)

    plt.step(pat_24_list, y, where='post', label='24')

    pat_25_list.sort()
    ecdf = ECDF(pat_25_list)
    y = ecdf(pat_25_list)

    plt.step(pat_25_list, y, where='post', label='25')

    plt.legend()
    plt.show()


def draw_patterns():
    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1.5))
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = [
    #     'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean.csv']

    # pattern_list = [1001.02, 1001.03, 1002.03, 1001.1, 1045.89, 1045.9, 2001.06, 2003.06, 3001.07, 3001.09, 3001.08,
    #                 3004.08, 4001.12, 4006.12, 5001.13, 5001.15, 5001.14, 5007.14, 7002.2, 14003.42, 14021.42, 18027.54,
    #                 18004.54, 1]
    # pattern_label = ["(1, 1, 0.02)", "(1, 1, 0.03)", "(1, 2, 0.03)", "(1, 1, 0.1)", "(1, 45, 0.89)", "(1, 45, 0.9)",
    #                  "(2, 1, 0.06)", "(2, 3, 0.06)", "(3, 1, 0.07)", "(3, 1, 0.09)", "(3, 1, 0.08)", "(3, 4, 0.08)",
    #                  "(4, 1, 0.12)", "(4, 6, 0.12)", "(5, 1, 0.13)", "(5, 1, 0.15)", "(5, 1, 0.14)", "(5, 7, 0.14)",
    #                  "(7, 2, 0.2)", "(14, 3, 0.42)", "(14, 21, 0.42)", "(18, 27, 0.54)", "(18, 4, 0.54)", "Other"]

    df = pd.read_csv('F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square_2.csv')
    # df = pd.read_csv('F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv')
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df.NormPrice = np.around(df.NormPrice, 2)
    # df = df.sort_values('Timestamp')
    # df.reset_index(drop=True, inplace=True)

    ax = fig.add_subplot(2, 4, 1)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark_3) == 3004.08], df.NormPrice[abs(df.pattern_mark_3) == 3004.08], color='red')
                # marker=m_list[1], label='Pattern 1')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(3, 4, 0.08)')

    # ax = fig.add_subplot(1, 4, 3)
    # ax.plot(df.Timestamp, df.SpotPrice, ".:", color='black')
    # ax.scatter(df.Timestamp[abs(df.pattern_mark_3) == 2003.06], df.SpotPrice[abs(df.pattern_mark_3) == 2003.06], color='red')
    #             # marker=m_list[2], label='Pattern 2')
    ax.set_xticks([])
    # ax.set_yticks([])
    # ax.set_title('(2, 3, 0.06)')

    df = pd.read_csv('F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square_2.csv')
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df.NormPrice = np.around(df.NormPrice, 2)

    ax = fig.add_subplot(2, 4, 2)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark_3) == 3001.08], df.NormPrice[abs(df.pattern_mark_3) == 3001.08], color='red')
                # marker=m_list[3], label='Pattern 12')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(3, 1, 0.08)')

    ax = fig.add_subplot(2, 4, 3)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark_3) == 5007.14], df.NormPrice[abs(df.pattern_mark_3) == 5007.14], color='red')
                # marker=m_list[4], label='Pattern 13')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(5, 7, 0.14)')

    # ax = fig.add_subplot(2, 4, 7)
    # ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    # ax.scatter(df.Timestamp[abs(df.pattern_mark) == 4007.14], df.NormPrice[abs(df.pattern_mark) == 4007.14],
    #            color='red')
    # # marker=m_list[4], label='Pattern 13')
    #
    # ax.set_xticks([])
    # # ax.set_yticks([])
    # ax.set_title('(4, 7, 0.14)')

    df = pd.read_csv('F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square_2.csv')
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df.NormPrice = np.around(df.NormPrice, 2)
    # df = df.sort_values('Timestamp')
    # df.reset_index(drop=True, inplace=True)

    ax = fig.add_subplot(2, 4, 4)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark_3) == 5001.14], df.NormPrice[abs(df.pattern_mark_3) == 5001.14], color='red')
                # marker=m_list[8], label='Pattern 22')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(5, 1, 0.14)')

    df = pd.read_csv('F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square_2.csv')
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df.NormPrice = np.around(df.NormPrice, 2)
    # df = df.sort_values('Timestamp')
    # df.reset_index(drop=True, inplace=True)

    ax = fig.add_subplot(2, 4, 5)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark) == 2003.07], df.NormPrice[abs(df.pattern_mark) == 2003.07],
               color='red')
    # marker=m_list[8], label='Pattern 22')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(2, 3, 0.07)')

    ax = fig.add_subplot(2, 4, 6)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark) == 1003.07], df.NormPrice[abs(df.pattern_mark) == 1003.07],
               color='red')
    # marker=m_list[8], label='Pattern 22')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(1, 3, 0.07)')

    df = pd.read_csv(
        'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square_2.csv')
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df.NormPrice = np.around(df.NormPrice, 2)
    # df = df.sort_values('Timestamp')
    # df.reset_index(drop=True, inplace=True)

    ax = fig.add_subplot(2, 4, 7)
    ax.plot(df.Timestamp, df.NormPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark) == 3001.12], df.NormPrice[abs(df.pattern_mark) == 3001.12],
               color='red')
    # marker=m_list[8], label='Pattern 22')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(3, 1, 0.12)')

    ax = fig.add_subplot(2, 4, 8)
    ax.plot(df.Timestamp, df.SpotPrice, ".:", color='black')
    ax.scatter(df.Timestamp[abs(df.pattern_mark) == 4006.12], df.SpotPrice[abs(df.pattern_mark) == 4006.12],
               color='red')
    # marker=m_list[8], label='Pattern 22')

    ax.set_xticks([])
    # ax.set_yticks([])
    ax.set_title('(4, 6, 0.12)')
    plt.show()


def count_num_traces():
    # fig, (ax1, ax2) = plt.subplots(1, 2, tight_layout=True, figsize=set_size(fraction=2))
    instList = "F:/alibaba_work/*_full_traces_updated/*vpc__clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = ['./alibaba_work/cn-beijing_full_traces/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean2.csv']
    total_count = len(instList)
    # all_data_list = []
    # 0, 1, 2,
    # patterns_epoch_1 = {1: [], 2: [], 12: [], 13: [], 14: [], 15: [], 22: [], 23: [], 24: [], 25: []}
    # patterns_epoch_2 = {1: 0, 2: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 22: 0, 23: 0, 24: 0, 25: 0, 26: 0, 27: 0, 28: 0}

    # Following line is patterns by peaks counting increase and decrease steps
    # patterns_epoch_1 = {401: [], 402: [], 403: [], 404: [], 405: [], 406: [], 407: [], 301: [], 302: [], 303: [], 304: [], 305: [], 306: [], 307: [], 701: [], 702: [], 703: [], 704: [], 8001: [], 8002: [], 707: [], 705: [], 706: [], 201: [], 202: [], 203: [], 204: [], 205: [], 206: [], 207: [], 7000: [], 601: [], 602: [], 603: [], 604: [], 605: [], 606: [], 607: [], 101: [], 102: [], 103: [], 104: [], 105: [], 106: [], 107: [], 501: [], 502: [], 503: [], 504: [], 505: [], 506: [], 507: []}

    # Following line is patterns by peak height
    # patterns_epoch_1 = {0.24: [], 0.27: [], 0.26: [], 0.25: [], 0.5: [], 0.75: [], 0.76: [], 0.51: [], 0.52: [], 0.28: [], 0.23: [], 0.22: [], 0.21: [], 0.54: [], 0.79: [], 0.88: [], 0.38: [], 0.63: [], 0.47: [], 0.72: [], 0.18: [], 0.19: [], 0.42: [], 0.43: [], 0.44: [], 0.2: [], 0.45: [], 0.46: [], 0.41: [], 0.67: [], 0.85: [], 0.69: [], 0.15: [], 0.16: [], 0.17: [], 0.8: [], 0.55: [], 0.14: [], 0.89: [], 0.39: [], 0.64: [], 0.48: [], 0.73: [], 0.57: [], 0.82: [], 0.12: [], 0.13: [], 0.84: [], 0.59: [], 0.58: [], 0.35: [], 0.6: [], 0.66: [], 0.86: [], 0.36: [], 0.68: [], 0.11: [], 0.77: [], 0.61: [], 0.7: [], 0.09: [], 0.1: [], 0.08: [], 0.56: [], 0.81: [], 0.9: [], 0.4: [], 0.65: [], 0.74: [], 0.49: [], 0.83: [], 0.06: [], 0.07: [], 0.34: [], 0.29: [], 0.3: [], 0.31: [], 0.32: [], 0.33: [], 0.05: [], 0.04: [], 0.53: [], 0.78: [], 0.87: [], 0.37: [], 0.62: [], 0.03: [], 0.71: [], 0.02: [], 0.01: [], 0.91: []}
    # patterns_epoch_1 = {7002.42: [], 14004.36: [], 7003.26: [], 7003.24: [], 14004.43: [], 14004.34: [], 14004.32: [], 7003.33: [], 7004.4: [], 7004.26: [], 8002.42: [], 7006.33: [], 2001.52: [], 2001.02: [], 7008.17: [], 7008.42: [], 7009.42: [], 7009.17: [], 7009.26: [], 8003.17: [], 5007.2: [], 10004.51: [], 8012.28: [], 1001.64: [], 33045.9: [], 4001.13: [], 1001.84: [], 1001.59: [], 4004.13: [], 2001.03: [], 1003.09: [], 25003.74: [], 25005.73: [], 25006.76: [], 25006.75: [], 25006.74: [], 25006.73: [], 25006.7: [], 25005.75: [], 25005.74: [], 25006.78: [], 25005.71: [], 25006.71: [], 25024.75: [], 25028.74: [], 18001.54: [], 25032.74: [], 25034.68: [], 25034.67: [], 25036.71: [], 25037.74: [], 25038.75: [], 25037.73: [], 18002.54: [], 18004.48: [], 18004.56: [], 18004.47: [], 18005.49: [], 18006.54: [], 18007.54: [], 2002.28: [], 18022.53: [], 18023.54: [], 18024.5: [], 18025.48: [], 18025.54: [], 5001.16: [], 5001.11: [], 5001.24: [], 5001.19: [], 5001.06: [], 5002.13: [], 5002.42: [], 5002.16: [], 5002.28: [], 5002.18: [], 5003.2: [], 5003.11: [], 5002.14: [], 5002.11: [], 5002.12: [], 5004.14: [], 5004.09: [], 5003.08: [], 5003.19: [], 5003.18: [], 5005.17: [], 5005.1: [], 5005.12: [], 5004.2: [], 5005.15: [], 5006.12: [], 5006.14: [], 5006.15: [], 5006.1: [], 5006.17: [], 5006.16: [], 5006.11: [], 5007.15: [], 17003.48: [], 17004.46: [], 17004.48: [], 17004.51: [], 17007.82: [], 17004.53: [], 17009.56: [], 17004.45: [], 17004.49: [], 17004.54: [], 5008.18: [], 5009.17: [], 17015.54: [], 5009.19: [], 17015.53: [], 5009.39: [], 5009.36: [], 17020.4: [], 17021.51: [], 5010.19: [], 17023.46: [], 17024.48: [], 17025.54: [], 17026.51: [], 17027.54: [], 17026.54: [], 17024.47: [], 17026.5: [], 17025.5: [], 17024.54: [], 17025.49: [], 17025.51: [], 15005.55: [], 15005.43: [], 2001.09: [], 2001.84: [], 2001.68: [], 2003.09: [], 2003.18: [], 3001.36: [], 3001.68: [], 3001.27: [], 2004.18: [], 5036.88: [], 8009.24: [], 5041.82: [], 9001.27: [], 9002.3: [], 9002.21: [], 9002.19: [], 9002.32: [], 9003.33: [], 9003.27: [], 9003.31: [], 9003.26: [], 9003.23: [], 9004.39: [], 9003.3: [], 9005.32: [], 9007.28: [], 9007.25: [], 9008.28: [], 9008.29: [], 9009.18: [], 9010.27: [], 9011.23: [], 9012.23: [], 9013.27: [], 9014.28: [], 9015.31: [], 9016.33: [], 1001.66: [], 5001.2: [], 1002.16: [], 5002.2: [], 5002.22: [], 1003.16: [], 15016.39: [], 5003.54: [], 5003.36: [], 5004.11: [], 5005.13: [], 5005.11: [], 1001.01: [], 1002.09: [], 1001.26: [], 1002.26: [], 1002.22: [], 1003.05: [], 1001.09: [], 1001.17: [], 1001.13: [], 1002.13: [], 1002.17: [], 1003.06: [], 1005.14: [], 1006.12: [], 1007.17: [], 1007.13: [], 1010.2: [], 1014.88: [], 1013.87: [], 1012.8: [], 5006.13: [], 1021.42: [], 1014.28: [], 1022.79: [], 1025.89: [], 1025.5: [], 1027.54: [], 1028.88: [], 1027.9: [], 1024.8: [], 1031.76: [], 1024.85: [], 1033.8: [], 1034.68: [], 1033.9: [], 1036.88: [], 1037.74: [], 1038.82: [], 1037.73: [], 1040.8: [], 1041.82: [], 1041.81: [], 1043.9: [], 1044.87: [], 1045.9: [], 1045.89: [], 1042.88: [], 1044.88: [], 1045.88: [], 1044.89: [], 1044.9: [], 5008.36: [], 5009.2: [], 5010.2: [], 10001.29: [], 10001.32: [], 10001.33: [], 10001.35: [], 10002.24: [], 10002.26: [], 10002.31: [], 10002.28: [], 10002.3: [], 10003.36: [], 10003.23: [], 10003.22: [], 10003.27: [], 10003.45: [], 10004.33: [], 10003.32: [], 10003.26: [], 10003.3: [], 10003.24: [], 10004.3: [], 10005.24: [], 10005.3: [], 10004.26: [], 9002.35: [], 10006.28: [], 9002.33: [], 2001.25: [], 10008.42: [], 10009.41: [], 10010.24: [], 10010.25: [], 9003.42: [], 9003.37: [], 9003.35: [], 10011.22: [], 10011.28: [], 10011.26: [], 10011.27: [], 10012.22: [], 10012.54: [], 10012.28: [], 10012.26: [], 10012.23: [], 10013.3: [], 10013.27: [], 10013.26: [], 10013.25: [], 10013.29: [], 10014.28: [], 10014.27: [], 10014.29: [], 10014.3: [], 10014.34: [], 10015.54: [], 10015.29: [], 10015.31: [], 10015.32: [], 10014.26: [], 10016.33: [], 10017.42: [], 10018.54: [], 9005.42: [], 34045.9: [], 10004.62: [], 3002.07: [], 4002.18: [], 4002.11: [], 4002.27: [], 1001.82: [], 1001.32: [], 1001.57: [], 4003.11: [], 4003.36: [], 8002.4: [], 1002.32: [], 4004.27: [], 9011.42: [], 1003.32: [], 26005.74: [], 26005.73: [], 26006.76: [], 26006.75: [], 26006.74: [], 26005.77: [], 26006.79: [], 26005.78: [], 26006.78: [], 26006.81: [], 26011.74: [], 9012.33: [], 26008.76: [], 26015.79: [], 26022.79: [], 26031.76: [], 26034.68: [], 26035.71: [], 26036.72: [], 26037.74: [], 26037.73: [], 26038.76: [], 26039.78: [], 26037.75: [], 26039.77: [], 6001.22: [], 6001.36: [], 6001.2: [], 6003.2: [], 6003.22: [], 6003.47: [], 6004.54: [], 6005.22: [], 6006.11: [], 18001.53: [], 6007.13: [], 18003.54: [], 18003.48: [], 18005.54: [], 18004.54: [], 18004.53: [], 18004.5: [], 18004.52: [], 18004.49: [], 18004.57: [], 18004.51: [], 18005.5: [], 18005.53: [], 18014.5: [], 18015.54: [], 18016.57: [], 1001.23: [], 18019.47: [], 1001.73: [], 18022.54: [], 18023.52: [], 18024.54: [], 18024.48: [], 18025.5: [], 18026.51: [], 18027.54: [], 18027.53: [], 18026.54: [], 18027.56: [], 18025.49: [], 18026.53: [], 18025.53: [], 18027.55: [], 1003.48: [], 2001.16: [], 2001.32: [], 2001.66: [], 2001.82: [], 2002.07: [], 15003.42: [], 15003.35: [], 15003.47: [], 15003.43: [], 15003.41: [], 15004.51: [], 15004.44: [], 15003.46: [], 15004.41: [], 15004.49: [], 15005.45: [], 15005.44: [], 17003.49: [], 10002.42: [], 3001.09: [], 15006.48: [], 3001.16: [], 10002.4: [], 15005.61: [], 17003.43: [], 15007.44: [], 3001.34: [], 3001.25: [], 15008.44: [], 15008.53: [], 15008.66: [], 17004.57: [], 10003.4: [], 3002.34: [], 10003.37: [], 17004.5: [], 3002.16: [], 3002.25: [], 17004.47: [], 10003.35: [], 15013.43: [], 10001.28: [], 10002.32: [], 10002.23: [], 10002.29: [], 10002.33: [], 10002.25: [], 10003.31: [], 10003.33: [], 10003.29: [], 10003.28: [], 10003.25: [], 10003.42: [], 10005.28: [], 10008.28: [], 10008.24: [], 10009.32: [], 10009.33: [], 10010.31: [], 10010.35: [], 10011.29: [], 10012.29: [], 10013.28: [], 10015.28: [], 10015.3: [], 10016.54: [], 15018.36: [], 15017.41: [], 15017.42: [], 15019.51: [], 15019.37: [], 15018.43: [], 17006.67: [], 15021.45: [], 2001.21: [], 2001.17: [], 2001.2: [], 2001.01: [], 2001.05: [], 2001.14: [], 2001.06: [], 2001.07: [], 2002.08: [], 2002.06: [], 2002.03: [], 2002.18: [], 2003.06: [], 2003.12: [], 1001.89: [], 1001.14: [], 2003.1: [], 2003.07: [], 2004.12: [], 2003.08: [], 2004.08: [], 2004.14: [], 2004.07: [], 2005.12: [], 2005.14: [], 2006.12: [], 2005.1: [], 2006.13: [], 2006.24: [], 4006.16: [], 2006.15: [], 2006.18: [], 1002.14: [], 1002.39: [], 1003.39: [], 1004.14: [], 15003.39: [], 2001.19: [], 2001.15: [], 2002.23: [], 2002.27: [], 2002.15: [], 2002.16: [], 2001.18: [], 2002.09: [], 2002.24: [], 2001.13: [], 2002.13: [], 2003.14: [], 2004.09: [], 2005.09: [], 2006.14: [], 2007.14: [], 2008.15: [], 2043.9: [], 2044.9: [], 2045.9: [], 10013.24: [], 7001.2: [], 7001.22: [], 7002.2: [], 7003.31: [], 1001.05: [], 1001.8: [], 7005.7: [], 1001.55: [], 1001.3: [], 1002.55: [], 1002.3: [], 7006.2: [], 2001.73: [], 7009.29: [], 2003.48: [], 20004.55: [], 20005.53: [], 11003.33: [], 11003.35: [], 4003.16: [], 8002.26: [], 27005.74: [], 27006.75: [], 27006.76: [], 27006.74: [], 27007.83: [], 27006.82: [], 27006.81: [], 27005.76: [], 4004.16: [], 27005.77: [], 27006.8: [], 20024.63: [], 7001.15: [], 27033.8: [], 27034.68: [], 7001.19: [], 7001.21: [], 27037.74: [], 7002.24: [], 7002.22: [], 7002.23: [], 7002.18: [], 7002.37: [], 7003.37: [], 7003.36: [], 7002.27: [], 7002.17: [], 7004.21: [], 7003.57: [], 7004.66: [], 7004.57: [], 7003.17: [], 7004.18: [], 7005.18: [], 7004.24: [], 7004.19: [], 7004.17: [], 7006.22: [], 7006.16: [], 7006.19: [], 7006.21: [], 7006.17: [], 7007.23: [], 7007.15: [], 7007.17: [], 7007.2: [], 7007.14: [], 7007.21: [], 7008.16: [], 7008.15: [], 7008.21: [], 7008.2: [], 7008.19: [], 7009.18: [], 7009.2: [], 7009.21: [], 7009.19: [], 7008.32: [], 7010.24: [], 7010.21: [], 7010.18: [], 7010.19: [], 7010.22: [], 7011.24: [], 7011.2: [], 7010.2: [], 7011.21: [], 7011.42: [], 7012.42: [], 7011.22: [], 7012.24: [], 7012.51: [], 7011.3: [], 8003.26: [], 10002.22: [], 19002.54: [], 19003.59: [], 19004.54: [], 19004.52: [], 19004.51: [], 19004.53: [], 19005.5: [], 19005.57: [], 19010.62: [], 19005.77: [], 19004.57: [], 19004.56: [], 19011.64: [], 19007.57: [], 19007.56: [], 19016.57: [], 19010.83: [], 19019.54: [], 19011.58: [], 19022.54: [], 19023.56: [], 19024.54: [], 19025.5: [], 19026.54: [], 19027.54: [], 19027.53: [], 19028.55: [], 19026.52: [], 19029.59: [], 19029.57: [], 19028.57: [], 19028.56: [], 19026.53: [], 19030.59: [], 6004.14: [], 8002.2: [], 1001.21: [], 8002.36: [], 1001.46: [], 1001.71: [], 8003.22: [], 8003.36: [], 1002.21: [], 1002.46: [], 8004.54: [], 2001.3: [], 2001.39: [], 2001.89: [], 2002.14: [], 2002.05: [], 2002.39: [], 2003.05: [], 3001.39: [], 3001.32: [], 3001.23: [], 11001.45: [], 11002.31: [], 11002.33: [], 11002.32: [], 11002.29: [], 11003.34: [], 11003.28: [], 11003.4: [], 11003.37: [], 11003.36: [], 11003.31: [], 11003.26: [], 11004.42: [], 11006.4: [], 11009.32: [], 11009.33: [], 11010.35: [], 11010.36: [], 11011.27: [], 11011.26: [], 11013.32: [], 11013.26: [], 11014.29: [], 11015.3: [], 11016.36: [], 11017.34: [], 11018.36: [], 11020.51: [], 11020.39: [], 11021.42: [], 3003.23: [], 3003.14: [], 8011.2: [], 25005.78: [], 25005.68: [], 25005.76: [], 5001.27: [], 5001.18: [], 5001.09: [], 1002.12: [], 17005.47: [], 12003.4: [], 5002.09: [], 12003.49: [], 5002.25: [], 5002.82: [], 12003.28: [], 5002.43: [], 12004.63: [], 5003.16: [], 12004.42: [], 5003.25: [], 5003.43: [], 1004.12: [], 3001.04: [], 3002.18: [], 3001.06: [], 3001.07: [], 3001.19: [], 3001.18: [], 3001.28: [], 3001.15: [], 3002.11: [], 3002.15: [], 3002.28: [], 3003.09: [], 3004.08: [], 3005.1: [], 3006.12: [], 3007.14: [], 3013.87: [], 5005.18: [], 5005.09: [], 3037.74: [], 12001.42: [], 12001.46: [], 3035.85: [], 5005.27: [], 12002.36: [], 12002.3: [], 12002.42: [], 12002.29: [], 12003.42: [], 12003.35: [], 12003.29: [], 12003.36: [], 12003.3: [], 12003.33: [], 12003.34: [], 12003.37: [], 12003.31: [], 12003.26: [], 12005.36: [], 12004.31: [], 12005.33: [], 12005.42: [], 12004.37: [], 12006.31: [], 12005.37: [], 12005.74: [], 12006.6: [], 5006.18: [], 12007.37: [], 12008.39: [], 12009.36: [], 5007.16: [], 5002.21: [], 12013.32: [], 12014.27: [], 5008.16: [], 12016.31: [], 12016.33: [], 12016.54: [], 12016.42: [], 12017.34: [], 12018.39: [], 12018.37: [], 12018.34: [], 12018.45: [], 12019.39: [], 1001.78: [], 1001.03: [], 1001.53: [], 1002.25: [], 1002.03: [], 5003.46: [], 2001.12: [], 2001.87: [], 2001.71: [], 2002.12: [], 2002.46: [], 2001.22: [], 4001.14: [], 4001.32: [], 4002.14: [], 4002.23: [], 4003.14: [], 28006.84: [], 28006.83: [], 28006.82: [], 28007.83: [], 28007.82: [], 28006.81: [], 28007.8: [], 28033.8: [], 17005.46: [], 28037.74: [], 28039.78: [], 28039.77: [], 28041.82: [], 28041.81: [], 6001.16: [], 6001.23: [], 6002.18: [], 6002.16: [], 13004.35: [], 6003.23: [], 17001.6: [], 20003.58: [], 20004.56: [], 20004.64: [], 20006.63: [], 20004.57: [], 20004.61: [], 20004.62: [], 20004.6: [], 20004.59: [], 20004.58: [], 20005.57: [], 20005.6: [], 17003.45: [], 17003.44: [], 17003.46: [], 17003.54: [], 20017.6: [], 17003.47: [], 20021.51: [], 17005.64: [], 17005.49: [], 6006.18: [], 6006.16: [], 17005.51: [], 20027.54: [], 20028.56: [], 20029.57: [], 20030.6: [], 20027.53: [], 20030.59: [], 20028.55: [], 20027.55: [], 17007.49: [], 20030.61: [], 17009.49: [], 17009.75: [], 6007.27: [], 17011.49: [], 1001.44: [], 1001.19: [], 6008.16: [], 17016.53: [], 1002.19: [], 17020.51: [], 6009.41: [], 17022.44: [], 17022.45: [], 17023.44: [], 1003.44: [], 17024.49: [], 4001.07: [], 4001.15: [], 4001.18: [], 4001.12: [], 4001.1: [], 4002.15: [], 4002.19: [], 4002.07: [], 4001.09: [], 4001.11: [], 4002.12: [], 4002.16: [], 4002.13: [], 4002.09: [], 4003.13: [], 4004.1: [], 4004.15: [], 4004.11: [], 4003.08: [], 4004.14: [], 4005.15: [], 4005.1: [], 4005.12: [], 4005.09: [], 4004.09: [], 4006.12: [], 4006.22: [], 4006.13: [], 4006.1: [], 4005.14: [], 4007.15: [], 4006.14: [], 4006.11: [], 4007.27: [], 4007.13: [], 4008.16: [], 4008.15: [], 4007.12: [], 4007.18: [], 12001.36: [], 12002.26: [], 12002.32: [], 12004.53: [], 12002.33: [], 12002.31: [], 12002.27: [], 12003.38: [], 12003.39: [], 12010.35: [], 12010.36: [], 12004.36: [], 12013.31: [], 12014.34: [], 12014.28: [], 12015.37: [], 12015.36: [], 12018.38: [], 12011.37: [], 12015.29: [], 12016.32: [], 12017.33: [], 12018.36: [], 12017.36: [], 12016.36: [], 12021.42: [], 12018.35: [], 12020.39: [], 12020.42: [], 12022.54: [], 3002.21: [], 3002.05: [], 3002.3: [], 3003.05: [], 1001.1: [], 1001.6: [], 1001.35: [], 1001.85: [], 1003.35: [], 1003.1: [], 4001.2: [], 4001.16: [], 4002.21: [], 4002.17: [], 4002.2: [], 4003.12: [], 4003.09: [], 4005.08: [], 4005.13: [], 4007.17: [], 4007.16: [], 4003.07: [], 4004.07: [], 4005.11: [], 4006.15: [], 4007.14: [], 4008.14: [], 4009.16: [], 4010.2: [], 4027.9: [], 4033.8: [], 4041.82: [], 4045.9: [], 7001.25: [], 7001.18: [], 14003.53: [], 7002.16: [], 7002.25: [], 14003.37: [], 7002.34: [], 14004.37: [], 14004.42: [], 7003.23: [], 7003.25: [], 14004.49: [], 14004.31: [], 1001.51: [], 7005.16: [], 1001.76: [], 22005.61: [], 22005.66: [], 22005.67: [], 22005.68: [], 7006.27: [], 7006.32: [], 22005.58: [], 22007.82: [], 1003.51: [], 1003.26: [], 2001.1: [], 2001.85: [], 7008.27: [], 7008.18: [], 2002.1: [], 2002.19: [], 22020.67: [], 9001.42: [], 9001.25: [], 9001.28: [], 9001.36: [], 9002.27: [], 9002.26: [], 9002.23: [], 9002.25: [], 9002.22: [], 9003.22: [], 9003.25: [], 9003.24: [], 9002.24: [], 9002.28: [], 9004.48: [], 9004.22: [], 9003.4: [], 9003.28: [], 9003.36: [], 9005.71: [], 9005.83: [], 9005.25: [], 9004.27: [], 9004.24: [], 9006.27: [], 9005.27: [], 9005.22: [], 9005.26: [], 9005.23: [], 9007.19: [], 9007.23: [], 9006.4: [], 22033.65: [], 9008.27: [], 9008.42: [], 9008.25: [], 9009.22: [], 9009.21: [], 9009.42: [], 9009.24: [], 9009.41: [], 9010.25: [], 9010.2: [], 9010.22: [], 9010.24: [], 9010.23: [], 9011.22: [], 9011.21: [], 9011.26: [], 9011.27: [], 9011.24: [], 9012.26: [], 9012.27: [], 9012.22: [], 9012.25: [], 9012.24: [], 9013.25: [], 9013.28: [], 9013.26: [], 9013.33: [], 9013.31: [], 9014.27: [], 9014.26: [], 9013.3: [], 9013.42: [], 9013.24: [], 9015.3: [], 9015.41: [], 9016.53: [], 9016.42: [], 4002.3: [], 29005.8: [], 29006.82: [], 29007.83: [], 29007.82: [], 29006.83: [], 29006.84: [], 4002.37: [], 1001.42: [], 1001.67: [], 4003.3: [], 4003.8: [], 29039.78: [], 29041.82: [], 29044.87: [], 4004.12: [], 1003.42: [], 1004.17: [], 4006.21: [], 14020.38: [], 21004.64: [], 21004.63: [], 21005.64: [], 21005.62: [], 21004.62: [], 21005.63: [], 21004.58: [], 21011.73: [], 21004.59: [], 21005.68: [], 21006.81: [], 21006.56: [], 21012.63: [], 21024.58: [], 21026.63: [], 21027.54: [], 21028.55: [], 21030.59: [], 21031.62: [], 21032.68: [], 21032.63: [], 21030.6: [], 21032.64: [], 21032.62: [], 5005.16: [], 15003.37: [], 8002.23: [], 15004.4: [], 15004.38: [], 15004.37: [], 8003.18: [], 8003.41: [], 8003.27: [], 8003.48: [], 8004.25: [], 2001.26: [], 27006.71: [], 13001.4: [], 13001.39: [], 13003.39: [], 13003.38: [], 13003.4: [], 13003.34: [], 13003.37: [], 13004.34: [], 13003.41: [], 13003.42: [], 13003.33: [], 13004.54: [], 13004.42: [], 13014.34: [], 13015.36: [], 13014.28: [], 13017.35: [], 13018.36: [], 13018.38: [], 13020.4: [], 13020.39: [], 13018.35: [], 13021.42: [], 13019.42: [], 13017.38: [], 13019.38: [], 13019.39: [], 13020.38: [], 13020.42: [], 13023.45: [], 3001.03: [], 3002.1: [], 3002.19: [], 3002.03: [], 8010.41: [], 14001.53: [], 3002.39: [], 14003.42: [], 14003.41: [], 14003.39: [], 14003.4: [], 14003.35: [], 14004.51: [], 14004.45: [], 14004.39: [], 14004.48: [], 14004.54: [], 14004.35: [], 14005.48: [], 14004.4: [], 14004.38: [], 14004.33: [], 8011.27: [], 14005.42: [], 14007.42: [], 14007.45: [], 14008.43: [], 14010.36: [], 8012.41: [], 1001.08: [], 1001.58: [], 1001.83: [], 27040.8: [], 14015.41: [], 27041.8: [], 5001.55: [], 1002.08: [], 14018.48: [], 14019.38: [], 14019.37: [], 5001.13: [], 5002.17: [], 5002.1: [], 5003.13: [], 5002.19: [], 5001.15: [], 5001.12: [], 5001.14: [], 5001.1: [], 5002.23: [], 5002.15: [], 5003.42: [], 5004.13: [], 5005.14: [], 5006.2: [], 5007.13: [], 5009.18: [], 5009.15: [], 5011.73: [], 14022.43: [], 5013.4: [], 5025.89: [], 5027.9: [], 5003.12: [], 5003.3: [], 5003.14: [], 5003.28: [], 1001.12: [], 1001.87: [], 1001.04: [], 1001.16: [], 1001.25: [], 1002.04: [], 1001.9: [], 1001.07: [], 5045.9: [], 5045.88: [], 1002.07: [], 1002.05: [], 5044.87: [], 1002.06: [], 1002.1: [], 1003.14: [], 1003.12: [], 1003.07: [], 1003.08: [], 1004.09: [], 1005.12: [], 1004.07: [], 1005.82: [], 1004.08: [], 5004.12: [], 1005.09: [], 1005.1: [], 1005.86: [], 1007.14: [], 1007.15: [], 1009.18: [], 1010.83: [], 1011.73: [], 1012.86: [], 3004.11: [], 5006.3: [], 5006.28: [], 5007.14: [], 5007.12: [], 5007.37: [], 1024.89: [], 5008.14: [], 5008.28: [], 5009.3: [], 2002.74: [], 1034.67: [], 1001.74: [], 1001.24: [], 1039.9: [], 1041.8: [], 1042.89: [], 1002.74: [], 1003.24: [], 9002.36: [], 2001.08: [], 9002.29: [], 9002.2: [], 22028.55: [], 9003.18: [], 9003.5: [], 9003.32: [], 9003.68: [], 9003.41: [], 9003.2: [], 2002.42: [], 9003.29: [], 3001.46: [], 9004.54: [], 2003.42: [], 9004.5: [], 2003.67: [], 22030.62: [], 2004.42: [], 2005.08: [], 5002.3: [], 4001.28: [], 30006.82: [], 30007.82: [], 30007.83: [], 30006.84: [], 30006.83: [], 30006.9: [], 30006.89: [], 30006.88: [], 30025.89: [], 4002.1: [], 4002.35: [], 4002.28: [], 4002.44: [], 30036.88: [], 30041.82: [], 30042.84: [], 30045.9: [], 30045.88: [], 30045.89: [], 1001.15: [], 1001.4: [], 4003.1: [], 4003.35: [], 1002.15: [], 19004.58: [], 19004.45: [], 19004.48: [], 19004.49: [], 19004.6: [], 19005.68: [], 19005.53: [], 19005.46: [], 1003.9: [], 1003.4: [], 9012.29: [], 6001.14: [], 6001.28: [], 6001.21: [], 6001.12: [], 6002.19: [], 6002.3: [], 6002.28: [], 19024.47: [], 19025.54: [], 6003.21: [], 6003.19: [], 6003.3: [], 6001.13: [], 6001.18: [], 6001.35: [], 22004.64: [], 22004.62: [], 22005.64: [], 6002.15: [], 6002.14: [], 6002.21: [], 6002.2: [], 6002.22: [], 22005.62: [], 22004.63: [], 22005.63: [], 6002.12: [], 6003.39: [], 6003.15: [], 6003.14: [], 22019.68: [], 6003.18: [], 6005.12: [], 6005.18: [], 6005.13: [], 6005.15: [], 6005.27: [], 6006.15: [], 22027.66: [], 22027.54: [], 6006.17: [], 22030.6: [], 22030.59: [], 22031.62: [], 6006.21: [], 6007.16: [], 6007.17: [], 22033.66: [], 6008.15: [], 6008.18: [], 6008.2: [], 22032.68: [], 6009.2: [], 6009.19: [], 6009.21: [], 6009.16: [], 6008.21: [], 6009.42: [], 6009.18: [], 6010.41: [], 6009.36: [], 6010.2: [], 6005.14: [], 6011.24: [], 6005.71: [], 6011.42: [], 6010.22: [], 6012.27: [], 6006.19: [], 6006.12: [], 6007.14: [], 1001.06: [], 1001.56: [], 6009.28: [], 6009.53: [], 2001.24: [], 2001.74: [], 14001.42: [], 14002.42: [], 14003.47: [], 14003.44: [], 14003.48: [], 14004.58: [], 14003.45: [], 14003.36: [], 14003.38: [], 14003.43: [], 14011.41: [], 14012.43: [], 14004.41: [], 14006.8: [], 14013.43: [], 14016.31: [], 14017.36: [], 14017.42: [], 14017.41: [], 14018.36: [], 14018.35: [], 14019.42: [], 14019.39: [], 14020.39: [], 14021.42: [], 14021.41: [], 14023.45: [], 14020.4: [], 14020.42: [], 14026.51: [], 14022.45: [], 14023.54: [], 10002.27: [], 3001.08: [], 10002.36: [], 3001.74: [], 3001.76: [], 3001.24: [], 3001.17: [], 10003.54: [], 3002.33: [], 3002.17: [], 10003.41: [], 10003.39: [], 10004.54: [], 10004.36: [], 3003.17: [], 6001.19: [], 6002.13: [], 6003.24: [], 6004.18: [], 6003.13: [], 6003.16: [], 6004.13: [], 6004.16: [], 6002.24: [], 6002.23: [], 6003.17: [], 6003.25: [], 6005.16: [], 6005.17: [], 6006.14: [], 6007.15: [], 6008.19: [], 6009.17: [], 6010.21: [], 6011.21: [], 6012.24: [], 6013.4: [], 6026.53: [], 3002.06: [], 8003.33: [], 10012.3: [], 10012.41: [], 10013.36: [], 7001.28: [], 7001.14: [], 7002.21: [], 7002.19: [], 7002.28: [], 7002.44: [], 7003.3: [], 7003.21: [], 7003.28: [], 11001.36: [], 11002.28: [], 11002.3: [], 11002.26: [], 11002.74: [], 11002.41: [], 11003.42: [], 11003.29: [], 11003.39: [], 11003.38: [], 11003.27: [], 11004.33: [], 11003.48: [], 11003.25: [], 11003.3: [], 11003.32: [], 11004.5: [], 11005.74: [], 11004.49: [], 11004.54: [], 11004.34: [], 11006.38: [], 11006.37: [], 11006.32: [], 11006.84: [], 7004.8: [], 11007.32: [], 1001.22: [], 11010.32: [], 1001.72: [], 11013.31: [], 11013.3: [], 11013.29: [], 11014.28: [], 11014.32: [], 11014.27: [], 11014.42: [], 11014.34: [], 11015.29: [], 11015.32: [], 11015.33: [], 11015.31: [], 1002.47: [], 11016.32: [], 11016.31: [], 11016.33: [], 11016.42: [], 11015.39: [], 11017.32: [], 11017.33: [], 1003.22: [], 2001.81: [], 2001.9: [], 1004.47: [], 7008.3: [], 2002.9: [], 7010.3: [], 2004.06: [], 2005.15: [], 31006.91: [], 31007.84: [], 4001.17: [], 4001.08: [], 4001.24: [], 4001.33: [], 31041.82: [], 31042.84: [], 31045.9: [], 31045.88: [], 11003.54: [], 31045.89: [], 11003.5: [], 4002.08: [], 4002.24: [], 11004.32: [], 11004.3: [], 11004.29: [], 1001.88: [], 1001.38: [], 1001.63: [], 4003.42: [], 4003.49: [], 4003.17: [], 4004.08: [], 4004.17: [], 1003.38: [], 1003.13: [], 1004.13: [], 23003.68: [], 23004.64: [], 23005.73: [], 23005.64: [], 23005.68: [], 23005.67: [], 23005.75: [], 23006.75: [], 23005.69: [], 23005.74: [], 23005.66: [], 23005.65: [], 23019.68: [], 23028.68: [], 23031.68: [], 23032.68: [], 23033.68: [], 23034.68: [], 23035.69: [], 23034.67: [], 23037.88: [], 23033.66: [], 23033.65: [], 4002.22: [], 15004.57: [], 8001.28: [], 8002.21: [], 1001.79: [], 1001.54: [], 1001.29: [], 8003.28: [], 16003.49: [], 16003.41: [], 16003.47: [], 16003.5: [], 16003.44: [], 16004.45: [], 16004.49: [], 16004.44: [], 16004.47: [], 16005.48: [], 16005.44: [], 16005.46: [], 16005.43: [], 16006.49: [], 8004.53: [], 15001.54: [], 15003.45: [], 15004.48: [], 15004.45: [], 15003.44: [], 15003.4: [], 15003.48: [], 15004.62: [], 15008.5: [], 15005.62: [], 15004.54: [], 15004.42: [], 15013.46: [], 15015.37: [], 15007.46: [], 15017.34: [], 15018.44: [], 15019.42: [], 15020.42: [], 15021.42: [], 15020.4: [], 15023.45: [], 15021.41: [], 15023.46: [], 15022.44: [], 15022.45: [], 15022.54: [], 15020.39: [], 15025.54: [], 15023.44: [], 3002.22: [], 15027.54: [], 2002.22: [], 16020.47: [], 16022.43: [], 16023.45: [], 16023.46: [], 3001.22: [], 16023.47: [], 3001.9: [], 3001.1: [], 3001.14: [], 3001.12: [], 3001.11: [], 3001.21: [], 3002.13: [], 3002.08: [], 3002.14: [], 3002.12: [], 3002.09: [], 3003.12: [], 3003.1: [], 3003.08: [], 3003.07: [], 3003.11: [], 3004.09: [], 3004.12: [], 3004.14: [], 3004.07: [], 3004.1: [], 3005.08: [], 3005.11: [], 3005.82: [], 3005.12: [], 3005.14: [], 3006.11: [], 3006.09: [], 3006.14: [], 3005.09: [], 2004.1: [], 3007.13: [], 3006.2: [], 3007.28: [], 3003.06: [], 3006.24: [], 3008.16: [], 3004.06: [], 8011.39: [], 8011.28: [], 3005.15: [], 7001.16: [], 7002.15: [], 7002.26: [], 7002.14: [], 7002.3: [], 7002.32: [], 7003.19: [], 7003.4: [], 7003.2: [], 7003.18: [], 7003.16: [], 7003.15: [], 7004.16: [], 7005.17: [], 7006.15: [], 7009.16: [], 7011.25: [], 7009.22: [], 7010.23: [], 7011.23: [], 7012.23: [], 7013.27: [], 7014.42: [], 3006.22: [], 5001.17: [], 5001.08: [], 7034.68: [], 12003.32: [], 5002.24: [], 12003.45: [], 12003.41: [], 5002.08: [], 1001.5: [], 12004.52: [], 5003.1: [], 5003.15: [], 5003.24: [], 5003.17: [], 12004.32: [], 5004.1: [], 5004.15: [], 5005.9: [], 5006.24: [], 5007.17: [], 5007.24: [], 5008.15: [], 5008.17: [], 3002.23: [], 5009.24: [], 5010.42: [], 1001.7: [], 1001.2: [], 1002.2: [], 1003.2: [], 1003.45: [], 2001.04: [], 2001.38: [], 2001.29: [], 2002.04: [], 21004.74: [], 21004.61: [], 21004.6: [], 21005.74: [], 21005.59: [], 21005.6: [], 21005.56: [], 2003.04: [], 32045.9: [], 32045.89: [], 4001.06: [], 8001.24: [], 8001.23: [], 8001.22: [], 8001.36: [], 8001.27: [], 8002.25: [], 8002.28: [], 8002.22: [], 8002.19: [], 8002.24: [], 8003.4: [], 8003.23: [], 8003.19: [], 8003.42: [], 8003.2: [], 8003.21: [], 8004.18: [], 8004.22: [], 8004.24: [], 8003.24: [], 8004.23: [], 8005.24: [], 8004.2: [], 8004.27: [], 8005.19: [], 8006.19: [], 8006.15: [], 8005.26: [], 8005.27: [], 8007.28: [], 8007.2: [], 8007.17: [], 6004.19: [], 1001.86: [], 1001.11: [], 1001.36: [], 4003.15: [], 4003.06: [], 8009.2: [], 8009.19: [], 8009.21: [], 8009.18: [], 8009.42: [], 8010.22: [], 8010.27: [], 8010.24: [], 8010.19: [], 8010.2: [], 8011.24: [], 8011.23: [], 8011.42: [], 8011.21: [], 8010.25: [], 8012.26: [], 8012.22: [], 8012.25: [], 8012.42: [], 1002.11: [], 8013.26: [], 8013.4: [], 8012.24: [], 8012.34: [], 8013.25: [], 1002.36: [], 8013.42: [], 8015.42: [], 1003.36: [], 4005.22: [], 6001.17: [], 6001.15: [], 24005.67: [], 24005.68: [], 24006.82: [], 24008.71: [], 24005.71: [], 24005.72: [], 24006.71: [], 24005.75: [], 24005.66: [], 24014.74: [], 13003.48: [], 24020.71: [], 6002.17: [], 6002.1: [], 6002.26: [], 6002.35: [], 24030.73: [], 24034.68: [], 24035.69: [], 24036.72: [], 24034.67: [], 24034.73: [], 24035.72: [], 24035.7: [], 24035.73: [], 13004.36: [], 6003.42: [], 13004.32: [], 13004.29: [], 6004.15: [], 6005.74: [], 15004.43: [], 6007.42: [], 6007.4: [], 1001.02: [], 1001.52: [], 1001.27: [], 6008.42: [], 6008.17: [], 1002.02: [], 16001.45: [], 16002.48: [], 16003.39: [], 16004.54: [], 16004.51: [], 16004.53: [], 16004.64: [], 16003.43: [], 16003.45: [], 16003.48: [], 16003.46: [], 16004.43: [], 16004.48: [], 16013.47: [], 16014.5: [], 16007.49: [], 16017.41: [], 16018.36: [], 16019.47: [], 16020.54: [], 16021.45: [], 16022.46: [], 16020.46: [], 16021.42: [], 16022.44: [], 16023.48: [], 16024.47: [], 16024.48: [], 16026.54: [], 16027.54: [], 16022.45: [], 16025.51: [], 16024.49: [], 2001.11: [], 2001.61: [], 2001.7: [], 1004.52: [], 2002.2: [], 2002.11: [], 2003.11: [], 3001.2: [], 3001.13: [], 3001.29: [], 3002.2: [], 3002.04: [], 3002.88: [], 3002.29: [], 26005.71: [], 26005.76: [], 26006.82: [], 3003.13: [], 3003.04: [], 26006.71: [], 8001.21: [], 8002.27: [], 8002.3: [], 8002.17: [], 8002.31: [], 8002.18: [], 8003.25: [], 8008.16: [], 8008.2: [], 8010.21: [], 8008.19: [], 8009.17: [], 8005.18: [], 8006.21: [], 8006.25: [], 8007.25: [], 8008.22: [], 8009.22: [], 8010.23: [], 8011.22: [], 8012.23: [], 8013.27: [], 8014.27: [], 8016.33: [], 8016.42: [], 1001.18: [], 1001.68: [], 1002.18: [], 13001.35: [], 13001.5: [], 13002.31: [], 13002.36: [], 13002.34: [], 13002.32: [], 13003.36: [], 13003.31: [], 13003.32: [], 13003.35: [], 13003.45: [], 13004.39: [], 13003.44: [], 13004.37: [], 13003.74: [], 13003.82: [], 13005.56: [], 13004.49: [], 13004.31: [], 13005.78: [], 13004.38: [], 13007.42: [], 13007.41: [], 13007.58: [], 13007.47: [], 13011.4: [], 13011.39: [], 13015.37: [], 13016.31: [], 13016.39: [], 13017.36: [], 13017.33: [], 13017.37: [], 13017.34: [], 13018.34: [], 13018.39: [], 13019.37: [], 7001.26: []}
    # patterns_epoch_1 = {1001.02: [], 1001.03: [], 1002.03: [], 2001.06: [], 2003.06: [], 3001.07: [], 3001.08: [],
    #                     3001.09: [], 3004.08: [], 3005.09: [], 4001.12: [], 4006.12: [], 5001.14: [], 5001.15: [],
    #                     5007.14: [], 14003.42: [], 1: [], 7000: [], 8001: [], 8002: []}

    # Patterns from effective price traces
    # patterns_epoch_1 = {7002.4: [], 14004.43: [], 7003.24: [], 7003.26: [], 8002.42: [], 2001.52: [], 2001.02: [], 7008.42: [], 7009.26: [], 5007.2: [], 8012.28: [], 1001.64: [], 4001.13: [], 4002.13: [], 1001.84: [], 1001.59: [], 4003.13: [], 4004.13: [], 2001.03: [], 1003.09: [], 25003.74: [], 25005.74: [], 25006.75: [], 25006.76: [], 25006.74: [], 25005.73: [], 25006.73: [], 25005.78: [], 25006.78: [], 25005.71: [], 25011.74: [], 25006.71: [], 25025.89: [], 25028.74: [], 14020.39: [], 18001.54: [], 25032.74: [], 25037.74: [], 25038.75: [], 25037.73: [], 25037.75: [], 18004.64: [], 2002.28: [], 18023.54: [], 18024.5: [], 5001.18: [], 5001.11: [], 5001.27: [], 5001.2: [], 5001.24: [], 5002.28: [], 5002.12: [], 5002.24: [], 5002.18: [], 5002.21: [], 5003.31: [], 5002.3: [], 5003.15: [], 5003.17: [], 5003.5: [], 5004.14: [], 5004.2: [], 5003.14: [], 5004.15: [], 5003.18: [], 5005.16: [], 5005.83: [], 5005.17: [], 5005.32: [], 5005.12: [], 5006.14: [], 5006.15: [], 5006.17: [], 5006.21: [], 5006.13: [], 5007.15: [], 5007.14: [], 5006.3: [], 17003.48: [], 17004.5: [], 17004.59: [], 17004.52: [], 17004.51: [], 17004.53: [], 17004.49: [], 17006.81: [], 17004.54: [], 17004.45: [], 5009.17: [], 5009.19: [], 5009.39: [], 5009.36: [], 17017.6: [], 5009.18: [], 8003.24: [], 17011.49: [], 17021.51: [], 5010.42: [], 8003.35: [], 17024.48: [], 17025.5: [], 17026.51: [], 17027.54: [], 17026.54: [], 17027.56: [], 17025.54: [], 17025.49: [], 17026.5: [], 17025.53: [], 17025.48: [], 2001.09: [], 2001.84: [], 2001.68: [], 1004.5: [], 2002.18: [], 2003.18: [], 3001.36: [], 3001.68: [], 3001.27: [], 2004.18: [], 3002.43: [], 3003.11: [], 3003.43: [], 9001.42: [], 9002.33: [], 9002.29: [], 9002.3: [], 9002.32: [], 9003.32: [], 9003.28: [], 9003.42: [], 9003.26: [], 9003.3: [], 9004.39: [], 9003.33: [], 9004.48: [], 9005.25: [], 9006.27: [], 9008.24: [], 9008.28: [], 9009.32: [], 9010.27: [], 9011.26: [], 9013.27: [], 9013.28: [], 9014.26: [], 9015.37: [], 9016.33: [], 1001.16: [], 1001.66: [], 5001.13: [], 5001.22: [], 15015.79: [], 3003.25: [], 5002.2: [], 5002.22: [], 5002.11: [], 1003.16: [], 1003.41: [], 5003.47: [], 5003.29: [], 5003.36: [], 5004.11: [], 5005.13: [], 1006.91: [], 1001.01: [], 1001.26: [], 1001.8: [], 1002.22: [], 1002.26: [], 1003.05: [], 1004.47: [], 1005.8: [], 1001.09: [], 1001.17: [], 1002.13: [], 1003.42: [], 1005.09: [], 1006.25: [], 1006.12: [], 1007.13: [], 1008.16: [], 1010.2: [], 1011.74: [], 1014.88: [], 1013.87: [], 1021.42: [], 1014.28: [], 1022.79: [], 1025.89: [], 1025.5: [], 1027.54: [], 1028.88: [], 1027.9: [], 1024.8: [], 1031.76: [], 1024.85: [], 1033.8: [], 1034.68: [], 1033.9: [], 1036.88: [], 1037.74: [], 1038.82: [], 1037.73: [], 1040.8: [], 1041.82: [], 1041.81: [], 1043.9: [], 1044.87: [], 1045.9: [], 1045.89: [], 1042.88: [], 1044.88: [], 1045.88: [], 1042.89: [], 1044.89: [], 1044.9: [], 5008.36: [], 5009.2: [], 10001.29: [], 10001.32: [], 10001.33: [], 10001.36: [], 10002.36: [], 10002.42: [], 10002.26: [], 10002.31: [], 10002.28: [], 10003.33: [], 10003.42: [], 10003.31: [], 10003.36: [], 10003.27: [], 10004.3: [], 10003.45: [], 10004.33: [], 10003.37: [], 10003.3: [], 10005.77: [], 10005.28: [], 10005.3: [], 9002.24: [], 9002.35: [], 2001.25: [], 10008.42: [], 10009.41: [], 10010.32: [], 10010.62: [], 10010.83: [], 9003.35: [], 2002.25: [], 10011.27: [], 10011.26: [], 10011.28: [], 10012.54: [], 10012.28: [], 10012.3: [], 10012.41: [], 10013.31: [], 10013.28: [], 10013.29: [], 10013.36: [], 10014.28: [], 10014.29: [], 10014.3: [], 10014.34: [], 10015.3: [], 10015.54: [], 10015.29: [], 10015.31: [], 2003.25: [], 10016.33: [], 10018.54: [], 10004.62: [], 3002.07: [], 4002.27: [], 4002.11: [], 4002.18: [], 1001.07: [], 1001.82: [], 1001.32: [], 4003.36: [], 4003.11: [], 1001.57: [], 4003.18: [], 1002.32: [], 1003.32: [], 26005.77: [], 26006.75: [], 26006.74: [], 26006.79: [], 26006.81: [], 26006.76: [], 26005.76: [], 26006.82: [], 26008.76: [], 9012.33: [], 1004.07: [], 26031.76: [], 26038.76: [], 26039.78: [], 26039.77: [], 4007.18: [], 6002.2: [], 6002.31: [], 6003.22: [], 6003.47: [], 6003.36: [], 6005.22: [], 6006.22: [], 6006.38: [], 6006.79: [], 6006.2: [], 18001.53: [], 18002.54: [], 18003.54: [], 18004.54: [], 18004.53: [], 18004.62: [], 18005.62: [], 18004.52: [], 18004.51: [], 18005.5: [], 18005.67: [], 18004.56: [], 18005.53: [], 18005.54: [], 18006.54: [], 18007.54: [], 1001.23: [], 18019.54: [], 1001.73: [], 18021.51: [], 18022.54: [], 18023.52: [], 18024.54: [], 18022.53: [], 18026.54: [], 18027.54: [], 18027.53: [], 18029.59: [], 18026.52: [], 18026.53: [], 18027.55: [], 18025.5: [], 18025.54: [], 18030.59: [], 1002.23: [], 6009.36: [], 1003.48: [], 2001.16: [], 2001.07: [], 2001.32: [], 2001.66: [], 2001.82: [], 2002.07: [], 2003.07: [], 15003.47: [], 15003.43: [], 15003.46: [], 15003.5: [], 15004.54: [], 15004.48: [], 15004.51: [], 15004.44: [], 15005.75: [], 15005.43: [], 15005.55: [], 3001.34: [], 3001.25: [], 15007.46: [], 15007.44: [], 2004.07: [], 10003.26: [], 10003.35: [], 3002.25: [], 10001.28: [], 10002.4: [], 10002.27: [], 10004.26: [], 10004.51: [], 10004.5: [], 10002.29: [], 10002.32: [], 10002.33: [], 10003.34: [], 10003.32: [], 10003.28: [], 10004.54: [], 10005.75: [], 10006.28: [], 10009.33: [], 10010.31: [], 10010.35: [], 10010.36: [], 10011.29: [], 10012.29: [], 10013.3: [], 10014.27: [], 10015.28: [], 10016.54: [], 10017.42: [], 15017.41: [], 15018.43: [], 15020.47: [], 2001.28: [], 2001.18: [], 2001.26: [], 2001.24: [], 2001.13: [], 2002.34: [], 2002.09: [], 2002.24: [], 2001.21: [], 2001.17: [], 2003.08: [], 2003.42: [], 2002.2: [], 2002.03: [], 1001.89: [], 1001.14: [], 2003.36: [], 2003.09: [], 2003.05: [], 2003.06: [], 2004.09: [], 2004.12: [], 2005.1: [], 2004.08: [], 2005.09: [], 2006.11: [], 2006.12: [], 2005.14: [], 2006.13: [], 2006.24: [], 2006.15: [], 2006.18: [], 1002.14: [], 1002.39: [], 1003.39: [], 1004.64: [], 1004.14: [], 2001.3: [], 2001.1: [], 2001.19: [], 2001.15: [], 2002.19: [], 2002.23: [], 2002.1: [], 2002.27: [], 2002.16: [], 2003.19: [], 2003.31: [], 2004.39: [], 2004.13: [], 2005.12: [], 2006.14: [], 2007.14: [], 2008.15: [], 2013.87: [], 1006.89: [], 2043.9: [], 2044.9: [], 2045.9: [], 7002.2: [], 1001.05: [], 1001.55: [], 1001.3: [], 1002.05: [], 1002.3: [], 1002.55: [], 1003.3: [], 7007.47: [], 2001.73: [], 2001.23: [], 7009.29: [], 2003.48: [], 20005.59: [], 20005.58: [], 20006.56: [], 4001.16: [], 11003.49: [], 4003.16: [], 27005.8: [], 27006.82: [], 27006.83: [], 4004.66: [], 4004.16: [], 27006.8: [], 27007.8: [], 7001.2: [], 27033.8: [], 7001.19: [], 7001.22: [], 7001.26: [], 7002.24: [], 7002.22: [], 7002.23: [], 7002.18: [], 27041.81: [], 7003.39: [], 7002.27: [], 7003.23: [], 7003.19: [], 7003.3: [], 7004.54: [], 7003.2: [], 7003.18: [], 7004.18: [], 7004.21: [], 7005.18: [], 7005.7: [], 7005.78: [], 4006.16: [], 7006.33: [], 7006.27: [], 7007.25: [], 7007.17: [], 7007.19: [], 7007.58: [], 7007.37: [], 7007.21: [], 7008.2: [], 7008.19: [], 7008.3: [], 7008.27: [], 7008.32: [], 7009.18: [], 7009.19: [], 7009.42: [], 7009.22: [], 7009.2: [], 7010.21: [], 7010.19: [], 7010.22: [], 7010.2: [], 7010.23: [], 7011.24: [], 7011.2: [], 7011.25: [], 7011.21: [], 7011.42: [], 7012.42: [], 7011.22: [], 7012.24: [], 7011.3: [], 19003.59: [], 19004.58: [], 19004.57: [], 19005.57: [], 19004.56: [], 19007.57: [], 19007.56: [], 19004.55: [], 19011.64: [], 19005.68: [], 19004.59: [], 19005.56: [], 19019.68: [], 19023.56: [], 19024.58: [], 19028.56: [], 19029.57: [], 19028.55: [], 19028.57: [], 8002.36: [], 1001.21: [], 1001.46: [], 1001.71: [], 8003.22: [], 1002.46: [], 8004.54: [], 2001.05: [], 2001.14: [], 2001.39: [], 2001.89: [], 2002.14: [], 2002.05: [], 2002.39: [], 2002.3: [], 2003.14: [], 3001.14: [], 3001.32: [], 3004.27: [], 2004.14: [], 11001.45: [], 11002.31: [], 11002.33: [], 11002.32: [], 11002.74: [], 11002.41: [], 11003.38: [], 11003.34: [], 11003.36: [], 11003.31: [], 11003.42: [], 11003.35: [], 11004.42: [], 11005.33: [], 11006.31: [], 11007.32: [], 11011.37: [], 11011.73: [], 11011.4: [], 11013.32: [], 11013.31: [], 11014.32: [], 11014.34: [], 11015.32: [], 11016.32: [], 11017.33: [], 11018.36: [], 11020.51: [], 11021.42: [], 3003.07: [], 3003.23: [], 1001.12: [], 25005.76: [], 25005.75: [], 1002.12: [], 12003.37: [], 5002.23: [], 5002.16: [], 12003.4: [], 5002.25: [], 5002.82: [], 1003.37: [], 12003.44: [], 12004.6: [], 5003.25: [], 12004.35: [], 1004.12: [], 3001.16: [], 3002.15: [], 3002.18: [], 3004.11: [], 3002.22: [], 3001.19: [], 3001.15: [], 3001.18: [], 3002.11: [], 3001.09: [], 3002.09: [], 3003.14: [], 3004.08: [], 3005.1: [], 3006.12: [], 3007.14: [], 3011.73: [], 5004.5: [], 3033.8: [], 5005.18: [], 3035.85: [], 3037.74: [], 12001.4: [], 12001.5: [], 12001.42: [], 5005.27: [], 12002.36: [], 12002.42: [], 12002.34: [], 12002.32: [], 12003.31: [], 12003.32: [], 12003.39: [], 12003.42: [], 12003.45: [], 12004.49: [], 12004.33: [], 12003.35: [], 12003.36: [], 12003.34: [], 12005.37: [], 12005.42: [], 12004.34: [], 5006.16: [], 12005.75: [], 12006.75: [], 12005.36: [], 12005.56: [], 12005.8: [], 5006.18: [], 5006.32: [], 5007.16: [], 12012.63: [], 12013.32: [], 5008.16: [], 12016.54: [], 12016.42: [], 12017.35: [], 12017.38: [], 12017.34: [], 12017.33: [], 12018.37: [], 12018.34: [], 12018.45: [], 12019.39: [], 1001.78: [], 1001.03: [], 1001.53: [], 1002.03: [], 1002.28: [], 2001.12: [], 2001.87: [], 2001.71: [], 2002.12: [], 2002.46: [], 2003.12: [], 2001.22: [], 4001.14: [], 4002.14: [], 4002.23: [], 4003.57: [], 28006.84: [], 28006.83: [], 28006.82: [], 28007.82: [], 28007.83: [], 4003.14: [], 28006.81: [], 4003.39: [], 4004.14: [], 4004.57: [], 4004.07: [], 28041.82: [], 4005.14: [], 4006.14: [], 6001.18: [], 6001.16: [], 6001.23: [], 6002.18: [], 6002.16: [], 6002.25: [], 6002.27: [], 1004.08: [], 1004.58: [], 6003.18: [], 6003.25: [], 6003.23: [], 6003.5: [], 6004.5: [], 6005.27: [], 6005.16: [], 17001.6: [], 20003.58: [], 20004.57: [], 20004.61: [], 20004.62: [], 20004.6: [], 20004.59: [], 20005.6: [], 20004.58: [], 20006.63: [], 20004.64: [], 17003.49: [], 20006.81: [], 17003.54: [], 17004.48: [], 17004.57: [], 20020.71: [], 17005.5: [], 17005.64: [], 17005.49: [], 6006.16: [], 17005.51: [], 20020.67: [], 20024.63: [], 20030.59: [], 20030.6: [], 17007.49: [], 20032.63: [], 20030.61: [], 17009.49: [], 6007.27: [], 1001.44: [], 1001.19: [], 6008.16: [], 1002.19: [], 6009.41: [], 1003.44: [], 17024.54: [], 17024.47: [], 17025.51: [], 4001.15: [], 4001.19: [], 4001.18: [], 4001.12: [], 4001.1: [], 4002.19: [], 4002.28: [], 4002.1: [], 4002.15: [], 4001.11: [], 4003.09: [], 4003.3: [], 4003.15: [], 4003.4: [], 4002.22: [], 4004.09: [], 4004.26: [], 4004.1: [], 4004.15: [], 4004.11: [], 4005.13: [], 4004.4: [], 4005.11: [], 4005.15: [], 4005.18: [], 4006.1: [], 4006.15: [], 4005.1: [], 4005.12: [], 4006.11: [], 4007.12: [], 4006.12: [], 4006.22: [], 4006.13: [], 4007.14: [], 4007.15: [], 4008.16: [], 4007.27: [], 4008.15: [], 4007.13: [], 12001.35: [], 12001.36: [], 12003.38: [], 12004.63: [], 12005.74: [], 12003.41: [], 12004.37: [], 12004.57: [], 12004.42: [], 12004.53: [], 12004.36: [], 3001.12: [], 12007.37: [], 12008.39: [], 12009.36: [], 12015.36: [], 12015.37: [], 3001.21: [], 12019.38: [], 12014.34: [], 12012.43: [], 12016.36: [], 12017.36: [], 12018.36: [], 12018.35: [], 12018.39: [], 12021.42: [], 12020.39: [], 12020.42: [], 12022.54: [], 3002.12: [], 3002.3: [], 3002.05: [], 3003.05: [], 3003.3: [], 3003.8: [], 1001.1: [], 1001.6: [], 1001.35: [], 1001.85: [], 1002.1: [], 1003.1: [], 1003.35: [], 4001.09: [], 4001.2: [], 4001.24: [], 4002.17: [], 4002.2: [], 4002.16: [], 4002.29: [], 4002.09: [], 4002.21: [], 4002.3: [], 4003.35: [], 4003.12: [], 4004.08: [], 4005.09: [], 4007.17: [], 4007.16: [], 4008.14: [], 4009.16: [], 4010.2: [], 1004.1: [], 1004.6: [], 4025.89: [], 4027.9: [], 4036.88: [], 1005.1: [], 4041.82: [], 4044.87: [], 4045.9: [], 4045.88: [], 7001.25: [], 7002.34: [], 7003.32: [], 7003.25: [], 7003.27: [], 7003.48: [], 1001.51: [], 1001.76: [], 22004.63: [], 22005.62: [], 22005.64: [], 22005.63: [], 22005.66: [], 22005.67: [], 22006.76: [], 22005.68: [], 22005.65: [], 1003.51: [], 1003.26: [], 2001.85: [], 1005.26: [], 2003.1: [], 9001.27: [], 9001.25: [], 9001.28: [], 9001.35: [], 9001.36: [], 9002.27: [], 9002.26: [], 9002.25: [], 9002.28: [], 9002.23: [], 9003.36: [], 9003.25: [], 9003.24: [], 9003.31: [], 9003.27: [], 9004.27: [], 9003.4: [], 9003.37: [], 9004.54: [], 9003.68: [], 9005.24: [], 9005.27: [], 9004.38: [], 9004.26: [], 9004.45: [], 9007.82: [], 22033.65: [], 9008.42: [], 9009.33: [], 9009.56: [], 9009.42: [], 9009.75: [], 9009.41: [], 9010.25: [], 9010.24: [], 9010.35: [], 9011.27: [], 9011.42: [], 9012.26: [], 9012.27: [], 9012.23: [], 9012.29: [], 9012.25: [], 9013.26: [], 9013.25: [], 9013.33: [], 9013.29: [], 9013.31: [], 9014.28: [], 9014.27: [], 9013.3: [], 9013.42: [], 9013.24: [], 9015.31: [], 9015.3: [], 9015.32: [], 9015.41: [], 9016.53: [], 4001.55: [], 9016.42: [], 4002.12: [], 4002.37: [], 29007.84: [], 1001.67: [], 4003.37: [], 29036.88: [], 29042.84: [], 29044.87: [], 4004.12: [], 4004.8: [], 1004.17: [], 4006.21: [], 4007.37: [], 21004.64: [], 21004.63: [], 21005.64: [], 21005.62: [], 21004.62: [], 21005.63: [], 21006.81: [], 21005.68: [], 21004.74: [], 21005.61: [], 21005.74: [], 21007.82: [], 21026.63: [], 21030.62: [], 21031.62: [], 21032.68: [], 21032.63: [], 21032.64: [], 21032.62: [], 21037.88: [], 8002.25: [], 8003.27: [], 8004.25: [], 8004.57: [], 2001.01: [], 13001.39: [], 13001.53: [], 13003.39: [], 13003.38: [], 13003.4: [], 13003.37: [], 13003.36: [], 13003.41: [], 13003.42: [], 13004.62: [], 13005.64: [], 13003.45: [], 13013.43: [], 13013.47: [], 13013.46: [], 13015.37: [], 13017.36: [], 13018.38: [], 13019.42: [], 13020.4: [], 13020.39: [], 13021.42: [], 13019.38: [], 13018.36: [], 13019.39: [], 13018.35: [], 13020.42: [], 13020.38: [], 13023.45: [], 3002.1: [], 3002.19: [], 3002.28: [], 3003.1: [], 3003.28: [], 8010.41: [], 3002.14: [], 14003.43: [], 3004.1: [], 14003.42: [], 14003.41: [], 14003.4: [], 14004.54: [], 14004.42: [], 14004.51: [], 14004.45: [], 14003.45: [], 14005.61: [], 14005.78: [], 14005.5: [], 14005.42: [], 14004.49: [], 8012.41: [], 1001.08: [], 1001.58: [], 1001.83: [], 27039.78: [], 14014.74: [], 27040.8: [], 14015.41: [], 27041.8: [], 14016.39: [], 14019.42: [], 5001.15: [], 5002.19: [], 5003.54: [], 5003.13: [], 5003.16: [], 5004.13: [], 5001.14: [], 5001.16: [], 5001.12: [], 5001.35: [], 5002.13: [], 5003.33: [], 5004.54: [], 5005.14: [], 5006.2: [], 5007.13: [], 5008.18: [], 5009.15: [], 5010.19: [], 5013.26: [], 5013.4: [], 5026.53: [], 5003.3: [], 5003.28: [], 5003.46: [], 1001.04: [], 1001.87: [], 1001.25: [], 1001.42: [], 1001.06: [], 1002.17: [], 1002.09: [], 1002.04: [], 1002.21: [], 1002.25: [], 1002.16: [], 1002.33: [], 1003.08: [], 1002.08: [], 1002.07: [], 1004.54: [], 1003.12: [], 1003.07: [], 1003.27: [], 1003.14: [], 1005.14: [], 1005.68: [], 1004.09: [], 1005.12: [], 1004.49: [], 1006.13: [], 1006.49: [], 1005.82: [], 1005.57: [], 1006.36: [], 1007.17: [], 1007.41: [], 1007.14: [], 1007.15: [], 1006.79: [], 1007.82: [], 1007.9: [], 1007.12: [], 1009.18: [], 1010.83: [], 5005.71: [], 1011.73: [], 1012.8: [], 1012.24: [], 1012.86: [], 1013.26: [], 5006.12: [], 5006.28: [], 1019.38: [], 10004.56: [], 1022.44: [], 1023.46: [], 1024.89: [], 5008.14: [], 5008.28: [], 1029.58: [], 5009.3: [], 5009.53: [], 2002.74: [], 1034.67: [], 1001.74: [], 1001.24: [], 1039.78: [], 1039.9: [], 1041.8: [], 1002.74: [], 1003.24: [], 9002.36: [], 2001.08: [], 9003.41: [], 2002.08: [], 2002.33: [], 2002.42: [], 2002.17: [], 1005.74: [], 3001.46: [], 2003.67: [], 2004.42: [], 2005.08: [], 4001.28: [], 30006.9: [], 30006.89: [], 30006.88: [], 4002.35: [], 4002.44: [], 30036.88: [], 30045.9: [], 30045.88: [], 30045.89: [], 1001.9: [], 1001.15: [], 1001.4: [], 4003.1: [], 4003.28: [], 1002.15: [], 1003.9: [], 1003.4: [], 1004.4: [], 6001.21: [], 6002.28: [], 6002.21: [], 6002.19: [], 6002.44: [], 1006.9: [], 6001.17: [], 6001.15: [], 6001.14: [], 22004.64: [], 22004.62: [], 6002.26: [], 6002.24: [], 6002.14: [], 6002.23: [], 6002.33: [], 6002.32: [], 6002.3: [], 6003.21: [], 6003.16: [], 6003.2: [], 6003.33: [], 6003.4: [], 6003.37: [], 6003.39: [], 6003.17: [], 6005.15: [], 6004.16: [], 22022.79: [], 6005.18: [], 6004.19: [], 6006.17: [], 22027.66: [], 6006.21: [], 6006.18: [], 6006.15: [], 6007.15: [], 6007.17: [], 22033.66: [], 22034.68: [], 22032.68: [], 6008.18: [], 6008.15: [], 6008.2: [], 6007.14: [], 6008.21: [], 6009.19: [], 6009.21: [], 6009.16: [], 6008.42: [], 6009.42: [], 6010.21: [], 6010.18: [], 6009.18: [], 6009.17: [], 6010.41: [], 6011.24: [], 6005.71: [], 6011.42: [], 6010.22: [], 6012.27: [], 6012.51: [], 6006.37: [], 6006.19: [], 1001.56: [], 1002.06: [], 1002.31: [], 6009.28: [], 1003.06: [], 2001.74: [], 14001.42: [], 14002.42: [], 14003.47: [], 14003.44: [], 14003.48: [], 14003.39: [], 14003.46: [], 14004.58: [], 14004.48: [], 14004.4: [], 14011.41: [], 14003.53: [], 14004.41: [], 14006.78: [], 14007.42: [], 14007.45: [], 14008.43: [], 14014.5: [], 14017.42: [], 14017.41: [], 14021.51: [], 14019.51: [], 14022.45: [], 14020.42: [], 14021.42: [], 14021.41: [], 14020.4: [], 14023.45: [], 14026.51: [], 14022.54: [], 14022.43: [], 14023.54: [], 10002.3: [], 3001.17: [], 3001.24: [], 3001.74: [], 3001.76: [], 3001.33: [], 10003.29: [], 3002.08: [], 3002.24: [], 3002.17: [], 10003.54: [], 10003.41: [], 10003.39: [], 10004.36: [], 3003.08: [], 3003.17: [], 10004.29: [], 8002.33: [], 6001.22: [], 6001.36: [], 6003.3: [], 6003.42: [], 6004.54: [], 6003.19: [], 6001.2: [], 6001.28: [], 6004.17: [], 6002.22: [], 6002.15: [], 6003.24: [], 6004.18: [], 6005.17: [], 6006.4: [], 6007.16: [], 6008.19: [], 6009.2: [], 6010.2: [], 6011.21: [], 6012.24: [], 6013.4: [], 6034.68: [], 3002.06: [], 7001.28: [], 7002.21: [], 7002.19: [], 7003.21: [], 11001.36: [], 11001.46: [], 11002.3: [], 11003.39: [], 11003.48: [], 11003.37: [], 11003.3: [], 11003.32: [], 11004.33: [], 11004.31: [], 11004.53: [], 11003.33: [], 11003.54: [], 11005.74: [], 11004.52: [], 11004.49: [], 11004.54: [], 11004.34: [], 11006.32: [], 11006.6: [], 11006.84: [], 1001.22: [], 1001.72: [], 11011.39: [], 11011.58: [], 11013.3: [], 11014.42: [], 1002.47: [], 11015.33: [], 11015.29: [], 11015.39: [], 11016.31: [], 11016.36: [], 11016.33: [], 11016.42: [], 11017.34: [], 11017.32: [], 11018.38: [], 1003.22: [], 11020.39: [], 2001.06: [], 2001.81: [], 2001.9: [], 2002.06: [], 2002.15: [], 2002.9: [], 7010.3: [], 2004.06: [], 2005.15: [], 31006.91: [], 4001.08: [], 4001.17: [], 31045.9: [], 31045.89: [], 4002.08: [], 4002.24: [], 11004.32: [], 1001.88: [], 1001.13: [], 1001.38: [], 4003.42: [], 4003.49: [], 1001.63: [], 4003.08: [], 4003.33: [], 4004.24: [], 4004.17: [], 1003.38: [], 1003.13: [], 1004.13: [], 23003.68: [], 23005.68: [], 23005.73: [], 23005.67: [], 23005.69: [], 23005.74: [], 23005.66: [], 1006.88: [], 23028.68: [], 23031.68: [], 23032.68: [], 23033.68: [], 23034.68: [], 23035.69: [], 23034.67: [], 8001.28: [], 8002.28: [], 1001.79: [], 1001.54: [], 1001.29: [], 8003.21: [], 8003.28: [], 8003.39: [], 16003.49: [], 16003.45: [], 16004.48: [], 16004.53: [], 16004.45: [], 16004.49: [], 16004.54: [], 16005.47: [], 16004.47: [], 3003.16: [], 16005.46: [], 8004.53: [], 15001.54: [], 15001.45: [], 15003.45: [], 15004.49: [], 15004.42: [], 15005.45: [], 15005.44: [], 15003.44: [], 15004.43: [], 15004.52: [], 15007.83: [], 15003.48: [], 15004.45: [], 15006.48: [], 15015.53: [], 15015.54: [], 15008.44: [], 15018.44: [], 15019.42: [], 15020.51: [], 15021.45: [], 15022.44: [], 15023.45: [], 15022.45: [], 15023.46: [], 15026.54: [], 15022.46: [], 15025.54: [], 15022.43: [], 15023.44: [], 15027.54: [], 2002.22: [], 16023.44: [], 3001.06: [], 3001.22: [], 3001.9: [], 3001.08: [], 3001.07: [], 3001.11: [], 3001.13: [], 3001.1: [], 3002.23: [], 3002.21: [], 3002.2: [], 3002.16: [], 3002.42: [], 3003.06: [], 3003.19: [], 3003.18: [], 3003.42: [], 3003.13: [], 3003.09: [], 3003.12: [], 3004.09: [], 3004.12: [], 3004.14: [], 3005.12: [], 3005.08: [], 3005.11: [], 3005.82: [], 3004.07: [], 3006.11: [], 3006.09: [], 3006.14: [], 3005.14: [], 3005.09: [], 3007.13: [], 2004.1: [], 3006.2: [], 3007.28: [], 3006.24: [], 3003.15: [], 3008.16: [], 3004.06: [], 8011.39: [], 8011.28: [], 3005.15: [], 7001.21: [], 7002.25: [], 7002.31: [], 7002.17: [], 7002.3: [], 7002.28: [], 7002.42: [], 7003.36: [], 7003.28: [], 7004.2: [], 7004.19: [], 7005.19: [], 7006.25: [], 7006.19: [], 7007.23: [], 7007.2: [], 7008.21: [], 7009.21: [], 7010.24: [], 7011.23: [], 7012.23: [], 7013.27: [], 7014.42: [], 3006.22: [], 5001.17: [], 5002.17: [], 5002.15: [], 5002.42: [], 5002.26: [], 5002.35: [], 1001.5: [], 12004.43: [], 5003.24: [], 5003.42: [], 5005.15: [], 5005.67: [], 5005.42: [], 5005.26: [], 5006.4: [], 5006.24: [], 5007.17: [], 5007.24: [], 5008.15: [], 5008.17: [], 5009.24: [], 1001.7: [], 1001.2: [], 1002.2: [], 1003.2: [], 1003.45: [], 2001.04: [], 2001.38: [], 2001.29: [], 2002.04: [], 2002.13: [], 2002.29: [], 21004.61: [], 2003.04: [], 4001.06: [], 8001.23: [], 8001.21: [], 8001.22: [], 8001.36: [], 8001.27: [], 8002.32: [], 8002.22: [], 8002.24: [], 8002.21: [], 8002.23: [], 8003.43: [], 8003.25: [], 8003.4: [], 8003.23: [], 8003.42: [], 8003.26: [], 8004.22: [], 8004.24: [], 8004.23: [], 8003.41: [], 8005.64: [], 8005.24: [], 8005.22: [], 8005.26: [], 8005.78: [], 8005.53: [], 8007.2: [], 1001.86: [], 1001.11: [], 8008.28: [], 8008.5: [], 4003.31: [], 1001.36: [], 8009.21: [], 8009.42: [], 8008.66: [], 8008.25: [], 8009.24: [], 8010.22: [], 8010.27: [], 8010.24: [], 8010.25: [], 8010.2: [], 8011.23: [], 8011.21: [], 8011.42: [], 8011.22: [], 8011.27: [], 8012.25: [], 8012.34: [], 8012.42: [], 8012.24: [], 1002.11: [], 8013.26: [], 8013.4: [], 8012.23: [], 8012.26: [], 8013.25: [], 1002.36: [], 8013.42: [], 8015.42: [], 1003.36: [], 4005.9: [], 4005.22: [], 24005.75: [], 24006.7: [], 24007.83: [], 24006.73: [], 24008.71: [], 24005.71: [], 24006.82: [], 24005.72: [], 24006.71: [], 1005.86: [], 6002.17: [], 24024.75: [], 24030.73: [], 1006.86: [], 24034.73: [], 24035.71: [], 24036.72: [], 24036.71: [], 24035.72: [], 24035.7: [], 24035.73: [], 6003.15: [], 6003.26: [], 5002.14: [], 6005.74: [], 6007.42: [], 6007.4: [], 1001.02: [], 1001.52: [], 1001.27: [], 6008.17: [], 1002.02: [], 1002.27: [], 16002.48: [], 16003.47: [], 16004.51: [], 16004.64: [], 16003.54: [], 16004.58: [], 16005.48: [], 16006.49: [], 16003.48: [], 16004.46: [], 16003.46: [], 16005.73: [], 16007.49: [], 16006.67: [], 16016.53: [], 16016.57: [], 16019.47: [], 16020.54: [], 16020.46: [], 16022.45: [], 16023.48: [], 16024.47: [], 16024.48: [], 16026.51: [], 16027.54: [], 16026.54: [], 16025.51: [], 16023.46: [], 16024.49: [], 16023.47: [], 2001.2: [], 2001.11: [], 2001.61: [], 2001.7: [], 1004.52: [], 2002.11: [], 2003.11: [], 3001.2: [], 3001.04: [], 3002.13: [], 3002.88: [], 3003.2: [], 8001.24: [], 8002.27: [], 8003.36: [], 8004.27: [], 8005.23: [], 8002.26: [], 8002.3: [], 8008.29: [], 8008.27: [], 8008.53: [], 8002.29: [], 8003.33: [], 8004.62: [], 8005.62: [], 8006.8: [], 8007.28: [], 8008.22: [], 8009.22: [], 8010.23: [], 8011.24: [], 8012.22: [], 8013.27: [], 8014.27: [], 8016.33: [], 8016.42: [], 8027.54: [], 1001.18: [], 1001.68: [], 1002.18: [], 13003.35: [], 13003.44: [], 13003.74: [], 13003.82: [], 13003.48: [], 13004.39: [], 13004.37: [], 13004.35: [], 13004.38: [], 13004.36: [], 13005.48: [], 13004.54: [], 13004.49: [], 13004.45: [], 13007.42: [], 13007.41: [], 13015.36: [], 13016.39: [], 13017.37: [], 13018.39: [], 13018.48: [], 13019.37: []}
    patterns_epoch_1 = {1001.02: [], 1001.03: [], 1001.1: [], 1002.03: [], 1045.89: [], 1045.9: [], 2001.06: [], 2003.06: [], 3001.07: [], 3001.08: [], 3001.09: [], 3004.08: [], 4001.12: [], 4006.12: [], 5001.13: [], 5001.14: [], 5001.15: [], 5007.14: [], 7002.2: [], 14003.42: [], 14021.42: [], 18004.54: [], 18027.54: [], 1: []}

    count_static = 0
    # count_static_epoch_2 = 0
    count_static_epoch_1 = 0
    total_insts_epoch_1 = 0
    # total_insts_epoch_2 = 0
    no_patterns_list_epoch_1 = []
    # no_patterns_list_epoch_2 = []
    for num, inst in enumerate(instList, 1):
        # print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')

        if df.SpotPrice.nunique() == 1:
            count_static += 1
        else:
            if not df.empty:
                total_insts_epoch_1 += 1
                if df.SpotPrice.nunique() == 1:
                    count_static_epoch_1 += 1
                else:
                    no_pat_flag = 0
                    for pat in patterns_epoch_1.keys():
                        # pat_count = df.pattern_mark_2[df.pattern_mark_2 == pat].count()
                        pat_count = df.pattern_mark_3[df.pattern_mark_3 == pat].count()
                        # if pat == 3004.08 and pat_count == 513:
                        #     print("######## 513", inst)
                        # elif pat == 1002.03 and pat_count == 476:
                        #     print("******** 476", inst)
                        if pat_count > 0:
                            patterns_epoch_1[pat].append(pat_count)
                            no_pat_flag = 1
                    if no_pat_flag == 0:
                        no_patterns_list_epoch_1.append(inst)
                        # ax1.plot(df.Timestamp, df.NormPrice, '.:')
            # df1 = df[df.Timestamp < pd.to_datetime("12/1/2019 00:00:00 AM")].copy()
            # if not df1.empty:
            #     total_insts_epoch_1 += 1
            #     if df1.SpotPrice.nunique() == 1:
            #         count_static_epoch_1 += 1
            #     else:
            #         no_pat_flag = 0
            #         for pat in patterns_epoch_1.keys():
            #             if df1.pattern_mark[df1.pattern_mark == pat].count() > 0:
            #                 patterns_epoch_1[pat] += 1
            #                 no_pat_flag = 1
            #         if no_pat_flag == 0:
            #             no_patterns_list_epoch_1.append(inst)
            #             ax1.plot(df1.Timestamp, df1.NormPrice, '.:')

            # df2 = df[df.Timestamp >= pd.to_datetime("12/1/2019 00:00:00 AM")].copy()
            # if not df2.empty:
            #     total_insts_epoch_2 += 1
            #     if df2.SpotPrice.nunique() == 1:
            #         count_static_epoch_2 += 1
            #     else:
            #         no_pat_flag = 0
            #         for pat in patterns_epoch_2.keys():
            #             if df2.pattern_mark[df2.pattern_mark == pat].count() > 0:
            #                 patterns_epoch_2[pat] += 1
            #                 no_pat_flag = 1
            #         if no_pat_flag == 0:
            #             no_patterns_list_epoch_2.append(inst)
            #             ax2.plot(df2.Timestamp, df2.NormPrice, '.:')

    print("Number of insts that are completely static:", count_static)

    # print("Epoch 1:")
    print("total number of insts: ", total_insts_epoch_1)
    print("Static insts:", count_static_epoch_1)
    print("Insts without patterns:", len(no_patterns_list_epoch_1))

    print("Pattern, Number of instances, Min, Max, Mean, Median")
    # print("Pattern, Number of instances, Min, Max, Mean")
    for key, value in patterns_epoch_1.items():
        length = len(value)
        if length > 0:
            value.sort()
            mid = length // 2
            res = (value[mid] + value[~mid]) / 2
            print(key, ",", length, ",", min(value), ",", max(value), ",", sum(value)/length, ",", res)
            # print(key, ",", length, ",", min(value), ",", max(value), ",", sum(value) / length)
        else:
            print(key, ",", length, " , 0, 0, 0, 0")
            # print(key, ",", length, " , 0, 0, 0")

    # print("Number of insts that show each pattern:", patterns_epoch_1.items())

    # print("Epoch 2:")
    # print("total number of insts: ", total_insts_epoch_2)
    # print("Static insts:", count_static_epoch_2)
    # print("Number of insts that show each pattern:", patterns_epoch_2.items())
    # print("Insts without patterns:", len(no_patterns_list_epoch_2))

    # plt.show()
    # print("Total static:", count_static)
    # print("Total insts epoch 2:", total_insts_epoch_2)
    # print("Static in second epoch:", count_static_epoch_2)


def mark_patterns_by_peak():
    # instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    # instList = glob.glob(instList)
    instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square.csv']
    total_count = len(instList)

    # interesting_pats = [1001.02,1001.03,1002.03,2001.06,2003.06,3001.07,3001.08,3001.09,3004.08,3005.09,4001.12,4006.12,5001.14,5001.15,5007.14,14003.42]
    not_interesting_pats = [7002.4, 14004.43, 7003.24, 7003.26, 8002.42, 2001.52, 2001.02, 7008.42, 7009.26, 5007.2,
                            8012.28, 1001.64, 4002.13, 1001.84, 1001.59, 4003.13, 4004.13, 2001.03, 1003.09, 25003.74,
                            25005.74, 25006.75, 25006.76, 25006.74, 25005.73, 25006.73, 25005.78, 25006.78, 25005.71,
                            25011.74, 25006.71, 25025.89, 25028.74, 18001.54, 4007.13, 25032.74, 25038.75, 25037.73,
                            25037.75, 18004.64, 2002.28, 18023.54, 18024.5, 5001.35, 5001.18, 5001.11, 5001.27, 5001.2,
                            5002.28, 5002.12, 5002.04, 5002.24, 5002.18, 5003.31, 5002.21, 5003.15, 5003.5, 5004.14,
                            5004.2, 5003.17, 5003.14, 5004.15, 5005.16, 5005.83, 5005.17, 5005.32, 5005.12, 5006.15,
                            5006.17, 5006.21, 5006.13, 5007.15, 5006.3, 17003.48, 17004.5, 17004.59, 17004.52, 17004.53,
                            17006.81, 17004.54, 17004.45, 5009.17, 5009.19, 5009.39, 5009.36, 17017.6, 5009.18,
                            17011.49, 17021.51, 5010.42, 8003.35, 17024.48, 17025.5, 17026.51, 17027.54, 17026.54,
                            17027.56, 17025.54, 17025.49, 17026.5, 17025.53, 17025.48, 2001.84, 2001.68, 1004.5,
                            2002.18, 2003.18, 3001.36, 3001.68, 3001.27, 2004.18, 3002.43, 3003.11, 3003.43, 9001.42,
                            9002.33, 9002.29, 9002.3, 9002.32, 9003.32, 9003.28, 9003.42, 9003.26, 9003.3, 9004.39,
                            9003.33, 9004.48, 9005.25, 9006.27, 9008.24, 9008.28, 9009.32, 9010.27, 9011.26, 9013.27,
                            9013.28, 9014.28, 9015.37, 9016.33, 1001.66, 5001.22, 15015.79, 3003.25, 5002.2, 5002.22,
                            5002.11, 1003.16, 1003.41, 5003.47, 5003.29, 5003.36, 5004.11, 5005.13, 1006.91, 1001.26,
                            1001.8, 1002.05, 1002.22, 1002.26, 1003.05, 1004.47, 1005.8, 1002.13, 1005.09, 1004.54,
                            1006.25, 1006.12, 1007.13, 1008.16, 1010.2, 1011.74, 1014.88, 1013.87, 1021.42, 1014.28,
                            1022.79, 1025.89, 1025.5, 1027.54, 1028.88, 1027.9, 1024.8, 1031.76, 1024.85, 1033.8,
                            1034.68, 1033.9, 1036.88, 1037.74, 1038.82, 1037.73, 1040.8, 1041.82, 1041.81, 1043.9,
                            1044.87, 1042.88, 1045.88, 1042.89, 1041.8, 1044.89, 1044.9, 1044.88, 5008.36, 5009.2,
                            5006.06, 10001.29, 10001.32, 10001.33, 10001.36, 10002.36, 10002.42, 10002.26, 10002.31,
                            10003.33, 10003.42, 10003.31, 10003.36, 10003.27, 10004.3, 10003.45, 10004.33, 10003.37,
                            10005.77, 10005.28, 10005.3, 9002.24, 9002.35, 2001.25, 10008.42, 10009.41, 10010.32,
                            10010.62, 10010.83, 9003.35, 2002.25, 10011.27, 10011.26, 10011.28, 10012.54, 10012.28,
                            10012.3, 10012.41, 10013.31, 10013.28, 10013.29, 10013.36, 10014.29, 10014.3, 10014.34,
                            10015.54, 10015.29, 10015.31, 2003.25, 10016.33, 10018.54, 10004.62, 4002.27, 4002.18,
                            1001.82, 1001.32, 4003.36, 4003.11, 1001.57, 4003.18, 1002.32, 1003.07, 26005.77, 26006.82,
                            26006.75, 26006.74, 26006.79, 26006.81, 26006.76, 26005.76, 1003.32, 26008.76, 9012.33,
                            1004.07, 26031.76, 26038.76, 26039.78, 26039.77, 4007.18, 6002.2, 6002.31, 6003.22, 6003.47,
                            6003.36, 6003.06, 6005.22, 6006.22, 6006.38, 6006.79, 6006.2, 18001.53, 18002.54, 18003.54,
                            18004.53, 18004.62, 18005.62, 18004.52, 18004.51, 18005.5, 18005.67, 18004.56, 18005.53,
                            18005.54, 18006.54, 18007.54, 1001.23, 18019.54, 1001.73, 18021.51, 18022.54, 18023.52,
                            18024.54, 18022.53, 18026.54, 18027.53, 18029.59, 18026.52, 18026.53, 18027.55, 18025.5,
                            18025.54, 18030.59, 1002.23, 6009.36, 1003.48, 2001.16, 2001.32, 2001.66, 2001.82, 2002.07,
                            15003.47, 15003.43, 15003.46, 15003.5, 15004.54, 15004.48, 15004.51, 15004.44, 15005.75,
                            15005.43, 15005.55, 3001.34, 3001.25, 15007.46, 15007.44, 2004.07, 10003.26, 10003.35,
                            3002.25, 10001.28, 10002.4, 10002.27, 10004.26, 10004.51, 10004.5, 10002.29, 10002.32,
                            10002.33, 10003.34, 10003.32, 10003.28, 10004.54, 10005.75, 10006.28, 10009.33, 10010.31,
                            10010.35, 10010.36, 10011.29, 10012.29, 10013.3, 10014.27, 10015.28, 10016.54, 10017.42,
                            15017.41, 15018.43, 15020.47, 2001.28, 2001.18, 2001.26, 2001.24, 2001.13, 2002.34, 2002.09,
                            2002.24, 2001.21, 2001.17, 2003.42, 2002.2, 2002.03, 2002.11, 1001.89, 2003.09, 2003.12,
                            2004.09, 2004.12, 2005.1, 2005.09, 2006.11, 2006.12, 2005.14, 2006.13, 2006.24, 2006.15,
                            2006.18, 1002.14, 1002.39, 1003.39, 1004.64, 1004.14, 2001.3, 2001.1, 2001.19, 2001.15,
                            2002.19, 2002.23, 2002.1, 2002.27, 2002.16, 2003.19, 2003.31, 2004.39, 2004.13, 2005.12,
                            2006.14, 2007.14, 2008.15, 2013.87, 1006.89, 2043.9, 2044.9, 2045.9, 1001.55, 1001.3,
                            1002.3, 1002.55, 1003.3, 7007.47, 2001.73, 2001.23, 7009.29, 2003.48, 20005.59, 20005.58,
                            20006.56, 4001.16, 11003.49, 4003.16, 27005.8, 27006.82, 27006.83, 4004.66, 4004.16,
                            27006.8, 27007.8, 7001.21, 27033.8, 7001.2, 7001.17, 7001.22, 7002.24, 7002.22, 7002.23,
                            7002.18, 27041.81, 7003.39, 7002.27, 7003.23, 7003.19, 7003.3, 7004.54, 7003.2, 7003.18,
                            7004.18, 7004.21, 7005.18, 7005.7, 7005.78, 4006.16, 7006.33, 7006.27, 7007.25, 7007.17,
                            7007.19, 7007.58, 7007.37, 7007.21, 7008.2, 7008.19, 7008.3, 7008.27, 7008.32, 7009.21,
                            7009.18, 7009.19, 7009.42, 7009.22, 7010.21, 7010.19, 7010.22, 7010.23, 7011.24, 7011.2,
                            7011.25, 7011.42, 7012.42, 7011.22, 7012.24, 7011.3, 19003.59, 19004.58, 19004.57, 19005.57,
                            19004.56, 19007.57, 19007.56, 19004.55, 19011.64, 19005.68, 19004.59, 19005.56, 19019.68,
                            19023.56, 19024.58, 19028.56, 19029.57, 19028.55, 19028.57, 8002.36, 1001.21, 1001.46,
                            1001.71, 8003.22, 1002.46, 8004.54, 2001.14, 2001.39, 2001.89, 2002.14, 2002.39, 2002.3,
                            2003.14, 3001.32, 3004.27, 2004.14, 11001.45, 11002.31, 11002.33, 11002.74, 11002.41,
                            11003.38, 11003.34, 11003.36, 11003.31, 11003.42, 11003.35, 11004.42, 11005.33, 11006.31,
                            11007.32, 11011.37, 11011.73, 11011.4, 11013.32, 11013.31, 11014.32, 11014.34, 11015.32,
                            11016.32, 11018.36, 11020.51, 11021.42, 3003.07, 3003.23, 25005.76, 25005.75, 1002.12,
                            12003.4, 5002.23, 5002.16, 5002.25, 5002.82, 1003.37, 12003.44, 12004.6, 5003.18, 5003.25,
                            12004.35, 1004.12, 3001.16, 3002.15, 3002.18, 3004.11, 3002.22, 3001.19, 3001.15, 3001.18,
                            3002.11, 3003.14, 3006.12, 3007.14, 3011.73, 5004.5, 3033.8, 5005.18, 3035.85, 3037.74,
                            12001.4, 12001.5, 12001.42, 5005.27, 12002.36, 12002.42, 12002.34, 12002.32, 12003.42,
                            12003.39, 12003.45, 12003.35, 12004.49, 12004.33, 12004.36, 12003.34, 12003.37, 12005.37,
                            12005.42, 12004.34, 5006.16, 12005.75, 12006.75, 12005.36, 12005.56, 12005.8, 5006.18,
                            5006.32, 5007.16, 12012.63, 12013.32, 5008.16, 12016.54, 12016.42, 12017.35, 12017.38,
                            12017.34, 12017.33, 12018.37, 12018.45, 12019.39, 1001.78, 1001.53, 1002.28, 2001.87,
                            2001.71, 2002.12, 2002.46, 1005.03, 2001.22, 4002.14, 4002.23, 4003.57, 28006.84, 28006.83,
                            28006.82, 28007.82, 28007.83, 4003.14, 28006.81, 4003.39, 4004.14, 4005.14, 4006.14,
                            6001.18, 6001.23, 6002.25, 6002.27, 1004.58, 6003.18, 6003.25, 6003.23, 6003.5, 6004.09,
                            6004.5, 6005.27, 6005.16, 17001.6, 20003.58, 20004.57, 20004.61, 20004.62, 20004.6,
                            20004.59, 20005.6, 20004.58, 20006.63, 20004.64, 17003.49, 20006.81, 17003.54, 17004.48,
                            17004.57, 20020.71, 17005.5, 17005.64, 17005.49, 6006.16, 17005.51, 20020.67, 20024.63,
                            20030.59, 20030.6, 17007.49, 20032.63, 20030.61, 17009.49, 6007.27, 1001.44, 1001.19,
                            1002.19, 6009.41, 1003.44, 17024.54, 17024.47, 17025.51, 4001.19, 4001.03, 4001.18, 4002.19,
                            4002.28, 4002.15, 4003.09, 4003.3, 4003.15, 4003.4, 4004.09, 4004.26, 4004.57, 4004.1,
                            4004.15, 4005.13, 4004.11, 4004.4, 4005.11, 4005.15, 4005.18, 4006.1, 4006.15, 4007.12,
                            4006.22, 4006.13, 4007.15, 4008.16, 4007.27, 4008.15, 12001.35, 12001.36, 12003.38,
                            12004.63, 12005.74, 12003.41, 12004.37, 12004.57, 12004.42, 12004.53, 12003.32, 12007.37,
                            12008.39, 12009.36, 12015.36, 12015.37, 12018.34, 12019.38, 12014.34, 12012.43, 12016.36,
                            12017.36, 12018.35, 12018.39, 12021.42, 12020.39, 12020.42, 12022.54, 3002.12, 3002.3,
                            3002.05, 3003.05, 3003.3, 3003.8, 1001.6, 1001.35, 1001.85, 1002.1, 1003.1, 1003.35,
                            4001.09, 4001.2, 4001.24, 4002.17, 4002.2, 4002.16, 4002.29, 4002.09, 4002.21, 4002.3,
                            4003.35, 4003.12, 4004.08, 4005.09, 4007.17, 4007.16, 4008.14, 4009.16, 4010.2, 1004.1,
                            1004.6, 4025.89, 4027.9, 4036.88, 1005.1, 4041.82, 4044.87, 4045.9, 4045.88, 7001.25,
                            7002.34, 7003.32, 7003.25, 7003.27, 7003.48, 1001.51, 1001.76, 22004.63, 22005.62, 22005.64,
                            22005.63, 22005.66, 22005.67, 22006.76, 22005.68, 22005.65, 1003.51, 1003.26, 2001.85,
                            1005.26, 2003.1, 9001.27, 9001.25, 9001.28, 9001.35, 9001.36, 9002.25, 9002.28, 9002.23,
                            9003.36, 9003.25, 9003.24, 9003.31, 9004.27, 9003.4, 9003.37, 9004.54, 9003.68, 9005.24,
                            9005.27, 9004.38, 9004.26, 9004.45, 9007.82, 22033.65, 9008.42, 9009.33, 9009.56, 9009.42,
                            9009.75, 9009.41, 9010.25, 9010.24, 9010.35, 9011.27, 9011.42, 9012.26, 9012.27, 9012.23,
                            9012.29, 9012.25, 9013.25, 9013.33, 9013.29, 9013.31, 9014.26, 9013.3, 9013.42, 9013.24,
                            9015.31, 9015.3, 9015.32, 9015.41, 9016.53, 4001.55, 9016.42, 4002.37, 29007.84, 1001.42,
                            1001.17, 1001.67, 4003.37, 29036.88, 29042.84, 29044.87, 4004.12, 4004.8, 1004.17, 4006.21,
                            4007.37, 21004.64, 21004.63, 21005.64, 21005.62, 21005.63, 21006.81, 21005.68, 21004.74,
                            21005.61, 21005.74, 21007.82, 21026.63, 21030.62, 21031.62, 21032.68, 21032.63, 21032.64,
                            21032.62, 21037.88, 8002.25, 8003.27, 8004.25, 8004.57, 13001.39, 13001.53, 13003.38,
                            13003.4, 13003.37, 13003.36, 13003.41, 13003.42, 13004.62, 13005.64, 13003.45, 13013.43,
                            13013.47, 13013.46, 13015.37, 13017.36, 13018.38, 13019.42, 13020.4, 13021.42, 13019.38,
                            13018.36, 13019.39, 13018.35, 13020.42, 13019.37, 13020.38, 13023.45, 3002.19, 3002.28,
                            3003.1, 3003.28, 8010.41, 3002.14, 14003.43, 3004.1, 14003.4, 14004.54, 14004.42, 14004.51,
                            14004.45, 14003.45, 14005.61, 14005.78, 14005.5, 14005.42, 14004.49, 8012.41, 1001.58,
                            1001.83, 27039.78, 14014.74, 27040.8, 14015.41, 27041.8, 14016.39, 14019.42, 5002.19,
                            5003.54, 5003.13, 5003.16, 5004.13, 5001.01, 5001.12, 5003.33, 5004.54, 5005.14, 5006.2,
                            5008.18, 5009.15, 5010.19, 5013.26, 5013.4, 5026.53, 5003.3, 5003.28, 5003.46, 1001.87,
                            1001.25, 1002.17, 1002.09, 1002.21, 1002.25, 1002.16, 1002.33, 1002.08, 1002.07, 1003.12,
                            1003.42, 1003.27, 1003.14, 1005.14, 1005.68, 1004.09, 1005.12, 1004.49, 1006.13, 1006.49,
                            1005.82, 1005.57, 1006.36, 1007.17, 1007.41, 1007.14, 1007.15, 1006.79, 1007.82, 1007.03,
                            1007.9, 1007.12, 1009.18, 1010.83, 5005.71, 1011.73, 1012.8, 1012.24, 1012.86, 1013.26,
                            5006.12, 5006.28, 1019.38, 10004.56, 1022.44, 1023.46, 1024.89, 5008.14, 5008.28, 1029.58,
                            5009.3, 5009.53, 2002.74, 1034.67, 1001.74, 1001.24, 1001.49, 1039.78, 1039.9, 1002.74,
                            1003.24, 9002.36, 9003.41, 2002.08, 2002.33, 2002.42, 2002.17, 1005.74, 3001.21, 3001.46,
                            2003.67, 2004.42, 5002.3, 4001.28, 30006.9, 30006.89, 30006.88, 4002.35, 4002.44, 30036.88,
                            30045.9, 30045.88, 30045.89, 1001.15, 1001.4, 4003.28, 1002.15, 1003.9, 1003.4, 1004.4,
                            6001.21, 6002.28, 6002.21, 6002.44, 1006.9, 6001.17, 6001.15, 6001.14, 22004.64, 22004.62,
                            6002.26, 6002.24, 6002.14, 6002.23, 6002.33, 6002.32, 6002.3, 6003.21, 6003.16, 6003.2,
                            6003.33, 6003.4, 6003.37, 6003.39, 6003.17, 6005.15, 6004.16, 22022.79, 6005.18, 6004.19,
                            6006.17, 22027.66, 6006.21, 6006.18, 6006.15, 6007.15, 6007.17, 22033.66, 22034.68,
                            22032.68, 6008.18, 6008.15, 6008.2, 6007.14, 6008.21, 6009.19, 6009.21, 6009.16, 6008.42,
                            6009.42, 6010.21, 6010.18, 6010.41, 6011.24, 6005.71, 6011.42, 6012.27, 6012.51, 6006.37,
                            6006.19, 1001.56, 1002.31, 6009.28, 2001.74, 14001.42, 14002.42, 14003.47, 14003.44,
                            14003.48, 14003.39, 14003.46, 14004.58, 14004.48, 14004.4, 14011.41, 14003.53, 14004.41,
                            14006.78, 14007.42, 14007.45, 14008.43, 14014.5, 14017.42, 14017.41, 14021.51, 14019.51,
                            14022.45, 14020.42, 14021.41, 14020.4, 14023.45, 14026.51, 14022.54, 14022.43, 14023.54,
                            3001.17, 3001.24, 3001.74, 3001.76, 3001.33, 10003.29, 3002.24, 3002.17, 10003.54, 10003.41,
                            10003.39, 10004.36, 3003.17, 10004.29, 8002.33, 3006.08, 6001.22, 6001.36, 6003.3, 6003.42,
                            6004.54, 6003.19, 6001.2, 6001.28, 6004.17, 6002.22, 6002.15, 6003.24, 6004.18, 6005.17,
                            6006.4, 6007.16, 6008.19, 6009.2, 6010.2, 6011.21, 6012.24, 6013.4, 6034.68, 3002.06,
                            7001.28, 7003.21, 11001.36, 11001.46, 11002.3, 11003.39, 11003.48, 11003.37, 11003.3,
                            11004.33, 11004.31, 11004.53, 11003.54, 11005.74, 11004.52, 11004.49, 11004.54, 11004.34,
                            11006.32, 11006.6, 11006.84, 1001.22, 1001.72, 11011.39, 11011.58, 11013.3, 11014.42,
                            1002.47, 11015.33, 11015.29, 11015.39, 11016.31, 11016.36, 11016.33, 11016.42, 11017.34,
                            11017.32, 11018.38, 11018.34, 1003.22, 11020.39, 2001.81, 2001.9, 2002.15, 2002.9, 7010.3,
                            2004.06, 2005.15, 31006.91, 4001.08, 4001.17, 31045.9, 31045.89, 4002.08, 4002.24, 11004.32,
                            1001.88, 1001.38, 4003.42, 4003.49, 1001.63, 4003.08, 4003.33, 4004.24, 4004.17, 1003.38,
                            1003.13, 1004.13, 23003.68, 23005.68, 23005.73, 23005.67, 23005.69, 23005.74, 23005.66,
                            1006.88, 23028.68, 23031.68, 23032.68, 23033.68, 23035.69, 23034.67, 4002.22, 8001.28,
                            8002.28, 1001.79, 1001.54, 1001.29, 8003.21, 8003.28, 8003.39, 16003.49, 16003.45, 16004.48,
                            16004.53, 16004.45, 16004.49, 16004.54, 16005.47, 16004.47, 3003.16, 16005.46, 8004.53,
                            15001.54, 15001.45, 15004.49, 15004.42, 15005.45, 15005.44, 15003.44, 15004.43, 15004.52,
                            15007.83, 15003.48, 15004.45, 15006.48, 15015.53, 15015.54, 15008.44, 15018.44, 15019.42,
                            15020.51, 15021.45, 15022.44, 15022.45, 15023.46, 15026.54, 15022.46, 15025.54, 15022.43,
                            15023.44, 15027.54, 2002.22, 16023.44, 3001.06, 3001.22, 3001.9, 3001.13, 3002.23, 3002.21,
                            3002.2, 3002.16, 3002.42, 3003.06, 3003.19, 3003.18, 3003.42, 3003.13, 3003.12, 3004.12,
                            3004.14, 3005.12, 3005.08, 3005.11, 3005.82, 3006.11, 3006.09, 3006.14, 3005.14, 3007.13,
                            2004.1, 3006.2, 3007.28, 3006.24, 3003.15, 3008.16, 3004.06, 8011.39, 8011.28, 3005.15,
                            7001.19, 7002.25, 7002.31, 7002.17, 7002.3, 7002.28, 7002.42, 7003.36, 7003.28, 7004.2,
                            7004.19, 7005.19, 7006.25, 7006.19, 7007.23, 7007.2, 7008.21, 7009.2, 7010.24, 7011.23,
                            7012.23, 7013.27, 7014.42, 3006.22, 5001.24, 5001.17, 5002.17, 5002.42, 5002.26, 5002.35,
                            1001.5, 12004.43, 5003.24, 5003.42, 5005.15, 5005.67, 5005.42, 5005.26, 5006.4, 5006.24,
                            5007.17, 5007.24, 5008.17, 5009.24, 1001.7, 1001.2, 1002.2, 1003.2, 1003.45, 2001.38,
                            2001.29, 2002.13, 2002.29, 21004.61, 21004.62, 2003.04, 4001.06, 8001.23, 8001.21, 8001.22,
                            8001.36, 8001.27, 8002.32, 8002.21, 8003.43, 8003.25, 8003.4, 8003.42, 8003.26, 8004.22,
                            8004.24, 8004.23, 8003.41, 8005.64, 8005.24, 8005.22, 8005.26, 8005.78, 8005.53, 8007.2,
                            1001.86, 8008.28, 8008.5, 4003.31, 1001.36, 8009.21, 8009.42, 8008.66, 8008.25, 8009.24,
                            8010.22, 8010.27, 8010.24, 8010.25, 8010.2, 8011.23, 8011.21, 8011.42, 8011.27, 8012.25,
                            8012.34, 8012.42, 1002.11, 8013.26, 8013.4, 8012.26, 7001.01, 8013.25, 1002.36, 8013.42,
                            8015.42, 1003.36, 4005.9, 4005.22, 4006.06, 24005.75, 24006.7, 24007.83, 24006.73, 24008.71,
                            24005.71, 24006.82, 24005.72, 24006.71, 1005.86, 24024.75, 24030.73, 1006.86, 24034.73,
                            24035.71, 24036.72, 24036.71, 24035.72, 24035.7, 24035.73, 6003.15, 6003.26, 6005.74,
                            6007.42, 6007.4, 1001.52, 1001.27, 6008.17, 1002.02, 1002.27, 16002.48, 16003.47, 16004.51,
                            16004.64, 16003.54, 16004.58, 16005.48, 16006.49, 16004.46, 16003.46, 16005.73, 16007.49,
                            16006.67, 16016.53, 16016.57, 16019.47, 16020.46, 16020.54, 16022.45, 16023.48, 16024.47,
                            16026.51, 16027.54, 16026.54, 16025.51, 16023.46, 16024.49, 16023.47, 2001.2, 2001.61,
                            2001.7, 1004.52, 2003.36, 2003.11, 3001.2, 3001.04, 3002.13, 3002.88, 3003.2, 8001.24,
                            8002.27, 8003.36, 8004.27, 8005.23, 8002.26, 8002.3, 8008.29, 8008.27, 8008.53, 8002.29,
                            8003.33, 8004.62, 8005.62, 8006.8, 8007.28, 8008.22, 8009.22, 8010.23, 8011.24, 8012.22,
                            8013.27, 8014.27, 8016.33, 8016.42, 8027.54, 1001.68, 1002.18, 13003.35, 13003.44, 13003.74,
                            13003.82, 13003.48, 13004.39, 13004.37, 13004.35, 13004.38, 13004.36, 13005.48, 13004.54,
                            13004.49, 13004.45, 13007.42, 13007.41, 13015.36, 13016.39, 13017.37, 13018.39, 13018.48,
                            7001.26, 1001.01, 1001.04, 1001.05, 1001.06, 1001.07, 1001.08, 1001.09, 1001.11, 1001.12, 1001.13, 1001.14, 1001.16, 1001.18, 1001.9, 1002.04, 1002.06, 1003.06, 1003.08, 1004.08, 2001.04, 2001.05, 2001.07, 2001.08, 2001.09, 2001.11, 2001.12, 2002.04, 2002.05, 2002.06, 2003.05, 2003.07, 2003.08, 2004.08, 3001.1, 3001.11, 3001.12, 3001.14, 3002.07, 3002.08, 3002.09, 3002.1, 3003.08, 3003.09, 3004.07, 3004.09, 3005.09, 3005.1, 4001.1, 4001.11, 4001.13, 4001.14, 4001.15, 4002.1, 4002.11, 4002.12, 4003.1, 4005.1, 4005.12, 4006.11, 4007.14, 5001.16, 5002.13, 5002.14, 5002.15, 5006.14, 5007.13, 5008.15, 6001.16, 6002.16, 6002.17, 6002.18, 6002.19, 6008.16, 6009.17, 6009.18, 7002.19, 7002.21, 7010.2, 7011.21, 8002.22, 8002.23, 8002.24, 8003.23, 8003.24, 8011.22, 8012.23, 8012.24, 9002.26, 9002.27, 9003.27, 9013.26, 9014.27, 10002.28, 10002.3, 10003.3, 10014.28, 10015.3, 11002.32, 11003.32, 11003.33, 11017.33, 12003.36, 12018.36, 13003.39, 13020.39, 14003.41, 15003.45, 15023.45, 16003.48, 16024.48, 17004.49, 17004.51, 23034.68, 25037.74, 28041.82]

    pat_list = []
    # pat_info = dict()
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        # df.reset_index(drop=True, inplace=True)

        df.NormPrice = np.around(df.NormPrice, 2)

        df['pattern_mark_4'] = np.nan

        peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for i, curr in enumerate(peaks_index_list[1:-2], 1):
                # Set the next relevant peak value
                if df['NormPrice'].iloc[curr] == df['NormPrice'].iloc[peaks_index_list[i + 1]]:
                    next = peaks_index_list[i + 2]
                    curr_2 = peaks_index_list[i + 1]
                else:
                    next = peaks_index_list[i + 1]
                    curr_2 = curr

                if df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next] and df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[prev] and \
                        df.TimeInSeconds.iloc[next] - df.TimeInSeconds.iloc[prev] <= 172800:
                    inc_steps = curr - prev
                    df_check = df.iloc[prev + 1: curr].copy()
                    inc_steps -= df_check.Description[df_check.Description == 0].count()

                    dec_steps = next - curr_2
                    df_check = df.iloc[curr_2 + 1: next].copy()
                    dec_steps -= df_check.Description[df_check.Description == 0].count()

                    height = np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)

                    # print(inc_steps, dec_steps, height)
                    pat_mark = (inc_steps * 1000) + dec_steps + height

                    if pat_mark in not_interesting_pats:
                        print("updated pat 1")
                        pat_mark = 1
                    
                    # if pat_mark not in interesting_pats:
                    #     if inc_steps > 7 and dec_steps >= 7:
                    #         pat_mark = 8001
                    #     elif inc_steps > 7 and dec_steps <= 7:
                    #         pat_mark = 8002
                    #     elif inc_steps <= 7 and dec_steps >= 7:
                    #         pat_mark = 7000
                    #     else:
                    #         pat_mark = 1

                    pat_list.append(pat_mark)
                    df.loc[[prev], 'pattern_mark_4'] = pat_mark
                    df.loc[np.arange(prev + 1, next + 1, 1), 'pattern_mark_4'] = -1*pat_mark


                prev = curr

        # plt.plot(df.Timestamp, df.NormPrice, ':k')
        # plt.scatter(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], marker='o', color='black')

        # pat_set = set(pat_list)
        # for pat in pat_set:
        #     plt.scatter(df.Timestamp[abs(df.pattern_mark_4) == pat], df.NormPrice[abs(df.pattern_mark_4) == pat], marker='x', label=pat)
        df.to_csv(inst, index=False)

    print(set(pat_list))
    # for pat, info in pat_info.items():
    #     print("inc steps: ", pat, "dec steps: ", set(info))
    #
    # plt.legend()
    # plt.show()


def mark_long_patterns_by_peak_and_time():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean__square.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv']
    total_count = len(instList)

    pat_list = []
    pat_dict = dict()
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        # df.reset_index(drop=True, inplace=True)

        df.NormPrice = np.around(df.NormPrice, 2)

        df['long_pattern_mark_2'] = np.nan
        df['pattern_interval_2'] = np.nan

        peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for i, curr in enumerate(peaks_index_list[1:-2], 1):
                # Set the next relevant peak value
                if df['NormPrice'].iloc[curr] == df['NormPrice'].iloc[peaks_index_list[i + 1]]:
                    next = peaks_index_list[i + 2]
                    curr_2 = peaks_index_list[i + 1]
                else:
                    next = peaks_index_list[i + 1]
                    curr_2 = curr

                if (df['NormPrice'].iloc[next] - df['NormPrice'].iloc[prev] <= 0.03)  and df['NormPrice'].iloc[curr] > \
                        df['NormPrice'].iloc[prev] and \
                        df.TimeInSeconds.iloc[next] - df.TimeInSeconds.iloc[prev] > 172800:
                    inc_steps = curr - prev
                    df_check = df.iloc[prev + 1: curr].copy()
                    inc_steps -= df_check.Description[df_check.Description == 0].count()

                    dec_steps = next - curr_2
                    df_check = df.iloc[curr_2 + 1: next].copy()
                    dec_steps -= df_check.Description[df_check.Description == 0].count()

                    height = np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)
                    interval = (df.TimeInSeconds.iloc[next] - df.TimeInSeconds.iloc[prev])

                    # print(inc_steps, dec_steps, height)
                    pat_mark = (inc_steps * 1000) + dec_steps + height

                    if (pat_mark, interval) in pat_dict.keys():
                        pat_dict[(pat_mark, interval)] += 1
                    else:
                        pat_dict[(pat_mark, interval)] = 1

                    pat_list.append((pat_mark, interval))
                    df.loc[[prev], 'long_pattern_mark_2'] = pat_mark
                    df.loc[np.arange(prev + 1, next + 1, 1), 'long_pattern_mark_2'] = -1 * pat_mark
                    df.loc[[prev], 'pattern_interval_2'] = interval
                    df.loc[np.arange(prev + 1, next + 1, 1), 'pattern_interval_2'] = -1 * interval

                prev = curr

        # plt.plot(df.Timestamp, df.NormPrice, ':k')
        # plt.scatter(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], marker='o', color='black')
        #
        # pat_set = set(pat_list)
        # for pat,interval in pat_set:
        #     plt.scatter(df.Timestamp[(abs(df.long_pattern_mark) == pat) & (abs(df.pattern_interval) == interval)], df.NormPrice[(abs(df.long_pattern_mark) == pat) & (abs(df.pattern_interval) == interval)], marker='x', label=pat)
        df.to_csv(inst, index=False)

    print(dict(sorted(pat_dict.items(), key=lambda item: item[1])))
    # for pat, info in pat_info.items():
    #     print("inc steps: ", pat, "dec steps: ", set(info))
    #
    # plt.legend()
    # plt.show()


def patterns_by_family():
    family_list = ['t1', 's2', 's3', 'c1', 'c2', 's1', 'm1', 'm2', 'n1', 'n2', 'e3', 'sn1', 'sn2', 'n4', 'mn4', 'xn4',
                    'e4', 'gn3', 'se1', 'ga1', 'gn4', 'i1', 'cm4', 'ce4', 'c4', 'sc1', 'd1', 'sn1ne', 'sn2ne', 'se1ne',
                    'f1', 'gn5', 'd1ne', 'c5', 'g5', 'r5', 'hfc5', 'hfg5', 'i2', 're4', 'gn5i', 'gn5t', 't5', 'sccg5',
                    'scch5', 'sccgn5d', 'sccgn5', 'ebmg5', 'ebmr5', 'f2', 'ebmr4', 'ebmg4', 'ebma1', 'gn6p', 'g5se',
                    'ebmgn5t', 'sccgn5t', 'sccgn6', 'ebmgn5', 'ebmhfg5', 'ebmhfg4', 'ebmc4', 'ic5', 'gn5d', 'ebmgn5i',
                    'ebmi2', 'f3', 'gn6v', 'ebmi3', 're5', 'i2g', 'r1', 're4e', 'ebmg5s', 'ebmgn5ts', 'ebmg5s-eni', 'i2ne',
                    'i2gne', 'vgn5i', 'ebmc5s', 'ebmgn6t', 'g6', 'gn6i', 'ebmg6', 'ebmm5d', 'ebmr5s', 'ebmhd1', 'ebma3',
                    'ebmgn6', 'ebmgn6i', 'ec1', 'eg1', 'ebmhd2', 'ebmhd3', 'er1', 'v5', 'ebmi2_ant', 'ebmg6e', 'ebmc6e',
                    'ed2', 'ebmi3s', 'sn1nec', 'sn2nec', 'g6e', 'c6', 'se1nec', 'r6', 'ed1', 'scch5s', 'sccg5s', 't6',
                    'ebmgn6e', 'hfc6', 'hfg6', 'ht6c', 'ht6g', 'ht6r', 'gn6e', 'hfr6', 'ebmgn6v', 'ebman1', 'c5t', 'd2s',
                    'sccgn6ne', 'ebmc6', 'ebmr6', 'ebmhd4', 'ebmi5_8nvme', 'ebmhfr6', 'ebmhfg6', 'ebmhfc6', 'gn6t',
                    'ebmbz_c40m128', 'ebmbz_c56m128', 'ebmhd5', 'ebmre6_aep', 'ebmre6_6t', 'ebmre6_3t', 'ed3', 'vgn6iv',
                    'ebmgn6ex', 'ic6', 'ebmi2s', 'sccgn6ex', 'g5ne', 'g5nenl', 'faas_image', 'ebmtest_pov', 'sccc6',
                    'scchfr6', 'scchfg6', 'scchfc6', 'sccr6', 'sccg6', 'ec3', 'er3', 'eg3', 'ebmc6_aep', 's6', 'ebmi2g',
                    'vgn6i', 'kvmtest_hdd', 'kvmtest_ssd', 'ebmr7', 'ebmg7', 'ebmc7', 'i2d', 're6', 'd2c',
                    'video_transcoding', 'kvmtest_f45s1', 'kvmtest_f41', 'kvmtest_f43']

    # family_list = ['sn1ne']

    for num, family in enumerate(family_list, 1):
        # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean__square.csv']
        instList = "F:/alibaba_work/*_full_traces_updated/*__ecs_" + family + "_*clean__square.csv"
        instList = glob.glob(instList)
        count_insts = len(instList)

        family_pat_list = []
        for inst in instList:
            df = pd.read_csv(inst)

            family_pat_list.extend(abs(df.pattern_mark.dropna().unique()))

        print(family, ", ", count_insts, ", ", sorted(list(set(family_pat_list))))


def mark_patterns_by_peak_height():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    pat_list = []
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)

        df.NormPrice = np.around(df.NormPrice, 2)

        df['pattern_mark_h'] = np.nan

        peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
        if peaks_index_list:
            prev = peaks_index_list[0]
            for i, curr in enumerate(peaks_index_list[1:-2], 1):
                # Set the next relevant peak value
                if df['NormPrice'].iloc[curr] == df['NormPrice'].iloc[peaks_index_list[i + 1]]:
                    next = peaks_index_list[i + 2]
                else:
                    next = peaks_index_list[i + 1]

                if df['NormPrice'].iloc[prev] == df['NormPrice'].iloc[next] and df['NormPrice'].iloc[curr] > \
                        df['NormPrice'].iloc[prev] and \
                        df.TimeInSeconds.iloc[next] - df.TimeInSeconds.iloc[prev] <= 172800:
                    pat_mark = np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)

                    pat_list.append(pat_mark)
                    df.loc[[prev], 'pattern_mark_h'] = pat_mark
                    df.loc[np.arange(prev + 1, next + 1, 1), 'pattern_mark_h'] = -1 * pat_mark

                prev = curr

        # plt.plot(df.Timestamp, df.NormPrice, '.:k')
        # plt.plot(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], '*')

        # pat_set = set(pat_list)
        # for pat in pat_set:
        #     plt.scatter(df.Timestamp[abs(df.pattern_mark_h) == pat], df.NormPrice[abs(df.pattern_mark_h) == pat],
        #                 label=pat)
        df.to_csv(inst, index=False)

    print(set(pat_list))

    # plt.legend()
    # plt.show()


def count_patterns_per_height():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean.csv"
    instList = glob.glob(instList)

    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    total_count = len(instList)

    pat_info = dict()
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        # df = df[df.Timestamp < pd.to_datetime("1/1/2021 00:00:00 AM")]

        df['pattern_mark_h'] = abs(df['pattern_mark_h'])
        df['pattern_mark_2'] = abs(df['pattern_mark_2'])

        grouped = df.groupby('pattern_mark_2')
        for name, group in grouped:
            if name in pat_info:
                pat_info[name].extend(group['pattern_mark_h'].unique().tolist())
            else:
                pat_info[name] = group['pattern_mark_h'].unique().tolist()

    for height, pat in pat_info.items():
        print(height, ":", set(pat))


def count_static_traces():
    instList = "F:/alibaba_work/*_full_traces_updated/*vpc.csv"
    instList = glob.glob(instList)

    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean.csv']
    total_count = len(instList)

    count_vpc = 0
    inst_dict = dict()
    for num, inst in enumerate(instList, 1):
        # print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)

        if df.SpotPrice.nunique() <= 1:
            inst, _ = inst.split("__vpc")
            if inst in inst_dict.keys():
                inst_dict[inst] += 1
            else:
                inst_dict[inst] = 1
            count_vpc += 1

    print(count_vpc, "/", total_count)

    instList = "F:/alibaba_work/*_full_traces_updated/*vpc__clean__square_2.csv"
    instList = glob.glob(instList)

    count_square = 0
    for num, inst in enumerate(instList, 1):
        # print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)

        if df.SpotPrice.nunique() <= 1:
            inst, _ = inst.split("__vpc")
            if inst in inst_dict.keys():
                inst_dict[inst] += 1
            else:
                inst_dict[inst] = 1
            count_square += 1

    print(count_square, "/", total_count)

    # instList = "F:/alibaba_work/*_full_traces_updated/*vpc__clean_2.csv"
    # instList = glob.glob(instList)
    #
    # count_clean_2 = 0
    # for num, inst in enumerate(instList, 1):
    #     # print("Processing ", inst, num, "/", total_count)
    #     df = pd.read_csv(inst)
    #
    #     if df.SpotPrice.nunique() == 1:
    #         if inst in inst_dict.keys():
    #             inst_dict[inst] += 1
    #         else:
    #             inst_dict[inst] = 1
    #         count_clean_2 += 1
    #
    # print(count_clean_2, "/", total_count)

    print([k for (k,v) in inst_dict.items() if v == 1])
    # print(inst_dict)
    # inst_dict = dict(sorted(inst_dict.items(), key=lambda item: item[1]))


def find_inst_with_norm_price_greater_than_1():
    instList = "F:/alibaba_work/*_full_traces_updated/*vpc.csv"
    instList = glob.glob(instList)

    for inst in instList:
        df = pd.read_csv(inst)
        if df.NormPrice.max() > 1:
            print(inst)
    print("finished processing")


def calc_pattern_probability():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_e4_small__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    # info_dict = dict()

    # Matrix based on height
    # height_list = [round(num, 2) for num in np.arange(-1.0, 1.01, 0.01)]
    # mat_size = len(height_list)
    # mat = np.zeros((mat_size, mat_size))

    # Matrix based on steps
    step_list = [num for num in np.arange(-100, 101, 1)]
    mat_size = len(step_list)

    mat = np.zeros((mat_size, mat_size))

    jump_data = []
    tmp = []
    # print(step_list)
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')

        df.NormPrice = np.around(df.NormPrice, 2)

        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:k')
        # plt.scatter(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], marker='o',
        #             color='red')

        # df = df[df.Timestamp >= pd.to_datetime('1/1/2020 00:00:00 AM')].copy()
        # df = df[(df.Timestamp >= pd.to_datetime('9/1/2019 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('1/1/2020 00:00:00 AM'))].copy()
        df.reset_index(drop=True, inplace=True)

        if not df.empty:
            peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
            # print(peaks_index_list)
            if len(peaks_index_list) >= 3:
                prev = peaks_index_list[0]
                for i, curr in enumerate(peaks_index_list[1:-2], 1):
                    # Set the next relevant peak value
                    if df['NormPrice'].iloc[curr] == df['NormPrice'].iloc[peaks_index_list[i + 1]]:
                        next = peaks_index_list[i + 2]
                        curr_2 = peaks_index_list[i + 1]
                    else:
                        next = peaks_index_list[i + 1]
                        curr_2 = curr

                    # # Matrix based on height
                    # inc_h = np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)
                    # dec_h = np.around(df['NormPrice'].iloc[next] - df['NormPrice'].iloc[curr], 3)
                    #
                    # row = height_list.index(inc_h)
                    # col = height_list.index(dec_h)
                    # mat[row][col] += 1

                    # Matrix based on steps
                    if df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[prev]:
                        inc_steps = curr - prev
                        df_check = df.iloc[prev + 1: curr].copy()
                        inc_steps -= df_check.Description[df_check.Description == 0].count()
                        t_inc = inc_steps+(np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3))

                        dec_steps = next - curr_2
                        df_check = df.iloc[curr_2 + 1: next].copy()
                        dec_steps -= df_check.Description[df_check.Description == 0].count()
                        dec_steps *= -1
                        t_dec = dec_steps - (np.around(df['NormPrice'].iloc[curr_2] - df['NormPrice'].iloc[next], 3))

                        jump_data.append((inc_steps, np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)))
                        jump_data.append((-1*dec_steps, np.around(df['NormPrice'].iloc[next] - df['NormPrice'].iloc[curr_2], 3)))
                        tmp.append((t_inc,t_dec))
                    else:
                        inc_steps = curr - prev
                        df_check = df.iloc[prev + 1: curr].copy()
                        inc_steps -= df_check.Description[df_check.Description == 0].count()
                        inc_steps *= -1
                        t_inc = inc_steps - (np.around(df['NormPrice'].iloc[prev] - df['NormPrice'].iloc[curr], 3))

                        dec_steps = next - curr_2
                        df_check = df.iloc[curr_2 + 1: next].copy()
                        dec_steps -= df_check.Description[df_check.Description == 0].count()
                        t_dec = dec_steps + (np.around(df['NormPrice'].iloc[next] - df['NormPrice'].iloc[curr_2], 3))

                        jump_data.append((-1 * inc_steps, np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)))
                        jump_data.append((dec_steps, np.around(df['NormPrice'].iloc[next] - df['NormPrice'].iloc[curr_2], 3)))
                        tmp.append((t_inc, t_dec))
                    # print(inc_steps, dec_steps)

                    row = step_list.index(inc_steps)
                    col = step_list.index(dec_steps)
                    mat[row][col] += 1

                    prev = curr

    #             if inc_steps > 0:
    #                 mat[]
    #
    #             # if df['NormPrice'].iloc[curr] > df['NormPrice'].iloc[prev]:
    #             #     inc_steps = curr - prev
    #             #     # df_check = df.iloc[prev + 1: curr].copy()
    #             #     # inc_steps -= df_check.Description[df_check.Description == 0].count()
    #             #     inc_height = np.around(df['NormPrice'].iloc[curr] - df['NormPrice'].iloc[prev], 3)
    #             #
    #             #     dec_steps = next - curr_2
    #             #     # df_check = df.iloc[curr_2 + 1: next].copy()
    #             #     # dec_steps -= df_check.Description[df_check.Description == 0].count()
    #             #     dec_height = np.around(df['NormPrice'].iloc[curr_2] - df['NormPrice'].iloc[next], 3)
    #             #
    #             #     if (inc_steps, inc_height, dec_steps, dec_height) in info_dict:
    #             #         info_dict[(inc_steps, inc_height, dec_steps, dec_height)][0] += 1
    #             #     else:
    #             #         info_dict[(inc_steps, inc_height, dec_steps, dec_height)] = [1,0]
    #             #
    #             #     tmp_set.add((inc_steps, inc_height, dec_steps, dec_height))
    #
                # prev = curr
    #
    #     for t in tmp_set:
    #         info_dict[t][1] += 1
    #

    fig = plt.figure(tight_layout=True, figsize=set_size())
    plt.scatter(*zip(*jump_data))
    plt.xlabel("Number of steps")
    plt.ylabel("Height of jump")

    fig = plt.figure(tight_layout=True, figsize=set_size())
    plt.scatter(*zip(*tmp))
    plt.xlabel("Inc")
    plt.ylabel("Dec")

    sum_mat = np.sum(mat)
    for i in range(len(mat)):
        for j in range(len(mat)):
            mat[i][j] = mat[i][j] / sum_mat

    print(np.sum(mat))

    fig = plt.figure(tight_layout=True, figsize=set_size())
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(mat, cmap='binary', norm=LogNorm())
    ax.set_xticks(np.arange(mat_size))
    ax.set_yticks(np.arange(mat_size))
    # ... and label them with the respective list entries
    ax.set_xticklabels(step_list)
    ax.set_yticklabels(step_list)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    fig.colorbar(im, location='right', shrink=0.8)
    # plt.title("After January 2020")

    plt.show()


def create_jump_transition_mat():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean__square_2.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]
    mat_size = len(price_list)
    mat = np.zeros((mat_size, mat_size))
    # print(step_list)
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df.NormPrice = np.around(df.NormPrice, 2)

        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:k')
        # plt.scatter(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], marker='o',
        #             color='red')

        df = df[df.Timestamp >= pd.to_datetime('1/1/2020 00:00:00 AM')].copy()
        # df = df[(df.Timestamp >= pd.to_datetime('9/1/2019 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('1/1/2020 00:00:00 AM'))].copy()
        df.reset_index(drop=True, inplace=True)

        if not df.empty:
            peaks_index_list = df.index[df.FixedPeakInfo.notna()].tolist()
            # print(peaks_index_list)
            if len(peaks_index_list) >= 2:
                prev = peaks_index_list[0]
                for i, curr in enumerate(peaks_index_list[1:], 1):
                    row = price_list.index(df['NormPrice'].iloc[prev])
                    col = price_list.index(df['NormPrice'].iloc[curr])

                    mat[row][col] += 1

                    prev = curr

    sum_mat = np.sum(mat)
    for i in range(len(mat)):
        for j in range(len(mat)):
            mat[i][j] = mat[i][j] / sum_mat

    print(np.sum(mat))

    fig = plt.figure(tight_layout=True, figsize=set_size())
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(mat, cmap='binary', norm=LogNorm())
    ax.set_xticks(np.arange(mat_size))
    ax.set_yticks(np.arange(mat_size))
    # ... and label them with the respective list entries
    ax.set_xticklabels(price_list)
    ax.set_yticklabels(price_list)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    fig.colorbar(im, location='right', shrink=0.8)
    plt.title("After January 2020")

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--function", help='The function we want to run')

    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    # regions = ['ap-northeast-1']

    args = parser.parse_args()

    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'

    if args.function:
        func = args.function
        if func == 'find_chevrons_in_all_regions':
            print("Running find_chevrons_in_all_regions function...")
            find_chevrons_in_all_regions()
        elif func == 'find_artificial_patterns':
            print("Running find_artificial_patterns function...")
            find_artificial_patterns()
        elif func == 'mark_seven_boom_pattern':
            print("Running mark_seven_boom_pattern function...")
            mark_seven_boom_pattern()
        elif func == 'add_pattern_column':
            print("Running add_pattern_column function...")
            add_pattern_column()
        elif func == 'mark_chevron_patterns':
            print("Running mark_chevron_patterns function...")
            mark_chevron_patterns()
        elif func == 'count_patterns_per_month':
            print("Running count_patterns_per_month function...")
            count_patterns_per_month()
        elif func == 'draw_patterns':
            print("Running draw_patterns function...")
            draw_patterns()
        elif func == 'mark_slope_pattern':
            print("Running mark_slope_pattern function...")
            mark_slope_pattern()
        elif func == 'mark_static_pattern':
            print("Running mark_static_pattern function...")
            mark_static_pattern()
        elif func == 'time_held_in_pattern':
            print("Running time_held_in_pattern function...")
            time_held_in_pattern()
        elif func == 'mark_first_epoch_patterns':
            print("Running mark_first_epoch_patterns function...")
            mark_first_epoch_patterns()
        elif func == 'mark_second_epoch_patterns':
            print("Running mark_second_epoch_patterns function...")
            mark_second_epoch_patterns()
        elif func == 'analyze_chevrons':
            print("Running analyze_chevrons function...")
            analyze_chevrons()

        elif func == 'find_band_width':
            print('Running find_band_width function...')
            find_band_width()

        elif func == 'count_num_traces':
            print("Running count_num_traces...")
            count_num_traces()

        elif func == 'mark_patterns_by_peak':
            print('Running mark_patterns_by_peak...')
            mark_patterns_by_peak()

        elif func == 'mark_patterns_by_peak_height':
            print('Running mark_patterns_by_peak_height...')
            mark_patterns_by_peak_height()

        elif func == 'count_patterns_per_height':
            print("Running count_patterns_per_height...")
            count_patterns_per_height()

        elif func == 'patterns_by_family':
            print("Running patterns_by_family...")
            patterns_by_family()

        elif func == 'mark_long_patterns_by_peak_and_time':
            print("Running mark_long_patterns_by_peak_and_time...")
            mark_long_patterns_by_peak_and_time()

        elif func == 'count_static_traces':
            print("Running count_static_traces...")
            count_static_traces()

        elif func == 'find_inst_with_norm_price_greater_than_1':
            print("Running find_inst_with_norm_price_greater_than_1...")
            find_inst_with_norm_price_greater_than_1()

        elif func == 'calc_pattern_probability':
            print("Running calc_pattern_probability...")
            calc_pattern_probability()

        elif func == 'create_jump_transition_mat':
            print("Running create_jump_transition_mat...")
            create_jump_transition_mat()

    print("Finish")
