import os.path
from os import path
import glob
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
import scipy.stats
register_matplotlib_converters()

import matplotlib
# matplotlib.use('Agg')

import pandas as pd, numpy as np, seaborn as sns, scipy
from contextlib import closing
import argparse
from statsmodels.distributions.empirical_distribution import ECDF
from scipy import stats
from collections import Counter

# plt.style.use('grayscale')
plt.style.use('seaborn-colorblind')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 12,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10
    }

plt.rcParams.update(tex_fonts)


# ################ Begin helper functions ################
# width=229.87749
def set_size(width = 441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier


def round_down(n, decimals=0):
    multiplier = 10 ** decimals
    return np.floor(n * multiplier) / multiplier

# ################ End helper functions ##################


# #############################################################################
# ####                 Add 'TimeDelta' and 'PriceDelta' columns.          #####
# #############################################################################
def add_time_and_price_delta_cols():
    # instList = "./"+region+"_2020_traces/*clean.csv"
    # instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"

    # instList = "./alibaba_work/" + region + "_full_traces/*_clean.csv"
    print("Not using region parameter: ", )
    instList = "F:/alibaba_work/*_full_traces_updated/*_clean__square_2.csv"
    instList = glob.glob(instList)
    total = len(instList)
    
    for i, inst in enumerate(instList,1):
        print("processing ", inst, i, "/", total)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        
        df['TimeDelta'] = df['TimeInSeconds'].diff()
        df['PriceDelta'] = df['SpotPrice'].diff()

        df['NormPriceDelta'] = (df.PriceDelta / df.OriginPrice)

        df.to_csv(inst, index=False)


def add_slope_col():
    instList = "F:/alibaba_work/*_full_traces_updated/*_clean_2.csv"
    instList = glob.glob(instList)
    total = len(instList)

    for i, inst in enumerate(instList, 1):
        print("processing ", inst, i, "/", total)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df['Slope'] = (df.NormPriceDelta / df.TimeDelta)

        df.to_csv(inst, index=False)


##############################################################################
# ####          Create the fixed normalized price delta column.          #####
# #### We are fixing the "error" based on 1%,2%,3% jumps in the price.   #####
##############################################################################
def add_normalized_price_and_delta_cols(region, epsilon1, epsilon2, epsilon3):
    # instList = "./"+region+"_traces/*_with_tags.csv"
    # instList = "./alibaba_work/" + region + "_full_traces/*_with_tags.csv"
    #instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"

    # instList = "./alibaba_work/*_full_traces/*_clean2.csv"
    # instList = "./alibaba_work/*_full_traces_updated/*_clean.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*_clean__square.csv"
    instList = glob.glob(instList)
    total_count = len(instList)

    for index, inst in enumerate(instList):
        print("processing ",inst, "(", index, "/", total_count, ")")
        df = pd.read_csv(inst)

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')
        
        df['OPCents'] = (df.OriginPrice * 100)  # OPCents - Origin Price in Cents
        # df['NormPrice'] = df.SpotPrice / df.OriginPrice
        # df['NormPriceDelta'] = (df.PriceDelta / df.OriginPrice)
        df['FixedNormPriceDelta'] = df['NormPriceDelta']

        # #################### Decreasing prices #####################
        # Fix the values for the decreasing prices using 1%
        df['DecFracToCompare1'] = (df.OPCents * -1 * epsilon1)
        df['DecFracToCompare1'] = round(df.DecFracToCompare1,1)        
        df['DecFracToCompare1'] = (df.DecFracToCompare1/df.OPCents)
        
        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare1) < 0.00001),'FixedNormPriceDelta'] = (-1*epsilon1)
        
        # Fix the values for the decreasing prices using 2%
        df['DecFracToCompare2'] = (df.OPCents * -1 * epsilon2)
        df['DecFracToCompare2'] = round(df.DecFracToCompare2,1)        
        df['DecFracToCompare2'] = (df.DecFracToCompare2/df.OPCents)
        
        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare2) < 0.00001),'FixedNormPriceDelta'] = (-1*epsilon2)
        
        # Fix the values for the decreasing prices using 3%
        df['DecFracToCompare3'] = (df.OPCents * -1 * epsilon3)
        df['DecFracToCompare3'] = round(df.DecFracToCompare3,1)        
        df['DecFracToCompare3'] = (df.DecFracToCompare3/df.OPCents)
        
        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare3) < 0.00001),'FixedNormPriceDelta'] = (-1*epsilon3)

        # Fix the values for the decreasing prices using 6%
        df['DecFracToCompare4'] = (df.OPCents * -1 * 0.06)
        df['DecFracToCompare4'] = round(df.DecFracToCompare4, 1)
        df['DecFracToCompare4'] = (df.DecFracToCompare4 / df.OPCents)

        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare4) < 0.00001), 'FixedNormPriceDelta'] = (-1 * 0.06)

        # Fix the values for the decreasing prices using 8%
        df['DecFracToCompare5'] = (df.OPCents * -1 * 0.08)
        df['DecFracToCompare5'] = round(df.DecFracToCompare5, 1)
        df['DecFracToCompare5'] = (df.DecFracToCompare5 / df.OPCents)

        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare5) < 0.00001), 'FixedNormPriceDelta'] = (-1 * 0.08)

        # Fix the values for the decreasing prices using 16%
        df['DecFracToCompare6'] = (df.OPCents * -1 * 0.16)
        df['DecFracToCompare6'] = round(df.DecFracToCompare6, 1)
        df['DecFracToCompare6'] = (df.DecFracToCompare6 / df.OPCents)

        df.loc[(abs(df.NormPriceDelta - df.DecFracToCompare6) < 0.00001), 'FixedNormPriceDelta'] = (-1 * 0.16)

        # #################### Increasing prices #####################
        # Fix the values for the decreasing prices using 1%
        df['IncFracToCompare1'] = (df.OPCents * epsilon1)
        df['IncFracToCompare1'] = round(df.IncFracToCompare1,1)        
        df['IncFracToCompare1'] = (df.IncFracToCompare1/df.OPCents)
        
        df.loc[(abs(df.NormPriceDelta - df.IncFracToCompare1) < 0.00001),'FixedNormPriceDelta'] = (epsilon1)
        
        # Fix the values for the decreasing prices using 2%
        df['IncFracToCompare2'] = (df.OPCents * epsilon2)
        df['IncFracToCompare2'] = round(df.IncFracToCompare2,1)        
        df['IncFracToCompare2'] = (df.IncFracToCompare2/df.OPCents)
        
        df.loc[(abs(df.NormPriceDelta - df.IncFracToCompare2) < 0.00001),'FixedNormPriceDelta'] = (epsilon2)
        
        # Fix the values for the decreasing prices using 2%
        df['IncFracToCompare3'] = (df.OPCents * epsilon3)
        df['IncFracToCompare3'] = round(df.IncFracToCompare3,1)        
        df['IncFracToCompare3'] = (df.IncFracToCompare3/df.OPCents)
        
        df.loc[(abs(df.NormPriceDelta - df.IncFracToCompare3) < 0.00001),'FixedNormPriceDelta'] = (epsilon3)

        df.drop(columns=['OPCents','DecFracToCompare1', 'DecFracToCompare2', 'DecFracToCompare3', 'DecFracToCompare4', 'DecFracToCompare5', 'DecFracToCompare6', 'IncFracToCompare1', 'IncFracToCompare2', 'IncFracToCompare3'], inplace=True)

        df.to_csv(inst, index=False)


##############################################################################
#####  This is a manual function to check that the percentiles we chose  #####
#####  are correct. We decided to check this in a different way.         #####
#####     Check that the 2% and 3% of the origin price is the correct    #####
#####     percentile. To do this we check what happens when we use:      #####
#####     1.9%, 2.1%, 2.9%, 3.1%                                         #####
##############################################################################
def add_bad_normalized_percentiles(region, epsilon1, epsilon2, epsilon3, epsilon4):
    # instList = "./"+region+"_traces/*_with_tags.csv"
    instList = "F:/alibaba_work/*_full_traces_updated/*_clean.csv"
    instList = glob.glob(instList)
    total_count = len(instList)

    for num, inst in enumerate(instList, 1):
        print("processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df['OPCents'] = (df.OriginPrice * 100)  # OPCents - Origin Price in Cents

        df['BadFixedNormPriceDelta1'] = df['NormPriceDelta']
        df['BadFixedNormPriceDelta2'] = df['NormPriceDelta']
        df['BadFixedNormPriceDelta3'] = df['NormPriceDelta']
        df['BadFixedNormPriceDelta4'] = df['NormPriceDelta']

        ##################### Decreasing prices #####################
        # Fix the values for the decreasing prices using 1.9%
        df['BadDecFracToCompare'] = (df.OPCents * -1 * epsilon1)
        df['BadDecFracToCompare'] = round(df.BadDecFracToCompare, 1)
        df['BadDecFracToCompare'] = (df.BadDecFracToCompare / df.OPCents)

        df.loc[(abs(df.BadFixedNormPriceDelta1 - df.BadDecFracToCompare) < 0.00001), 'BadFixedNormPriceDelta1'] = (
                    -1 * epsilon1)

        # Fix the values for the decreasing prices using 2.1%
        df['BadDecFracToCompare'] = (df.OPCents * -1 * epsilon2)
        df['BadDecFracToCompare'] = round(df.BadDecFracToCompare, 1)
        df['BadDecFracToCompare'] = (df.BadDecFracToCompare / df.OPCents)

        df.loc[(abs(df.BadFixedNormPriceDelta2 - df.BadDecFracToCompare) < 0.00001), 'BadFixedNormPriceDelta2'] = (
                    -1 * epsilon2)

        ##################### Increasing prices #####################
        # Fix the values for the decreasing prices using 2.9%
        df['BadIncFracToCompare'] = (df.OPCents * epsilon3)
        df['BadIncFracToCompare'] = round(df.BadIncFracToCompare, 1)
        df['BadIncFracToCompare'] = (df.BadIncFracToCompare / df.OPCents)

        df.loc[(abs(df.BadFixedNormPriceDelta3 - df.BadIncFracToCompare) < 0.00001), 'BadFixedNormPriceDelta3'] = (
            epsilon3)

        # Fix the values for the decreasing prices using 3.1%
        df['BadIncFracToCompare'] = (df.OPCents * epsilon4)
        df['BadIncFracToCompare'] = round(df.BadIncFracToCompare, 1)
        df['BadIncFracToCompare'] = (df.BadIncFracToCompare / df.OPCents)

        df.loc[(abs(df.BadFixedNormPriceDelta4 - df.BadIncFracToCompare) < 0.00001), 'BadFixedNormPriceDelta4'] = (
            epsilon4)

        df.drop(columns=['OPCents', 'BadDecFracToCompare', 'BadIncFracToCompare'], inplace=True)
        df.to_csv(inst, index=False)


def draw_normalized_price_delta_ecdf_per_region():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    regions = ['*']

    normPrice_delta_dec_epoch_1 = []
    normPrice_delta_dec_epoch_2 = []

    normPrice_delta_inc_epoch_1 = []
    normPrice_delta_inc_epoch_2 = []

    # normPrice_delta_dec_epoch_1_peak = []
    # normPrice_delta_dec_epoch_2_peak = []
    #
    # normPrice_delta_inc_epoch_1_peak = []
    # normPrice_delta_inc_epoch_2_peak = []

    for region in regions:
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = glob.glob(instList)
        total_num = len(instList)

        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_num)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            # df1 = df[df['NormPriceDelta'] != 0.0].copy()
            # normPrice_delta_dec_epoch_1.extend(df1.NormPriceDelta[(df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta < 0.0)].dropna().tolist())
            # normPrice_delta_dec_epoch_2.extend(df1.NormPriceDelta[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta < 0.0)].dropna().tolist())
            #
            # normPrice_delta_inc_epoch_1.extend(df1.NormPriceDelta[(df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta > 0.0)].dropna().tolist())
            # normPrice_delta_inc_epoch_2.extend(df1.NormPriceDelta[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta > 0.0)].dropna().tolist())

            df2 = df[df.FixedPeakInfo.notnull()].copy()
            df2['PeakPrice_Delta'] = df2['SpotPrice'].diff()
            df2['PeakNormPriceDelta'] = (df2.PeakPrice_Delta / df2.OriginPrice)

            df2 = df2[df2['PeakNormPriceDelta'] != 0.0]
            normPrice_delta_dec_epoch_1.extend(df2.PeakNormPriceDelta[(df2.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.PeakNormPriceDelta < 0.0)].dropna().tolist())
            normPrice_delta_dec_epoch_2.extend(df2.PeakNormPriceDelta[(df2.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.PeakNormPriceDelta < 0.0)].dropna().tolist())

            normPrice_delta_inc_epoch_1.extend(df2.PeakNormPriceDelta[(df2.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.PeakNormPriceDelta > 0.0)].dropna().tolist())
            normPrice_delta_inc_epoch_2.extend(df2.PeakNormPriceDelta[(df2.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.PeakNormPriceDelta > 0.0)].dropna().tolist())

    fig = plt.figure(tight_layout='True',figsize=set_size(fraction=1))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Normalized Jumps (zoom-out)")
    # ax.set_title("Normalized Steps")

    normPrice_delta_inc_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1)
    y = ecdf(normPrice_delta_inc_epoch_1)
    inc1 = y
    plt.step(normPrice_delta_inc_epoch_1, y, color='black', linestyle=':', where='post', label="Increase epoch 1")

    print("step inc epoch 1 " + str(np.interp([0.02, 0.021, 0.022, 0.03, 0.031, 0.032], normPrice_delta_inc_epoch_1, y)))

    normPrice_delta_inc_epoch_2.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_2)
    y = ecdf(normPrice_delta_inc_epoch_2)
    inc2 = y
    ax.step(normPrice_delta_inc_epoch_2, y, color='silver', linestyle='-', where='post', label="Increase epoch 2")

    print("step inc epoch 2 " + str(np.interp([0.02, 0.021, 0.022, 0.03, 0.031, 0.032], normPrice_delta_inc_epoch_2, y)))

    normPrice_delta_dec_epoch_1 = [x * -1 for x in normPrice_delta_dec_epoch_1]
    normPrice_delta_dec_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1)
    y = ecdf(normPrice_delta_dec_epoch_1)
    dec1 = y
    ax.step(normPrice_delta_dec_epoch_1, y, color='blue', linestyle='-.', where='post', label="Decrease epoch 1")

    print("step dec epoch 1 "+str(np.interp([0.02,  0.021, 0.022, 0.03, 0.031, 0.032], normPrice_delta_dec_epoch_1,y)))

    normPrice_delta_dec_epoch_2 = [x * -1 for x in normPrice_delta_dec_epoch_2]
    normPrice_delta_dec_epoch_2.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_2)
    y = ecdf(normPrice_delta_dec_epoch_2)
    dec2 = y
    ax.step(normPrice_delta_dec_epoch_2, y, color='lightblue', linestyle='--', where='post', label="Decrease epoch 2")

    print("step dec epoch 2 " + str(np.interp([0.02, 0.021, 0.022, 0.03, 0.031, 0.032], normPrice_delta_dec_epoch_2, y)))

    # print("Epoch 1:", stats.ks_2samp(inc1, dec1))
    # print("Epoch 2:", stats.ks_2samp(inc2, dec2))
    # print("Inc:", stats.ks_2samp(inc2, inc2))
    # print("Dec:", stats.ks_2samp(dec2, dec2))

    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=4, mode="expand", borderaxespad=0.)
    ax.legend(loc='lower right')
    ax.set_xlabel("Normalized jumps")
    # ax.set_xlabel("Normalized steps")
    # ax.set_xticks(np.arange(0, 0.6, 0.01))
    # ax.set_xticklabels(np.arange(0, 0.6, 0.01), rotation=90)
    ax.set_xlim(0, 0.6)
    # ax.set_yticks(np.arange(0, 1.0, 0.1))
    # ax.set_yticklabels(np.arange(0, 1.0, 0.1), fontsize=8)
    ax.set_ylabel("Probability")

    # plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/normalized_jumps_ecdf_by_epoch.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_fixed_normalized_price_delta_ecdf():
    normPrice_delta_dec_epoch_1 = []
    normPrice_delta_dec_epoch_2 = []

    normPrice_delta_inc_epoch_1 = []
    normPrice_delta_inc_epoch_2 = []

    normPrice_delta_dec_epoch_1_fixed = []
    normPrice_delta_dec_epoch_2_fixed = []

    normPrice_delta_inc_epoch_1_fixed = []
    normPrice_delta_inc_epoch_2_fixed = []

    instList = "F:/alibaba_work/*_full_traces_updated/*clean_2.csv"
    instList = glob.glob(instList)

    total_num = len(instList)

    for num, inst in enumerate(instList, 1):
        print(inst, num, "/", total_num)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)

        df1 = df[df['NormPriceDelta'] != 0.0].copy()
        normPrice_delta_dec_epoch_1.extend(df1.NormPriceDelta[(df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta < 0.0)].dropna().tolist())
        normPrice_delta_dec_epoch_2.extend(df1.NormPriceDelta[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta < 0.0)].dropna().tolist())

        normPrice_delta_inc_epoch_1.extend(df1.NormPriceDelta[(df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta > 0.0)].dropna().tolist())
        normPrice_delta_inc_epoch_2.extend(df1.NormPriceDelta[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta > 0.0)].dropna().tolist())

        df2 = df[df['FixedNormPriceDelta'] != 0.0].copy()
        normPrice_delta_dec_epoch_1_fixed.extend(df2.FixedNormPriceDelta[(df2.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.FixedNormPriceDelta < 0.0)].dropna().tolist())
        normPrice_delta_dec_epoch_2_fixed.extend(df2.FixedNormPriceDelta[(df2.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.FixedNormPriceDelta < 0.0)].dropna().tolist())

        normPrice_delta_inc_epoch_1_fixed.extend(df2.FixedNormPriceDelta[(df2.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.FixedNormPriceDelta > 0.0)].dropna().tolist())
        normPrice_delta_inc_epoch_2_fixed.extend(df2.FixedNormPriceDelta[(df2.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df2.FixedNormPriceDelta > 0.0)].dropna().tolist())

    fig = plt.figure(figsize=set_size())
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Normalized Price Delta VS Fixed Normalized Price Delta")

    normPrice_delta_dec_epoch_1 = [x * -1 for x in normPrice_delta_dec_epoch_1]
    normPrice_delta_dec_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1)
    y = ecdf(normPrice_delta_dec_epoch_1)
    ax.step(normPrice_delta_dec_epoch_1, y, color='black', linestyle='-', where='post', label="Decrease epoch 1")

    print("step dec epoch 1 "+str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1,y)))

    normPrice_delta_dec_epoch_1_fixed = [x * -1 for x in normPrice_delta_dec_epoch_1_fixed]
    normPrice_delta_dec_epoch_1_fixed.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1_fixed)
    y = ecdf(normPrice_delta_dec_epoch_1_fixed)
    ax.step(normPrice_delta_dec_epoch_1_fixed, y, color='silver', linestyle='--', where='post', label="Fixed decrease epoch 1")

    print("fixed dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1_fixed, y)))

    normPrice_delta_inc_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1)
    y = ecdf(normPrice_delta_inc_epoch_1)
    ax.step(normPrice_delta_inc_epoch_1, y, color='blue', linestyle='-', where='post', label="Increase epoch 1")

    print("step inc epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1, y)))

    normPrice_delta_inc_epoch_1_fixed.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1_fixed)
    y = ecdf(normPrice_delta_inc_epoch_1_fixed)
    ax.step(normPrice_delta_inc_epoch_1_fixed, y, color='lightblue', linestyle='--', where='post', label="Fixed increase epoch 1")

    print("fixed inc epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1_fixed, y)))

    normPrice_delta_dec_epoch_2 = [x * -1 for x in normPrice_delta_dec_epoch_2]
    normPrice_delta_dec_epoch_2.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_2)
    y = ecdf(normPrice_delta_dec_epoch_2)
    ax.step(normPrice_delta_dec_epoch_2, y, color='purple', linestyle='-', where='post', label="Decrease epoch 2")

    print("step dec epoch 2 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_2, y)))

    normPrice_delta_dec_epoch_2_fixed = [x * -1 for x in normPrice_delta_dec_epoch_2_fixed]
    normPrice_delta_dec_epoch_2_fixed.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_2_fixed)
    y = ecdf(normPrice_delta_dec_epoch_2_fixed)
    ax.step(normPrice_delta_dec_epoch_2_fixed, y, color='plum', linestyle='--', where='post', label="Fixed Decrease epoch 2")

    print("fixed dec epoch 2 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_2_fixed, y)))

    normPrice_delta_inc_epoch_2.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_2)
    y = ecdf(normPrice_delta_inc_epoch_2)
    ax.step(normPrice_delta_inc_epoch_2, y, color='red', linestyle='-', where='post', label="Increase epoch 2")

    print("step inc epoch 2 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_2, y)))

    normPrice_delta_inc_epoch_2_fixed.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_2_fixed)
    y = ecdf(normPrice_delta_inc_epoch_2_fixed)
    ax.step(normPrice_delta_inc_epoch_2_fixed, y, color='lightsalmon', linestyle='--', where='post', label="Fixed increase epoch 2")

    print("fixed inc epoch 2 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_2_fixed, y)))

    ax.legend()
    ax.set_xlabel("Normalized steps")
    # ax.set_xticks(np.arange(0, 0.21, 0.01))
    # ax.set_xticklabels(np.arange(0, 0.21, 0.01), rotation=90)
    ax.set_xlim(0, 0.6)
    ax.set_ylabel("Probability")

    plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/fixed_normalized_steps_ecdf_by_epoch.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_bad_fixed_normalized_price_delta_ecdf():
    normPrice_delta_dec_epoch_1 = []
    normPrice_delta_dec_epoch_1_fixed = []
    normPrice_delta_dec_epoch_1_bad1 = []
    normPrice_delta_dec_epoch_1_bad2 = []

    normPrice_delta_inc_epoch_1 = []
    normPrice_delta_inc_epoch_1_fixed = []
    normPrice_delta_inc_epoch_1_bad1 = []
    normPrice_delta_inc_epoch_1_bad2 = []

    instList = "F:/alibaba_work/*_full_traces_updated/*clean_2.csv"
    instList = glob.glob(instList)

    total_num = len(instList)

    for num, inst in enumerate(instList, 1):
        print(inst, num, "/", total_num)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)

        # df1 = df[df['NormPriceDelta'] != 0.0].copy()
        normPrice_delta_dec_epoch_1.extend(df.NormPriceDelta[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.NormPriceDelta < 0.0)].dropna().tolist())
        normPrice_delta_dec_epoch_1_fixed.extend(df.FixedNormPriceDelta[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.FixedNormPriceDelta < 0.0)].dropna().tolist())
        normPrice_delta_dec_epoch_1_bad1.extend(df.BadFixedNormPriceDelta1[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.BadFixedNormPriceDelta1 < 0.0)].dropna().tolist())
        normPrice_delta_dec_epoch_1_bad2.extend(df.BadFixedNormPriceDelta2[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.BadFixedNormPriceDelta2 < 0.0)].dropna().tolist())

        # df2 = df[df['FixedNormPriceDelta'] != 0.0].copy()
        normPrice_delta_inc_epoch_1.extend(df.NormPriceDelta[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.NormPriceDelta > 0.0)].dropna().tolist())
        normPrice_delta_inc_epoch_1_fixed.extend(df.FixedNormPriceDelta[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.FixedNormPriceDelta > 0.0)].dropna().tolist())
        normPrice_delta_inc_epoch_1_bad1.extend(df.BadFixedNormPriceDelta3[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.BadFixedNormPriceDelta3 > 0.0)].dropna().tolist())
        normPrice_delta_inc_epoch_1_bad2.extend(df.BadFixedNormPriceDelta4[(df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.BadFixedNormPriceDelta4 > 0.0)].dropna().tolist())

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Decrease steps")

    normPrice_delta_dec_epoch_1 = [x * -1 for x in normPrice_delta_dec_epoch_1]
    normPrice_delta_dec_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1)
    y = ecdf(normPrice_delta_dec_epoch_1)
    ax.step(normPrice_delta_dec_epoch_1, y, color='silver', linestyle='-', where='post', label="Normalized steps")

    print("step dec epoch 1 "+str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1,y)))

    normPrice_delta_dec_epoch_1_fixed = [x * -1 for x in normPrice_delta_dec_epoch_1_fixed]
    normPrice_delta_dec_epoch_1_fixed.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1_fixed)
    y = ecdf(normPrice_delta_dec_epoch_1_fixed)
    ax.step(normPrice_delta_dec_epoch_1_fixed, y, color='black', linestyle='--', where='post', label="Numerical algorithm with 2%")

    print("fixed dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1_fixed, y)))

    normPrice_delta_dec_epoch_1_bad1 = [x * -1 for x in normPrice_delta_dec_epoch_1_bad1]
    normPrice_delta_dec_epoch_1_bad1.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1_bad1)
    y = ecdf(normPrice_delta_dec_epoch_1_bad1)
    ax.step(normPrice_delta_dec_epoch_1_bad1, y, color='red', linestyle=':', where='post', label="Numerical algorithm with 1.9%")

    print("bad 0.019 dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1_bad1, y)))

    normPrice_delta_dec_epoch_1_bad2 = [x * -1 for x in normPrice_delta_dec_epoch_1_bad2]
    normPrice_delta_dec_epoch_1_bad2.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1_bad2)
    y = ecdf(normPrice_delta_dec_epoch_1_bad2)
    ax.step(normPrice_delta_dec_epoch_1_bad2, y, color='blue', linestyle='-.', where='post', label="Numerical algorithm with 2.1%")

    print("bad 0.021 dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1_bad2, y)))

    ax.legend(loc='lower right')
    ax.set_xlabel("Normalized steps")
    # ax.set_xticks(np.arange(0, 0.21, 0.01))
    # ax.set_xticklabels(np.arange(0, 0.21, 0.01), rotation=90)
    # ax.set_xlim(0.015, 0.025)
    ax.set_ylabel("Probability")

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Increase steps")

    normPrice_delta_inc_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1)
    y = ecdf(normPrice_delta_inc_epoch_1)
    ax.step(normPrice_delta_inc_epoch_1, y, color='silver', linestyle='-', where='post', label="Normalized steps")

    print("step inc epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1, y)))

    normPrice_delta_inc_epoch_1_fixed.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1_fixed)
    y = ecdf(normPrice_delta_inc_epoch_1_fixed)
    ax.step(normPrice_delta_inc_epoch_1_fixed, y, color='black', linestyle='--', where='post', label="Numerical algorithm with 3%")

    print("fixed inc epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1_fixed, y)))

    normPrice_delta_inc_epoch_1_bad1.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1_bad1)
    y = ecdf(normPrice_delta_inc_epoch_1_bad1)
    ax.step(normPrice_delta_inc_epoch_1_bad1, y, color='red', linestyle=':', where='post',
            label="Numerical algorithm with 2.9%")

    print("bad 0.029 dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1_bad1, y)))

    normPrice_delta_inc_epoch_1_bad2.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1_bad2)
    y = ecdf(normPrice_delta_inc_epoch_1_bad2)
    ax.step(normPrice_delta_inc_epoch_1_bad2, y, color='blue', linestyle='-.', where='post',
            label="Numerical algorithm with 3.1%")

    print("bad 0.031 dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1_bad2, y)))

    ax.legend(loc='lower right')
    ax.set_xlabel("Normalized steps")
    # ax.set_xticks(np.arange(0, 0.21, 0.01))
    # ax.set_xticklabels(np.arange(0, 0.21, 0.01), rotation=90)
    # ax.set_xlim(0.025, 0.035)
    ax.set_ylabel("Probability")

    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/fixed_normalized_steps_ecdf_by_epoch.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_PriceDelta_boxplot_per_region():
    fig = plt.figure(figsize=(15, 10))
    regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
                    'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
                    'eu-west-1','me-east-1','us-east-1','us-west-1']
    #regions = ['eu-west-1','me-east-1','us-east-1']
     
    data = dict() 
    for region in regions:
        instList = "./"+region+"_2020_traces/*vpc.csv"
        instList = glob.glob(instList)

        norm_priceDelta_list = []
        tmp_list = []
        norm_infoDelta_list = []
        for inst in instList:
            df = pd.read_csv(inst)

            df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
            
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True,inplace=True)
            
            length = df.Timestamp.count()
            if length > 3:
                #df = df[df.Description == 'decrease']
                #if df.empty != True:
                #if 'Description' in df:
                #    if slope == 'const':
                #        df = df[df.Description != 'const']
                #    else:
                #        df = df[df.Description == slope]
                
                #df = df[df['NormPriceDelta'].notna()]
                df = df[df.NormPriceDelta > 0.0]
                tmp_list.extend(df.NormPriceDelta.dropna().to_numpy())
                        
                    #df = df[df['NormInfo'].notna()]
                    #df['NormInfoDelta'] = df['NormInfo'].diff()
                    #df = df[df.NormPriceDelta != 0.0]
                    #norm_infoDelta_list.extend(df.NormInfoDelta.dropna().to_numpy())
        
        
        data.update({region: tmp_list})
        #norm_priceDelta_list.append(tmp_list)
        #print(norm_priceDelta_list)
    fig, ax = plt.subplots()
    ax.boxplot(data.values())
    ax.set_xticklabels(data.keys())
    print(data.keys())
        #plt.boxplot(norm_priceDelta_list, showfliers=False)
    
        #name = region + ' Norm Step Price Delta'
        #plt.step(norm_priceDelta_list, y, where='post', label=name)
        
        #norm_infoDelta_list = sorted(norm_infoDelta_list)
        ##print(norm_infoDelta_list)
        #ecdf = ECDF(norm_infoDelta_list)
        #y = ecdf(norm_infoDelta_list)
        #
        #name = region + ' Norm Jump Price Delta'
        #plt.step(norm_infoDelta_list, y, where='post', label=name)
    
    #title = region+" PriceDelta"
    #plt.title(title)
    #plt.legend()
    #plt.xlim(-0.025,0.0)
    plt.show()
    #plt.savefig(file)


def draw_Norm_PriceDelta_boxplot_per_month():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']

    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=5))

    for num, region in enumerate(regions, 1):
        print(region)
        instList = "./alibaba_work/" + region + "_full_traces_updated/*clean.csv"
        instList = glob.glob(instList)

        data = dict()
        for inst in instList:
            # print(inst)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/16/2020 00:00:00 AM"))]
            # df['month_year'] = df['Timestamp'].dt.to_period('M')
            df['week'] = df['Timestamp'].dt.to_period('W')
            # df['year_num'] = df.Timestamp.dt.year - 2019
            # df['week_num'] = (df.Timestamp.dt.isocalendar().week + (df.year_num * 52))
            # df['week_num'] = (df.Timestamp.dt.week + (df.year_num * 52))
            # df.loc[df['week_num'] == 1, 'week_num'] = 53

            # grouped = df.groupby('month_year')
            grouped = df.groupby('week')
            for name, group in grouped:
                group1 = group[group['NormPriceDelta'] < 0.0].copy()
                if name in data:
                    data[name].extend(group1.NormPriceDelta.dropna().tolist())
                else:
                    data[name] = group1.NormPriceDelta.dropna().tolist()

        data = dict(sorted(data.items()))

        # fig, ax = plt.subplots(figsize=set_size(fraction=2))
        ax = fig.add_subplot(5, 4, num)
        ax.set_title(region)
        ax.boxplot(data.values(), showfliers=False)
        # ax.violinplot(data.values(), showmeans=False, showmedians=True)
        ax.set_xticklabels(data.keys(), rotation=90, fontsize=8)
        ax.set_ylim(-0.3, 0)
        # print(data.keys())

    plt.tight_layout()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/analyze_decrease_slopes_by_week_boxplot_divided_to_regions_2.png"
    plt.savefig(file, bbox_inches="tight")


def draw_normalized_price_delta_ecdf_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = glob.glob(instList)
        total_count = len(instList)

        data = dict()
        data_dec = dict()
        data_inc = dict()
        for num,inst in enumerate(instList,1):
            print(inst, num, "/", total_count)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            # df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
            df['month_year'] = df['Timestamp'].dt.to_period('M')

            grouped = df.groupby('month_year')
            for month, group in grouped:
                if month in data:
                    data[month].extend(group.NormPriceDelta.dropna().tolist())
                else:
                    data[month] = group.NormPriceDelta.dropna().tolist()

                group_dec = group[group['NormPriceDelta'] < 0.0].copy()
                if month in data_dec:
                    data_dec[month].extend(group_dec.NormPriceDelta.dropna().tolist())
                else:
                    data_dec[month] = group_dec.NormPriceDelta.dropna().tolist()

                group_inc = group[group['NormPriceDelta'] > 0.0].copy()
                if month in data_inc:
                    data_inc[month].extend(group_inc.NormPriceDelta.dropna().tolist())
                else:
                    data_inc[month] = group_inc.NormPriceDelta.dropna().tolist()

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
        data = dict(sorted(data.items()))
        # ax = fig.add_subplot(4, 5, num)
        # ax = fig.add_subplot(1, 1, 1)
        # ax.set_title("Decrease steps per month")

        f1 = 0
        f2 = 0
        for month, values in data.items():
            # values.sort()
            if values:
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 01-2020")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                else:
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="After 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')

        # plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Normalized steps")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(-0.2, 0.2, 0.01))
        plt.xlim(-0.2, 0.2)

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
        # # ax = fig.add_subplot(4, 5, num)
        # ax = fig.add_subplot(1, 2, 1)
        # ax.set_title("Decrease steps per month")
        #
        data_dec = dict(sorted(data_dec.items()))
        f1 = 0
        f2 = 0
        for month, values in data_dec.items():
            # values.sort()
            if values:
                values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                print(month, str(np.interp([0.019, 0.0195, 0.02, 0.0205, 0.021], values, y)))
                if month.to_timestamp(freq ='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 01-2020")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                else:
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="After 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')

        # plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Normalized steps")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0.0, 0.2)

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
        # ax = fig.add_subplot(1, 2, 2)
        # ax.set_title("Increase steps per month")
        #
        data_inc = dict(sorted(data_inc.items()))
        f1 = 0
        f2 = 0
        for month, values in data_inc.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 01-2020")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                else:
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="After 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')

        # plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Normalized steps")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0.0, 0.2)

    # plt.tight_layout()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/analyze_slopes_by_month_decrease_vs_increase.png"
    # plt.savefig(file, bbox_inches="tight")
    plt.show()

# Draw the normalized price delta cdfs per week for each region separately.
def draw_normalized_price_delta_ecdf_per_week():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    # regions = ['*']

    fig = plt.figure(figsize=set_size(fraction=4))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "./alibaba_work/" + region + "_full_traces_updated/*clean.csv"
        instList = glob.glob(instList)

        # tmp_list = []
        data = dict()
        for inst in instList:
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
            # df['week'] = df['Timestamp'].dt.to_period('W')

            df['Year-Week'] = df['Timestamp'].dt.strftime('%Y-%W')

            grouped = df.groupby('Year-Week')
            for name, group in grouped:
                # group1 = group[group['NormPriceDelta'] < 0.0].copy()
                group1 = group[group['FixedNormPriceDelta'] < 0.0].copy()
                if name in data:
                    # data[name].extend(group1.NormPriceDelta.dropna().tolist())
                    data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    # data[name] = group1.NormPriceDelta.dropna().tolist()
                    data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        data = dict(sorted(data.items()))

        ax = fig.add_subplot(4, 5, num)
        ax.set_title(region)
        week_list = []
        value_list = []
        for month, values in data.items():
            # values.sort()
            if values:
                values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                # if month.to_timestamp(freq ='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):

                ax.step(values, y, '-', where='post', label=month)
                # print(str(month) + " " + str(np.interp([0.02], values, y)))
                week_list.append(str(month))
                value_list.append(str(np.interp([0.02], values, y)))
                # else:
                #     ax.step(values, y, '-.', where='post', label=month)
        print(region, week_list)
        print(region, value_list)

        # plt.legend(loc='upper center', ncol=9, fancybox=True, shadow=True)
        # ax.legend()
        # ax.set_xlabel("Normalized steps")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        ax.set_xlim(-0.01, 0.25)

    handles, labels = ax.get_legend_handles_labels()
    lax = fig.add_subplot(5, 4, 20)
    lax.legend(handles, labels, borderaxespad=0, ncol=1)
    lax.axis("off")
    # fig.legend(handles, labels, loc='upper center', ncol=12, fancybox=True, shadow=True)

    plt.tight_layout()
    file = "./cloud_analyzing_repo/alibaba_scripts/graphs/analyze_fixed_decrease_slopes_ecdf_by_week_split_to_regions_folded.png"
    plt.savefig(file, bbox_inches="tight")


# Draw the normalized price delta cdfs per region for each week separately.
def draw_normalized_price_delta_ecdf_separated_per_week():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    # regions = ['*']

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))

    # week_list = ['2019-50', '2019-51', '2019-52', '2020-00', '2020-01', '2020-02', '2020-03', '2020-04', '2020-05']
    week_list = ['2019-50', '2019-51', '2020-01']
    # week_list = ['2020-13', '2020-14', '2020-15']

    for num, week in enumerate(week_list, 1):
        print(week)
        data = dict()
        for region in regions:
            # print(region)
            # instList = ['./alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
            instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
            instList = glob.glob(instList)

            for inst in instList:
                df = pd.read_csv(inst)
                df['Timestamp'] = pd.to_datetime(df['Timestamp'])
                df = df.sort_values('Timestamp')
                df.reset_index(drop=True, inplace=True)

                # df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
                # df['week'] = df['Timestamp'].dt.to_period('W')

                df['Year-Week'] = df['Timestamp'].dt.strftime('%Y-%W')
                df = df[df['Year-Week'] == week]

                df = df[df['NormPriceDelta'] < 0.0].copy()
                if region in data:
                    data[region].extend(df.NormPriceDelta.dropna().tolist())
                    # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    data[region] = df.NormPriceDelta.dropna().tolist()
                    # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        # data = dict(sorted(data.items()))

        # ax = fig.add_subplot(2, 5, num)
        ax = fig.add_subplot(1, 3, num)
        ax.set_title(week)
        for region, values in data.items():
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                ax.step(values, y, '-', where='post', label=region)

        ax.set_xlim(-0.2, 0.01)

    # Create the legend
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=2)

    # handles, labels = ax.get_legend_handles_labels()
    # fig.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=6, mode="expand", borderaxespad=0.)
    # fig.subplots_adjust(bottom=0.75)
    # lax = fig.add_subplot(2, 2, 4)
    # lax.legend(handles, labels, borderaxespad=0, ncol=2)
    # lax.axis("off")
    # fig.legend(handles, labels, loc="center right", ncol=4, fancybox=True, shadow=True)
    # plt.subplots_adjust(right=0.85)
    # plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/normalized_prices_ecdf_split_by_week.png"
    # plt.savefig(file, bbox_inches="tight")


# Draw the normalized price delta cdfs per region for each month separately.
def draw_normalized_price_delta_ecdf_separated_per_month():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    # regions = ['*']

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))

    month_list = ['2019-12', '2020-01']
    month_t_list = ['December 2019', 'January 2020']

    for num, month in enumerate(month_list, 1):
        print(month)
        data = dict()
        for region in regions:
            # print(region)
            # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean_2.csv']
            instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
            instList = glob.glob(instList)

            for inst in instList:
                df = pd.read_csv(inst)
                df['Timestamp'] = pd.to_datetime(df['Timestamp'])
                df = df.sort_values('Timestamp')
                df.reset_index(drop=True, inplace=True)

                df = df[(df.Timestamp >= pd.to_datetime("11/01/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/1/2020 00:00:00 AM"))]
                # df['week'] = df['Timestamp'].dt.to_period('W')

                df['month_year'] = df['Timestamp'].dt.to_period('M')
                df = df[df['month_year'] == month]

                df = df[df['NormPriceDelta'] < 0.0].copy()
                if region in data:
                    data[region].extend(df.NormPriceDelta.dropna().tolist())
                    # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    data[region] = df.NormPriceDelta.dropna().tolist()
                    # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        # data = dict(sorted(data.items()))

        # ax = fig.add_subplot(2, 5, num)
        ax = fig.add_subplot(1, len(month_list), num)
        ax.set_title(month_t_list[num-1])
        for region, values in data.items():
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                ax.step(values, y, '-', where='post', label=region)

        ax.set_xlim(-0.2, 0.01)

    # Create the legend
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=2)

    # handles, labels = ax.get_legend_handles_labels()
    # fig.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=6, mode="expand", borderaxespad=0.)
    # fig.subplots_adjust(bottom=0.75)
    # lax = fig.add_subplot(2, 2, 4)
    # lax.legend(handles, labels, borderaxespad=0, ncol=2)
    # lax.axis("off")
    # fig.legend(handles, labels, loc="center right", ncol=4, fancybox=True, shadow=True)
    # plt.subplots_adjust(right=0.85)
    # plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/normalized_prices_ecdf_split_by_week.png"
    # plt.savefig(file, bbox_inches="tight")


def analyze_slopes_by_inst_type():
    # typeList = ['t1', 's2', 's3', 'c1', 'c2', 's1', 'm1', 'm2', 'n1', 'n2', 'e3', 'sn1', 'sn2', 'n4', 'mn4', 'xn4', 'e4', 'gn3', 'se1', 'ga1', 'gn4', 'i1', 'cm4', 'ce4', 'c4', 'sc1', 'd1', 'sn1ne', 'sn2ne', 'se1ne', 'f1', 'gn5', 'd1ne', 'c5', 'g5', 'r5', 'hfc5', 'hfg5', 'i2', 're4', 'gn5i', 'gn5t', 't5', 'sccg5', 'scch5', 'sccgn5d', 'sccgn5', 'ebmg5', 'ebmr5', 'f2', 'ebmr4', 'ebmg4', 'ebma1', 'gn6p', 'g5se', 'ebmgn5t', 'sccgn5t', 'sccgn6', 'ebmgn5', 'ebmhfg5', 'ebmhfg4', 'ebmc4', 'ic5', 'gn5d', 'ebmgn5i', 'ebmi2', 'f3', 'gn6v', 'ebmi3', 're5', 'i2g', 'r1', 're4e', 'ebmg5s', 'ebmgn5ts', 'ebmg5s-eni', 'i2ne', 'i2gne', 'vgn5i', 'ebmc5s', 'ebmgn6t', 'g6', 'gn6i', 'ebmg6', 'ebmm5d', 'ebmr5s', 'ebmhd1', 'ebma3', 'ebmgn6', 'ebmgn6i', 'ec1', 'eg1', 'ebmhd2', 'ebmhd3', 'er1', 'v5', 'ebmi2_ant', 'ebmg6e', 'ebmc6e', 'ed2', 'ebmi3s', 'sn1nec', 'sn2nec', 'g6e', 'c6', 'se1nec', 'r6', 'ed1', 'scch5s', 'sccg5s', 't6', 'ebmgn6e', 'hfc6', 'hfg6', 'ht6c', 'ht6g', 'ht6r', 'gn6e', 'hfr6', 'ebmgn6v', 'ebman1', 'c5t', 'd2s', 'sccgn6ne', 'ebmc6', 'ebmr6', 'ebmhd4', 'ebmi5_8nvme', 'ebmhfr6', 'ebmhfg6', 'ebmhfc6', 'gn6t', 'ebmbz_c40m128', 'ebmbz_c56m128', 'ebmhd5', 'ebmre6_aep', 'ebmre6_6t', 'ebmre6_3t', 'ed3', 'vgn6iv', 'ebmgn6ex', 'ic6', 'ebmi2s', 'sccgn6ex', 'g5ne', 'g5nenl', 'faas_image', 'ebmtest_pov', 'sccc6', 'scchfr6', 'scchfg6', 'scchfc6', 'sccr6', 'sccg6', 'ec3', 'er3', 'eg3', 'ebmc6_aep', 's6', 'ebmi2g', 'vgn6i', 'kvmtest_hdd', 'kvmtest_ssd', 'ebmr7', 'ebmg7', 'ebmc7', 'i2d', 're6', 'd2c', 'video_transcoding', 'kvmtest_f45s1', 'kvmtest_f41', 'kvmtest_f43']

    # typeList = ['t1']
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    # regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))

    print("region,num_instances,2019-50,2019-51,2019-52,2020-00,2020-01,2020-02,2020-03,2020-04,2020-05")

    inst_typeList = []
    for num, tname in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        # instList = "./alibaba_work/*_full_traces_updated/*__ecs_" + tname + "_*clean.csv"
        instList = "F:/alibaba_work/" + tname + "_full_traces_updated/*clean_2.csv"
        # print(instList)
        instList = glob.glob(instList)
        inst_typeList.extend(instList)

        inst_count = len(instList)

        data = dict()
        for inst in instList:
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            # df.reset_index(drop=True, inplace=True)

            df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
            df['Year-Week'] = df['Timestamp'].dt.strftime('%Y-%W')

            grouped = df.groupby('Year-Week')
            for name, group in grouped:
                group1 = group[group['NormPriceDelta'] < 0.0].copy()
                # group1 = group[group['FixedNormPriceDelta'] < 0.0].copy()
                if name in data:
                    data[name].extend(group1.NormPriceDelta.dropna().tolist())
                    # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    data[name] = group1.NormPriceDelta.dropna().tolist()
                    # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        data = dict(sorted(data.items()))

        week_dict = {'2019-50': 0, '2019-51': 0, '2019-52': 0, '2020-00': 0, '2020-01': 0, '2020-02': 0, '2020-03': 0, '2020-04': 0, '2020-05': 0}
        # value_list = []
        for week, values in data.items():
            if values:
                values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                # week_list.append(str(week))
                week_dict[str(week)] = np.around(np.interp([0.02], values, y), 4)

        print(tname, ",", inst_count, ",", list(week_dict.values()))
        # print(tname, inst_count, value_list)

    # print(set([x for x in inst_typeList if inst_typeList.count(x) > 1]))
    # all_insts = "./alibaba_work/*_full_traces_updated/*clean.csv"
    # all_insts = glob.glob(all_insts)
    # print(len(inst_typeList), "/", len(all_insts))
    # print(list(set(all_insts) - set(inst_typeList)))
    # print(list(set(inst_typeList) - set(all_insts)))


def draw_slope_ecdf_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = glob.glob(instList)
        total_count = len(instList)

        data = dict()
        data_dec = dict()
        data_inc = dict()
        for num,inst in enumerate(instList,1):
            print(inst, num, "/", total_count)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            # df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
            df['month_year'] = df['Timestamp'].dt.to_period('M')

            grouped = df.groupby('month_year')
            for month, group in grouped:
                if month in data:
                    data[month].extend(group.Slope.dropna().tolist())
                else:
                    data[month] = group.Slope.dropna().tolist()

                group_dec = group[group['Slope'] < 0.0].copy()
                if month in data_dec:
                    data_dec[month].extend(group_dec.Slope.dropna().tolist())
                else:
                    data_dec[month] = group_dec.Slope.dropna().tolist()

                group_inc = group[group['Slope'] > 0.0].copy()
                if month in data_inc:
                    data_inc[month].extend(group_inc.Slope.dropna().tolist())
                else:
                    data_inc[month] = group_inc.Slope.dropna().tolist()

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
        data = dict(sorted(data.items()))
        # ax = fig.add_subplot(4, 5, num)
        # ax = fig.add_subplot(1, 1, 1)
        # ax.set_title("Decrease steps per month")

        f1 = 0
        f2 = 0
        f3 = 0
        f4 = 0
        for month, values in data.items():
            # values.sort()
            if values:
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                if month.to_timestamp(freq='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 9-2019")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="9-2019 to 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')
                elif pd.to_datetime("1/1/2020 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime("12/1/2020 00:00:00 AM"):
                    if f3 == 0:
                        plt.step(values, y, '-.', color='blue', where='post', label="01-2020 to 12-2020")
                        f3 = 1
                    else:
                        plt.step(values, y, '-.', color='blue', where='post')
                else:
                    if f4 == 0:
                        plt.step(values, y, '-.', color='green', where='post', label="after 12-2020")
                        f4 = 1
                    else:
                        plt.step(values, y, '-.', color='green', where='post')


        # plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Slope")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(-0.2, 0.2, 0.01))
        # plt.xlim(-0.2, 0.2)

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
        # # ax = fig.add_subplot(4, 5, num)
        # ax = fig.add_subplot(1, 2, 1)
        # ax.set_title("Decrease steps per month")
        #
        data_dec = dict(sorted(data_dec.items()))
        f1 = 0
        f2 = 0
        f3 = 0
        f4 = 0
        for month, values in data_dec.items():
            # values.sort()
            if values:
                values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                # print(month, str(np.interp([0.019, 0.0195, 0.02, 0.0205, 0.021], values, y)))
                if month.to_timestamp(freq='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 9-2019")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="9-2019 to 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')
                elif pd.to_datetime("1/1/2020 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime("12/1/2020 00:00:00 AM"):
                    if f3 == 0:
                        plt.step(values, y, '-.', color='blue', where='post', label="01-2020 to 12-2020")
                        f3 = 1
                    else:
                        plt.step(values, y, '-.', color='blue', where='post')
                else:
                    if f4 == 0:
                        plt.step(values, y, '-.', color='green', where='post', label="after 12-2020")
                        f4 = 1
                    else:
                        plt.step(values, y, '-.', color='green', where='post')

        # plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.title("Decrease")
        plt.xlabel("Slope")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0.0, 0.01)

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
        # ax = fig.add_subplot(1, 2, 2)
        # ax.set_title("Increase steps per month")
        #
        data_inc = dict(sorted(data_inc.items()))
        f1 = 0
        f2 = 0
        f3 = 0
        f4 = 0
        for month, values in data_inc.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                if month.to_timestamp(freq='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 9-2019")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="9-2019 to 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')
                elif pd.to_datetime("1/1/2020 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime("12/1/2020 00:00:00 AM"):
                    if f3 == 0:
                        plt.step(values, y, '-.', color='blue', where='post', label="01-2020 to 12-2020")
                        f3 = 1
                    else:
                        plt.step(values, y, '-.', color='blue', where='post')
                else:
                    if f4 == 0:
                        plt.step(values, y, '-.', color='green', where='post', label="after 12-2020")
                        f4 = 1
                    else:
                        plt.step(values, y, '-.', color='green', where='post')

        # plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Slope")
        plt.ylabel("Probability")
        plt.title("Increase")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0.0, 0.01)

    # plt.tight_layout()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/analyze_slopes_by_month_decrease_vs_increase.png"
    # plt.savefig(file, bbox_inches="tight")
    plt.show()


def draw_slope_ecdf_per_epoch():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    regions = ['*']

    dec_1 = []
    dec_2 = []
    dec_3 = []
    dec_4 = []

    inc_1 = []
    inc_2 = []
    inc_3 = []
    inc_4 = []

    for region in regions:
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = glob.glob(instList)
        total_num = len(instList)

        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_num)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            df1 = df[df['Slope'] != 0.0].copy()
            dec_1.extend(df1.Slope[(df1.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM")) & (df1.Slope < 0.0)].dropna().tolist())
            dec_2.extend(df1.Slope[(df1.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.Slope < 0.0)].dropna().tolist())
            dec_3.extend(df1.Slope[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM")) & (df1.Slope < 0.0)].dropna().tolist())
            dec_4.extend(df1.Slope[(df1.Timestamp >= pd.to_datetime("12/1/2020 00:00:00 AM")) & (df1.Slope < 0.0)].dropna().tolist())

            inc_1.extend(df1.Slope[(df1.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM")) & (df1.Slope > 0.0)].dropna().tolist())
            inc_2.extend(df1.Slope[(df1.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.Slope > 0.0)].dropna().tolist())
            inc_3.extend(df1.Slope[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM")) & (df1.Slope > 0.0)].dropna().tolist())
            inc_4.extend(df1.Slope[(df1.Timestamp >= pd.to_datetime("12/1/2020 00:00:00 AM")) & (df1.Slope > 0.0)].dropna().tolist())

    fig = plt.figure(tight_layout='True',figsize=set_size(fraction=1))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Normalized Jumps (zoom-out)")
    # ax.set_title("Normalized Steps")

    inc_1.sort()
    ecdf = ECDF(inc_1)
    y = ecdf(inc_1)
    plt.step(inc_1, y, color='black', linestyle='-', where='post', label="Inc 1")

    inc_2.sort()
    ecdf = ECDF(inc_2)
    y = ecdf(inc_2)
    ax.step(inc_2, y, color='red', linestyle='-', where='post', label="Inc 2")

    inc_3.sort()
    ecdf = ECDF(inc_3)
    y = ecdf(inc_3)
    ax.step(inc_3, y, color='blue', linestyle='-', where='post', label="Inc 3")

    inc_4.sort()
    ecdf = ECDF(inc_4)
    y = ecdf(inc_4)
    ax.step(inc_4, y, color='green', linestyle='-', where='post', label="Inc 4")

    # print("step inc epoch 2 " + str(np.interp([0.02, 0.021, 0.022, 0.03, 0.031, 0.032], normPrice_delta_inc_epoch_2, y)))

    dec_1 = [x * -1 for x in dec_1]
    dec_1.sort()
    ecdf = ECDF(dec_1)
    y = ecdf(dec_1)
    ax.step(dec_1, y, color='grey', linestyle='--', where='post', label="Dec 1")

    dec_2 = [x * -1 for x in dec_2]
    dec_2.sort()
    ecdf = ECDF(dec_2)
    y = ecdf(dec_2)
    ax.step(dec_2, y, color='salmon', linestyle='--', where='post', label="Dec 2")

    dec_3 = [x * -1 for x in dec_3]
    dec_3.sort()
    ecdf = ECDF(dec_3)
    y = ecdf(dec_3)
    ax.step(dec_3, y, color='dodgerblue', linestyle='--', where='post', label="Dec 3")

    dec_4 = [x * -1 for x in dec_4]
    dec_4.sort()
    ecdf = ECDF(dec_4)
    y = ecdf(dec_4)
    ax.step(dec_4, y, color='lightgreen', linestyle='--', where='post', label="Dec 4")

        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=4, mode="expand", borderaxespad=0.)
    ax.legend(loc='lower right')
    ax.set_xlabel("Slope")
    # ax.set_xlabel("Normalized steps")
    # ax.set_xticks(np.arange(0, 0.6, 0.01))
    # ax.set_xticklabels(np.arange(0, 0.6, 0.01), rotation=90)
    ax.set_xlim(0, 0.0005)
    # ax.set_yticks(np.arange(0, 1.0, 0.1))
    # ax.set_yticklabels(np.arange(0, 1.0, 0.1), fontsize=8)
    ax.set_ylabel("Probability")

    # plt.tight_layout()
    plt.show()


def draw_steps_per_time_interval():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing',
    #            'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1',
    #            'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    regions = ['*']

    data = []
    # data1 = []
    # data2 = []
    # data3 = []
    # data4 = []
    for region in regions:
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        # instList = [
        #     'F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean_2.csv']
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = glob.glob(instList)
        total_num = len(instList)

        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_num)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            df['TimeDeltaHours'] = df.TimeDelta / 3600
            df = df.dropna(subset=['TimeDeltaHours', 'NormPriceDelta'])

            data.extend(list(zip(df.TimeDeltaHours, df.NormPriceDelta, df.Timestamp)))

            # df1 = df[(df.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM"))].copy()
            # df2 = df[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM"))].copy()
            # df3 = df[(df.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))].copy()
            # df4 = df[(df.Timestamp >= pd.to_datetime("12/1/2020 00:00:00 AM"))].copy()
            #
            # if not df1.empty:
            #     data1.extend(list(zip(df1.TimeDeltaHours, df1.NormPriceDelta, df1.Timestamp)))
            # if not df2.empty:
            #     data2.extend(list(zip(df2.TimeDeltaHours, df2.NormPriceDelta, df2.Timestamp)))
            # if not df3.empty:
            #     data3.extend(list(zip(df3.TimeDeltaHours, df3.NormPriceDelta, df3.Timestamp)))
            # if not df4.empty:
            #     data4.extend(list(zip(df4.TimeDeltaHours, df4.NormPriceDelta, df4.Timestamp)))

            # plt.scatter(df.TimeDeltaHours ,df.NormPriceDelta, color='black', marker='+')

    # print(data_list)
    # print(data)

    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    # x, y, z = zip(*data1)
    # slope, intercept, r, p, stderr = scipy.stats.linregress(x, y)
    # line = f'Regression line 1: y={intercept:.3f}+{slope:.3f}x, r={r:.3f}'
    #
    # plt.scatter(x, y, color='black', marker='+', label='Epoch 1')
    # plt.plot(x, intercept + slope * np.array(x), color='grey', label=line)
    #
    # x, y, z = zip(*data2)
    # slope, intercept, r, p, stderr = scipy.stats.linregress(x, y)
    # line = f'Regression line 2: y={intercept:.3f}+{slope:.3f}x, r={r:.3f}'
    #
    # plt.scatter(x, y, color='red', marker='+', label='Epoch 2')
    # plt.plot(x, intercept + slope * np.array(x), color='salmon', label=line)
    #
    # x, y, z = zip(*data3)
    # slope, intercept, r, p, stderr = scipy.stats.linregress(x, y)
    # line = f'Regression line 3: y={intercept:.3f}+{slope:.3f}x, r={r:.3f}'
    #
    # plt.scatter(x, y, color='blue', marker='+', label='Epoch 3')
    # plt.plot(x, intercept + slope * np.array(x), color='lightblue', label=line)
    #
    # x, y, z = zip(*data4)
    # slope, intercept, r, p, stderr = scipy.stats.linregress(x, y)
    # line = f'Regression line 4: y={intercept:.3f}+{slope:.3f}x, r={r:.3f}'
    #
    # plt.scatter(x, y, color='green', marker='+', label='Epoch 4')
    # plt.plot(x, intercept + slope * np.array(x), color='lightgreen', label=line)

    # plt.xlabel("Time interval (h)")
    # plt.ylabel("Normalized steps")
    # plt.legend(loc='lower right')

    data = sorted(data, key=lambda x: x[2])
    x, y, z = zip(*data)

    r, p = scipy.stats.pearsonr(x, y)
    print("Pearson = ", r)
    rho, p = scipy.stats.spearmanr(x, y)
    print("Spearmanr = ", rho)
    tau, p = scipy.stats.kendalltau(x, y)
    print("Kendall = ", tau)

    slope, intercept, r, p, stderr = scipy.stats.linregress(x, y)
    line = f'Regression line: y={intercept:.3f}+{slope:.3f}x, r={r:.3f}'

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    plt.scatter(x, y, color='black', marker='+', label='Data')
    plt.plot(x, intercept + slope * np.array(x), label=line)
    plt.xlabel("Time interval (h)")
    plt.ylabel("Normalized steps")
    plt.legend(loc='lower right')

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    plt.scatter(z, y, color='black', marker='+', label='Data')
    plt.xlabel("Timestamp")
    plt.ylabel("Normalized steps")

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    plt.scatter(z, x, color='black', marker='+', label='Data')
    plt.xlabel("Timestamp")
    plt.ylabel("Time interval (h)")

    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--instType", help='The inst type for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
    parser.add_argument("--slope", help='The slope we want to examine')
    parser.add_argument("--ephoc", help='The ephoc we want to examine (1 - until September 2019, 2 - sincse September 2019)')
    parser.add_argument("--type", help='The type of epoch we are trying to find (NormPriceDelta / TimeDelta)')
    
  
    args = parser.parse_args()
    
    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'
    
    if args.instType:
        instType = args.instType
    else:
        instType = 'r6'
        
    if args.function:
        func = args.function
        if func == 'add_time_and_price_delta_cols':
            print("Running add_time_and_price_delta_cols function...")
            add_time_and_price_delta_cols()

        elif func == 'add_slope_col':
            print("Running add_slope_col function...")
            add_slope_col()
            
        elif func == 'add_normalized_price_and_delta_cols':
            print("Running add_normalized_price_and_delta_cols function...")
            add_normalized_price_and_delta_cols(region, 0.01, 0.02, 0.03)

        elif func == 'add_bad_normalized_percentiles':
            print("Running add_bad_normalized_percentiles...")
            add_bad_normalized_percentiles(region, 0.019, 0.021, 0.029, 0.031)
            
        elif func == 'draw_PriceDelta_boxplot_per_region':
            print('Running draw_PriceDelta_boxplot_per_region function...')
            draw_PriceDelta_boxplot_per_region()
        
        elif func == 'draw_bad_fixed_normalized_price_delta_ecdf':
            print('Running draw_bad_fixed_normalized_price_delta_ecdf function...')
            draw_bad_fixed_normalized_price_delta_ecdf()

        elif func == 'draw_normalized_price_delta_ecdf_per_region':
            print('Running draw_normalized_price_delta_ecdf_per_region function...')
            draw_normalized_price_delta_ecdf_per_region()

        elif func == 'draw_fixed_normalized_price_delta_ecdf':
            print('Running draw_fixed_normalized_price_delta_ecdf function...')
            draw_fixed_normalized_price_delta_ecdf()

        elif func == 'draw_normalized_price_delta_ecdf_per_month':
            print('Running draw_normalized_price_delta_ecdf_per_month...')
            draw_normalized_price_delta_ecdf_per_month()

        elif func == 'draw_normalized_price_delta_ecdf_separated_per_week':
            print('Running draw_normalized_price_delta_ecdf_separated_per_week function...')
            draw_normalized_price_delta_ecdf_separated_per_week()

        elif func == 'draw_normalized_price_delta_ecdf_separated_per_month':
            print("Running draw_normalized_price_delta_ecdf_separated_per_month function...")
            draw_normalized_price_delta_ecdf_separated_per_month()

        elif func == 'draw_Norm_PriceDelta_boxplot_per_month':
            print("Running draw_Norm_PriceDelta_boxplot_per_month...")
            draw_Norm_PriceDelta_boxplot_per_month()

        elif func == 'draw_slope_ecdf_per_month':
            print("Running draw_slope_ecdf_per_month...")
            draw_slope_ecdf_per_month()

        elif func == 'analyze_slopes_by_inst_type':
            print("Running analyze_slopes_by_inst_type function...")
            analyze_slopes_by_inst_type()

        elif func == 'draw_slope_ecdf_per_epoch':
            print("Running draw_slope_ecdf_per_epoch function...")
            draw_slope_ecdf_per_epoch()

        elif func == 'draw_steps_per_time_interval':
            print("Running draw_steps_per_time_interval function...")
            draw_steps_per_time_interval()
            
    print("Finish")
