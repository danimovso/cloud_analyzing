import glob
import matplotlib
import matplotlib.pyplot as plt

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import pandas as pd, numpy as np, seaborn as sns, scipy
import argparse
from statsmodels.distributions.empirical_distribution import ECDF

# plt.style.use('seaborn-colorblind')

# tex_fonts = {
#     # Use LaTeX to write all text
#     # "text.usetex": True,
#     "font.family": "serif",
#     # Use 10pt font in plots, to match 10pt font in document
#     "axes.labelsize": 14,
#     "font.size": 14,
#     # Make the legend/label fonts a little smaller
#     "legend.fontsize": 11,
#     "xtick.labelsize": 12,
#     "ytick.labelsize": 12
# }
#
# plt.rcParams.update(tex_fonts)

matplotlib.rcParams.update({
    # "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    # 'pgf.rcfonts': False,
})


def set_size(width=395.8225, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def fix_time_intervals_data():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    # fig = plt.figure(figsize=set_size(fraction=3))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        # instList = [
        #     'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        # instList = ['F:/alibaba_work\cn-hangzhou_full_traces_updated\cn_hangzhou_i__ecs_e3_3xlarge__optimized__vpc__clean.csv']
        instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean.csv"
        instList = glob.glob(instList)
        total_length = len(instList)

        # data = dict()
        for index, inst in enumerate(instList, 1):
            print(inst, index, "/", total_length)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('Timestamp')
            df.reset_index(drop=True, inplace=True)

            new_df = []
            length = df.Timestamp.count()
            print(length)
            i = 0
            while i < length - 1:
                new_df.append(
                    [df.OriginPrice.iloc[i], df.SpotPrice.iloc[i], df.NormPrice.iloc[i], df.TimeInSeconds.iloc[i],
                     df.Timestamp.iloc[i]])
                new_df.append([df.OriginPrice.iloc[i], df.SpotPrice.iloc[i], df.NormPrice.iloc[i],
                               df.TimeInSeconds.iloc[i + 1], df.Timestamp.iloc[i + 1]])
                i += 1

            new_df.append(
                [df.OriginPrice.iloc[-1], df.SpotPrice.iloc[-1], df.NormPrice.iloc[-1], df.TimeInSeconds.iloc[-1],
                 df.Timestamp.iloc[-1]])
            df2 = pd.DataFrame(new_df, columns=['OriginPrice', 'SpotPrice', 'NormPrice', 'TimeInSeconds', 'Timestamp'])
            df2.reset_index(drop=True, inplace=True)

            row_list = []
            length = df2.Timestamp.count()
            i = 1
            while i < length - 1:
                if df2.SpotPrice.iloc[i - 1] == df2.SpotPrice.iloc[i] == df2.SpotPrice.iloc[i + 1]:
                    row_list.append(i)
                i += 1

            df3 = df2.drop(row_list)
            # print(df3)

            name, csv = inst.split('.csv')
            file = name + "__square_2.csv"
            print("creating ", file)
            # print(df3)
            df3.to_csv(file, index=False)

            # plt.plot(df.Timestamp, df.NormPrice, '.:', label='origin')
            # plt.plot(df3.Timestamp, df3.NormPrice, 'x:', label='fixed')

    # plt.legend()
    # plt.show()
    # plt.savefig("./cloud_analyzing_repo/alibaba_scripts/checking_time_interval_fix.png", bbox_inches="tight")

# (5.47807, 2.4651315), (5.47807, 2.739035) (2.739035, 2.4651315)
def draw_time_delta_ecdf_per_epoch():
    fig = plt.figure(tight_layout=True, figsize=(4.1085525, 2.739035))
    # regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5','cn-beijing',
    #                 'cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen','cn-zhangjiakou','eu-central-1',
    #                 'eu-west-1','me-east-1','us-east-1','us-west-1']
    # regions = ['eu-west-1','me-east-1','us-east-1']
    regions = ['*']

    for region in regions:
        # instList = "./"+region+"_2020_traces/*clean.csv"
        # instList = "F:/alibaba_work/" + region + "_full_traces_updated/*__clean_2.csv"
        # instList = [
        #     'F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new.csv']
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*_clean_new_2.csv"
        instList = glob.glob(instList)

        total_length = len(instList)

        time_delta_epoch_1 = []
        time_delta_epoch_2 = []
        time_delta_epoch_3 = []

        static_count = 0
        non_static = 0
        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_length)
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df = df.sort_values('Timestamp')
            df = df.sort_values('MyIndex')
            df.reset_index(drop=True, inplace=True)

            if df.SpotPrice.nunique() > 1:
                non_static += 1
                df['TimeDeltaHours'] = df.TimeDelta / 3600

                df = df[df.clean == 0].copy()
                df = df[df.missing_data == 0].copy()
                # # print(df[['Timestamp', 'TimeDelta', 'FixedNormPriceDelta', 'clean']])

                # df = df[df.PeakInfo48.notnull()].copy()
                # df['TimeDeltaHours'] = df.TimeInSeconds.diff() / 3600
                # df['NormPriceJump'] = df.PeakInfo48.diff()
                #
                # df = df[df.missing_data == 0].copy()
                # df = df[df.NormPriceJump != 0].copy()

                time_delta_epoch_1.extend(
                    df.TimeDeltaHours[(df.Timestamp < pd.to_datetime("9/1/2019 00:00:00 AM"))].dropna().tolist())
                time_delta_epoch_2.extend(df.TimeDeltaHours[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (
                            df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())
                time_delta_epoch_3.extend(
                    df.TimeDeltaHours[(df.Timestamp > pd.to_datetime("12/1/2020 00:00:00 AM"))].dropna().tolist())
            else:
                static_count += 1

        print("Finished processing all instances. Starting to draw...")
        print("static: ", static_count, " non-static:", non_static, " total:", (non_static+static_count))
        # print("Decrease time intervals:")

        time_delta_epoch_1.sort()
        ecdf = ECDF(time_delta_epoch_1)
        y = ecdf(time_delta_epoch_1)
        plt.step(time_delta_epoch_1, y, '--', color='black', where='post', label="Before 09-2019")

        print("epoch 1 " + str(np.interp([12, 24, 48], time_delta_epoch_1, y)))

        time_delta_epoch_2.sort()
        ecdf = ECDF(time_delta_epoch_2)
        y = ecdf(time_delta_epoch_2)
        plt.step(time_delta_epoch_2, y, '-.', color='red', where='post', label="09-2019 until 12-2020")

        print("epoch 2 " + str(np.interp([12, 24, 48], time_delta_epoch_2, y)))

        time_delta_epoch_3.sort()
        ecdf = ECDF(time_delta_epoch_3)
        y = ecdf(time_delta_epoch_3)
        plt.step(time_delta_epoch_3, y, ':', color='blue', where='post', label="After 12-2020")

        print("epoch 3 " + str(np.interp([12, 24, 48], time_delta_epoch_3, y)))

    plt.legend(loc='lower right')
    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
    # plt.xlabel("Time between consecutive price changes [h]")
    plt.xlabel("Step length [h]")
    # plt.title("inc jumps")
    # plt.xlabel("Jumps length [h]")
    plt.ylabel("Probability")
    plt.xlim(0.0, 4)
    # plt.show()
    plt.savefig("C:/Users/user/Documents/RaaS/deconstructing-alibaba-cloud-pricing-paper/sigmetrics_figs/time_intervals_between_price_changes.pgf")
    # plt.savefig("./cloud_analyzing_repo/alibaba_scripts/graphs/time_intervals_between_price_changes_decrease.png",
    #             bbox_inches="tight")


def draw_time_delta_ecdf_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    fig = plt.figure(tight_layout=True, figsize=(4.1085525, 2.739035))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)
        total_length = len(instList)

        data = dict()
        for index, inst in enumerate(instList, 1):
            print(inst, index, "/", total_length)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df.loc[0, 'TimeDelta'] = 0
            # df = df[df['TimeDelta'] == 0].copy()
            # df = df.sort_values('Timestamp')
            df = df.sort_values('MyIndex')
            df = df[df.clean == 1]
            df.reset_index(drop=True, inplace=True)

            # df = df[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))]
            # df = df[(df.Timestamp < pd.to_datetime("2/1/2019 00:00:00 AM"))]
            df['month_year'] = df['Timestamp'].dt.to_period('M')
            if df.SpotPrice.nunique() > 1:
                grouped = df.groupby('month_year')
                for month, group in grouped:
                    group1 = group.copy()
                    group1['TimeDelta'] = group1['TimeInSeconds'].diff()
                    group1['TimeDeltaHours'] = group1.TimeDelta / 3600
                    group1 = group1[group1.missing_data == 0]
                    if month in data:
                        data[month].extend(group1.TimeDeltaHours.dropna().tolist())
                        # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                    else:
                        data[month] = group1.TimeDeltaHours.dropna().tolist()
                        # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        data = dict(sorted(data.items()))

        # ax = fig.add_subplot(4, 5, num)
        ax = fig.add_subplot(1, 1, num)
        # ax.set_title(region)

        f1 = 0
        f2 = 0
        f3 = 0
        # m_list = ['.', ',', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P', 'h', 'H', 'D']
        for month, values in data.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                # ax.step(values, y, '-.', where='post', label=month)
                if month.to_timestamp(freq='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                    if f1 == 0:
                        ax.step(values, y, '--', color='black', where='post', label="Before 09-2019")
                        f1 = 1
                    else:
                        ax.step(values, y, '--', color='black', where='post')
                elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime(
                        "12/1/2020 00:00:00 AM"):
                    if f2 == 0:
                        ax.step(values, y, '-.', color='red', where='post', label="09-2019 until 12-2020")
                        f2 = 1
                    else:
                        ax.step(values, y, '-.', color='red', where='post')
                else:
                    if f3 == 0:
                        ax.step(values, y, ':', color='blue', where='post', label="After 12-2020")
                        f3 = 1
                    else:
                        ax.step(values, y, ':', color='blue', where='post')

        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
        plt.legend(loc='lower right')
        # ax.legend(ncol=3, fancybox=True, shadow=True)
        # ax.set_xlabel("Time between consecutive price changes [h]")
        ax.set_xlabel("Step length [h]")
        ax.set_ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        ax.set_xlim(0.0, 4.0)
        # ax.set_ylim(0.0, 0.25)

    # plt.show()
    plt.savefig("C:/Users/user/Documents/RaaS/deconstructing-alibaba-cloud-pricing-paper/sigmetrics_figs/time_delta_ecdf_by_month.pgf")
    # plt.tight_layout()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/time_delta_ecdf_by_month_clean_traces_zoom-in.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_fraction_of_price_changes_at_round_hours():
    """

    :return:
    """
    fig = plt.figure(tight_layout=True, figsize=(5.47807, 2.4651315))
    instList = "../../alibaba_work/*_full_traces_updated/*clean_new_2.csv"
    instList = glob.glob(instList)

    all_data_list = []
    for num, inst in enumerate(instList, 1):
        print("proccessing ", inst, num)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df['Timestamp'] = df.Timestamp.dt.round("T")
        # df = df.sort_values('Timestamp')
        df = df.sort_values('MyIndex')
        df = df[df.clean == 0]

        if df.SpotPrice.nunique() > 1:
            start = pd.to_datetime("2018-01-01 00:00:00.000")
            df['TmpDiff'] = df['Timestamp'] - start
            df['seconds'] = df['TmpDiff'] / np.timedelta64(1, 's')

            df['TimeModulo3600'] = df['seconds'] % 3600
            df = df[df.missing_data == 0]

            grouped = df.groupby('month_year')
            for name, group in grouped:
                all_samples_count = group.TimeModulo3600.count()
                full_hour_samples_count = group.TimeModulo3600[group.TimeModulo3600 == 0.0].count()
                all_data_list.append((str(name), full_hour_samples_count, all_samples_count))

    d = {x: [0, 0] for x, _, _ in all_data_list}
    for name, full_count, all_count in all_data_list:
        d[name][0] += full_count
        d[name][1] += all_count

    month_year = []
    print("Month, Percent")
    for key, item in d.items():
        print(key, (item[0] / item[1]))
        month_year.append((key, (item[0] / item[1])))

    month_year = sorted(month_year, key=lambda x: x[0])
    zip(*month_year)
    plt.scatter(*zip(*month_year), marker='o')
    # plt.scatter(month_year,value)
    # Output = list(map(tuple, d.items()))
    # print(Output)
    # zip(*Output)

    # plt.scatter(*zip(*Output))
    plt.xlabel(r'Year - Month')
    plt.ylabel(r'Fraction')
    plt.xticks(rotation=90)
    plt.yticks()

    plt.show()
    # plt.savefig("C:/Users/user/Documents/RaaS/deconstructing-alibaba-cloud-pricing-paper/sigmetrics_figs/fraction_price_changes_per_round_hour.pgf")


def check_long_time_intervals():
    instList = "F:/alibaba_work/*_full_traces_updated/*clean_new.csv"
    instList = glob.glob(instList)
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_mn4_xlarge__optimized__clean_new.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean__square_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n2_7xlarge__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square.csv']
    # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_e4_small__optimized__vpc__clean_2.csv']
    total_count = len(instList)

    old_all_jump_interval = 0
    old_long_jump_intervals = 0
    all_jump_interval = 0
    long_jump_intervals = 0
    for num, inst in enumerate(instList, 1):
        print("Processing ", inst, num, "/", total_count)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        # df = df[df.clean == 0].copy()

        df.NormPrice = np.around(df.NormPrice, 2)
        # fig = plt.figure(tight_layout=True, figsize=set_size())
        # plt.plot(df.Timestamp, df.NormPrice, '.:', color='black')

        df = df[df.FixedPeakInfo.notna()]
        df['jump_interval'] = df.TimeInSeconds.diff()
        df = df[df.missing_data == 0].copy()
        # plt.scatter(df.Timestamp, df.NormPrice, color='red')

        # print(df[df.jump_interval > 0].head(10))
        all_jump_interval += df.Timestamp[df.jump_interval > 0].count()
        long_jump_intervals += df.Timestamp[df.jump_interval > 86400].count()
        # print(df.Timestamp[df.jump_interval > 86400].count())

        name, csv = inst.split('__clean_new.csv')
        inst2 = name + "__vpc__clean__square_2.csv"
        df2 = pd.read_csv(inst2)
        df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
        df2 = df2[df2.FixedPeakInfo.notna()]
        df2['jump_interval'] = df2.TimeInSeconds.diff()
        old_all_jump_interval += df2.Timestamp[df2.jump_interval > 0].count()
        old_long_jump_intervals += df2.Timestamp[df2.jump_interval > 86400].count()

        # print(df2['jump_interval'].head(10))
        # print(df2.Timestamp[df2.jump_interval > 86400].count())

        # plt.scatter(df2.Timestamp, df2.NormPrice, marker='x', color='yellow')

    # plt.show()

    print("New traces: ", long_jump_intervals, all_jump_interval, (long_jump_intervals/all_jump_interval))
    print("Old traces: ", old_long_jump_intervals, old_all_jump_interval, (old_long_jump_intervals/old_all_jump_interval))


def draw_jump_length_ecdf_per_month():
    # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
    instList = "../../alibaba_work/*_full_traces_updated/*clean_new_2.csv"
    instList = glob.glob(instList)
    total_length = len(instList)

    data = dict()
    for index, inst in enumerate(instList, 1):
        print(inst, index, "/", total_length)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')

        # df = df[(df.Timestamp >= pd.to_datetime("9/1/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/1/2020 00:00:00 AM"))]
        # df = df[(df.Timestamp < pd.to_datetime("2/1/2019 00:00:00 AM"))]
        df['month_year'] = df['Timestamp'].dt.to_period('M')
        if df.SpotPrice.nunique() > 1:
            grouped = df.groupby('month_year')
            for month, group in grouped:
                group1 = group.copy()
                group1 = group1[group1.PeakInfo48.notnull()].copy()
                group1['TimeDeltaHours'] = group1.TimeInSeconds.diff() / 3600
                group1['NormPriceJump'] = group1.PeakInfo48.diff()

                group1 = group1[group1.missing_data == 0].copy()
                group1 = group1[group1.NormPriceJump != 0].copy()
                if month in data:
                    data[month].extend(group1.TimeDeltaHours.dropna().tolist())
                    # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    data[month] = group1.TimeDeltaHours.dropna().tolist()
                    # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

    data = dict(sorted(data.items()))

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title(region)

    f1 = 0
    f2 = 0
    f3 = 0
    # m_list = ['.', ',', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P', 'h', 'H', 'D']
    for month, values in data.items():
        # values.sort()
        if values:
            # values = [x * -1 for x in values]
            values.sort()
            ecdf = ECDF(values)
            y = ecdf(values)
            # ax.step(values, y, '-.', where='post', label=month)
            if month.to_timestamp(freq='M') < pd.to_datetime("9/1/2019 00:00:00 AM"):
                if f1 == 0:
                    ax.step(values, y, '--', color='black', where='post', label="Before 09-2019")
                    f1 = 1
                else:
                    ax.step(values, y, '--', color='black', where='post')
            elif pd.to_datetime("9/1/2019 00:00:00 AM") <= month.to_timestamp(freq='M') < pd.to_datetime(
                    "12/1/2020 00:00:00 AM"):
                if f2 == 0:
                    ax.step(values, y, '-.', color='red', where='post', label="09-2019 until 12-2020")
                    f2 = 1
                else:
                    ax.step(values, y, '-.', color='red', where='post')
            else:
                if f3 == 0:
                    ax.step(values, y, ':', color='blue', where='post', label="After 12-2020")
                    f3 = 1
                else:
                    ax.step(values, y, ':', color='blue', where='post')

    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
    plt.legend(loc='lower right')
    # ax.legend(ncol=3, fancybox=True, shadow=True)
    ax.set_xlabel("Jump length [h]")
    ax.set_ylabel("Probability")
    # ax.set_xticks(np.arange(0, 0.2, 0.01))
    # ax.set_xlim(0.0, 1.0)
    # ax.set_ylim(0.0, 0.25)

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--instType", help='The inst type for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')

    args = parser.parse_args()

    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'

    if args.instType:
        instType = args.instType
    else:
        instType = 'r6'

    if args.function:
        func = args.function
        if func == 'draw_time_delta_ecdf_per_month':
            print('Running draw_time_delta_ecdf_per_month function...')
            draw_time_delta_ecdf_per_month()

        elif func == 'draw_fraction_of_price_changes_at_round_hours':
            print("Running draw_fraction_of_price_changes_at_round_hours function...")
            draw_fraction_of_price_changes_at_round_hours()

        elif func == 'fix_time_intervals_data':
            print("Running fix_time_intervals_data function...")
            fix_time_intervals_data()

        elif func == 'draw_time_delta_ecdf_per_epoch':
            print("Running draw_time_delta_ecdf_per_epoch function...")
            draw_time_delta_ecdf_per_epoch()

        elif func == 'check_long_time_intervals':
            print("Running check_long_time_intervals function...")
            check_long_time_intervals()

        elif func == 'draw_jump_length_ecdf_per_month':
            print("Running draw_jump_length_ecdf_per_month function...")
            draw_jump_length_ecdf_per_month()