import os.path
from os import path
import glob
import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib import dates
import pandas as pd, numpy as np
import argparse
from pandas.plotting import register_matplotlib_converters


register_matplotlib_converters()

# plt.style.use('grayscale')
# plt.style.use('seaborn-colorblind')


tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 10,
    "font.size": 10,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 8,
    "xtick.labelsize": 8,
    "ytick.labelsize": 8
}
#
plt.rcParams.update(tex_fonts)

# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     # "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     # 'pgf.rcfonts': False,
# })

# 229.87749
def set_size(width=229.87749, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


# The following instances show the distinct chevron in July 2020
# instList = ['./alibaba_work/cn-beijing_2020_traces/cn_beijing_h__ecs_ic5_large__optimized__vpc__clean.csv', './alibaba_work/cn-hangzhou_2020_traces/cn_hangzhou_i__ecs_t5_c1m2_large__optimized__vpc__clean.csv', './alibaba_work/cn-hangzhou_2020_traces/cn_hangzhou_i__ecs_t5_c1m2_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-hangzhou_2020_traces/cn_hangzhou_i__ecs_t5_c1m4_large__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_d1_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_d1_14xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_c5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_c5_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_g6_8xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_g6_6xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_g6_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_g6_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_c6_8xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_c6_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_c6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_hfc5_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_hfc5_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_hfc5_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_g5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_g5_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_n4_small__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_mn4_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_mn4_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_ic5_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_r5_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_r5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_r6_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_r6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_t5_c1m1_large__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_n4_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_g__ecs_c6_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_c6_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shanghai_2020_traces/cn_shanghai_f__ecs_c5_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_t5_lc1m2_large__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_t5_lc1m1_small__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_e__ecs_t5_c1m4_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_b__ecs_se1ne_2xlarge__optimized__vpc__clean.csv', './alibaba_work/us-west-1_2020_traces/us_west_1b__ecs_i1_2xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_c__ecs_t5_c1m2_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_c__ecs_t5_c1m2_large__optimized__vpc__clean.csv', './alibaba_work/us-west-1_2020_traces/us_west_1b__ecs_i1_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_b__ecs_se1ne_4xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_c__ecs_i2_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-beijing_2020_traces/cn_beijing_c__ecs_n4_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_c__ecs_t5_c1m1_xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_b__ecs_se1ne_large__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_b__ecs_se1ne_3xlarge__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_e__ecs_c6_large__optimized__vpc__clean.csv', './alibaba_work/cn-shenzhen_2020_traces/cn_shenzhen_e__ecs_hfc6_xlarge__optimized__vpc__clean.csv']

# Instances in correlation (for Fig. 15 in paper)
# instList = ['D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_r5_2xlarge__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_r5_large__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_g5_large__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_g5_large__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_g5_4xlarge__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_g5_xlarge__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_r5_xlarge__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_g5_3xlarge__optimized__clean_new_2.csv',
#            'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_r5_3xlarge__optimized__clean_new_2.csv']


# Insts displaying the chevron of 22 July 2020
# instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_ic5_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_t5_c1m2_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_t5_c1m2_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_t5_c1m4_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_d1_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_d1_14xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_c5_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_c5_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_g6_8xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_g6_6xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_g6_4xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_g6_3xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_c6_8xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_c6_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_c6_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_hfc5_4xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_hfc5_3xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_hfc5_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_g5_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_g5_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_n4_small__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_mn4_4xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_mn4_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_ic5_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_r5_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_r5_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_r6_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_r6_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_t5_c1m1_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_n4_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_c6_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_c6_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_c5_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_t5_lc1m2_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_t5_lc1m1_small__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_t5_c1m4_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_b__ecs_se1ne_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_i1_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_c__ecs_t5_c1m2_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_c__ecs_t5_c1m2_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_i1_4xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_b__ecs_se1ne_4xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_i2_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_c__ecs_n4_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_c__ecs_t5_c1m1_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_b__ecs_se1ne_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_b__ecs_se1ne_3xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_c6_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfc6_xlarge__optimized__vpc__clean__square.csv']

# More insts in correlation:
# instList = ['D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m1_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_xn4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_n1_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_n2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfc5_large__optimized__clean_new_2.csv']
# instList = ['D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_s6_c1m2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m1_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_xn4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_n1_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_n2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfc5_large__optimized__clean_new_2.csv']
# instList = ['F:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_small__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_large__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m2_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m1_small__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_small__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_e4_xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_xn4_small__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_mn4_2xlarge__optimized__vpc__clean__square.csv', 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_f__ecs_n4_4xlarge__optimized__vpc__clean__square.csv']

# The Instances used to create Fig. 4.a (real life applications - price predictions) in the paper
# instList = ['D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_ic5_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5a__ecs_g5_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_e__ecs_e4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_sn2ne_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_a__ecs_sn1ne_22xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_sn2ne_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_sn2ne_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_t5_c1m4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_e__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_ic5_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5b__ecs_c6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_b__ecs_c5_16xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5b__ecs_g6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_10xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc5_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c8g1_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c4g1_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c24g1_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c16g1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfc6_10xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfc6_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfg6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_c6_13xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_n4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfg6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfc6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_b__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfg6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_c6_16xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_c5_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_c5_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-qingdao_full_traces_updated/cn_qingdao_c__ecs_t5_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_t5_c1m1_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfc6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-qingdao_full_traces_updated/cn_qingdao_c__ecs_t5_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-qingdao_full_traces_updated/cn_qingdao_c__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_n4_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_mn4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_n4_4xlarge__optimized__clean_new_2.csv']
instList = ['D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_a__ecs_sn1ne_22xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/us-east-1_full_traces_updated/us_east_1a__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_sn2ne_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_a__ecs_sn2ne_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_t5_c1m4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfr6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_e__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_ic5_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_g__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5b__ecs_c6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_b__ecs_c5_16xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-5_full_traces_updated/ap_southeast_5b__ecs_g6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_t5_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_g6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfg6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_e__ecs_hfg6_10xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc5_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_s6_c1m4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c8g1_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c4g1_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c24g1_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_gn6i_c16g1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfc6_10xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfc6_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_h__ecs_hfg6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_c6_13xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m2_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_s6_c1m4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_f__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfc6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/ap-southeast-1_full_traces_updated/ap_southeast_1a__ecs_n4_small__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfg6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfc6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_j__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-huhehaote_full_traces_updated/cn_huhehaote_b__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfg6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_4xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_c6_16xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfc6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_3xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_c5_6xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_large__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfg6_xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_b__ecs_c5_8xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-qingdao_full_traces_updated/cn_qingdao_c__ecs_t5_c1m2_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_h__ecs_t5_c1m1_2xlarge__optimized__clean_new_2.csv', 'D:/alibaba_work/cn-shenzhen_full_traces_updated/cn_shenzhen_d__ecs_t5_c1m1_4xlarge__optimized__clean_new_2.csv']

# Found the August Mesa - figure 4.b:
# instList = ['D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_medium__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn1_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_6xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g6_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_6xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_8xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c5_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_6xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r5_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g5_8xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_g6_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c6_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_2xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_large__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_sn2ne_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_3xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1ne_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_c6_xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_r6_4xlarge__optimized__clean_new_2.csv',
# 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_se1_8xlarge__optimized__clean_new_2.csv',]

# instList = ['F:/alibaba_work\\cn-qingdao_full_traces_updated\\cn_qingdao_b__ecs_e3_7xlarge__optimized__vpc.csv',
# 'F:/alibaba_work\\cn-qingdao_full_traces_updated\\cn_qingdao_b__ecs_e3_7xlarge__optimized__vpc__clean_2.csv',
# 'F:/alibaba_work\\cn-qingdao_full_traces_updated\\cn_qingdao_b__ecs_e3_7xlarge__optimized__vpc__clean__square_2.csv']

#  The missing chevron values:
# instList = ['F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_b__ecs_gn5_c4g1_xlarge__optimized__vpc__clean__square_2.csv'] #, 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_d__ecs_gn5_c4g1_xlarge__optimized__vpc__clean.csv',]
            # 'F:/alibaba_work/cn-shanghai_full_traces_updated/cn_shanghai_e__ecs_gn5_c4g1_xlarge__optimized__vpc__clean.csv', 'F:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_f__ecs_gn5_c4g1_xlarge__optimized__vpc__clean.csv']
# instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square_2.csv']
# instList = ['F:/alibaba_work/cn-zhangjiakou_full_traces_updated/cn_zhangjiakou_c__ecs_hfc6_8xlarge__optimized__vpc.csv']
# instList = ['F:/alibaba_work/cn-hongkong_full_traces_updated/cn_hongkong_b__ecs_n1_xlarge__optimized__vpc.csv']

hours = ['00:00:00', '01:00:00', '02:00:00', '03:00:00', '04:00:00', '05:00:00',
         '06:00:00', '07:00:00', '08:00:00', '09:00:00', '10:00:00', '11:00:00',
         '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00', '17:00:00',
         '18:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00', '23:00:00']

#   (5.47807, 2.4651315), (5.47807, 2.739035) (2.739035, 1.69281672638)
def spotPrice_trace_for_inst_list(instList):
    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    ax = fig.add_subplot(1,1,1)
    colors = ['blue', 'orange', 'green', 'red', 'purple', 'turquoise', 'magenta', 'navy', 'salmon']
    #           'turquoise', 'plum', 'violet', 'tan', 'coral', 'ivory', 'silver', 'white']
    m_list = ['.', 'o', 'v', '^', '<', '>', '1', 's', '*', '+', 'x', 'd', '2', '3', '4', '8', 'h', 'X', 'p', 'P',
              'h', 'H', 'D']
    print(len(instList))
    for num, inst in enumerate(instList,0):
        print(inst)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df[df.missing_data == 0]
        # plt.scatter(df.Timestamp, df.SpotPrice, color='red')

        # df = df[(df.Timestamp >= pd.to_datetime('6/23/2021 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('06/24/2021  00:00:00 AM'))]
        # df = df[(df.Timestamp >= pd.to_datetime('7/1/2020 00:00:00 AM')) & (
        #             df.Timestamp < pd.to_datetime('10/1/2020  00:00:00 AM'))]

        # For the Mesa pattern
        # df = df[(df.Timestamp >= pd.to_datetime('8/18/2020 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('8/26/2020  00:00:00 AM'))]

        # For the price prediction based on correlation figure
        df = df[(df.Timestamp >= pd.to_datetime('8/14/2020 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('8/20/2020 00:00:00 AM'))]
        # print(df.head(5))

        # _, name = inst.split("cn_shenzhen_")
        # name, _ = name.split("__optimized__clean_new_2.csv")
        # zone, name = name.split("__ecs_")
        # nameZone = name + " (zone " + zone + ")"

        ax.plot(df.Timestamp, df.NormPrice, '.:', markersize=2)
        # ax.plot(df.Timestamp, df.SpotPrice, ':', color=colors[num], marker=m_list[num], label=nameZone)
        # plt.plot(df.Timestamp, df.NormPrice, 'o:', label=name[0])
        # plt.scatter(df.Timestamp[df.FixedPeakInfo.notna()], df.NormPrice[df.FixedPeakInfo.notna()], marker='o', color='red')

    # ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
    # plt.setp(ax.get_xticklabels(), rotation=45)
    ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m  %H:%M'))
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # title = "Pattern found in normalized spot price"
    plt.ylabel('Normalized Spot price')
    plt.xticks(rotation = 45)
    plt.xlabel('Timestamp')
    # plt.title(title, fontsize='18')
    # plt.savefig("./cloud_analyzing_repo/alibaba_scripts/graphs/correlated_instances.png")
    plt.show()
    # plt.savefig("C:/Users/user/Documents/RaaS/deconstructing-alibaba-cloud-pricing-paper/sigmetrics_figs/insts_in_correlation.png")

def spotPrice_trace_for_inst():
    # file1 = 'F:/alibaba_work/cn-hongkong_full_traces_updated/cn_hongkong_b__ecs_n2_7xlarge__optimized__vpc.csv'
    # file2 = 'F:/alibaba_work/cn-hongkong_full_traces_updated/cn_hongkong_b__ecs_n2_7xlarge__optimized__vpc__clean__square.csv'
    # file3 = 'F:/alibaba_work/cn-hongkong_full_traces_updated/cn_hongkong_b__ecs_n2_7xlarge__optimized__vpc__clean_2.csv'
    # file1 = 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv'
    # file2 = "F:/alibaba_work/us-west-1_full_traces_updated/us_west_1b__ecs_n2_medium__optimized__vpc__clean__square.csv"
    # file2 = 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean__square_2.csv'
    # file3 = 'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean_2.csv'

    file1 = 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc_2.csv'
    file2 = 'D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__clean_new_2.csv'

    fig = plt.figure(tight_layout=True, figsize=(5.47807, 2.4651315))
    ax = fig.add_subplot(1, 1, 1)
    df = pd.read_csv(file1)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df[(df.Timestamp >= pd.to_datetime('5/3/2020 12:00:00 PM')) & (df.Timestamp < pd.to_datetime('5/5/2020 00:00:00 AM'))]
    ax.scatter(df.Timestamp, df.SpotPrice, marker='x', color='deepskyblue', label="Original spot price trace")

    df = pd.read_csv(file2)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('MyIndex')
    df = df[(df.Timestamp >= pd.to_datetime('5/3/2020 12:00:00 PM')) & (df.Timestamp < pd.to_datetime('5/5/2020 00:00:00 AM'))]
    ax.plot(df.Timestamp, df.SpotPrice, ".:", color='black', label="Effective price trace")
    ax.scatter(df.Timestamp[df.PeakInfo48.notnull() & (df.clean==1)], df.SpotPrice[df.PeakInfo48.notnull() & (df.clean==1)], marker='o', color='red', label="Extremum prices")
    # plt.plot(df.Timestamp, df.NormPrice, ".:", color='Black', label="Normalized spot price")
    #

    #
    # df = pd.read_csv(file3)
    # df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    # df.loc[0, 'TimeDelta'] = 0
    # df = df[df['TimeDelta'] == 0]
    # plt.plot(df.Timestamp[pd.notna(df.PeakInfo)], df.SpotPrice[pd.notna(df.PeakInfo)], marker='*', color='red', label="Price change trace")

    # df = df.sort_values('Timestamp')
    # df = df[(df.Timestamp >= pd.to_datetime('1/27/2019 00:00:00 AM')) & (
    #             df.Timestamp < pd.to_datetime('1/30/2019  00:00:00 AM'))]

    # plt.scatter(df.Timestamp[abs(df.pattern_mark_3) == 1001.03], df.NormPrice[abs(df.pattern_mark_3) == 1001.03], marker='x', color='red', label="pattern (3, 1, 0.08)")
    # plt.plot(df.Timestamp, df.NormPrice, ".-", color='Black', label="Normalized spot price")
    # plt.scatter(df.Timestamp[pd.notna(df.FixedPeakInfo)], df.NormPrice[pd.notna(df.FixedPeakInfo)], color='red', label="Extremum prices")

    # plt.xticks([])
    # plt.yticks([])
    # plt.title(inst)
    ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m  %H:%M'))
    plt.setp(ax.get_xticklabels(), rotation = 45)
    ax.set_xlabel("Timestamp")
    ax.set_ylabel("Preemptible price (\$)")
    ax.legend(loc='upper left')
    # plt.show()
    plt.savefig("C:/Users/user/Documents/RaaS/deconstructing-alibaba-cloud-pricing-paper/sigmetrics_figs/show_trace_types.pgf")

def spotPrice_trace_for_region():
    instList = "F:/alibaba_work/*_full_traces_updated/*vpc.csv"
    # instList = "./"+region+"_traces/*_with_tags.csv"
    instList = glob.glob(instList)
    count_total = len(instList)

    fig = plt.figure(figsize=set_size())
    for num, inst in enumerate(instList,1):
        print(num, "/", count_total)
        df = pd.read_csv(inst)
        # df[['Date','Time']] = df.Timestamp.str.split(expand=True)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df = df[((df.Timestamp > pd.to_datetime("01/12/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("01/13/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("03/05/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("03/06/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("04/05/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("04/06/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("05/05/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("05/06/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("06/05/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("06/06/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("07/05/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("07/06/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("08/04/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("08/05/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("09/02/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("09/03/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("10/01/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("10/02/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("11/04/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("11/05/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("12/03/19 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/04/19 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("01/03/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("01/04/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("02/01/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("02/02/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("03/01/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("03/02/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("03/28/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("03/29/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("04/20/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("04/21/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("05/01/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("05/02/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("05/19/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("05/20/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("06/09/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("06/10/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("07/11/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("07/12/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("08/06/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("08/07/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("09/02/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("09/03/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("10/09/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("10/04/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("11/08/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("11/09/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("12/06/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/07/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("12/30/20 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/31/20 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("01/31/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("02/01/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("03/01/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("03/02/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("03/25/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("03/26/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("04/27/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("04/28/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("05/21/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("05/22/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("06/17/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("06/18/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("07/12/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("07/13/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("08/10/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("08/11/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("09/10/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("09/11/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("10/10/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("10/11/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("11/09/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("11/10/21 00:00:00 AM"))) |
                ((df.Timestamp > pd.to_datetime("12/09/21 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("12/10/21 00:00:00 AM")))]

        # df = df[(df.Timestamp >= pd.to_datetime("8/1/2020 00:00:00 AM")) & (
        #             df.Timestamp < pd.to_datetime("9/1/2020 00:00:00 AM"))]
        # df = df[df.SpotPrice >= 2000]
        # df = df[df.Timestamp >= "1/1/2020 00:00:00 AM"]
        # if df.empty != True:

        # _,_,instName = inst.split("/")
        # print(instName)
        # instName,_ = instName.split("__optimized__vpc")
        # print(instName)
        #
        # label = instName+" all samples"
        # 1001.03	622800

        # plt.plot(df.Timestamp, df.SpotPrice, '.:')
        # if df.long_pattern_mark[df.long_pattern_mark == 1001.03].count() >= 1 and df.pattern_interval[df.pattern_interval == 622800].count() >= 1:
        #     print(inst)
        #     plt.plot(df.Timestamp, df.SpotPrice, '.:')
        #     plt.scatter(df.Timestamp[abs(df.long_pattern_mark) == 1001.03], df.SpotPrice[abs(df.long_pattern_mark) == 1001.03], marker='x', color='black')

        # label = instName+" Spot price every hour"
        # df = df[df.Time.isin(hours)]
        # plt.scatter(df.Timestamp,df.SpotPrice,label=label)
        plt.scatter(df.Timestamp,df.SpotPrice)

    # print("finished loop")
    # plt.legend()
    # title = "Spot price (" + region + ")"
    # plt.title(title)
    plt.show()


def draw_chevron():
    fig = plt.figure(tight_layout=True, figsize=(5.47807, 3.3856))

    instList = "F:/alibaba_work/*_full_traces_updated/*__clean.csv"
    instList = glob.glob(instList)

    all_count = len(instList)
    count = 0
    for num, inst in enumerate(instList,1):
        print("processing ", inst, num, "/", all_count)
        df = pd.read_csv(inst)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df = df[(df.Timestamp >= pd.to_datetime('2020-07-22 02:00:00')) & (df.Timestamp <= pd.to_datetime('2020-07-22 04:00:00'))].copy()
        # df.reset_index(drop=True, inplace=True)
        df.set_index('Timestamp', inplace=True)
        print(df.index)

        # if not df.empty and abs(df['pattern_mark_2'].iloc[0]) == 101:
        if len(df.index) >= 3 and df['NormPrice'].loc[pd.to_datetime('2020-07-22 02:00:00')] == df['NormPrice'].loc[pd.to_datetime('2020-07-22 04:00:00')] and \
            df['NormPrice'].loc[pd.to_datetime('2020-07-22 03:00:00')] > df['NormPrice'].loc[pd.to_datetime('2020-07-22 02:00:00')]:
            plt.plot(df.index, df.NormPrice, ".-")
            count += 1

    print(count)

    plt.xticks([pd.to_datetime('2020-07-22 02:00:00'), pd.to_datetime('2020-07-22 03:00:00'), pd.to_datetime('2020-07-22 04:00:00')], ["02:00", "03:00", "04:00"], rotation=90)
    plt.xlabel("Timestamp")
    plt.ylabel("Normalized price")
    plt.show()


def draw_patterns():
    """
    The function creates Figure 13 in the paper.
    :return:
    """
    fig = plt.figure(tight_layout='True', figsize = set_size())

    ax = fig.add_subplot(2,1,1)
    #  Ceil - Floor zig-zag in decrease:
    inst = '../../alibaba_work/ap-southeast-2_full_traces_updated/ap_southeast_2a__ecs_se1ne_2xlarge__optimized__clean_new_2.csv'

    df = pd.read_csv(inst)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('MyIndex')
    df = df[(df.Timestamp >= pd.to_datetime('2019-02-07 00:00:00')) & (df.Timestamp <= pd.to_datetime('2019-02-10 00:00:00'))].copy()

    ax.plot(df.Timestamp, df.NormPrice, '.:', color='black')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == -0.014], df.NormPrice[np.round(df.PriceDelta, 3) == -0.014],
               marker='x', color='deepskyblue', label='floor')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == -0.013], df.NormPrice[np.round(df.PriceDelta, 3) == -0.013],
               s=30, facecolors='none', edgecolors='red', label='ceil')

    ax.legend(loc='lower right')
    ax.set_xlabel("Timestamp\n\n(a) From 2019.")
    ax.set_ylabel("Normalized price")
    # ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m-%y %H:%M'))
    plt.setp(ax.get_xticklabels(), rotation=45)

    ax = fig.add_subplot(2,1,2)
    # Ceil - Floor zig-zag in increase:
    inst = '../../alibaba_work\cn-beijing_full_traces_updated/cn_beijing_k__ecs_hfc6_3xlarge__optimized__clean_new_2.csv'

    df = pd.read_csv(inst)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('MyIndex')

    df = df[(df.Timestamp >= pd.to_datetime('2021-03-02 00:00:00')) & (df.Timestamp <= pd.to_datetime('2021-03-06 00:00:00'))].copy()

    ax.plot(df.Timestamp, df.NormPrice, '.:', color='black')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == 0.012], df.NormPrice[np.round(df.PriceDelta, 3) == 0.012],
               marker='x', color='deepskyblue', label='floor')
    ax.scatter(df.Timestamp[np.round(df.PriceDelta, 3) == 0.013], df.NormPrice[np.round(df.PriceDelta, 3) == 0.013],
               s=30, facecolors='none', edgecolors='red', label='ceil')

    ax.legend(loc='lower right')
    ax.set_xlabel("Timestamp\n\n(b) From 2021.")
    ax.set_ylabel("Normalized price")
    # ax.xaxis.set_major_formatter(dates.DateFormatter('%d-%m-%y %H:%M'))
    plt.setp(ax.get_xticklabels(), rotation=45)

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
    parser.add_argument("--start", help='The starting timestamp from which to filter the traces')
    parser.add_argument("--function", help='The function we want to run')
    # parser.add_argument("--end", help='The ending timestamp from which to filter the traces')

    args = parser.parse_args()

    if args.region:
        region = args.region
        # if args.start:
        #     start = args.start
        # else:
        #     start = "2018-11-13 00:00:00"

    if args.function:
        func = args.function
        if func == 'draw_instances_in_correlation':
            print("Running draw_instances_in_correlation function...")
            spotPrice_trace_for_inst_list(instList)

        elif func == 'spotPrice_trace_for_region':
            print("Running spotPrice_trace_for_region function...")
            spotPrice_trace_for_region()

        elif func == 'spotPrice_trace_for_inst':
            print("Running spotPrice_trace_for_inst function...")
            spotPrice_trace_for_inst()

        elif func == 'draw_chevron':
            print("Running draw_chevron function...")
            draw_chevron()

        elif func == 'draw_patterns':
            print("Running draw_patterns function...")
            draw_patterns()


    print("Finish")
