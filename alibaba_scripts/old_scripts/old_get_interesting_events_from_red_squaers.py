import pandas as pd
import numpy as np
# %matplotlib
import matplotlib.pyplot as plt
import itertools

# file = "../../Alibaba_log_files/Correlation_tables/all_instances_weighted_august_2020_corr_with_dendrogram_labels_threshold_02_F10_ordered.csv"
file = "./alibaba_work/feature_files/best_all_instances_weighted_august_2020_corr.csv"


################################################
# Plot the matrix heatmap.                     #
# If you want to add the values to each cell   #
# set val_flag == 1.                           #
################################################
def show(mat, title, num, index, val_flag=0):
    mat = np.around(mat,1)
    # fig, ax = plt.subplots()
    ax = fig.add_subplot(1, num,index)
    im = ax.imshow(mat, cmap='jet')
    if val_flag:
        for i in range(len(mat)):
           for j in range(len(mat)):
               text = ax.text(j, i, mat[i, j],
                              ha="center", va="center", color="w")
    ax.set_title(title)


################################################
# Plot the heatmap directly from the the       #
# dataframe and add the names of the insts     #
# as the axes of the image.                    #
################################################
def show_df(df,title,num,index):
    ax = fig.add_subplot(1, num, index)
    ax.imshow(df.values, cmap='jet')
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(df.columns.tolist())))
    ax.set_yticks(np.arange(len(df.index.values)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(df.columns)
    ax.set_yticklabels(df.index.values)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.set_title(title)


################################################
# Color each square in the matrix with a       #
# different color.                             #
################################################
def show_squares_by_color(df,squares,title,num,index):
    mat = np.zeros_like(df.values)
    for i, s in enumerate(squares):
        # print(i, s)
        for row in range(s[0], s[-1] + 1):
            for col in range(s[0], s[-1] + 1):
                mat[row][col] = i + 100

    ax = fig.add_subplot(1, num, index)
    ax.imshow(mat, cmap='jet')
    # # We want to show all ticks...
    # ax.set_xticks(np.arange(len(df.columns.tolist())))
    # ax.set_yticks(np.arange(len(df.index.values)))
    # # ... and label them with the respective list entries
    # ax.set_xticklabels(df.columns)
    # ax.set_yticklabels(df.index.values)
    # # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    ax.set_title(title)


def calc_squares(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    min_red = max(min_red, 0.01)
    for i in range(1, size - 1):
        if (abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] * mat[i - 1][i] > 0 and mat[i][
            i + 1] > min_red):
            #       if (abs(mat[i][i+1]) >min_red and mat[i][i+1] > min_red):
            temp.append(i)  # continue the square
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)  # why does this repeat in both cases?
                temp = [i]  # start a new square

    if temp:  # i think this case can happen regardless
        temp.append(size - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([size - 1])

    # print(squares)
    return squares


df = pd.read_csv(file)
df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
df = df.dropna()
df.set_index("inst", inplace=True)

threshold = 0.95
squares = calc_squares(df.values, len(df.values), threshold)

fig = plt.figure(figsize=(25, 20))

show(df.values,"The squares",2,1)
show_squares_by_color(df,squares,"Mark the red squares",2,2)
fig.tight_layout()
plt.show()

for index, s in enumerate(squares):
    inst_list = []
    for i in s:
        name = df.columns.tolist()[i]
        reg, _, _ = name.split("__")
        reg = reg.replace('_', '-')
        reg = reg[:-1]
        if reg[-1] == '-':
            reg = reg[:-1]
        inst = "./alibaba_work/" + reg + "_2020_traces/" + name + "__vpc__clean.csv"
        inst_list.append(inst)

    df2 = pd.read_csv(inst_list[0], usecols = ['OriginPrice', 'SpotPrice', 'TimeInSeconds', 'Timestamp', 'NormPrice'])
    for inst in inst_list[1:] :
        #print(inst)
        df3 = pd.read_csv(inst, usecols = ['OriginPrice', 'SpotPrice', 'TimeInSeconds', 'Timestamp', 'NormPrice'])
        df2 = df2.append(df3, ignore_index=True)

    df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
    # df2 = df2[df2.TimeInSeconds < 84153600]
    # df2 = df2[df2.TimeInSeconds > 81471600]
    print("The size of the square is: "+str(len(s)))
    print(inst_list)
    print(df2.Timestamp.value_counts().head(20))
    print("==============================================")


