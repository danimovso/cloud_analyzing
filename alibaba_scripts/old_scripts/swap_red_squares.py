import pandas as pd
import numpy as np
# %matplotlib
import matplotlib.pyplot as plt
import itertools


# plt.style.use('grayscale')
# plt.style.use('tableau-colorblind10')

tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 12,
    "font.size": 12,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 10,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10
}

plt.rcParams.update(tex_fonts)


def set_size(width=441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim



# file = "../../Alibaba_log_files/Correlation_tables/small_partial_august_2020_corr.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_inst_august_2020_weighted_corr_with_dendrogram_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_weighted_august_2020_corr_with_dendrogram_labels_threshold_02_F10_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_inst_October_weighted_val_matrix.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_July_2020_corr_matrix_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_April_2020_corr_matrix_ordered_DBSCAN.csv"
# file = "../../Alibaba_log_files/Correlation_tables/corr_matrix_30_to_05_january_ordered_DBSCAN.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_inst_July_weighted_val_matrix.csv"

### The correct and up to date files:
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_corr_weighted_with_agglomerative_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/cropped_july_2020_corr_weighted_agglomerative_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/cropped_august_2020_corr_weighted_agglomerative_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/cropped_september_2020_corr_weighted_agglomerative_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/cropped_october_2020_corr_weighted_agglomerative_ordered.csv"
file = "../../Alibaba_log_files/Correlation_tables/old_corr_files/small_partial_august_2020_corr.csv"


# file = "../../Alibaba_log_files/Correlation_tables/all_instances_October_2020_corr_weighted_with_agglomerative_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_July_2020_corr_weighted_with_agglomerative_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_september_2020_corr_weighted_with_agglomerative_ordered.csv"


################################################
# Plot the matrix heatmap.                     #
# If you want to add the values to each cell   #
# set val_flag == 1.                           #
################################################
def show(mat, title, num, index, val_flag=0):
    mat = np.around(mat,1)
    # fig, ax = plt.subplots()
    ax = fig.add_subplot(3, num,index)
    im = ax.imshow(mat, cmap='jet')
    if val_flag:
        for i in range(len(mat)):
           for j in range(len(mat)):
               text = ax.text(j, i, mat[i, j],
                              ha="center", va="center", color="w")
    # ax.set_title(title)
    fig.colorbar(im, location='right', shrink=0.5)


################################################
# Plot the heatmap directly from the the       #
# dataframe and add the names of the insts     #
# as the axes of the image.                    #
################################################
def show_df(df,title,num,index):
    ax = fig.add_subplot(1, num, index)
    ax.imshow(df.values, cmap='jet')
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(df.columns.tolist())))
    ax.set_yticks(np.arange(len(df.index.values)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(df.columns)
    ax.set_yticklabels(df.index.values)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.set_title(title)


################################################
# Color each square in the matrix with a       #
# different color.                             #
################################################
def show_squares_by_color(df,squares,title,num,index):
    mat = np.zeros_like(df.values)
    for i, s in enumerate(squares):
        # print(i, s)
        for row in range(s[0], s[-1] + 1):
            for col in range(s[0], s[-1] + 1):
                mat[row][col] = i + 100

    ax = fig.add_subplot(1, num, index)
    ax.imshow(mat, cmap='jet')
    # # We want to show all ticks...
    # ax.set_xticks(np.arange(len(df.columns.tolist())))
    # ax.set_yticks(np.arange(len(df.index.values)))
    # # ... and label them with the respective list entries
    # ax.set_xticklabels(df.columns)
    # ax.set_yticklabels(df.index.values)
    # # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    ax.set_title(title)


def get_insts_in_squares(df, squares, file):
    f = open(file, "w")
    f.write(str(df.columns.tolist()) + "\n")
    f.write("==================================================\n")
    f.write("==================================================\n")
    for index, s in enumerate(squares):
        f.write("Square number: " + str(index) + " ,Len square: " + str(len(s)) + " ,Row numbers: [" + str(
            s[0]) + "," + str(s[-1]) + "]\n")
        inst_list = []
        for i in s:
            name = df.columns.tolist()[i]
            reg, _, _ = name.split("__")
            reg = reg.replace('_', '-')
            reg = reg[:-1]
            if reg[-1] == '-':
                reg = reg[:-1]
            inst = "./alibaba_work/" + reg + "_traces/" + name + "__vpc__clean.csv"
            inst_list.append(inst)
        f.write(str(inst_list) + "\n")
        # rows = [list(df.index.values)[i] for i in s]
        # print(rows)
        f.write("==================================================\n")
        # f.write(rows)

    f.close()


def calc_squares(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    min_red = max(min_red, 0.01)
    for i in range(1, size - 1):
        if (abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] * mat[i - 1][i] > 0 and mat[i][
            i + 1] > min_red):
            #       if (abs(mat[i][i+1]) >min_red and mat[i][i+1] > min_red):
            temp.append(i)  # continue the square
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)  # why does this repeat in both cases?
                temp = [i]  # start a new square

    if temp:  # i think this case can happen regardless
        temp.append(size - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([size - 1])

    # print(squares)
    return squares


def calc_daig(mat):
    return np.trace(mat, offset=1)


def calc_red_distance(mat):
    val = 0.0
    size = len(mat)
    for i in range(size):
        for j in range(i + 1, size):
            if (mat[i][j] > 0.0):
                val += mat[i][j] * (j - i)  # j >i always
    return val / size


def get_corners(squares):
    corners = []
    for s in squares:
        corners.append((s[0], s[-1]))
    # print(corners)
    return corners


def goal(c1, c2, mat, contend):
    c1_val = mat[c1[1]][c2[0]]
    if (contend == 0):
        max_val = c1_val
        return max_val
    if (contend == 1):
        max_val = c1_val * (c1[1] - c1[0] + 1)  # weigh in the length of the next square
        return max_val

    if (contend == 2):  # sum of corners
        max_val = (c1_val + mat[c1[1]][c2[1]]) * (c2[1] - c2[0] + 1)
    if contend == 3:
        max_val = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1) * (
                c1[1] - c1[0] + 1)
    if contend == 4:
        a = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1)
        max_val = a * (c2[0] - c1[1])
    if contend == 5:
        max_val = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * ((c2[1] - c2[0] + 1))
        # the size of the original square (1) is constant, no need to consider it

    if contend == 6:
        a = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1)
        max_val = a * a * (c2[0] - c1[1])
    if contend == 7:
        a = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1)
        max_val = a * a
    if contend == 8:
        mid1 = round((c1[1] + c1[0]) / 2)
        mid2 = round((c2[1] + c2[0]) / 2)
        max_val = mat[mid1][mid2] * ((c2[1] - c2[0] + 1))

    if (contend == 9):  # full sum of correlation matrix
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                max_val += mat[i][j]
    if (contend == 10):  # full sum of correlation mat , weighted by distance
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                max_val += mat[i][j]
        max_val *= (c2[0] - c1[1])

    if (contend == 11):  # real average value of the correlation
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                max_val += mat[i][j]
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))

    if contend == 12:  # real average value of the correlation, time distance
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                max_val += max(mat[i][j], 0) * abs(i - j)
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))

    if contend == 13:  # real average value of the abs correlation
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                max_val += abs(mat[i][j])
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))

    if contend == 14:  # real average value of the positive correlation, intended as a first stage before sorting the blue patches
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                max_val += max(0, mat[i][j])  # the max has a small effect on the score
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))
    if contend == 15:  # real average value of the positive correlation, intended as a first stage before sorting the blue patches
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                t = max(0, mat[i][j])
                max_val += t * t  # stress high values
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))

    if contend == 16:  # stress strong values, red or blue
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                t = mat[i][j]
                max_val += t * t  # stress high values
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))
    return max_val


def find_and_reconnect_squares(squares, mat, contend):
    size = len(mat)
    corners = get_corners(squares)
    i1 = 0
    while i1 < len(corners) - 2:  # no need to check if there is something after, but nothing to swap

        c1 = corners[i1]
        AB = goal(c1, corners[i1 + 1], mat, contend)  # the current connection, to be lost
        max_index_c2 = i1
        max_index_c3 = i1
        max_val = 0
        for i2, c2 in enumerate(corners[i1 + 2:], start=(i1 + 2)):
            # comments are for checking C  - to be moved after A
            AD = goal(c1, c2, mat, contend)  # the new connection AD
            # basic criterion, to see is this even worth considering?

            if (AD > AB):
                CD = goal(corners[i2 - 1], c2, mat, contend)  # the connection before c2, to be lost CD

                # todo find, optimize!
                # how big should the chunk be, where should the part between i1 and i2 move to?
                for i3, c3 in enumerate(corners[i2:], start=i2):
                    # is this the right starting point?
                    # c3 would be the last square to be moved

                    DB = goal(c3, corners[i1 + 1], mat, contend)  # the connection after the found section, to be made
                    # אם מעבירים רבוע ה לאחרי רבוע א צריך להוסיף אה הב דו ולהחסיר דה הו אב. ואז לבדוק האם זה חיובי

                    criterion1 = AD + DB - (CD + AB)
                    if i3 < len(corners) - 1:
                        # c3 is not the last square
                        CE = goal(c3, corners[i3 + 1], mat, contend)
                        BE = goal(corners[i2 - 1], corners[i3 + 1], mat, contend)
                        criterion1 += BE - CE

                    if (criterion1 > max_val):
                        max_index_c3 = i3
                        max_index_c2 = i2
                        max_val = criterion1
                    # if (i2-i1>5):
                    #    break
                    # if (i3-i2>5):
                    # break

        # print ("c1c2",goal(c1,corners[max_index_c2],mat,contend))

        if max_val > 0:
            # print ("max val:",max_val,"i1",i1,"i2",max_index_c2,"i3",max_index_c3,goal(c1,corners[max_index_c2],mat,contend))
            tmp_list = squares[
                       max_index_c2:max_index_c3 + 1]  # Get the elements (squares) from index1 to index2 including index2 and keep them in a temp list
            del squares[
                max_index_c2:max_index_c3 + 1]  # Delete the elements (squares) from index1 to index2 including index2
            squares[i1 + 1:i1 + 1] = tmp_list  # Insert the removed elements into the given index in the squares list
            tmp_list2 = corners[
                        max_index_c2:max_index_c3 + 1]  # Get the elements (squares) from index1 to index2 including index2 and keep them in a temp list

            del corners[
                max_index_c2:max_index_c3 + 1]  # Delete the elements (squares) from index1 to index2 including index2
            corners[i1 + 1:i1 + 1] = tmp_list2
        i1 = max_index_c3 + 1

    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    return col_order, squares, mat


def find_and_swap_tartan_squares(squares, mat, contend):
    size = len(mat)
    corners = get_corners(squares)
    if len(corners) > 1:
        first_corner = corners[1]
        i1 = 2
        while i1 < len(corners) - 2:

            cm2 = corners[i1 - 2]
            cm1 = corners[i1 - 1]
            c0 = corners[i1]
            c1 = corners[i1 + 1]
            c2 = corners[i1 + 2]

            A01 = goal(c0, c1, mat, contend)
            A12 = goal(c1, c2, mat, contend)
            Am2m1 = goal(cm2, cm1, mat, contend)
            Am10 = goal(cm1, c0, mat, contend)
            Am11 = goal(cm1, c1, mat, contend)
            criterion = A01 < A12 and Am10 < Am2m1 and Am11 > A01 and Am11 > Am10
            max_index = i1
            if (criterion):
                #            print(i1)
                max_index = i1 + 1
                elem = squares.pop(i1)
                squares.insert(i1 + 1, elem)
                elem2 = corners.pop(i1)
                corners.insert(i1 + 1, elem2)

            i1 = max_index + 1
    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    return col_order, squares, mat


df = pd.read_csv(file)
df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
df = df.dropna()
df.set_index("inst", inplace=True)
mat = df.values
# mat = np.around(mat,1)
# print("before: ",calc_daig(mat))

fig = plt.figure(tight_layout=True, figsize=set_size())
show(mat, "Before", 1, 1)

# show(mat, 'Before: ' + str(calc_daig(mat)) + " " + str(calc_red_distance(mat)), 3, 1)

mat2 = mat
squares = []

threshold = 0.95  # 0.8
levels = 5
contend = 0
tartanlevels = 10  # 10
# 1 better than 2. 5 better than both.
for j in range(levels):
    threshold_j = threshold / levels * (j + 1);
    print(threshold_j)

    for i in range(tartanlevels):
        squares = calc_squares(mat2, len(mat2), threshold_j)
        col_order, squares, mat2 = find_and_reconnect_squares(squares, mat2, contend)

        columns = [df.columns.tolist()[i] for i in col_order]
        rows = [list(df.index.values)[i] for i in col_order]
        df = df.reindex(columns, axis=1)
        df = df.reindex(rows, axis=0)

    for i in range(tartanlevels):
        squares = calc_squares(mat2, len(mat2), threshold_j)
        col_order, squares, mat2 = find_and_swap_tartan_squares(squares, mat2, contend)

        columns = [df.columns.tolist()[i] for i in col_order]
        rows = [list(df.index.values)[i] for i in col_order]
        df = df.reindex(columns, axis=1)
        df = df.reindex(rows, axis=0)

    L = len(squares)
    a = calc_daig(mat2)
    print(j, L, a, calc_red_distance(mat2))

# fig = plt.figure(tight_layout=True, figsize=set_size())
show(mat2, "mid", 1, 2)
# show(mat2, 'middle: ' + str(calc_daig(mat2)) + " " + str(calc_red_distance(mat2)), 3, 2)

contend = 15
tartanlevels = 5
levels = 10
for min_level in range(levels):
    for j in range(min_level,levels):
        threshold_j = threshold / levels * (j + 1);
        print(threshold_j)

        for i in range(tartanlevels):
            squares = calc_squares(mat2, len(mat2), threshold_j)
            col_order, squares, mat2 = find_and_reconnect_squares(squares, mat2, contend)

            columns = [df.columns.tolist()[i] for i in col_order]
            rows = [list(df.index.values)[i] for i in col_order]
            df = df.reindex(columns, axis=1)
            df = df.reindex(rows, axis=0)

        for i in range(tartanlevels):
            squares = calc_squares(mat2, len(mat2), threshold_j)
            col_order, squares, mat2 = find_and_swap_tartan_squares(squares, mat2, contend)

            columns = [df.columns.tolist()[i] for i in col_order]
            rows = [list(df.index.values)[i] for i in col_order]
            df = df.reindex(columns, axis=1)
            df = df.reindex(rows, axis=0)


        L = len(squares)
        a = calc_daig(mat2)
        print(j, L, a, calc_red_distance(mat2))

# print(calc_daig(mat2))

# fig = plt.figure(tight_layout=True, figsize=set_size())
show(mat2, "after", 1, 3)
# show_df(df,'After: ' + str(calc_daig(df.values)) + " " + str(calc_red_distance(df.values)),1,1)
# show(mat2, 'Actual August After: ' + str(calc_daig(mat2)) + " " + str(calc_red_distance(mat2)), 1, 1)
# show(mat2, "October", 1, 1)
# show(df.values, 'After: ' + str(calc_daig(df.values)) + " " + str(calc_red_distance(df.values)), 4, 3)

# show_squares_by_color(df, squares, "Color by square", 4, 4)

# file_name = "../../Alibaba_log_files/Correlation_tables/cropped_july_2020_red_squares.txt"
# file_name = "../../Alibaba_log_files/Correlation_tables/cropped_august_2020_red_squares.txt"
# file_name = "../../Alibaba_log_files/Correlation_tables/cropped_september_2020_red_squares.txt"
# file_name = "../../Alibaba_log_files/Correlation_tables/cropped_october_2020_red_squares.txt"
# file_name = "../../Alibaba_log_files/Correlation_tables/all_instances_October_2020_red_squares.txt"
# file_name = "../../Alibaba_log_files/Correlation_tables/all_instances_July_2020_red_squares.txt"
# file_name = "../../Alibaba_log_files/Correlation_tables/all_instances_september_2020_red_squares.txt"
# get_insts_in_squares(df, squares, file_name)

print("finished!")
# fig.tight_layout()
plt.show()



