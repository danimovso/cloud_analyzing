import os.path
from os import path
import glob
import matplotlib.pyplot as plt
import pandas as pd, numpy as np, seaborn as sns, scipy
from contextlib import closing
import argparse


def create_plot_per_date(region):
    instList = "./"+region+"_traces/*__vpc.csv"
    #instList = "./"+region+"_traces/*_e3_*__vpc.csv"
    instList = glob.glob(instList)

    dates = ['2019-07-07','2019-07-08','2019-07-09','2019-07-10','2019-07-11','2019-07-12','2019-07-13']
    #dates = ['2019-07-01', '2019-07-02','2019-07-03','2019-07-04','2019-07-05','2019-07-06','2019-07-07','2019-07-08',
    #         '2019-07-09','2019-07-10','2019-07-11','2019-07-12','2019-07-13','2019-07-14','2019-07-15','2019-07-16',
    #         '2019-07-17','2019-07-18','2019-07-19','2019-07-20','2019-07-21','2019-07-22','2019-07-23','2019-07-24',
    #         '2019-07-25','2019-07-26','2019-07-27','2019-07-28','2019-07-29','2019-07-30','2019-07-31']
    
    fig = plt.figure(figsize=(15, 8))
    for inst in instList:
        print(inst)
        df = pd.read_csv(inst)
        df[['Date','Time']] = df.Timestamp.str.split(expand=True)
        df = df[df.Date.isin(dates)]
        
        if not df.empty:
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df.set_index('Timestamp',inplace=True)
        
            _,_,name = inst.split("/")
            name,_ = name.split("__optimized__vpc.csv")
            print(name)
            df.SpotPrice.resample('1h').count().plot(label=name)
    #plt.legend()
    
    title = "Price changes per hour in 07-13 July 2019 ("+region+")"
    plt.xlabel("Date")
    plt.ylabel("Number of price changes per hour")
    plt.title(title)
    #plt.show()
    file = "./graphs/price_changes_per_hour_"+region+".png"
    plt.savefig(file)

def create_plot_by_hour(region):
    instList = "./"+region+"_full_traces/*__vpc.csv"
    #instList = "./"+region+"_traces/*_e3_*__vpc.csv"
    instList = glob.glob(instList)

    hours = ['00:00:00', '01:00:00', '02:00:00', '03:00:00', '04:00:00', '05:00:00', 
             '06:00:00', '07:00:00', '08:00:00', '09:00:00', '10:00:00', '11:00:00',
             '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00', '17:00:00',
             '18:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00', '23:00:00']
    
    fig = plt.figure(figsize=(15, 8))
    for inst in instList:
        print(inst)
        df = pd.read_csv(inst)
        df[['Date','Time']] = df.Timestamp.str.split(expand=True)
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df.set_index('Timestamp',inplace=True)

        df['Count'] = df.SpotPrice.resample('1h').count()
        df = df[df.columns.drop({'OriginPrice','SpotPrice','TimeInSeconds'})]
        df = df[df.Time.isin(hours)]
        
        if not df.empty:
            grouped = df.groupby('Time')
            
            hour_list = []
            mean_list = []
            for name,group in grouped:
                hour_list.append(name)
                mean_list.append(group.Count.mean())
            
            plt.plot(hour_list,mean_list)
        
    #plt.legend()
    
    title = "Average Price changes per hour ("+region+")"
    plt.xlabel("Date")
    plt.ylabel("Number of price changes per hour")
    plt.xticks(rotation=45)
    plt.title(title)
    #plt.show()
    file = "./graphs/average_price_changes_per_hour_"+region+".png"
    plt.savefig(file)    


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    #parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
  
    args = parser.parse_args()
    region = args.region
    #inst = args.inst
    
    create_plot_by_hour(region)
    print("Finish")