#########################################################
##   ##
#########################################################
import os.path
from os import path
import pandas as pd
import matplotlib.pyplot as plt
import glob
#import statsmodels.api as sm
import numpy as np
from statsmodels.distributions.empirical_distribution import ECDF
import argparse


def create_ecdf(region):
    
    regionDir = "./"+region+"_ecdf_files/*"
    instDirList = glob.glob(regionDir)
    
    fig = plt.figure(figsize=(20, 20))
    for instDir in instDirList:
        if path.isdir(instDir):
            _,inst = instDir.split("_files/")
            
            file = "./"+region+"_ecdf_files/"+inst+"/"+inst+"__causation.csv"
            print(file)
    
            df = pd.read_csv(file)
            df = df.sort_values('Diff')
            
            ecdf = ECDF(df['Diff'])
            y = ecdf(df['Diff'])
            
            plt.step(df['Diff'],y,label=inst)
            
    plt.title(region,fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend()
    title = "./causation_ecdf_graphs/"+region+"_causation_ecdf.png"
    plt.savefig(title)

def create_ecdf_inst(region,inst):
    
    regionDir = "./"+region+"_ecdf_files/*"
    instDirList = glob.glob(regionDir)
    
    fig = plt.figure(figsize=(20, 20))
            
    file = "./"+region+"_ecdf_files/"+inst+"/"+inst+"__causation.csv"
    print(file)

    df = pd.read_csv(file)
    df = df.sort_values('Diff')
    
    ecdf = ECDF(df['Diff'])
    y = ecdf(df['Diff'])
    
    plt.step(df['Diff'],y,label=inst)
            
    plt.title(region,fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend()
    plt.show()
    #title = "./causation_ecdf_graphs/"+region+"_causation_ecdf.png"
    #plt.savefig(title)            
            

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name')
    parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
  
    args = parser.parse_args()
    region = args.region
    inst = args.inst
    
    #create_ecdf(region)
    create_ecdf_inst(region,inst)
    print("Finish")