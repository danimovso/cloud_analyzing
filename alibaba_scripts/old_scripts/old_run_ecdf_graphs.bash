#!/bin/bash

python ./scripts/create_ecdf_graph.py --region ap-southeast-1 --inst ap_southeast_1b__ecs_i1_4xlarge &
python ./scripts/create_ecdf_graph.py --region cn-shenzhen --inst cn_shenzhen_a__ecs_mn4_2xlarge &
python ./scripts/create_ecdf_graph.py --region us-east-1 --inst us_east_1a__ecs_e3_large &
python ./scripts/create_ecdf_graph.py --region us-east-1 --inst us_east_1a__ecs_n1_tiny &
python ./scripts/create_ecdf_graph.py --region ap-southeast-1 --inst ap_southeast_1b__ecs_i1_c10d1_8xlarge &
python ./scripts/create_ecdf_graph.py --region ap-southeast-1 --inst ap_southeast_1b__ecs_xn4_small &
