#!/bin/bash

#ap-northeast-1 ap-south-1 ap-southeast-1 ap-southeast-2 ap-southeast-3 ap-southeast-5 cn-beijing cn-hangzhou cn-hongkong cn-huhehaote cn-qingdao cn-shanghai cn-shenzhen cn-zhangjiakou eu-central-1 eu-west-1 me-east-1 us-east-1 us-west-1
     
for region in ap-northeast-1 ap-south-1 ap-southeast-1 ap-southeast-2 ap-southeast-3 ap-southeast-5 cn-beijing cn-hangzhou cn-hongkong cn-huhehaote cn-qingdao cn-shanghai cn-shenzhen cn-zhangjiakou eu-central-1 eu-west-1 me-east-1 us-west-1
do
    echo "Working on $region..."
    python3 ./scripts/calculate_complicated_ecdf.py --region $region > "./${region}_ecdf_files/${region}.txt" 2> "./${region}_ecdf_files/${region}_errors.txt" &
done

echo "Finished sending the scripts for all regions..."


#python3 ./scripts/old_calculate_complicated_ecdf.py --region us-east-1 > "./cn-beijing_ecdf_files/us-east-1.txt" 2> "./cn-beijing_ecdf_files/us-east-1_errors.txt"