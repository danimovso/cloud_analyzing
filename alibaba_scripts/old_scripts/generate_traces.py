import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import random
import bisect
from numpy import linalg as LA

# plt.style.use('grayscale')
# plt.style.use('seaborn-colorblind')

tex_fonts = {
        # Use LaTeX to write all text
        # "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 12,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 10,
        "xtick.labelsize": 10,
        "ytick.labelsize": 10
    }

plt.rcParams.update(tex_fonts)

def set_size(width=441.01772, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


# ****************************************************** #
#   This function draws a matrix from a given df         #
# ****************************************************** #
def show_df(df, title, row, col, index):
    ax = fig.add_subplot(row, col, index)
    im = ax.imshow(np.around(df.values, 4), cmap='turbo')
    # # We want to show all ticks...
    # ax.set_xticks(np.arange(len(df.columns.tolist())))
    # ax.set_yticks(np.arange(len(df.columns.tolist())))
    # # ... and label them with the respective list entries
    # ax.set_xticklabels(df.columns)
    # ax.set_yticklabels(df.columns)
    # # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    ax.set_title(title)
    # im.set_clim(0, 1.0)
    plt.colorbar(im, shrink=0.5)


# ****************************************************** #
#   This function draws the 4 transition matrices        #
#   that are in mat                                      #
# ****************************************************** #
def show_transition_matrices(mat, t, price_list):
    df2 = pd.DataFrame(mat[0], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Increase"), 2, 2, 1)
    # show_df(df2, "Increase", 2, 2, 1)

    df2 = pd.DataFrame(mat[1], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Increase - stay"), 2, 2, 2)
    # show_df(df2, "Increase - stay", 2, 2, 2)

    df2 = pd.DataFrame(mat[2], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Decrease - stay"), 2, 2, 3)
    # show_df(df2, "Decrease - stay", 2, 2, 3)

    df2 = pd.DataFrame(mat[3], columns=price_list)
    df2 = df2.replace(0, np.nan)
    show_df(df2,(t+" Decrease"), 2, 2, 4)
    # show_df(df2, "Decrease", 2, 2, 4)


# ****************************************************** #
#   This function draws the 4 transition matrices        #
#   that are in mat                                      #
# ****************************************************** #
def show_state_transition_matrix_thesis(mat, axs):
    # ax = fig.add_subplot(2, 2, 1)
    im = axs[0,0].imshow(mat[0], cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
    axs[0,0].set_title("Increase")

    # ax = fig.add_subplot(2, 2, 2)
    im = axs[0,1].imshow(mat[1], cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
    axs[0,1].set_title("Increase - stay")

    # ax = fig.add_subplot(2, 2, 3)
    im = axs[1,0].imshow(mat[2], cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
    axs[1,0].set_title("Decrease - stay")

    # ax = fig.add_subplot(2, 2, 4)
    im = axs[1,1].imshow(mat[3], cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
    axs[1,1].set_title("Decrease")

    # im.set_clim(0, 1.0)
    fig.colorbar(im, ax=axs[:, 1], location='right', shrink=0.8)


def show_transition_matrix_thesis(mat):
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(mat, cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], norm=LogNorm())
    # im = ax.imshow(mat, cmap='binary', interpolation='none', extent=[0.01, 1.01, 1.01, 0.01], vmin = 0, vmax = 1)
    # im = ax.imshow(np.around(mat, 4), cmap='turbo')

    # im.set_clim(0, 0.005)
    fig.colorbar(im, location='right', shrink=0.8)


# ****************************************************** #
#   This function draws the trace defined by df          #
# ****************************************************** #
def show_trace(traces_dfs, title):
    for num, df in enumerate(traces_dfs, 1):
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        # df = df[(df.Timestamp >= pd.to_datetime('9/1/2019 00:00:00 AM')) & (df.Timestamp < pd.to_datetime('11/1/2020 00:00:00 AM'))]
        plt.plot(df.Timestamp, df.NormPrice, '.-', label=num)
    plt.xticks(rotation=45)
    plt.xlabel("Timestamp")
    plt.ylabel("Normalized spot price")
    plt.legend(loc='lower right')
    plt.title(title)


# ****************************************************** #
#   This function randomly chooses a value from a list   #
#   with given weights                                   #
# ****************************************************** #
def choice(prices, weights, flag=1):
    if flag:
        assert len(prices) == len(weights)
        cdf_vals = np.cumsum(weights)
        x = random.random()
        idx = bisect.bisect_left(cdf_vals, x)
        if idx == len(prices):
            idx -= 1
        return prices[idx]
    else:
        next_price = random.choices(prices, weights=weights, k=1)
        # print(next_price[0])
        return next_price[0]


# ****************************************************** #
#   This function normalizes the rows of the given       #
#   matrix. Each cell is normalized by the sum of its    #
#   row.                                                 #
# ****************************************************** #
def normalize_state_transition_mat_per_row(mat):
    all_sums = []
    for p in [0, 1, 2, 3]:
        sum_list = np.sum(mat[p], axis=1)
        all_sums.append(sum_list)
        for i in range(len(mat[p])):
            sum_row = sum_list[i]
            if sum_row != 0:
                for j in range(len(mat[p])):
                    mat[p][i][j] = mat[p][i][j] / sum_row

    return mat


def normalize_state_transition_mat(mat):
    sum_mat = np.sum(mat)
    for p in [0, 1, 2, 3]:
        for i in range(len(mat[p])):
            for j in range(len(mat[p])):
                mat[p][i][j] = mat[p][i][j] / sum_mat

    return mat


def normalize_transition_mat_per_row(mat):
    sum_list = np.sum(mat, axis=1)
    for i in range(len(mat)):
        sum_row = sum_list[i]
        if sum_row != 0:
            for j in range(len(mat)):
                mat[i][j] = mat[i][j] / sum_row

    return mat


def normalize_transition_mat(mat):
    sum_mat = np.sum(mat)
    for i in range(len(mat)):
        for j in range(len(mat)):
            mat[i][j] = mat[i][j] / sum_mat

    return mat


# ****************************************************** #
#   This function calculates the transition matrices     #
#   created by the given df                              #
# ****************************************************** #
def create_gen_trace_state_transition_mat(df, price_list):
    df = df.sort_values('Timestamp')
    df.NormPrice = np.around(df.NormPrice, 2)

    mat_size = len(price_list)
    mat = np.zeros((4, mat_size, mat_size))

    prev = df.NormPrice.iloc[0]
    curr = df.NormPrice.iloc[1]

    # 0 - increase, 1 - stay, 2 - decrease
    if curr > prev:
        state = 0
    elif curr < prev:
        state = 3
    else:
        state = 1

    length = df.Timestamp.count()
    for index in range(1, length-1):
        curr = df.NormPrice.iloc[index]

        # increase in price
        if curr > prev:
            row = price_list.index(prev)
            col = price_list.index(curr)
            mat[state][row][col] += 1
            state = 0
            # decrease in price
        elif curr < prev:
            row = price_list.index(prev)
            col = price_list.index(curr)
            mat[state][row][col] += 1
            state = 3
        # price stays the same
        else:
            if state == 0 or state == 1:
                row = price_list.index(prev)
                col = price_list.index(curr)
                mat[state][row][col] += 1
                state = 1
            elif state == 3 or state == 2:
                row = price_list.index(prev)
                col = price_list.index(curr)
                mat[state][row][col] += 1
                state = 2

        prev = curr

    return mat


def create_gen_trace_transition_mat(df, price_list):
    df = df.sort_values('Timestamp')
    df.NormPrice = np.around(df.NormPrice, 2)

    mat_size = len(price_list)
    mat = np.zeros((mat_size, mat_size))

    length = df.Timestamp.count()
    prev = df.NormPrice.iloc[0]
    for index in range(1, length):
        curr = df.NormPrice.iloc[index]

        row = price_list.index(prev)
        col = price_list.index(curr)
        mat[row][col] += 1

        prev = curr

    return mat


def calc_folded_pdfs(mat, price_list):
    size_mat = len(mat)
    data = []

    for row in range(0, size_mat):
        for col in range(0, size_mat):
            data.append(((col - row) / 100, mat[row][col], row))

    d = {x: [0, 0] for x, _, _ in data}
    for price, val, row in data:
        d[price][0] += val
        d[price][1] += 1

    new_d = {x: 0 for x in d.keys()}
    for price in d.keys():
        # print(d[price][0])
        if d[price][0] != 0:
            new_d[price] = (d[price][0] / d[price][1])
        else:
            new_d[price] = 0
            # print(new_d[price])

    # inc_data = new_d.items()
    data = list(map(tuple, new_d.items()))
    data = sorted(data, key=lambda x: x[0])

    return data


def calc_state_folded_pdfs(mat):
    size_mat = len(mat[0])
    inc_data = []
    dec_data = []
    inc_s_data = []
    dec_s_data = []

    for p in [0, 1, 2, 3]:
        for row in range(0, size_mat):
            for col in range(0, size_mat):
                if p == 0:
                    inc_data.append(((col - row) / 100, mat[p][row][col], row))
                elif p == 1:
                    inc_s_data.append(((col - row) / 100, mat[p][row][col], row))
                elif p == 2:
                    dec_s_data.append(((col - row) / 100, mat[p][row][col], row))
                else:
                    dec_data.append(((col - row) / 100, mat[p][row][col], row))

    d = {x: [0.0, 0.0] for x, _, _ in inc_data}
    for price, val, row in inc_data:
        d[price][0] += val
        d[price][1] += 1

    new_d = {x: 0.0 for x in d.keys()}
    for price in d.keys():
        # print(d[price][0])
        if d[price][0] != 0.0:
            new_d[price] = (d[price][0] / d[price][1])
            # print(new_d[price])

    # inc_data = new_d.items()
    inc_data = list(map(tuple, new_d.items()))
    inc_data = sorted(inc_data, key=lambda x: x[0])

    d = {x: [0, 0] for x, _, _ in inc_s_data}
    for price, val, row in inc_s_data:
        d[price][0] += val
        d[price][1] += 1

    new_d = {x: 0 for x in d.keys()}
    for price in d.keys():
        if d[price][0] == 0.0:
            new_d[price] = 0.0
        else:
            new_d[price] = (d[price][0] / d[price][1])

    inc_s_data = list(map(tuple, new_d.items()))
    inc_s_data = sorted(inc_s_data, key=lambda x: x[0])

    d = {x: [0, 0] for x, _, _ in dec_s_data}
    for price, val, row in dec_s_data:
        d[price][0] += val
        d[price][1] += 1

    new_d = {x: 0 for x in d.keys()}
    for price in d.keys():
        if d[price][0] == 0.0:
            new_d[price] = 0.0
        else:
            new_d[price] = (d[price][0] / d[price][1])

    dec_s_data = list(map(tuple, new_d.items()))
    dec_s_data = sorted(dec_s_data, key=lambda x: x[0])

    d = {x: [0, 0] for x, _, _ in dec_data}
    for price, val, row in dec_data:
        d[price][0] += val
        d[price][1] += 1

    new_d = {x: 0 for x in d.keys()}
    for price in d.keys():
        if d[price][0] == 0.0:
            new_d[price] = 0.0
        else:
            new_d[price] = (d[price][0] / d[price][1])

    dec_data = list(map(tuple, new_d.items()))
    dec_data = sorted(dec_data, key=lambda x: x[0])

    return inc_data, inc_s_data, dec_s_data, dec_data


def calc_folded_cdfs(data):
    ax = fig.add_subplot(1, 1, 1)

    prices, weights = zip(*data)
    cdf = np.cumsum(weights)

    # plotting PDF and CDF
    ax.plot(prices, weights, color="red", label="PDF")
    ax.plot(prices, cdf, label="CDF")
    ax.legend()


def calc_state_folded_cdfs(inc_data, inc_s_data, dec_s_data, dec_data):
    ax = fig.add_subplot(2, 2, 1)

    inc_prices, inc_weights = zip(*inc_data)
    cdf = np.cumsum(inc_weights)
    # print(cdf)

    # plotting PDF and CDF
    ax.plot(inc_prices, inc_weights, color="red", label="PDF")
    ax.plot(inc_prices, cdf, label="CDF")
    ax.legend()
    ax.set_title("Increase")

    ax = fig.add_subplot(2, 2, 2)

    inc_s_prices, inc_s_weights = zip(*inc_s_data)
    cdf = np.cumsum(inc_s_weights)

    # plotting PDF and CDF
    ax.plot(inc_s_prices, inc_s_weights, color="red", label="PDF")
    ax.plot(inc_s_prices, cdf, label="CDF")
    ax.legend()
    ax.set_title("Increase - Stay")

    ax = fig.add_subplot(2, 2, 3)

    dec_s_prices, dec_s_weights = zip(*dec_s_data)
    cdf = np.cumsum(dec_s_weights)

    # plotting PDF and CDF
    ax.plot(dec_s_prices, dec_s_weights, color="red", label="PDF")
    ax.plot(dec_s_prices, cdf, label="CDF")
    ax.legend()
    ax.set_title("Decrease - Stay")

    ax = fig.add_subplot(2, 2, 4)

    dec_prices, dec_weights = zip(*dec_data)
    cdf = np.cumsum(dec_weights)

    # plotting PDF and CDF
    ax.plot(dec_prices, dec_weights, color="red", label="PDF")
    ax.plot(dec_prices, cdf, label="CDF")
    ax.legend()
    ax.set_title("Decrease")


# ****************************************************** #
#   This function randomly chooses the starting point    #
#   of a generated trace                                 #
# ****************************************************** #
def calc_start_vals():
    start_prices, start_weights = zip(
        *[(1.0, 7478), (0.1, 1248), (0.18, 752), (0.46, 577), (0.26, 505), (0.09, 374), (0.32, 367), (0.84, 201),
          (0.11, 193), (0.58, 141), (0.12, 129), (0.19, 63), (0.2, 52), (0.17, 51), (0.85, 42), (0.98, 42), (0.16, 36),
          (0.27, 31), (0.13, 25), (0.5, 24), (0.07, 22), (0.33, 20), (0.8, 19), (0.3, 19), (0.28, 17), (0.6, 15),
          (0.56, 15), (0.47, 14), (0.36, 14), (0.21, 12), (0.22, 10), (0.25, 9), (0.24, 9), (0.86, 9), (0.68, 8),
          (0.59, 7), (0.29, 6), (0.83, 6), (0.49, 6), (0.08, 6), (0.52, 5), (0.38, 5), (0.65, 5), (0.44, 4), (0.35, 4),
          (0.42, 4), (0.94, 4), (0.87, 4), (0.76, 3), (0.61, 3), (0.34, 3), (0.71, 3), (0.41, 3), (0.72, 3), (0.99, 2),
          (0.74, 2), (0.64, 2), (0.88, 2), (0.48, 2), (0.37, 2), (0.54, 2), (0.57, 2), (0.97, 2), (0.92, 2), (0.96, 2),
          (0.73, 2), (0.9, 1), (0.62, 1), (0.7, 1), (0.82, 1), (0.55, 1), (0.15, 1), (0.78, 1), (0.79, 1), (0.77, 1),
          (0.63, 1), (0.23, 1), (0.95, 1), (0.05, 1), (0.14, 1), (0.67, 1), (0.4, 1)])
    start_weights = np.array(start_weights)
    weights_sum = start_weights.sum()
    start_weights = [x / weights_sum for x in start_weights]
    start_price = random.choices(start_prices, weights=start_weights, k=1)

    start_state = random.choice(['inc', 'inc_s', 'dec_s', 'dec'])
    return start_price[0], start_state


# ****************************************************** #
#   This function generates a fake trace and names it    #
#   using the given 'name'                               #
# ****************************************************** #
# def simulator_with_states(name, mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data, flag=1):
def simulator_with_states(name, mat, price_list, flag=1):
    min_band_list = np.array([0.1, 0.18, 0.24, 0.26, 0.32, 0.36, 0.42, 0.46, 0.58, 0.8])

    df_gen = pd.DataFrame({'Timestamp': pd.date_range(start=pd.to_datetime("1/1/2020 00:00:00 AM"),
                                                      end=pd.to_datetime("7/1/2021 00:00:00 AM"), freq='h')})
    # start_price, start_state = calc_start_vals()
    start_price = 0.18
    start_state = 'dec_s'
    normPrice = [start_price]
    state = start_state

    # # Set the value of the bottom of the band
    # if start_price in min_band_list:
    #     band_bottom = start_price
    # else:
    #     band_bottom = min_band_list[min_band_list <= start_price]
    #     if band_bottom.size != 0:
    #         band_bottom = band_bottom.max()
    #     else:
    #         band_bottom = 0.0
    #
    # print(start_price, band_bottom)

    size_trace = df_gen.Timestamp.count()
    for i in range(size_trace - 1):
        prev_price = normPrice[i]

        # decide if to update bottom value
        # if (prev_price in min_band_list) or (prev_price == band_bottom):
        #     if state == 'inc':
        #         weights = mat[0][price_list.index(prev_price)]
        #         prices = [x - prev_price for x in price_list]
        #     elif state == 'inc_s':
        #         weights = mat[1][price_list.index(prev_price)]
        #         prices = [x - prev_price for x in price_list]
        #     elif state == 'dec_s':
        #         weights = mat[2][price_list.index(prev_price)]
        #         prices = [x - prev_price for x in price_list]
        #     else:  # state == 'dec'
        #         weights = mat[3][price_list.index(prev_price)]
        #         prices = [x - prev_price for x in price_list]
        #
        #     # new_price = choice(price_list, weights, 1)
        #     num = choice(prices, weights, 0)
        #     new_price = prev_price + num
        #     while new_price > 1.0 or new_price <= 0.04:
        #         if new_price > 1.0:
        #             new_price = 1.0
        #         elif new_price <= 0.04:
        #             num = choice(prices, weights, 0)
        #             new_price = prev_price + num
        #     normPrice.append(round(new_price, 2))
        #
        #     if new_price > prev_price:
        #         band_bottom = prev_price
        # else:
        # calculate probability of dropping to the bottom of the band
        # if prev_price not in min_band_list:
        #     if state == 'inc':
        #         drop_chance = mat[0][int(prev_price * 100) - 1][int(band_bottom * 100) - 1]
        #     elif state == 'inc_s':
        #         drop_chance = mat[1][int(prev_price * 100) - 1][int(band_bottom * 100) - 1]
        #     elif state == 'dec_s':
        #         drop_chance = mat[2][int(prev_price * 100) - 1][int(band_bottom * 100) - 1]
        #     else:  # state == 'dec'
        #         drop_chance = mat[3][int(prev_price * 100) - 1][int(band_bottom * 100) - 1]
        # else:
        #     drop_chance = 0.0
        #
        # # If we do drop - drop, and don't sample from the cdf
        # if random.random() < drop_chance:
        #     normPrice.append(band_bottom)
        #     new_price = band_bottom
        # # If we don't drop - sample randomly from the cdf
        # else:
        # if flag == 1:
        #     if state == 'inc':
        #         prices, weights = zip(*inc_data)
        #     elif state == 'inc_s':
        #         prices, weights = zip(*inc_s_data)
        #     elif state == 'dec_s':
        #         prices, weights = zip(*dec_s_data)
        #     else:  # state == 'dec'
        #         prices, weights = zip(*dec_data)
        #
        #     num = choice(prices, weights)
        #     # print("num:", num)
        #     new_price = prev_price + num
        #     while new_price > 1.0 or new_price <= 0.04:
        #         if new_price > 1.0:
        #             new_price = 1.0
        #         elif new_price <= 0.04:
        #             num = choice(prices, weights)
        #             new_price = prev_price + num
        #     # print(num)
        #     normPrice.append(new_price)
        if flag == 0:
            if state == 'inc':
                weights = mat[0][price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]
            elif state == 'inc_s':
                weights = mat[1][price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]
            elif state == 'dec_s':
                weights = mat[2][price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]
            else:  # state == 'dec'
                weights = mat[3][price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]

            # new_price = choice(price_list, weights, 1)
            num = choice(prices, weights, 0)
            new_price = prev_price + num
            while new_price > 1.0 or new_price <= 0.04:
                if new_price > 1.0:
                    new_price = 1.0
                elif new_price <= 0.04:
                    num = choice(prices, weights, 0)
                    new_price = prev_price + num
            normPrice.append(round(new_price, 2))
        else:
            if state == 'inc':
                weights = mat[price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]
            elif state == 'inc_s':
                weights = mat[price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]
            elif state == 'dec_s':
                weights = mat[price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]
            else:  # state == 'dec'
                weights = mat[price_list.index(prev_price)]
                prices = [x - prev_price for x in price_list]

            # new_price = choice(price_list, weights, 1)
            num = choice(prices, weights, 0)
            new_price = prev_price + num
            while new_price > 1.0 or new_price <= 0.04:
                if new_price > 1.0:
                    new_price = 1.0
                elif new_price <= 0.04:
                    num = choice(prices, weights, 0)
                    new_price = prev_price + num
            normPrice.append(round(new_price, 2))

            # if (prev_price in min_band_list) and (prev_price != band_bottom) and (new_price > prev_price):
            #     band_bottom = prev_price
            # elif new_price < band_bottom:
            #     band_bottom = min_band_list[min_band_list <= new_price]
            #     if band_bottom.size != 0:
            #         band_bottom = band_bottom.max()
            #     else:
            #         band_bottom = 0.0

        # set next state
        if new_price > prev_price:
            state = 'inc'
        elif new_price == prev_price:
            if state == 'inc' or state == 'inc_s':
                state = 'inc_s'
            elif state == 'dec' or state == 'dec_s':
                state = 'dec_s'
        else:
            state = 'dec'

    df_gen['NormPrice'] = normPrice
    # name = "./alibaba_work/generated_traces_epoch_2/trace_" + name + ".csv"
    # print(name)
    # df_gen.to_csv(name, index=False)
    return df_gen


def simulator_without_states(name, mat, price_list, data, flag=1):
    min_band_list = np.array([0.1, 0.18, 0.24, 0.26, 0.32, 0.36, 0.42, 0.46, 0.58, 0.8])

    df_gen = pd.DataFrame({'Timestamp': pd.date_range(start=pd.to_datetime("1/1/2020 00:00:00 AM"),
                                                      end=pd.to_datetime("7/1/2021 00:00:00 AM"), freq='h')})
    start_price, start_state = calc_start_vals()
    start_price = 0.18
    normPrice = [start_price]

    # Set the value of the bottom of the band
    # if start_price in min_band_list:
    #     band_bottom = start_price
    # else:
    #     band_bottom = min_band_list[min_band_list <= start_price]
    #     if band_bottom.size != 0:
    #         band_bottom = band_bottom.max()
    #     else:
    #         band_bottom = 0.0
    #
    # print(start_price, band_bottom)

    size_trace = df_gen.Timestamp.count()
    for i in range(size_trace - 1):
        prev_price = normPrice[i]

        # # decide if to update bottom value
        # if (prev_price in min_band_list) or (prev_price == band_bottom):
        #     weights = mat[price_list.index(prev_price)]
        #     prices = [x - prev_price for x in price_list]
        #
        #     # new_price = choice(price_list, weights, 1)
        #     num = choice(prices, weights, 0)
        #     new_price = prev_price + num
        #     while new_price > 1.0 or new_price <= 0.04:
        #         if new_price > 1.0:
        #             new_price = 1.0
        #         elif new_price <= 0.04:
        #             num = choice(prices, weights, 0)
        #             new_price = prev_price + num
        #     normPrice.append(round(new_price, 2))
        #
        #     if new_price > prev_price:
        #         band_bottom = prev_price
        # else:
        # calculate probability of dropping to the bottom of the band
        # if prev_price not in min_band_list:
        #     drop_chance = mat[int(prev_price * 100)-1][int(band_bottom * 100)-1]
        # else:
        #     drop_chance = 0.0
        #
        # print("drop chance:", drop_chance)
        # # If we do drop - drop, and don't sample from the cdf
        # if random.random() < drop_chance:
        #     normPrice.append(band_bottom)
        # # If we don't drop - sample randomly from the cdf
        # else:
        if flag:
            prices, weights = zip(*data)

            num = choice(prices, weights)
            # print("num:", num)
            new_price = prev_price + num
            while new_price > 1.0 or new_price <= 0.04:
                if new_price > 1.0:
                    new_price = 1.0
                elif new_price <= 0.04:
                    num = choice(prices, weights)
                    new_price = prev_price + num
            # print(num)
            normPrice.append(new_price)
        else:
            weights = mat[price_list.index(prev_price)]
            prices = [x - prev_price for x in price_list]

            # new_price = choice(price_list, weights, 1)
            num = choice(prices, weights, 0)
            new_price = prev_price + num
            while new_price > 1.0 or new_price <= 0.04:
                if new_price > 1.0:
                    new_price = 1.0
                elif new_price <= 0.04:
                    num = choice(prices, weights, 0)
                    new_price = prev_price + num
            normPrice.append(round(new_price, 2))

            # print(prev_price, new_price)
            # if (prev_price in min_band_list) and (prev_price != band_bottom) and (new_price > prev_price):
            #     band_bottom = prev_price
            # elif new_price < band_bottom:
            #     band_bottom = min_band_list[min_band_list <= new_price]
            #     if band_bottom.size != 0:
            #         band_bottom = band_bottom.max()
            #     else:
            #         band_bottom = 0.0
            #
            # print(band_bottom)

    df_gen['NormPrice'] = normPrice
    # name = "./alibaba_work/generated_traces_epoch_2/trace_" + name + ".csv"
    # print(name)
    # df_gen.to_csv(name, index=False)
    return df_gen


price_list = [round(num, 2) for num in np.arange(0.01, 1.01, 0.01)]

fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
# # inst = "../../Alibaba_log_files/interesting_traces/cn_zhangjiakou_c__ecs_hfc6_8xlarge__optimized__vpc.csv"
# inst = "../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv"
inst = "F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc.csv"

df_original = pd.read_csv(inst)
show_trace([df_original], inst)


# ################# START WITHOUT STATES ################# #
# file = "../../Alibaba_log_files/transition_matrices/transition_mat_all_instances_epoch_2.npy"
# file = "../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge_transition_mat_epoch_2.npy"
# with open(file, 'rb') as f:
#     mat = np.load(f)

file = "F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc.csv"
df = pd.read_csv(file)
df['Timestamp'] = pd.to_datetime(df['Timestamp'])
df = df.sort_values('Timestamp')
df.reset_index(drop=True, inplace=True)

mat = create_gen_trace_transition_mat(df, price_list)

# Draw the transition matrix
fig = plt.figure(constrained_layout=True, figsize=set_size(fraction=1))
plt.suptitle("Original trace transition mat")
mat = normalize_transition_mat_per_row(mat)
show_transition_matrix_thesis(mat)

# fig = plt.figure(constrained_layout=True, figsize=set_size(fraction=1))
# mat = normalize_transition_mat(mat)
# print(mat.max())
#
# values = []
# for row in range(len(mat)):
#     for col in range(len(mat)):
#         values.append((row, col, mat[row][col]))
#
# values = sorted(values, key=lambda x: x[2])
# print(values)
# show_transition_matrix_thesis(mat)

# fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
# data = calc_folded_pdfs(mat, price_list)
# calc_folded_cdfs(data)
# #
# # Generate traces
# df_gen1 = simulator_without_states("1", mat, price_list, data)
# df_gen2 = simulator_without_states("2", mat, price_list, data)
# df_gen3 = simulator_without_states("3", mat, price_list, data)
#
# # Draw generated traces
# fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
# # show_trace([df_gen1], "Generates traces - without states")
# show_trace([df_gen1, df_gen2, df_gen3], "Generates traces - without states - with CDF")
#
# print("Without CDF")

# Draw generated traces
fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
for i in range(1, 2):
    # df_gen = simulator_without_states(i, mat, price_list, data, 0)
    df_gen = simulator_with_states(i, mat, price_list, flag=1)
    df_gen['Timestamp'] = pd.to_datetime(df_gen['Timestamp'])
    df_gen = df_gen.sort_values('Timestamp')

    plt.plot(df_gen.Timestamp, df_gen.NormPrice, '.--', label=i)

plt.xticks(rotation=45)
plt.xlabel("Timestamp")
plt.ylabel("Normalized spot price")
plt.legend(loc='lower right')
plt.title("Generates traces - without states")

fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
plt.suptitle("Generated trace transition mat")
gen_mat = create_gen_trace_transition_mat(df_gen, price_list)
gen_mat = normalize_transition_mat_per_row(gen_mat)
show_transition_matrix_thesis(gen_mat)

print("dist without state:", np.linalg.norm(gen_mat-mat))
# df_gen1 = simulator_without_states("1", mat, price_list, data, 0)
# df_gen2 = simulator_without_states("2", mat, price_list, data, 0)
# df_gen3 = simulator_without_states("3", mat, price_list, data, 0)
#
# # show_trace([df_gen1], "Generates traces - without states")
# show_trace([df_gen1, df_gen2, df_gen3], "Generates traces - without states - without CDF")

# # fig = plt.figure()
# # cal_generated_trace_transition_mat(df_gen, price_list)
#
# # ################# END WITHOUT STATES ################# #
# print("=====================================================================")
# # ################# START WITH STATES ################# #
# file = "../../Alibaba_log_files/transition_matrices/state_transition_mat_all_instances_epoch_2.npy"
# file = "../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge_state_transition_mat_epoch_2.npy"
# with open(file, 'rb') as f:
#     mat = np.load(f)

file = "F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc.csv"
df = pd.read_csv(file)
df['Timestamp'] = pd.to_datetime(df['Timestamp'])
df = df.sort_values('Timestamp')
df.reset_index(drop=True, inplace=True)

mat = create_gen_trace_state_transition_mat(df, price_list)


# Draw transition matrices
fig, axs = plt.subplots(2, 2, constrained_layout=True, figsize=set_size(fraction=2))
plt.suptitle("Original trace state transition mat")
mat = normalize_state_transition_mat_per_row(mat)
show_state_transition_matrix_thesis(mat, axs)
#
# # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
# inc_data, inc_s_data, dec_s_data, dec_data = calc_state_folded_pdfs(mat)
# # calc_state_folded_cdfs(inc_data, inc_s_data, dec_s_data, dec_data)
# # #
# # # # Generate traces
# # # df_gen1 = simulator_with_states("1", mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data)
# # # df_gen2 = simulator_with_states("2", mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data)
# # # df_gen3 = simulator_with_states("3", mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data)
# # #
# # # # Draw generated traces
# # # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
# # # # show_trace([df_gen1], "Generates traces - with states")
# # # show_trace([df_gen1, df_gen2, df_gen3], "Generates traces - with states - with CDFs")
# # #
# # # Generate traces
fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
for i in range(1, 2):
    df_gen = simulator_with_states(i, mat, price_list, flag=0)
    df_gen['Timestamp'] = pd.to_datetime(df_gen['Timestamp'])
    df_gen = df_gen.sort_values('Timestamp')

    plt.plot(df_gen.Timestamp, df_gen.NormPrice, '.-', label=i)

plt.xticks(rotation=45)
plt.xlabel("Timestamp")
plt.ylabel("Normalized spot price")
plt.legend(loc='lower right')
plt.title("Generates traces - with states")

fig, axs = plt.subplots(2, 2, constrained_layout=True, figsize=set_size(fraction=2))
plt.suptitle("Generated trace state transition mat")
gen_mat = create_gen_trace_state_transition_mat(df_gen, price_list)
gen_mat = normalize_state_transition_mat_per_row(gen_mat)
show_state_transition_matrix_thesis(gen_mat, axs)

print("dist with state:", np.linalg.norm(gen_mat-mat))
#
# # df_gen1 = simulator_with_states("1", mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data, 0)
# # df_gen2 = simulator_with_states("2", mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data, 0)
# # df_gen3 = simulator_with_states("3", mat, price_list, inc_data, inc_s_data, dec_s_data, dec_data, 0)
#
# # Draw generated traces
# # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
# # # show_trace([df_gen1], "Generates traces - with states")
# # show_trace([df_gen1, df_gen2, df_gen3, df_gen4, df_gen5, df_gen6, df_gen7, df_gen8, df_gen9, df_gen10], "Generates traces - with states")

# ################# END WITH STATES ################# #

plt.show()


