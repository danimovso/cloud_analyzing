import numpy as np
import pandas as pd
import seaborn as sns
#%matplotlib
import matplotlib.pyplot as plt
#import statsmodels.api as sm
import glob
import argparse

def create_ecdf(region,inst):
    fileName = "./"+region+"_traces/"+inst+"__ecdf.csv"
    print(fileName)
    df = pd.read_csv(fileName)
    #df = pd.read_csv("./ap-southeast-1_traces/ap_southeast_1b__ecs_i1_4xlarge__ecdf.csv")   
    plt.step(df.DiffTimeBucket,df.ecdfVal, 'C0o', label="Original")
    plt.legend()

    #dirName = "./ap-southeast-1_ecdf_files/ap_southeast_1b__ecs_i1_4xlarge/*_VS_*.csv"
    dirName = "./"+region+"_ecdf_files/"+inst+"/*_VS_*.csv"
    print(dirName)
        
    files = glob.glob(dirName)

    for file in files:
        print(file)
        df2 = pd.read_csv(file)
        _,name = file.split('VS_')
        name,_ = name.split('__ecdf')
        plt.step(df2.DiffTimeBucket,df2.ecdfVal, label=name)

    plt.xlim(-20,60)
    t = "ECDF causation for: "+inst
    plt.title(t)
    #plt.legend()
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
  
    args = parser.parse_args()
    region = args.region
    inst = args.inst
    
    create_ecdf(region,inst)
    print("Finish")