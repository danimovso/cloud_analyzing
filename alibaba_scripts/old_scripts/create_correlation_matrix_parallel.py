import glob
import argparse
# import matplotlib.pyplot as plt
import pandas as pd, numpy as np
# from contextlib import closing


def calc_relevant_inst_list():
    print("creating the list of instance names...")

    dirName = "./alibaba_work/*_full_traces/*vpc.csv"
    instList = glob.glob(dirName)

    f = open("./alibaba_work/feature_files/inst_list_for_calculating_corr_2020.txt", "w")

    names = []
    for inst in instList:
        # _, _, _, name1 = f.split('/')
        # name1, _ = name1.split('__vpc')
        df1 = pd.read_csv(inst)
        df1['Timestamp'] = pd.to_datetime(df1['Timestamp'])
        df1 = df1.sort_values('Timestamp')
        df1 = df1[df1.Timestamp >= "1/1/2020 00:00:00 AM"]
        df1.index = df1['Timestamp']
        if (df1.NormPrice.nunique() == 1) or (df1.Timestamp.count() == 0):
            continue
        else:
            names.append(inst)

    f.write(str(names))
    f.close()

    print("finished creating the list of instance names.")
    print("list of names length:" + str(len(names)))


def calc_corr_per_region(region, instList):

    dirName = "./alibaba_work/" + region + "_full_traces/*vpc.csv"
    files = glob.glob(dirName)

    f1 = open("./alibaba_work/feature_files/all_instances_corr_matrix_2020.csv", "w")
    f1.write("," + str(instList) + "\n")

    for file1 in files:
        print("calculating correlations for " + file1 + " ...")
        # _, _, _, name1 = file1.split('/')
        # name1, _ = name1.split('__vpc')
        df1 = pd.read_csv(file1)
        df1['Timestamp'] = pd.to_datetime(df1['Timestamp'])
        df1 = df1.sort_values('Timestamp')
        df1 = df1[df1.Timestamp >= "1/1/2020 00:00:00 AM"]
        df1 = df1[['Timestamp', 'NormPrice']].copy()
        df1['Timestamp'] = pd.to_datetime(df1['Timestamp'])
        if (df1.NormPrice.nunique() == 1) or (df1.Timestamp.count() == 0):
            continue
        df1 = df1.sort_values('Timestamp')

        row = []
        for file2 in instList:
            print("second", file2)
            # _, _, _, name2 = file2.split('/')
            # name2, _ = name2.split('__vpc')
            df2 = pd.read_csv(file2)
            df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
            df2 = df2.sort_values('Timestamp')
            df2 = df2[df2.Timestamp >= "1/1/2020 00:00:00 AM"]
            df2 = df2[['Timestamp', 'NormPrice']].copy()
            df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
            if (df2.NormPrice.nunique() == 1) or (df2.Timestamp.count() == 0):
                continue
            df2 = df2.sort_values('Timestamp')

            result = pd.merge(df1, df2, how='outer', on='Timestamp', suffixes=('_1', '_2'))
            corr_num = result['NormPrice_1'].corr(result['NormPrice_2'])

            row.append(float(corr_num))

        # write to the corr matrix file
        print(str(row))
        f1.write(file1 + "," + str(row) + "\n")

    f1.close()
    print("Finished creating correlation mat.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the function')
    parser.add_argument("--function", help='The function we want to run')
    parser.add_argument("--instList", help='The list of relevant instances we want to use')

    args = parser.parse_args()

    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'

    if args.instList:
        instList_file = args.instList
    else:
        instList_file = "./alibaba_work/feature_files/inst_list_for_calculating_corr_2020.txt"

    if args.function:
        func = args.function
        if func == 'calc_relevant_inst_list':
            print("Running calc_relevant_inst_list...")
            calc_relevant_inst_list()
            print("Finished running calc_relevant_inst_list")

        elif func == 'calc_corr_per_region':
            print("Running calc_corr_per_region...")

            my_file = open(instList_file, "r")
            content = my_file.read()
            instList = content.split(",")
            my_file.close()
            print(instList)

            # calc_corr_per_region(region, instList)
            # print("Finished running calc_corr_per_region")
