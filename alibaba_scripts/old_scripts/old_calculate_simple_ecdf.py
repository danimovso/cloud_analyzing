#########################################################
## Create ECDF image of all the traces in the region   ##
#########################################################
import pandas as pd
import glob
#import statsmodels.api as sm
import numpy as np
from statsmodels.distributions.empirical_distribution import ECDF

regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5',
           'cn-beijing','cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen',
           'cn-zhangjiakou','eu-central-1','eu-west-1','me-east-1','us-east-1','us-west-1']

for region in regions:
    dirName = region + "_traces/*__clean.csv"
    print(dirName)
    
    files = glob.glob(dirName)

    for file in files:
        df = pd.read_csv(file)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df['DiffTime'] = df['TimeInSeconds'].diff()
        df['DiffTime'] = df['DiffTime']/3600
        df = df.sort_values('DiffTime')
        
        ecdf = ECDF(df['DiffTime'])
        y = ecdf(df['DiffTime'])
        
        name,csv = file.split('__optimized__vpc__clean.csv')
        name = name + "__ecdf.csv"
        print(name)
        f1= open(name,"w")
        f1.write("DiffTimeBucket,ecdfVal\n")
        for diffTime, ecdfVal in zip(df['DiffTime'], y):
            tmpstr = str(diffTime)+","+str(ecdfVal)+"\n"
            f1.write(tmpstr)
        f1.close()

print("Finished!")
