import glob
import argparse
# import matplotlib.pyplot as plt
import pandas as pd, numpy as np
# from contextlib import closing

f1= open("./alibaba_work/feature_files/first_and_last_activity_non_static_insts.csv","w")
f1.write("inst, start_timestamp, first_activity, last_activity, last_timestamp\n")

f2= open("./alibaba_work/feature_files/first_and_last_activity_static_insts.csv\n","w")
f2.write("inst,start_timestamp, last_timestamp")

dirName = "./alibaba_work/*full_traces/*clean.csv"
# dirName = "./alibaba_work/cn-beijing_2020_traces/*clean.csv"
instList = glob.glob(dirName)

for inst in instList:
    df = pd.read_csv(inst)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    df = df.sort_values('Timestamp')
    if df.Timestamp.count() > 2:
        f1.write(str(inst) + "," + str(df.Timestamp.iloc[0]) + "," + str(df.Timestamp.iloc[2]) + "," + str(df.Timestamp.iloc[-2]) + "," + str(df.Timestamp.iloc[-1]) + "\n")
        # fRow = df.Timestamp.iloc[1]
        # lRow = df.iloc[-2]
        # print("first change: ")
        # print(fRow.Timestamp)
        # print("last row: ")
        # print(lRow.Timestamp)

    else:
        f2.write(str(inst) + "," + str(df.Timestamp.iloc[0]) + "," + str(df.Timestamp.iloc[-1]) + "\n")

f1.close()
f2.close()

