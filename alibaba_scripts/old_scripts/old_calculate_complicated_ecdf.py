import glob
import os
import os.path
from os import path
import pandas as pd, numpy as np, seaborn as sns, scipy
from contextlib import closing
import statsmodels.api as sm
import argparse

# -------------------- all regions list: --------------------
# ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5',
#  'cn-beijing','cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen',
#  'cn-zhangjiakou','eu-central-1','eu-west-1','me-east-1','us-east-1','us-west-1']

######################################
## Create dir files for each region ##
######################################
def createRegionDirs(region):
    dirName = region + "_ecdf_files"
    if not os.path.exists(dirName):
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ")
    else:    
        print("Directory " , dirName ,  " already exists")
     


def calc_ecdf_diff_time(region,regionInsts,allInsts):
    dirName = region + "_ecdf_files"
    for inst1 in regionInsts:
        _,_,name1 = inst1.split('/')
        name1,_ = name1.split('__optimized')
        print("Calculating the new ECDFs for: ",name1,"...")
        
        instDir = "./" + dirName + "/" + name1
        if not os.path.exists(instDir):
            os.mkdir(instDir)
            print("Directory " , instDir ,  " Created ")
        else:    
            print("Directory " , instDir ,  " already exists")
        
        df1 = pd.read_csv(inst1)
        #df1 = df1[df1.columns.drop(list(df1.filter(regex='Unnamed:')))]
        df1.sort_values('TimeInSeconds',inplace=True)
        df1.drop_duplicates(subset="TimeInSeconds", keep = 'first', inplace = True)
        
        for inst2 in allInsts:
            if inst1 != inst2:
                tlist1 = df1.TimeInSeconds.tolist()
                len1 = len(tlist1)
                
                _,_,name2 = inst2.split('/')
                name2,_ = name2.split('__optimized')
                
                newName = instDir+"/"+name1+"_VS_"+name2+"__ecdf.csv"
                
                # Checks if for some reason the file that was created is empty
                flag = 0
                if os.path.exists(newName):
                    df_check = pd.read_csv(newName)
                    if df_check.empty:
                        flag = 1
                        print("$$$$$$$$$$$$$$$$$$$$$$$$$$ The file: ", newName, " was empty!$$$$$$$$$$$$$$$$$$$$$$$$$$")
                    else:
                        flag = 2
                        
                if (not os.path.exists(newName)) or (flag != 0):
                    print("Calculating ecdf diff for: ",inst1,inst2)
                    
                    df2 = pd.read_csv(inst2)
                    #df2 = df2[df2.columns.drop(list(df2.filter(regex='Unnamed:')))]
                    df2.sort_values('TimeInSeconds',inplace=True)
                    df2.drop_duplicates(subset="TimeInSeconds", keep = 'first', inplace = True)
                    
                    tlist2 = df2.TimeInSeconds.tolist()
                    len2 = len(tlist2)
                    
                    diffTime = []
                    
                    # Remove the first Timestamp from each trace (We don't want to calculate it)
                    tlist1.pop(0)
                    tlist2.pop(0)
                    
                    # Trace A starts before Trace B
                    val1 = tlist1.pop(0)
                    while tlist1 and tlist2 and val1 < tlist2[0]:
                        diffTime.append(1e77)
                        val1 = tlist1.pop(0)
                    
                    # The "middle" section of the traces
                    val2 = tlist2.pop(0)
                    while tlist1 and tlist2:
                        if val1 >= val2 and val1 < tlist2[0]:
                            diffTime.append((val1-val2))
                            val1 = tlist1.pop(0)
                        elif val1 > val2 and val1 >= tlist2[0]:
                            diffTime.append((val1-tlist2[0]))
                            val1 = tlist1.pop(0)
                            val2 = tlist2.pop(0)
                            if not tlist2:
                                diffTime.append((val1-val2))
                    
                    
                    # Trace A is longer than Trace B
                    if tlist1 and not tlist2:
                        for val1 in tlist1:
                            diffTime.append((val1-val2))
                            
                    diffTime.sort()
                    diffTime = [x/3600 for x in diffTime]
                    
                    ecdf = sm.distributions.ECDF(diffTime)
                    y = ecdf(diffTime)
                    
                    #newName = instDir+"/"+name1+"_VS_"+name2+"__ecdf.csv"
                    f1 = open(newName,"w")
                    f1.write("DiffTimeBucket,ecdfVal\n")
                    for diffVal, ecdfVal in zip(diffTime, y):
                        tmpstr = str(diffVal)+","+str(ecdfVal)+"\n"
                        f1.write(tmpstr)
                    f1.close()
                    print("Finished creating file: ",newName)
                else:    
                    print("************************** ", newName," already exists! **************************")
            else:
                print("########################## The instances are equal ############################")

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
  
    args = parser.parse_args()
    region = args.region

    # Create the list of all the non-const traces in the given region.
    dirName = "./"+region+"_traces/*vpc__clean.csv"
    list = glob.glob(dirName)
    
    regionInsts = []
    for inst in list:
        if (len(open(inst).readlines(  )) > 3):
            regionInsts.append(inst)
    
    # Create the list of all non-const traces there are
    dirName = "./*_traces/*vpc__clean.csv"
    list2 = glob.glob(dirName)
    
    allInsts = []
    for inst in list2:
        if (len(open(inst).readlines(  )) > 3):
            allInsts.append(inst)
    
    #print("Create a new dir for the given region...")
    #createRegionDirs(region)
    
    print("Start calculating the new ecdf files...")
    calc_ecdf_diff_time(region,regionInsts,allInsts)
    print("Finish")
            
    
