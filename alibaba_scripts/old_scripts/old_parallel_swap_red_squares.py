import pandas as pd
import numpy as np
#%matplotlib
import matplotlib.pyplot as plt
import itertools
import multiprocessing
import os 
from operator import itemgetter

# file = "./alibaba_work/feature_files/test_swap_red_squares.csv"
file = "./alibaba_work/feature_files/small_partial_august_2020_corr.csv"
# file = "./alibaba_work/feature_files/partial_august_2020_corr_2_with_dendrogram_labels_threshold_02_ordered.csv"

def calc_daig(mat):
    return np.trace(mat, offset=1)    
    
def calc_daig_manualy(mat,col_order):
    s = 0
    for i in (range(len(col_order)-1)):
        s += mat[col_order[i]][col_order[i+1]]
    return s

#print(os.cpu_count())

df = pd.read_csv(file)
df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
df = df.dropna()
df.set_index('inst',inplace=True)
mat = df.values

cols = list(np.arange(0,len(mat)-1))
print(cols)
print("Working on file: ", file)
print("Start the parallel part")
print("================================================================================")


def make_permutaions(head):
    title = "./alibaba_work/permutaion_files/permutaion_strat_"+str(head)+".txt"
    f = open(title, "w")
    remaining_cols = cols 
    remaining_cols.remove(head)
    max_daig_val = 0
    max_daig_order = []
    all_pers = itertools.permutations(remaining_cols)
    for p in all_pers:
        temp = [head] + list(p)
        temp_daig_val = calc_daig_manualy(mat,temp)
        f.write(str(temp_daig_val)+"\n")
        if temp_daig_val > max_daig_val:
            max_daig_val = temp_daig_val
            max_daig_order = temp
            f.write("*************" + str(max_daig_val) + ": " + str(max_daig_order) + "\n")
            #print(max_daig_val)
    f.write("==============================================================================\n")
    f.write("MAX DAIG VAL: "+str(max_daig_val)+"\n")
    f.write("MAX ORDER: \n")
    f.write(str(max_daig_order))
    f.close()
    return (temp_daig_val,max_daig_order)


def main():
  
    #with multiprocessing.Pool(processes=multiprocessing.cpu_count() * 2, maxtasksperchild=2) as pool:
    with multiprocessing.Pool() as pool:
        all_pers = pool.map(make_permutaions,cols)
    
    print("Ended parallel part.")
    print("================================================================================")
    #print(all_pers)
    print("================================================================================")
    print(max(all_pers))



if __name__ == '__main__':
    main()