import pandas as pd
import numpy as np
#%matplotlib
import matplotlib.pyplot as plt
import itertools
# from scipy.interpolate import interp1d

# file = "../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_corr_with_dendrogram_labels_threshold_02_F10_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/partial_august_2020_corr_2_with_dendrogram_labels_threshold_02_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/test_swap_red_squares.csv"
file = "../../../Alibaba_log_files/Correlation_tables/small_partial_august_2020_corr.csv"


################################################
# Plot the matrix heatmap.                     #
# If you want to add the values to each cell   #
# set val_flag == 1.                           #
################################################
def show(mat, title, num, index, val_flag=0):
    mat = np.around(mat,1)
    #fig, ax = plt.subplots()
    ax = fig.add_subplot(1, num,index)
    im = ax.imshow(mat, cmap='jet')
    if val_flag:
        for i in range(len(mat)):
           for j in range(len(mat)):
               text = ax.text(j, i, mat[i, j],
                              ha="center", va="center", color="w")
    ax.set_title(title)


################################################
# Plot the heatmap directly from the the       #
# dataframe and add the names of the insts     #
# as the axes of the image.                    #
################################################
def show_df(df,title,num,index):
    ax = fig.add_subplot(1, num, index)
    ax.imshow(df.values, cmap='jet')
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(df.columns.tolist())))
    ax.set_yticks(np.arange(len(df.index.values)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(df.columns)
    ax.set_yticklabels(df.index.values)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.set_title(title)


################################################
# Color each square in the matrix with a       #
# different color.                             #
################################################
def show_squares_by_color(df,squares,title,num,index):
    mat = np.zeros_like(df.values)
    for i, s in enumerate(squares):
        # print(i, s)
        for row in range(s[0], s[-1] + 1):
            for col in range(s[0], s[-1] + 1):
                mat[row][col] = i + 100

    ax = fig.add_subplot(1, num, index)
    ax.imshow(mat, cmap='jet')
    # # We want to show all ticks...
    # ax.set_xticks(np.arange(len(df.columns.tolist())))
    # ax.set_yticks(np.arange(len(df.index.values)))
    # # ... and label them with the respective list entries
    # ax.set_xticklabels(df.columns)
    # ax.set_yticklabels(df.index.values)
    # # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
    #          rotation_mode="anchor")
    ax.set_title(title)


def get_insts_in_squares(df, squares, file):
    f = open(file, "w")
    for index, s in enumerate(squares):
        f.write("Square number: " + str(index) + " ,Len square: " + str(len(s)) + " ,Row numbers: [" + str(
            s[0]) + "," + str(s[-1]) + "]\n")
        inst_list = []
        for i in s:
            name = df.columns.tolist()[i]
            reg, _, _ = name.split("__")
            reg = reg.replace('_', '-')
            reg = reg[:-1]
            if reg[-1] == '-':
                reg = reg[:-1]
            inst = "./alibaba_work/" + reg + "_2020_traces/" + name + "__vpc__clean.csv"
            inst_list.append(inst)
        f.write(str(inst_list) + "\n")
        # rows = [list(df.index.values)[i] for i in s]
        # print(rows)
        f.write("==================================================\n")
        # f.write(rows)

    f.close()


def calc_squares(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    for i in range(1, size-1):
        if (abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] > min_red):
            #       if (abs(mat[i][i+1]) >min_red and mat[i][i+1] > min_red):
            temp.append(i)  # continue the square
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)  # why does this repeat in both cases?
                temp = [i]  # start a new square

    if temp:  # i think this case can happen regardless
        temp.append(size - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([size - 1])

    # print(squares)
    return squares


def calc_daig(mat):
    return np.trace(mat, offset=1)


def get_corners(squares):
    corners = []
    for s in squares:
        corners.append((s[0],s[-1]))
    # print(corners)
    return corners


def goal(c1, c2, mat, contend):
    c1_val = mat[c1[1]][c2[0]]
    max_val = c1_val
    if (contend == 0):
        max_val = c1_val
        return max_val
    if (contend == 1):
        max_val = c1_val * (c1[1] - c1[0] + 1)  # weigh in the length of the next square
        return max_val

    if (contend == 2):
        max_val = (c1_val + mat[c1[1]][c2[1]]) * (c2[1] - c2[0] + 1)
    if (contend == 3):
        max_val = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1) * (
                    c1[1] - c1[0] + 1)
    if (contend == 4):
        a = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1)
        max_val = a * (c2[0] - c1[1])
    if (contend == 5):
        max_val = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * ((c2[1] - c2[0] + 1))
        # the size of the original square (1) is constant, no need to consider it

    if (contend == 6):
        a = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1)
        max_val = a * a * (c2[0] - c1[1])
    if (contend == 7):
        a = (c1_val + mat[c1[1]][c2[1]] + mat[c1[0]][c2[1]] + mat[c1[0]][c2[0]]) * (c2[1] - c2[0] + 1)
        max_val = a * a

    if (contend == 8):
        mid1 = round((c1[1] + c1[0]) / 2)
        mid2 = round((c2[1] + c2[0]) / 2)
        max_val = mat[mid1][mid2] * ((c2[1] - c2[0] + 1))

    return max_val


def find_and_swap_squares_new(squares, mat, contend, threshold):
    size = len(mat)
    corners = get_corners(squares)
    min_red = min(0.9, 1.0 - threshold)
    i1 = 0
    while i1 < len(corners) - 1:
        #print(i1,squares)
        c1 = corners[i1]
        c2 = corners[i1 + 1]
        max_val = goal(c1, c2, mat, contend)
        AB = mat[c1[1]][c1[1] + 1]
        max_index = i1

        for i2, c2 in enumerate(corners[i1 + 1:], start=(i1 + 1)):
            # comments are for checking C  - to be moved after A
            AC = mat[c1[1]][c2[0]]
            BC = mat[c2[0] - 1][c2[0]]
            CB = mat[c2[1]][c1[1] + 1]

            # אם מעבירים רבוע ה לאחרי רבוע א צריך להוסיף אה הב דו ולהחסיר דה הו אב. ואז לבדוק האם זה חיובי
            criterion1 = AC + CB - (BC + AB)

            # C is not the last square
            if c2[1] < (size - 2):
                CD = mat[c2[1]][c2[1] + 1]
                BD = mat[c2[0] - 1][c2[1] + 1]
                criterion1 += BD - CD

            contender = goal(c1, c2, mat, contend)
            if (max_val < contender and (criterion1 > 0) and (abs(mat[c1[1]-1][c1[1]] - AC) < threshold)):
                max_val = contender
                max_index = i2

        if max_index != i1:
            elem = squares.pop(max_index)
            squares.insert(i1+1, elem)
            elem2 = corners.pop(max_index)
            corners.insert(i1+1, elem2)
        else:
            elem = squares.pop(max_index)
            squares.insert(i1 , elem)
            elem2 = corners.pop(max_index)
            corners.insert(i1 , elem2)
        i1 = max_index + 1
    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    # show(mat,"Before return")
    return col_order, squares, mat


def find_and_swap_tartan_squares(squares, mat, contend):
    size = len(mat)
    corners = get_corners(squares)
    i1 = 2
    while i1 < len(corners) - 2:

        cm2 = corners[i1 - 2]
        cm1 = corners[i1 - 1]
        c0 = corners[i1]
        c1 = corners[i1 + 1]
        c2 = corners[i1 + 2]

        A01 = goal(c0, c1, mat, contend)
        A12 = goal(c1, c2, mat, contend)
        Am2m1 = goal(cm2, cm1, mat, contend)
        Am10 = goal(cm1, c0, mat, contend)
        Am11 = goal(cm1, c1, mat, contend)
        criterion = A01 < A12 and Am10 < Am2m1 and Am11 > A01 and Am11 > Am10
        max_index = i1
        if (criterion):
            print(i1)
            max_index = i1 + 1
            elem = squares.pop(i1)
            squares.insert(i1 + 1, elem)
            elem2 = corners.pop(i1)
            corners.insert(i1 + 1, elem2)
        i1 = max_index + 1
    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    return col_order, squares, mat


####################################################
##  The old version of the swap squares function  ##
####################################################
def find_and_swap_squares_old(squares, mat, contend):
    size = len(mat)
    corners = get_corners(squares)
    i1 = 0
    while i1 < len(corners) - 1:
        c1 = corners[i1]
        c2 = corners[i1 + 1]
        max_val = goal(c1, c2, mat, contend)
        AB = mat[c1[1]][c1[1] + 1]
        max_index = i1

        for i2, c2 in enumerate(corners[i1 + 1:], start=(i1 + 1)):
            # comments are for checking C  - to be moved after A
            AC = mat[c1[1]][c2[0]]
            BC = mat[c2[0] - 1][c2[0]]
            # C is not the last square
            if c2[1] < (size - 2):
                CD = mat[c2[1]][c2[1] + 1]
                BD = mat[c2[0] - 1][c2[1] + 1]
                # C is the last square
            else:
                CD = 0.0
                BD = 0.0
            CB = mat[c2[1]][c1[1] + 1]  # EB

            # אם מעבירים רבוע ה לאחרי רבוע א צריך להוסיף אה הב דו ולהחסיר דה הו אב. ואז לבדוק האם זה חיובי
            criterion1 = AC + CB + BD - (BC + CD + AB)
            contender = goal(c1, c2, mat, contend)
            if (max_val < contender) and (criterion1 > 0):
                max_val = contender
                max_index = i2

        if max_index != i1:
            elem = squares.pop(max_index)
            squares.insert(i1 + 1, elem)
            elem2 = corners.pop(max_index)
            corners.insert(i1 + 1, elem2)
        i1 = max_index + 1
    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    # show(mat,"Before return")
    return squares, mat


##############################################################################################
# The 2 following function try to culculate the red squares based on a "running" average.    #
# The idea is that we want the red squares to be in the same density throughout the entire   #
# square.                                                                                    #
##############################################################################################
def calc_squares_2(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    sum = mat[0][1]
    num = 1
    for i in range(1, size-1):
        if (mat[i][i + 1] > min_red and abs(mat[i][i+1]-(sum/num)) < 0.2):
            temp.append(i)  # continue the square
            sum += mat[i][i+1]
            num += 1
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)
                squares.append(temp)
                temp = []
                sum = 0
                num = 1
            else:
                if temp:
                    squares.append(temp)
                temp = [i]
                sum = mat[i][i+1]
                num = 1


    if temp:
        temp.append(size - 1)
        squares.append(temp)
    else:
        squares.append([size - 1])

    #print(squares)
    return squares


def calc_squares_3(mat, size, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    s = max(mat[0][1], 0.5)
    n = 1
    avg = s/n
    for i in range(1, size-1):
        # print(temp)
        # print(i,mat[i][i + 1], avg)
        if (abs(mat[i][i + 1] - avg) < min_red):
            temp.append(i)  # continue the square
            s += mat[i][i + 1]
            n += 1
            avg = s/n
        else:
            if (abs(mat[i-1][i] - avg) < min_red and mat[i][i+1] < threshold):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
                s = 0.5
                n = 1
                avg = s/n
            else:
                if temp:
                    squares.append(temp)
                temp = [i]  # start a new square
                s = mat[i][i+1]
                n = 1
                avg = max(s/n,0.5)

    if temp:  # i think this case can happen regardless
        temp.append(size - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([size - 1])

    print(squares)
    return squares


#####################################################################################
# Function for swapping the rows within the red squares.                            #
# These are the function that Danielle tried and didn't work well.                  #
# The idea was to run them on each square after we found the best squares possible. #
#####################################################################################
def calc_iner_squares(mat, start, end, threshold):
    # threshold defines the unity of the square. a square must be with the same level of intensity, up to threshold
    squares = []
    temp = [start]
    min_red = min(0.9, 1.0 - threshold)
    for i in range(start+1, end-1):
        # print(i)
        if (abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] > min_red):
            #       if (abs(mat[i][i+1]) >min_red and mat[i][i+1] > min_red):
            temp.append(i)  # continue the square
        else:
            if (mat[i - 1][i] > min_red):
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)  # why does this repeat in both cases?
                temp = [i]  # start a new square

    if temp:  # i think this case can happen regardless
        temp.append(end - 1)  # even if temp is empty
        squares.append(temp)
    else:
        squares.append([end - 1])

    # print("iner: ",squares)
    return squares


def find_and_swap_iner_squares(squares, mat, start, end, contend, threshold):
    size = len(mat)
    corners = get_corners(squares)
    min_red = min(0.9, 1.0 - threshold)
    i1 = 0
    while i1 < len(corners) - 1:
        #print(i1,squares)
        c1 = corners[i1]
        c2 = corners[i1 + 1]
        max_val = goal(c1, c2, mat, contend)
        AB = mat[c1[1]][c1[1] + 1]
        max_index = i1

        for i2, c2 in enumerate(corners[i1 + 1:], start=(i1 + 1)):
            # comments are for checking C  - to be moved after A
            AC = mat[c1[1]][c2[0]]
            BC = mat[c2[0] - 1][c2[0]]
            # C is not the last square
            if c2[1] < (size - 2):
                CD = mat[c2[1]][c2[1] + 1]
                BD = mat[c2[0] - 1][c2[1] + 1]
                # C is the last square
            else:
                CD = 0.0
                BD = 0.0
            CB = mat[c2[1]][c1[1] + 1]  # EB

            # אם מעבירים רבוע ה לאחרי רבוע א צריך להוסיף אה הב דו ולהחסיר דה הו אב. ואז לבדוק האם זה חיובי
            criterion1 = AC + CB + BD - (BC + CD + AB)
            contender = goal(c1, c2, mat, contend)
            # if (max_val < contender and (criterion1 > 0) and (abs(mat[c1[1]-1][c1[1]] - AC) < threshold)):
            if (max_val < contender and (abs(mat[c1[1]-1][c1[1]] - AC) < threshold)):
                max_val = contender
                max_index = i2
        if max_index != i1:
            elem = squares.pop(max_index)
            squares.insert(i1+1, elem)
            elem2 = corners.pop(max_index)
            corners.insert(i1+1, elem2)
        else:
            elem = squares.pop(max_index)
            squares.insert(i1 , elem)
            elem2 = corners.pop(max_index)
            corners.insert(i1 , elem2)
        i1 = max_index + 1
    return squares


df = pd.read_csv(file)
df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
df = df.dropna()
df.set_index('inst',inplace=True)
mat = df.values
#print(mat)
#mat = np.around(mat,1)
#print("before: ",calc_daig(mat))


fig = plt.figure(figsize=(20, 12))

show(mat, 'Before: ' + str(calc_daig(mat)), 3, 1)

mat2 = mat

# threshold = 0.5  # 0.5
# levels = 100
# contend = 5
# contendtartan = 1
# tartanlevels = 10
# swaplevels = 5
# # 1 better than 2. 5 better than both.
# for j in range(levels):
#     threshold_j = threshold / levels * (j + 1);
#     print(threshold_j)
#
#     for i in range(tartanlevels):  # 10
#         squares = calc_squares(mat2, len(mat2), threshold_j)
#         L = len(squares)
#         # squares,mat2 = find_and_swap_squares(squares,mat2,contend)
#         col_order, squares, mat2 = find_and_swap_tartan_squares(squares, mat2, contendtartan)
#         columns = [df.columns.tolist()[i] for i in col_order]
#         rows = [list(df.index.values)[i] for i in col_order]
#         df = df.reindex(columns, axis=1)
#         df = df.reindex(rows, axis=0)
#         a = calc_daig(mat2)
#         print(j, L, a)
#     for i in range(swaplevels):
#         squares = calc_squares(mat2, len(mat2), threshold_j)
#         L = len(squares)
#         col_order, squares, mat2 = find_and_swap_squares_new(squares, mat2, contend,threshold_j)
#         columns = [df.columns.tolist()[i] for i in col_order]
#         rows = [list(df.index.values)[i] for i in col_order]
#         df = df.reindex(columns, axis=1)
#         df = df.reindex(rows, axis=0)
#         # squares,mat2 = find_and_swap_tartan_squares(squares,mat2,contend)
#         a = calc_daig(mat2)
#         print(j, L, a)


threshold = 0.5
levels=100
squares = []
for j in range(levels):
    threshold_j=threshold/levels*(j+1);
    # print(threshold_j)
    for i in range(5):
        squares = calc_squares(mat2,len(mat2),threshold_j)
        L = len(squares)
        # print(L)
        # squares,mat2 = find_and_swap_squares_old(squares,mat2,5)
        col_order, squares, mat2 = find_and_swap_squares_new(squares, mat2, 5, threshold_j)

        columns = [df.columns.tolist()[i] for i in col_order]
        rows = [list(df.index.values)[i] for i in col_order]
        df = df.reindex(columns, axis=1)
        df = df.reindex(rows, axis=0)
        a = calc_daig(mat2)
        # print(L,a/L,a)

# show_df(df,'After: '+str(calc_daig(df.values)),2,2)
show_df(df, 'After: ' + str(calc_daig(df.values)), 3, 2)

show_squares_by_color(df, squares, "Color by square", 3, 3)

# file_name = "../../Alibaba_log_files/Correlation_tables/partial_map_machines_to_red_square_threshold_05.txt"
# get_insts_in_squares(df, squares, file_name)

fig.tight_layout()
plt.show()
