import glob
import pandas as pd

# The relevant dates for the different months:
# April 2020 (TimeInSeconds): 70930800 - 73526400
# August 2020 (TimeInSeconds): 81471600 - 84153600
# July 2020 (TimeInSeconds): 78793218 - 81475200
# September 2020 (TimeInSeconds): 84150035 - 86745652
# October 2020 (TimeInSeconds): 86742024 - 89424024


# ######################################################### #
# This function retrieves all the points in the time frame  #
# in which we are calculating the correlation (August 2020)  #
# ######################################################### #
def get_num_all_points(file_list):
    f1 = open("./alibaba_work/feature_files/all_instances_august_2020_num_points_list.csv", "w")
    f1.write("inst, num_points\n")
    for f in file_list:
        _, _, _, name = f.split('/')
        name, _ = name.split('__vpc')
        df = pd.read_csv(f)
        df = df[df.TimeInSeconds < 84153600]
        df = df[df.TimeInSeconds > 81471600]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.index = df['Timestamp']
        if (df.NormPrice.nunique() == 1) or (df.Timestamp.count() == 0):
            continue
        else:
            count = df.Timestamp.count()
            print(name, str(count))
            f1.write(name + "," + str(count) + "\n")

    f1.close()


def get_num_changes_in_price(dirName):
    file_list = glob.glob(dirName)
    f1 = open("./alibaba_work/feature_files/all_instances_April_2020_num_changes_in_price_list.csv", "w")
    f1.write("inst, num_points\n")
    for f in file_list:
        _, _, _, name = f.split('/')
        name, _ = name.split('__vpc')
        df = pd.read_csv(f)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df = df[df.TimeInSeconds < 73526400]
        df = df[df.TimeInSeconds > 70930800]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        # if (df.NormPrice.nunique() == 1) or (df.Timestamp.count() == 0):
        if df.Timestamp.count() == 0:
            continue
        else:
            prev_price = df.NormPrice.loc[list(df.index.values)[0]]
            prev_index = list(df.index.values)[0]

            row_list = [prev_index]
            for index, row in df.iterrows():
                if row.NormPrice != prev_price:
                    if not (prev_index in row_list):
                        row_list.append(prev_index)
                    row_list.append(index)
                    prev_price = row.NormPrice
                prev_index = index
            row_list.append(index)
            df2 = df.loc[row_list]
            count = df2.Timestamp.count()
            print(name, str(count))
            f1.write(name + "," + str(count) + "\n")

    f1.close()


dirName = "./alibaba_work/*_2020_traces/*_clean.csv"


get_num_changes_in_price(dirName)


