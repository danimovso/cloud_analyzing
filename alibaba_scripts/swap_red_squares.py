import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools
import argparse


#####################################################################################################
# Plot the matrix heatmap
# Parameters: 1. mat - the correlation matrix
#             2. (nrows, ncols, index) - the subplot will take the index position on a grid with
#                                        nrows rows and ncols columns. index starts at 1 in the
#                                        upper left corner and increases to the right.
#####################################################################################################
def show(mat, title, nrows, ncols, index):
    ax = fig.add_subplot(nrows, ncols, index)
    im = ax.imshow(mat, cmap='jet')
    ax.set_title(title)
    # fig.colorbar(im, location='right', shrink=0.5)


#####################################################################################################
# Calculate the red squares:
# Parameters: 1. mat - the correlation matrix
#             2. size - the size of the matrix
#             3. threshold - defines the unity of the square.
#                            a square must be with the same level of intensity, up to threshold.
# Return value: the list of the red squares
#####################################################################################################
def calc_squares(mat, size, threshold):
    squares = []
    temp = [0]
    min_red = min(0.9, 1.0 - threshold)
    min_red = max(min_red, 0.01)
    for i in range(1, size - 1):
        if abs(mat[i][i + 1] - mat[i - 1][i]) < threshold and mat[i][i + 1] * mat[i - 1][i] > 0 and \
                mat[i][i + 1] > min_red:
            temp.append(i)  # continue the square
        else:
            if mat[i - 1][i] > min_red:
                temp.append(i)  # add the line i and end the square
                squares.append(temp)
                temp = []
            else:
                if temp:
                    squares.append(temp)
                temp = [i]  # start a new square

    if temp:
        temp.append(size - 1)
        squares.append(temp)
    else:
        squares.append([size - 1])

    return squares


#####################################################################################################
# Get the list of the corners of the red squares along the main diagonal:
# Parameters: 1. squares - the list of the red squares
# Return value: a list of all the corners
#####################################################################################################
def get_corners(squares):
    corners = []
    for s in squares:
        corners.append((s[0], s[-1]))
    return corners


#####################################################################################################
# Calculate the sum on the 1-diagonal:
# Parameters: 1. mat - the correlation matrix
# Return value: the sum of the diagonal in offset 1
#####################################################################################################
def calc_daig(mat):
    return np.trace(mat, offset=1)


#####################################################################################################
# Calculate the unity of the red clusters
# Parameters: 1. mat - the correlation matrix
# Return value: the unity of all the res squares
#####################################################################################################
def calc_red_distance(mat):
    val = 0.0
    size = len(mat)
    for i in range(size):
        for j in range(i + 1, size):
            if mat[i][j] > 0.0:
                val += mat[i][j] * (j - i)  # j >i always
    return val / size


#####################################################################################################
# Calculate the value of the optional swap
# Parameters:   1. c1, c2 - the corners of the square we are testing
#               2. mat - the correlation matrix
#               3. contend - the method we want to use to test the square marked by c1 and c2
# Return value: the value that matches the optional swap
#####################################################################################################
def goal(c1, c2, mat, contend):
    c1_val = mat[c1[1]][c2[0]]

    if contend == 0:
        max_val = c1_val
        return max_val

    if contend == 15:  # real average value of the positive correlation, intended as a first stage before sorting the blue patches
        max_val = 0
        for i in range(c1[0], c1[1] + 1):
            for j in range(c2[0], c2[1] + 1):
                t = max(0, mat[i][j])
                max_val += t * t  # stress high values
        max_val /= ((c2[1] - c2[0] + 1) * (c1[1] - c1[0] + 1))

    return max_val


#####################################################################################################
# Find and swap squares along the main diagonal
# Parameters:   1. squares - the current list of squares
#               2. mat - the correlation matrix
#               3. contend - the method used to choose the squares that will be swapped
# Return value: 1. col_order - the order of the cols used to sort the original dataframe (the matrix)
#               2. squares - the new list of squares
#               3. mat - the correlation matrix after sorting it based on the col_order
#####################################################################################################
def find_and_reconnect_squares(squares, mat, contend):
    corners = get_corners(squares)
    i1 = 0
    while i1 < len(corners) - 2:

        c1 = corners[i1]
        AB = goal(c1, corners[i1 + 1], mat, contend)  # the current connection, to be lost
        max_index_c2 = i1
        max_index_c3 = i1
        max_val = 0
        for i2, c2 in enumerate(corners[i1 + 2:], start=(i1 + 2)):
            AD = goal(c1, c2, mat, contend)

            if AD > AB:
                CD = goal(corners[i2 - 1], c2, mat, contend)

                for i3, c3 in enumerate(corners[i2:], start=i2):
                    DB = goal(c3, corners[i1 + 1], mat, contend)
                    criterion1 = AD + DB - (CD + AB)
                    if i3 < len(corners) - 1:
                        # c3 is not the last square
                        CE = goal(c3, corners[i3 + 1], mat, contend)
                        BE = goal(corners[i2 - 1], corners[i3 + 1], mat, contend)
                        criterion1 += BE - CE

                    if criterion1 > max_val:
                        max_index_c3 = i3
                        max_index_c2 = i2
                        max_val = criterion1

        if max_val > 0:
            # Get the elements (squares) from index1 to index2 including index2 and keep them in a temp list
            tmp_list = squares[max_index_c2:max_index_c3 + 1]
            # Delete the elements (squares) from index1 to index2 including index2
            del squares[max_index_c2:max_index_c3 + 1]
            # Insert the removed elements into the given index in the squares list
            squares[i1 + 1:i1 + 1] = tmp_list
            # Get the elements (squares) from index1 to index2 including index2 and keep them in a temp list
            tmp_list2 = corners[max_index_c2:max_index_c3 + 1]
            # Delete the elements (squares) from index1 to index2 including index2
            del corners[ max_index_c2:max_index_c3 + 1]
            corners[i1 + 1:i1 + 1] = tmp_list2
        i1 = max_index_c3 + 1

    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    return col_order, squares, mat


#####################################################################################################
# Shifting squares that interfere with the sequence of red squares
# Parameters:   1. squares - the current list of squares
#               2. mat - the correlation matrix
#               3. contend - the method used to choose the squares that will be swapped
# Return value: 1. col_order - the order of the cols used to sort the original dataframe (the matrix)
#               2. squares - the new list of squares
#               3. mat - the correlation matrix after sorting it based on the col_order
#####################################################################################################
def find_and_swap_tartan_squares(squares, mat, contend):
    corners = get_corners(squares)
    if len(corners) > 1:
        i1 = 2
        while i1 < len(corners) - 2:

            cm2 = corners[i1 - 2]
            cm1 = corners[i1 - 1]
            c0 = corners[i1]
            c1 = corners[i1 + 1]
            c2 = corners[i1 + 2]

            A01 = goal(c0, c1, mat, contend)
            A12 = goal(c1, c2, mat, contend)
            Am2m1 = goal(cm2, cm1, mat, contend)
            Am10 = goal(cm1, c0, mat, contend)
            Am11 = goal(cm1, c1, mat, contend)
            criterion = A01 < A12 and Am10 < Am2m1 and Am11 > A01 and Am11 > Am10
            max_index = i1
            if criterion:
                max_index = i1 + 1
                elem = squares.pop(i1)
                squares.insert(i1 + 1, elem)
                elem2 = corners.pop(i1)
                corners.insert(i1 + 1, elem2)

            i1 = max_index + 1

    col_order = list(itertools.chain.from_iterable(squares))
    mat = mat[col_order][:, col_order]
    return col_order, squares, mat


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--file", help='The csv file containing the correlation matrix')
    parser.add_argument("--threshold", help='The required threshold for the red squares')
    parser.add_argument("--levels_1", help='The number of threshold changes wanted in stage 1 (coarse-granularity) ')
    parser.add_argument("--tartan_1",
                        help='The number of iterations wanted to sort the matrix in stage 1 (fine-granularity)')
    parser.add_argument("--levels_2", help='The number of threshold changes wanted in stage 2 (coarse-granularity) ')
    parser.add_argument("--tartan_2",
                        help='The number of iterations wanted to sort the matrix in stage 2 (fine-granularity)')

    args = parser.parse_args()
    file = args.file

    if args.threshold:
        threshold = args.threshold
    else:
        threshold = 0.95

    fig = plt.figure(tight_layout=True)

    # Parse and read the csv file containing the correlation matrix
    df = pd.read_csv(file)
    df.set_index("inst", inplace=True)
    mat = df.values

    # show(mat, 'Before: ' + str(calc_daig(mat)) + " " + str(calc_red_distance(mat)), 1, 3, 1)

    # -------------------------- Begin stage 1 -------------------------- #
    # heuristically increase the sum of the values on the 1-diagonal
    print("Starting stage 1...")
    if args.levels_1:
        levels_1 = args.levels_1
    else:
        levels_1 = 5
    if args.tartan_1:
        tartan_1 = args.tartan_1
    else:
        tartan_1 = 10

    squares = []
    contend = 0
    for j in range(levels_1):
        threshold_j = threshold / levels_1 * (j + 1)

        for i in range(tartan_1):
            squares = calc_squares(mat, len(mat), threshold_j)
            col_order, squares, mat = find_and_reconnect_squares(squares, mat, contend)

            columns = [df.columns.tolist()[i] for i in col_order]
            rows = [list(df.index.values)[i] for i in col_order]
            df = df.reindex(columns, axis=1)
            df = df.reindex(rows, axis=0)

        for i in range(tartan_1):
            print(j,i)
            squares = calc_squares(mat, len(mat), threshold_j)
            col_order, squares, mat = find_and_swap_tartan_squares(squares, mat, contend)

            columns = [df.columns.tolist()[i] for i in col_order]
            rows = [list(df.index.values)[i] for i in col_order]
            df = df.reindex(columns, axis=1)
            df = df.reindex(rows, axis=0)

    print("End stage 1.")

    show(mat, 'End stage 1: ' + str(calc_daig(mat)) + " " + str(calc_red_distance(mat)), 1, 3, 2)
    # -------------------------- End stage 1 -------------------------- #

    # -------------------------- Begin stage 2 -------------------------- #
    # heuristically increase the unity of the clusters along the main diagonal
    print("Starting stage 2...")
    if args.levels_2:
        levels_2 = args.levels_2
    else:
        levels_2 = 20

    if args.tartan_2:
        tartan_2 = args.tartan_2
    else:
        tartan_2 = 10

    contend = 15

    for min_level in range(levels_2):
        for j in range(min_level, levels_2):
            threshold_j = threshold / levels_2 * (j + 1)

            for i in range(tartan_2):
                squares = calc_squares(mat, len(mat), threshold_j)
                col_order, squares, mat = find_and_reconnect_squares(squares, mat, contend)

                columns = [df.columns.tolist()[i] for i in col_order]
                rows = [list(df.index.values)[i] for i in col_order]
                df = df.reindex(columns, axis=1)
                df = df.reindex(rows, axis=0)

            for i in range(tartan_2):
                squares = calc_squares(mat, len(mat), threshold_j)
                col_order, squares, mat = find_and_swap_tartan_squares(squares, mat, contend)

                columns = [df.columns.tolist()[i] for i in col_order]
                rows = [list(df.index.values)[i] for i in col_order]
                df = df.reindex(columns, axis=1)
                df = df.reindex(rows, axis=0)

    print("End stage 2.")

    show(mat, 'End stage 2: ' + str(calc_daig(mat)) + " " + str(calc_red_distance(mat)), 1, 3, 3)
    # -------------------------- End stage 2 -------------------------- #

    plt.show()
