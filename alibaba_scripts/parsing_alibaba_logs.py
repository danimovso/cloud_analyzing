import sys
import os
import os.path
from os import path
import glob
import matplotlib.pyplot as plt
import pandas as pd, numpy as np, seaborn as sns, scipy
from contextlib import closing
import re
import argparse


######################################
## Create dir files for each region ##
######################################
def createRegionDirs():
    regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5',
               'cn-beijing','cn-hangzhou','cn-hongkong','cn-huhehaote','cn-qingdao','cn-shanghai','cn-shenzhen',
               'cn-zhangjiakou','eu-central-1','eu-west-1','me-east-1','us-east-1','us-west-1']
    for region in regions:
        dirName = "../../Alibaba_log_files/parsed_raw_logs_until_January_2022/" + region + "_full_logs"
        if not os.path.exists(dirName):
            os.mkdir(dirName)
            print("Directory " , dirName ,  " Created ")
        else:    
            print("Directory " , dirName ,  " already exists")


###################################################################
## Create the "..all_log.csv" files from all the seperate logs   ##
###################################################################
def createAllLogsFile(region):
    for region in regions:
        print("files for " + region)
        r = "../../Alibaba_log_files/logs/" + region + "*.csv"
        # r = "./logs/" + region + "*20.csv"
        files = glob.glob(r)
        print(files)

        # Create the joined region log file:
        #r2 = region + "_all_logs.csv"
        r2 = "../../Alibaba_log_files/parsed_raw_logs_until_January_2022/" + region + "_full_logs_until_January_2022.csv"
        if not path.exists(r2):
            f_full = open(r2,"a+")
            f_full.write("InstanceType|IoOptimized|NetworkType|OriginPrice|SpotPrice|Timestamp|ZoneId\n")
            print("Created new file: " + r2)

        # Extract only the relevant data from the original log file
        for f in files:
            print("parsing " + f + "...")
            # first get all lines from file
            with open(f, 'r') as f1:
                lines = f1.readlines()

            lines = [line.replace(' ', '') for line in lines]
            lines = [line.replace('|||', '') for line in lines]

            newLines = []
            for line in lines:
                if re.search(region,line):
                    newLines.append(line)

            f_full = open(r2,"a+")
            f_full.writelines(newLines)
            f_full.close()


###################################################################
# Split the "..all_log.csv" files into traces per instance type  #
###################################################################
def createTracePerInstance(regions):
    for region in regions:
        print(region)
        r = "../../Alibaba_log_files/parsed_raw_logs_until_January_2022/" + region + "_full_logs_until_January_2022.csv"
        print("Reading file " + r)
        a = pd.read_csv(r, delimiter='|')

        grouped = a.groupby(['ZoneId', 'InstanceType', 'IoOptimized', 'NetworkType'])

        num = 0
        for name, group in grouped:
            num += 1
            r = r.replace("_full_logs_until_January_2022.csv", "_full_logs/")
            b = '__'.join(name)
            b = b.replace(".", "_")
            b = b.replace("/", "_")
            b = b.replace("-", "_")
            b = r + b + ".csv"

            if not path.exists(b):
                f1= open(b,"a+")
                f1.write("OriginPrice,SpotPrice,Timestamp,TimeInSeconds\n")
                print("Created new file: " + b)
            else:
                f1= open(b,"a+")
                print("No new file created for: " + b)

            c = group.drop(['ZoneId','InstanceType', 'IoOptimized', 'NetworkType'],1)
            for index, row in c.iterrows():
                if row["Timestamp"] != "Timestamp":
                    tmp = row["Timestamp"].replace("T"," ")
                    tmp = tmp.replace("Z","")
                    start = pd.to_datetime("2018-01-01 00:00:00.000")
                    curr = pd.to_datetime(tmp)
                    ### The following line calculates the difference between two timestamps,
                    ### and then converts the difference in terms of seconds (timedelta64(1,'s') small s indicates seconds)
                    in_seconds = (curr-start)/np.timedelta64(1,'s')
                    f1.write("%s,%s,%s,%s\n" % (row["OriginPrice"],row["SpotPrice"],tmp,in_seconds))
            f1.close()

    print("Finished with ", region," ,", num, "files created")


####################################################
## Remove duplicate lines in the trace files.     ##
## Rearrange the columns to match Orna's request  ##
####################################################        
def dropDuplicateRows(region):
    dirName = "./alibaba_work/" + region + "_full_traces_updated/*vpc.csv"
    # dirName = region + "_2020_traces/*vpc.csv"
    print(dirName)
    
    files = glob.glob(dirName)

    num = 0
    for file in files:
        num += 1
        df = pd.read_csv(file)
        # print(df.head(2))
        columns_titles = ["OriginPrice","SpotPrice","TimeInSeconds","Timestamp"]
        df=df.reindex(columns=columns_titles)
        # print(df.head(2))
        # print(df.Timestamp.count())
        df.drop_duplicates(subset ="Timestamp",keep='first', inplace=True)
        # print(df.Timestamp.count())

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')

        df['NormPrice'] = df.SpotPrice / df.OriginPrice
        df['DiffTime'] = df['TimeInSeconds'].diff()
        df['month_year'] = df['Timestamp'].dt.to_period('M')

        # print("Finished working on "+region)
        df.to_csv(file, index=False)

    print("Finished with ", region, " ,", num, "files created")


####################################################
## Add the DiffTime column to all the trace files ##
####################################################
def addDiffTimeCol(region):
    #for region in regions:
    dirName = "./alibaba_work/" + region + "_full_traces/*.csv"
    print(dirName)
    
    files = glob.glob(dirName)

    for file in files:
        df = pd.read_csv(file)
        print(file)
        # df = df.drop(['DiffTime'], axis=1)
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        df = df.drop_duplicates(subset=['OriginPrice', 'SpotPrice', 'Timestamp'], keep='first')
        df = df.sort_values('TimeInSeconds')
        
        print("adding DiffTime column...")
        df['DiffTime'] = df['TimeInSeconds'].diff()
        df.to_csv(file)


####################################################################
##                            Clean logs                          ##
## Remove unnecessary lines from the trce files.                  ##
## We only keep the enteries that document a change in the Spot   ##
## price.                                                         ##
####################################################################
def cleanTraces(region):
    #for region in regions:
    # dirName = "./alibaba_work/" + region + "_full_traces/*.csv"
    dirName = "./alibaba_work/*_full_traces_updated/*_vpc.csv"
    # print(dirName)
    
    files = glob.glob(dirName)

    num = 0
    for file in files:
        num += 1
        # print("Processing ",file)
        df = pd.read_csv(file)
        
        # Clean the traces from excess columns
        df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
        # df['NormPrice'] = df['SpotPrice'] / df['OriginPrice']
        df = df[['OriginPrice','SpotPrice','TimeInSeconds','Timestamp','NormPrice']]
        
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.set_index('Timestamp',inplace=True,drop=False)
        df.reset_index(drop=True,inplace=True)
        
        prev_price = df.SpotPrice.iloc[0]
        prev_index = 0
        
        row_list = [0]
        for index, row in df.iterrows():
            if row.SpotPrice != prev_price:
                if not (prev_index in row_list):
                    row_list.append(prev_index)
                row_list.append(index)
                prev_price = row.SpotPrice
            prev_index = index
        row_list.append(index)

        #print(row_list)
        
        df2 = df.loc[row_list,['OriginPrice', 'SpotPrice', 'NormPrice', 'TimeInSeconds', 'Timestamp', 'month_year']].copy()

        if df2.Timestamp.count() < 2:
            print(file)

        name,csv = file.split('.csv')
        file = name + "__clean.csv"

        print("Finished file: ", file)
        df2.to_csv(file, index=False)
    print("Finished with ", region, " ,", num, "files created")


####################################################################
##                            Clean logs                          ##
## Remove unnecessary lines from the trce files.                  ##
## We only keep the enteries that document a change in the Spot   ##
## price.                                                         ##
####################################################################
def cleanTraces_2():
    # for region in regions:
    # dirName = "./alibaba_work/" + region + "_full_traces/*.csv"
    dirName = "F:/alibaba_work/*_full_traces_updated/*_vpc__clean.csv"
    # print(dirName)

    files = glob.glob(dirName)

    num = 0
    for i, file in enumerate(files,1):
        print(i)
        num += 1
        df = pd.read_csv(file)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)

        length = df.Timestamp.count()
        if length > 2:
            row_list = []
            i = 1
            while i < length - 1:
                if df.SpotPrice.iloc[i - 1] == df.SpotPrice.iloc[i]:
                    row_list.append(i)
                i += 1

            df2 = df.drop(row_list)

            if df2.Timestamp.count() < 2:
                print(file)
        else:
            df2 = df.copy()

        name, csv = file.split('__clean.csv')
        file = name + "__clean_2.csv"

        print("Finished file: ", file)
        df2.to_csv(file, index=False)
    print("Finished with ", region, " ,", num, "files created")


def cleanTraces_3():
    # for region in regions:
    # dirName = "./alibaba_work/" + region + "_full_traces/*.csv"
    dirName = "F:/alibaba_work/*_full_traces_updated/*_vpc.csv"
    instList = glob.glob(dirName)

    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc.csv']
    # instList = [
    #     'F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc__clean_2.csv']
    total_num = len(instList)

    # ignore_list = [(pd.to_datetime("01/07/19 00:00:00 AM"), pd.to_datetime("01/12/19 00:00:00 AM")),
    #                (pd.to_datetime("04/04/19 00:00:00 AM"), pd.to_datetime("04/05/19 00:00:00 AM")),
    #                (pd.to_datetime("05/04/19 00:00:00 AM"), pd.to_datetime("05/05/19 00:00:00 AM")),
    #                (pd.to_datetime("06/03/19 00:00:00 AM"), pd.to_datetime("06/05/19 00:00:00 AM")),
    #                (pd.to_datetime("07/04/19 00:00:00 AM"), pd.to_datetime("07/05/19 00:00:00 AM")),
    #                (pd.to_datetime("08/03/19 00:00:00 AM"), pd.to_datetime("08/04/19 00:00:00 AM")),
    #                (pd.to_datetime("10/01/19 00:00:00 AM"), pd.to_datetime("10/01/19 00:00:00 AM")),
    #                (pd.to_datetime("10/30/19 00:00:00 AM"), pd.to_datetime("11/04/19 00:00:00 AM")),
    #                (pd.to_datetime("12/03/19 00:00:00 AM"), pd.to_datetime("12/03/19 00:00:00 AM")),
    #                (pd.to_datetime("01/01/20 00:00:00 AM"), pd.to_datetime("01/03/20 00:00:00 AM")),
    #                (pd.to_datetime("02/28/20 00:00:00 AM"), pd.to_datetime("03/01/20 00:00:00 AM")),
    #                (pd.to_datetime("07/08/20 00:00:00 AM"), pd.to_datetime("07/11/20 00:00:00 AM")),
    #                (pd.to_datetime("11/04/20 00:00:00 AM"), pd.to_datetime("11/08/20 00:00:00 AM")),
    #                (pd.to_datetime("12/03/20 00:00:00 AM"), pd.to_datetime("12/06/20 00:00:00 AM")),
    #                (pd.to_datetime("01/24/21 00:00:00 AM"), pd.to_datetime("01/31/21 00:00:00 AM")),
    #                (pd.to_datetime("02/26/21 00:00:00 AM"), pd.to_datetime("03/01/21 00:00:00 AM")),
    #                (pd.to_datetime("04/19/21 00:00:00 AM"), pd.to_datetime("04/27/21 00:00:00 AM")),
    #                (pd.to_datetime("06/16/21 00:00:00 AM"), pd.to_datetime("06/17/21 00:00:00 AM")),
    #                (pd.to_datetime("08/07/21 00:00:00 AM"), pd.to_datetime("08/10/21 00:00:00 AM")),
    #                (pd.to_datetime("09/04/21 00:00:00 AM"), pd.to_datetime("09/10/21 00:00:00 AM")),
    #                (pd.to_datetime("10/06/21 00:00:00 AM"), pd.to_datetime("10/10/21 00:00:00 AM")),
    #                (pd.to_datetime("11/05/21 00:00:00 AM"), pd.to_datetime("11/09/21 00:00:00 AM")),
    #                (pd.to_datetime("12/05/21 00:00:00 AM"), pd.to_datetime("12/09/21 00:00:00 AM"))]

    ignore_list = [(pd.to_datetime("01/07/19 00:00:00 AM"), pd.to_datetime("01/12/19 00:00:00 AM")),
                    (pd.to_datetime("03/05/19 00:00:00 AM"), pd.to_datetime("03/05/19 00:00:00 AM")),
                    (pd.to_datetime("04/04/19 00:00:00 AM"), pd.to_datetime("04/05/19 00:00:00 AM")),
                    (pd.to_datetime("05/04/19 00:00:00 AM"), pd.to_datetime("05/05/19 00:00:00 AM")),
                    (pd.to_datetime("06/03/19 00:00:00 AM"), pd.to_datetime("06/05/19 00:00:00 AM")),
                    (pd.to_datetime("07/04/19 00:00:00 AM"), pd.to_datetime("07/05/19 00:00:00 AM")),
                    (pd.to_datetime("08/03/19 00:00:00 AM"), pd.to_datetime("08/04/19 00:00:00 AM")),
                    (pd.to_datetime("09/02/19 00:00:00 AM"), pd.to_datetime("09/02/19 00:00:00 AM")),
                    (pd.to_datetime("10/01/19 00:00:00 AM"), pd.to_datetime("10/01/19 00:00:00 AM")),
                    (pd.to_datetime("10/30/19 00:00:00 AM"), pd.to_datetime("11/04/19 00:00:00 AM")),
                    (pd.to_datetime("12/03/19 00:00:00 AM"), pd.to_datetime("12/03/19 00:00:00 AM")),
                    (pd.to_datetime("01/01/20 00:00:00 AM"), pd.to_datetime("01/03/20 00:00:00 AM")),
                    (pd.to_datetime("02/01/20 00:00:00 AM"), pd.to_datetime("02/01/20 00:00:00 AM")),
                    (pd.to_datetime("02/28/20 00:00:00 AM"), pd.to_datetime("03/01/20 00:00:00 AM")),
                    (pd.to_datetime("05/19/20 00:00:00 AM"), pd.to_datetime("05/19/20 00:00:00 AM")),
                    (pd.to_datetime("07/08/20 00:00:00 AM"), pd.to_datetime("07/11/20 00:00:00 AM")),
                    (pd.to_datetime("09/02/20 00:00:00 AM"), pd.to_datetime("09/02/20 00:00:00 AM")),
                    (pd.to_datetime("11/04/20 00:00:00 AM"), pd.to_datetime("11/08/20 00:00:00 AM")),
                    (pd.to_datetime("12/03/20 00:00:00 AM"), pd.to_datetime("12/06/20 00:00:00 AM")),
                    (pd.to_datetime("12/30/20 00:00:00 AM"), pd.to_datetime("12/30/20 00:00:00 AM")),
                    (pd.to_datetime("01/24/21 00:00:00 AM"), pd.to_datetime("01/31/21 00:00:00 AM")),
                    (pd.to_datetime("02/26/21 00:00:00 AM"), pd.to_datetime("03/01/21 00:00:00 AM")),
                    (pd.to_datetime("03/25/21 00:00:00 AM"), pd.to_datetime("03/25/21 00:00:00 AM")),
                    (pd.to_datetime("04/19/21 00:00:00 AM"), pd.to_datetime("04/27/21 00:00:00 AM")),
                    (pd.to_datetime("06/16/21 00:00:00 AM"), pd.to_datetime("06/17/21 00:00:00 AM")),
                    (pd.to_datetime("07/12/21 00:00:00 AM"), pd.to_datetime("07/12/21 00:00:00 AM")),
                    (pd.to_datetime("08/07/21 00:00:00 AM"), pd.to_datetime("08/10/21 00:00:00 AM")),
                    (pd.to_datetime("09/04/21 00:00:00 AM"), pd.to_datetime("09/10/21 00:00:00 AM")),
                    (pd.to_datetime("10/06/21 00:00:00 AM"), pd.to_datetime("10/10/21 00:00:00 AM")),
                    (pd.to_datetime("11/05/21 00:00:00 AM"), pd.to_datetime("11/09/21 00:00:00 AM")),
                    (pd.to_datetime("12/05/21 00:00:00 AM"), pd.to_datetime("12/09/21 00:00:00 AM"))]

    for num, file in enumerate(instList, 1):
        print(file, num, "/", total_num)
        df = pd.read_csv(file)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('Timestamp')
        df.reset_index(drop=True, inplace=True)

        df["missing_data"] = 0

        df.loc[(df.DiffTime >= 86400), 'missing_data'] = 1

        length = df.Timestamp.count()
        prev = df.Timestamp.iloc[0]
        for index in range(1, length):
            curr = df.Timestamp.iloc[index]

            for _, t in enumerate(ignore_list):
                if prev < t[0] and curr >= t[1]:
                    # print(curr)
                    df.loc[[index], 'missing_data'] = 2
            prev = curr

        # fig = plt.figure()
        # plt.scatter(df.Timestamp, df.SpotPrice, color='black')
        # plt.scatter(df.Timestamp[df.ignore == 1], df.SpotPrice[df.ignore == 1], color='red')
        #
        # plt.show()

        name, csv = file.split('_vpc.csv')
        file = name + "_vpc_2.csv"
        df.to_csv(file, index=False)



def new_cleanTraces():
    # instList = ['D:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new.csv']
    # instList = ['F:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1b__ecs_r6_large__optimized__vpc_2.csv']
    # instList = ['F:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc_2.csv']
    # instList = ['F:/alibaba_work/cn-hangzhou_full_traces_updated/cn_hangzhou_i__ecs_e3_3xlarge__optimized__vpc_2.csv']

    # instList = "F:/alibaba_work/*_full_traces_updated/*_vpc_2.csv"
    instList = "D:/alibaba_work/*_full_traces_updated/*__clean_new.csv"
    instList = glob.glob(instList)
    total_count = len(instList)
    # print(total_count)
    # fig = plt.figure()
    for index, inst in enumerate(instList, 1):
        print("processing ", inst, "(", index, "/", total_count, ")")
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df.reset_index(drop=True, inplace=True)

        length = df.Timestamp.count()
        # new_df = [[df.OriginPrice.iloc[0],df.SpotPrice.iloc[0],df.TimeInSeconds.iloc[0],df.Timestamp.iloc[0],
        #            df.NormPrice.iloc[0], df.month_year.iloc[0],df.missing_data.iloc[0]]]
        # prev_price = df.SpotPrice.iloc[0]
        # i = 1
        # while i < length - 1:
        #     if (df.missing_data.iloc[i] > 0) or (df.missing_data.iloc[i+1] > 0) or (df.SpotPrice.iloc[i] != prev_price):
        #         new_df.append([df.OriginPrice.iloc[i], df.SpotPrice.iloc[i], df.TimeInSeconds.iloc[i], df.Timestamp.iloc[i],
        #                         df.NormPrice.iloc[i], df.month_year.iloc[i], df.missing_data.iloc[i]])
        #         prev_price = df.SpotPrice.iloc[i]
        #     i += 1
        # new_df.append([df.OriginPrice.iloc[-1], df.SpotPrice.iloc[-1], df.TimeInSeconds.iloc[-1], df.Timestamp.iloc[-1],
        #                 df.NormPrice.iloc[-1], df.month_year.iloc[-1], df.missing_data.iloc[-1]])
        # 
        # df2 = pd.DataFrame(new_df, columns=['OriginPrice','SpotPrice','TimeInSeconds','Timestamp','NormPrice','month_year','missing_data'])
        # df2['Timestamp'] = pd.to_datetime(df2['Timestamp'])
        # df2 = df2.sort_values('Timestamp')
        # df2.reset_index(drop=True, inplace=True)
        # 
        # df2['Min'] = df2.NormPrice[((df2.NormPrice.shift(1) > df2.NormPrice) & (df2.NormPrice.shift(-1) > df2.NormPrice)) | \
        #                            ((df2.NormPrice.shift(1) >= df2.NormPrice) & (df2.NormPrice.shift(-1) > df2.NormPrice)) | \
        #                             ((df2.NormPrice.shift(1) > df2.NormPrice) & (df2.NormPrice.shift(-1) >= df2.NormPrice))]
        # df2['Max'] = df2.NormPrice[((df2.NormPrice.shift(1) < df2.NormPrice) & (df2.NormPrice.shift(-1) < df2.NormPrice)) | \
        #                            ((df2.NormPrice.shift(1) <= df2.NormPrice) & (df2.NormPrice.shift(-1) < df2.NormPrice)) | \
        #                            ((df2.NormPrice.shift(1) < df2.NormPrice) & (df2.NormPrice.shift(-1) <= df2.NormPrice))]
        # # df2['TimeDelta'] = df2['TimeInSeconds'].diff() / 3600
        # # df2['Sholder12'] = df2.NormPrice[(df2.TimeDelta.shift(-1) >= 12)]
        # # df2['Sholder24'] = df2.NormPrice[(df2.TimeDelta.shift(-1) >= 24)]
        # # df2['Sholder48'] = df2.NormPrice[(df2.TimeDelta.shift(-1) >= 48)]
        # 
        # length = df2.Timestamp.count()
        # df2['FixedPeakInfo'] = df2.NormPrice[pd.notna(df2['Min']) | pd.notna(df2['Max'])]
        # df2.loc[[0], 'FixedPeakInfo'] = df2.NormPrice.iloc[0]
        # df2.loc[[length-1], 'FixedPeakInfo'] = df2.NormPrice.iloc[length-1]
        # 
        # new_df = []
        # i = 0
        # j = 0
        # while i < length - 1:
        #     new_df.append([j, df2.OriginPrice.iloc[i], df2.SpotPrice.iloc[i], df2.TimeInSeconds.iloc[i], df2.Timestamp.iloc[i],
        #                    df2.NormPrice.iloc[i], df2.month_year.iloc[i], df2.missing_data.iloc[i], df2.Min.iloc[i], df2.Max.iloc[i], df2.FixedPeakInfo.iloc[i], 1])
        #     j += 1
        #     new_df.append([j, df2.OriginPrice.iloc[i], df2.SpotPrice.iloc[i], df2.TimeInSeconds.iloc[i+1], df2.Timestamp.iloc[i+1],
        #                    df2.NormPrice.iloc[i], df2.month_year.iloc[i+1], df2.missing_data.iloc[i+1], df2.Min.iloc[i], df2.Max.iloc[i], df2.FixedPeakInfo.iloc[i], 0])
        #     i += 1
        #     j += 1
        # # print(j)
        # new_df.append([j, df2.OriginPrice.iloc[-1], df2.SpotPrice.iloc[-1], df2.TimeInSeconds.iloc[-1], df2.Timestamp.iloc[-1],
        #                df2.NormPrice.iloc[-1], df2.month_year.iloc[-1], df2.missing_data.iloc[-1], df2.Min.iloc[-1], df2.Max.iloc[-1], df2.FixedPeakInfo.iloc[-1], 1])
        # 
        # # print(new_df)
        # df3 = pd.DataFrame(new_df,
        #                    columns=['MyIndex', 'OriginPrice', 'SpotPrice', 'TimeInSeconds', 'Timestamp', 'NormPrice', 'month_year',
        #                             'missing_data', 'Min', 'Max', 'FixedPeakInfo', 'clean'])
        # 
        # df3['Timestamp'] = pd.to_datetime(df3['Timestamp'])
        # df3 = df3.sort_values('MyIndex')
        # # df3.reset_index(drop=True, inplace=True)
        # 
        # df3['month_year'] = df3['Timestamp'].dt.to_period('M')
        # df3['month_year'] = df3['month_year'].dt.strftime('%Y-%m')
        # 
        # df3['TimeDelta'] = df3['TimeInSeconds'].diff()
        # df3['PriceDelta'] = df3['SpotPrice'].diff()
        # 
        # df3['NormPriceDelta'] = (df3.PriceDelta / df3.OriginPrice)

        df['Sholder12'] = df.NormPrice[(df.TimeDelta.shift(-1) >= (12*3600)) | (df.TimeDelta >= (12*3600))]
        df['Sholder24'] = df.NormPrice[(df.TimeDelta.shift(-1) >= (24*3600)) | (df.TimeDelta >= (24*3600))]
        df['Sholder48'] = df.NormPrice[(df.TimeDelta.shift(-1) >= (48*3600)) | (df.TimeDelta >= (48*3600))]

        df['PeakInfo12'] = df.NormPrice[pd.notna(df['FixedPeakInfo']) | pd.notna(df['Sholder12'])]
        df.loc[[0], 'PeakInfo12'] = df.NormPrice.iloc[0]
        df.loc[[length - 1], 'PeakInfo12'] = df.NormPrice.iloc[length - 1]

        df['PeakInfo24'] = df.NormPrice[pd.notna(df['FixedPeakInfo']) | pd.notna(df['Sholder24'])]
        df.loc[[0], 'PeakInfo24'] = df.NormPrice.iloc[0]
        df.loc[[length - 1], 'PeakInfo24'] = df.NormPrice.iloc[length - 1]

        df['PeakInfo48'] = df.NormPrice[pd.notna(df['FixedPeakInfo']) | pd.notna(df['Sholder48'])]
        df.loc[[0], 'PeakInfo48'] = df.NormPrice.iloc[0]
        df.loc[[length - 1], 'PeakInfo48'] = df.NormPrice.iloc[length - 1]

        name, csv = inst.split('_clean_new.csv')
        file = name + "_clean_new_2.csv"
        print("creating ", file, "...")
        df.to_csv(file, index=False)

        # plt.scatter(df.Timestamp, df.SpotPrice, marker='.', color='blue', label='Full trace')
    #     plt.plot(df.Timestamp, df.SpotPrice, '.:', color='black', label='Effective price trace')
    #     # plt.plot(df.Timestamp[df.clean == 1], df.SpotPrice[df.clean == 1], '.:', color='blue')
    #     plt.scatter(df.Timestamp[pd.notna(df.PeakInfo12)], df.SpotPrice[pd.notna(df.PeakInfo12)], marker='o', color='green', label='12')
    #     plt.scatter(df.Timestamp[pd.notna(df.FixedPeakInfo)], df.SpotPrice[pd.notna(df.FixedPeakInfo)], marker='o', color='yellow')
    #     plt.scatter(df.Timestamp[pd.notna(df.PeakInfo24)], df.SpotPrice[pd.notna(df.PeakInfo24)], marker='o', color='pink', label='24')
    #     plt.scatter(df.Timestamp[pd.notna(df.PeakInfo48)], df.SpotPrice[pd.notna(df.PeakInfo48)], marker='o', color='magenta', label='48')
    #     # plt.scatter(df3.Timestamp[df3.missing_data > 0], df3.SpotPrice[df3.missing_data > 0], marker='x',color='red', label='Ignore')
    #
    # plt.legend()
    # plt.show()


if __name__ == "__main__":

    regions = ['ap-northeast-1','ap-south-1','ap-southeast-1','ap-southeast-2','ap-southeast-3','ap-southeast-5',
                   'cn-beijing','cn-hangzhou','cn-hongkong','cn-huhehaote', 'cn-qingdao', 'cn-shanghai','cn-shenzhen',
                   'cn-zhangjiakou','eu-central-1','eu-west-1','me-east-1','us-east-1','us-west-1']

    parser = argparse.ArgumentParser(description='Parser functions.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    # parser.add_argument("--inst", help='The inst name for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
  
    args = parser.parse_args()
    
    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'
        
    if args.function:
        func = args.function
        if func == 'createRegionDirs':
            print("Start createRegionDirs....")
            createRegionDirs()
            print("Finished creating the directories")
        
        elif func == 'createAllLogsFile':
            print("Running createAllLogsFile function...")
            createAllLogsFile(regions)
            
        elif func == 'createTracePerInstance':
            print("Running createTracePerInstance function...")
            createTracePerInstance(regions)
            
        elif func == 'addDiffTimeCol':
            print("Running addDiffTimeCol function...")
            addDiffTimeCol(region)
            
        elif func == 'cleanTraces':
            print("Running cleanTraces function...")
            cleanTraces(region)

        elif func == 'cleanTraces_2':
            print("Running cleanTraces_2 function...")
            cleanTraces_2()

        elif func == 'cleanTraces_3':
            print("Running cleanTraces_3 function...")
            cleanTraces_3()

        elif func == 'dropDuplicateRows':
            print("Running dropDuplicateRows function...")
            dropDuplicateRows(region)

        elif func == 'new_cleanTraces':
            print("Running new_cleanTraces function...")
            new_cleanTraces()
    
    #createRegionDirs(regions)
    #createAllLogsFile(regions)
    #createTracePerInstance(regions)
    #addDiffTimeCol(regions)
    #cleanTraces(regions)
    
    print("finished")
