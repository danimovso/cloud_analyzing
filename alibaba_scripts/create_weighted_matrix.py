import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# import itertools


file = "../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_corr_no_static_matrix_work_copy.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_October_2020_corr_matrix_ordered.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_July_2020_corr_no_static_matrix.csv"
# file = "../../Alibaba_log_files/Correlation_tables/all_instances_April_2020_corr_no_static_matrix.csv"

file2 = "../../Alibaba_log_files/Correlation_tables/all_instances_august_2020_num_changes_in_price_list.csv"
# file2 = "../../Alibaba_log_files/Correlation_tables/all_instances_october_2020_num_changes_in_price_list.csv"
# file2 = "../../Alibaba_log_files/Correlation_tables/all_instances_July_2020_num_changes_in_price_list.csv"
# file2 = "../../Alibaba_log_files/Correlation_tables/all_instances_April_2020_num_changes_in_price_list.csv"

df = pd.read_csv(file)
df = df[df.columns.drop(list(df.filter(regex='Unnamed:')))]
df = df.dropna()
df.set_index('inst', inplace=True)
mat = df.values

new_df = df.copy()
df2 = pd.read_csv(file2)
df2.set_index('inst',inplace=True)

rows = df.index.values
cols = df.columns.tolist()

for r in rows:
    for c in cols:
        dfVal = df.at[r, c]
        rVal = df2.at[r," num_points"]
        cVal = df2.at[c," num_points"]
        minVal = min(rVal,cVal)
        newVal = dfVal * ((minVal/(rVal+cVal)) * 2)
        new_df.at[r, c] = newVal

print("Finished")

new_df.to_csv("../../Alibaba_log_files/Correlation_tables/all_inst_august_weighted_val_matrix.csv")
