import glob
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
import random
import math

register_matplotlib_converters()
# matplotlib.use('QtAgg')

import pandas as pd, numpy as np
import argparse
from statsmodels.distributions.empirical_distribution import ECDF

# plt.style.use('grayscale')
plt.style.use('seaborn-colorblind')

tex_fonts = {
    # Use LaTeX to write all text
    # "text.usetex": True,
    "font.family": "serif",
    # Use 10pt font in plots, to match 10pt font in document
    "axes.labelsize": 10,
    "font.size": 10,
    # Make the legend/label fonts a little smaller
    "legend.fontsize": 8,
    "xtick.labelsize": 8,
    "ytick.labelsize": 8
}

plt.rcParams.update(tex_fonts)

# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     # "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     # 'pgf.rcfonts': False,
# })


# ################ Begin helper functions ################
# width=441.01772
def set_size(width=229.87749, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** .5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier


def round_down(n, decimals=0):
    multiplier = 10 ** decimals
    return np.floor(n * multiplier) / multiplier


# ################ End helper functions ##################


# #############################################################################
# ####                 Add 'TimeDelta' and 'PriceDelta' columns.          #####
# #############################################################################
def add_time_and_price_delta_cols():
    # instList = "./"+region+"_2020_traces/*clean.csv"
    # instList = "./alibaba_work/" + region + "_full_traces/*_vpc.csv"

    # instList = "./alibaba_work/" + region + "_full_traces/*_clean.csv"
    print("Not using region parameter: ", )
    instList = "F:/alibaba_work/*_full_traces_updated/*_clean__square_2.csv"
    instList = glob.glob(instList)
    total = len(instList)

    for i, inst in enumerate(instList, 1):
        print("processing ", inst, i, "/", total)
        df = pd.read_csv(inst)
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        # df = df.sort_values('Timestamp')

        df['TimeDelta'] = df['TimeInSeconds'].diff()
        df['PriceDelta'] = df['SpotPrice'].diff()

        df['NormPriceDelta'] = (df.PriceDelta / df.OriginPrice)

        df.to_csv(inst, index=False)


def draw_price_delta_per_base_price():
    instList = "../../alibaba_work/*_full_traces_updated/*__clean_new_2.csv"
    instList = glob.glob(instList)
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
    total_count = len(instList)

    inc_data = set()
    dec_data = set()
    for index, inst in enumerate(instList, 1):
        print("processing ", inst, "(", index, "/", total_count, ")")
        df = pd.read_csv(inst)

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df = df[df.clean == 1].copy()
        df = df[df.missing_data == 0].copy()

        # df = df[(df.Timestamp >= pd.to_datetime("12/1/2020 00:00:00 AM"))].copy()
        if not df.empty:
            for basePrice, height in list(zip(df.NormPrice.shift(1), df.NormPriceDelta)):
                if height < 0:
                    dec_data.add((basePrice, abs(height)))
                elif height > 0:
                    inc_data.add((basePrice, height))

    inc_list = list(inc_data)
    dec_list = list(dec_data)
    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    plt.scatter(*zip(*inc_list), color='red', alpha=0.1, label="inc")
    plt.scatter(*zip(*dec_list), color='blue', alpha=0.1, label="dec")
    plt.ylabel("Normalized height")
    plt.xlabel("Normalized Base price")
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    plt.show()



def quantir(origin, option, percent, delta=0.001):
    """

    :param origin:
    :param option:
    :param percent: percent can be 0.02 for example
    :param delta: the quantization level, used to be 0.001
    :return:
    """
    required_change = origin * percent # change in dollars

    if option == 'RAND':
        r = random.random()
        if r > 0.5:
            realchange = delta * math.floor(required_change / delta)
        else:
            realchange = delta * math.ceil(required_change / delta)
    elif option == 'ROUND':
        realchange = delta * round(required_change / delta)
    elif option == 'FLOOR':
        realchange = delta * math.floor(required_change / delta)
    elif option == 'CEIL':
        realchange = delta * math.ceil(required_change / delta)

    # this line does nothing for cents, but it adds another layer of quntization error for yuans.
    realchangecents = 1e-3 * round(realchange / 1e-3) # additional cents truncation error

    return realchangecents / origin


def new_quantization_algo():
    """
    """
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_t6_c4m1_large__optimized__clean_new.csv']
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1b__ecs_t5_lc1m2_large__optimized__clean_new.csv']
    instList = "../../alibaba_work/*_full_traces_updated/*__clean_new_2.csv"
    instList = glob.glob(instList)
    total_count = len(instList)

    real_inc_data = set()
    real_dec_data = set()

    estimated_inc_data = set()
    estimated_dec_data = set()

    round_estimated_inc_data = set()
    round_estimated_dec_data = set()

    floor_estimated_inc_data = set()
    floor_estimated_dec_data = set()

    ceil_estimated_inc_data = set()
    ceil_estimated_dec_data = set()

    origin_price_set = set()

    count_traces = 0

    delta = 0.001
    percent1 = 0.03
    percent2 = -0.02
    for index, inst in enumerate(instList, 1):
        print("processing ", inst, "(", index, "/", total_count, ")")
        df = pd.read_csv(inst)

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df = df[df.clean == 1].copy()

        # df = df[(df.Timestamp >= pd.to_datetime("12/1/2020 00:00:00 AM"))].copy()
        if not df.empty:
            count_traces += 1
            origin_price_set.update(df.OriginPrice)

            for op, realDelta, missing_data in list(zip(df.OriginPrice, df.NormPriceDelta, df.missing_data)):
                if (missing_data == 0) and (op <= 1):
                    if realDelta > 0:
                        estimated_inc_data.add((op, quantir(op, 'RAND', percent1, delta)))
                        round_estimated_inc_data.add((op, quantir(op, 'ROUND', percent1, delta)))
                        floor_estimated_inc_data.add((op, quantir(op, 'FLOOR', percent1, delta)))
                        ceil_estimated_inc_data.add((op, quantir(op, 'CEIL', percent1, delta)))
                        real_inc_data.add((op, realDelta))

                    elif realDelta < 0:
                        estimated_dec_data.add((op, quantir(op, 'RAND', percent2, delta)))
                        round_estimated_dec_data.add((op, quantir(op, 'ROUND', percent2, delta)))
                        floor_estimated_dec_data.add((op, quantir(op, 'FLOOR', percent2, delta)))
                        ceil_estimated_dec_data.add((op, quantir(op, 'CEIL', percent2, delta)))
                        real_dec_data.add((op, realDelta))

    print(count_traces)

    inc_check_data = []
    dec_check_data = []
    for op in origin_price_set:
        if op <= 1.0:
            for h in np.arange(0.001, op, 0.001):
                inc_check_data.append((op, (h / op)))
                dec_check_data.append((op, ((-1 * h) / op)))

    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Decrease")
    ax.scatter(*zip(*dec_check_data), color='red', marker='o', label="expressible space", linewidths=1.0, alpha=0.3)
    ax.scatter(*zip(*estimated_dec_data), s=80, facecolors='none', edgecolors='blue', label="ceil or floor", linewidths=1.0)
    ax.scatter(*zip(*round_estimated_dec_data), s=50, facecolors='none', edgecolors='greenyellow', label="round", linewidths=1.0)
    ax.scatter(*zip(*real_dec_data), color='black', marker='x', label="trace data", linewidths=1.0, alpha=0.3)
    ax.set_ylabel("Height")
    ax.set_xlabel("Origin price ($)")
    ax.set_ylim(-0.024, -0.016)
    ax.set_xlim(0, 1)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    # file = "../../Alibaba_log_files/paper_figures/dec_after_2020.png"
    # plt.savefig(file, bbox_inches="tight")

    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=1))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Increase")
    ax.scatter(*zip(*inc_check_data), color='red', marker='o', label="expressible space", linewidths=1.0, alpha=0.3)
    ax.scatter(*zip(*estimated_inc_data), s=80, facecolors='none', edgecolors='blue', label="ceil or floor", linewidths=1.0)
    ax.scatter(*zip(*round_estimated_inc_data), s=50, facecolors='none', edgecolors='greenyellow', label="round", linewidths=1.0)
    ax.scatter(*zip(*real_inc_data), color='black', marker='x', label="trace data", linewidths=1.0, alpha=0.3)
    ax.set_ylabel("Height")
    ax.set_xlabel("Origin price ($)")
    ax.set_ylim(0.026, 0.034)
    ax.set_xlim(0, 1)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    # file = "../../Alibaba_log_files/paper_figures/inc_after_2020.png"
    # plt.savefig(file, bbox_inches="tight")

    plt.show()


def real_trace_data_divided_by_region():
    fig = plt.figure()
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['*']

    for num, region in enumerate(regions, 1):
        print(region)
        # instList = ['D:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)
        total_count = len(instList)

        # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']

        real_inc_data = []
        real_dec_data = []

        for index, inst in enumerate(instList, 1):
            print("processing ", inst, "(", index, "/", total_count, ")")
            df = pd.read_csv(inst)

            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('MyIndex')
            df = df[df.clean == 1].copy()

            for op, realDelta, missing_data in list(zip(df.OriginPrice, df.NormPriceDelta, df.missing_data)):
                if missing_data == 0:
                    if realDelta > 0:
                        real_inc_data.append((op, realDelta))
                    elif realDelta < 0:
                        real_dec_data.append((op, realDelta))

        ax = fig.add_subplot(5, 4, num)
        t = str(region) + " " + str(total_count)
        ax.set_title(t)
        ax.scatter(*zip(*real_inc_data), marker='x', linewidths=1.0, alpha=0.3)
        ax.scatter(*zip(*real_dec_data), marker='x', linewidths=1.0, alpha=0.3)

        ax.set_ylabel("Height")
        ax.set_xlabel("Origin price ($)")
        ax.set_ylim(-0.03, 0.04)
        ax.set_xlim(0, 1)
        # ax.legend()
    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)

    plt.show()


def testing_quantization_algo():
    """
    """
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_t6_c4m1_large__optimized__clean_new.csv']
    # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1b__ecs_t5_lc1m2_large__optimized__clean_new.csv']
    # instList = "D:/alibaba_work/*_full_traces_updated/*__clean_new_2.csv"
    instList = "../../alibaba_work/*_full_traces_updated/*__clean_new_2.csv"
    instList = glob.glob(instList)
    total_count = len(instList)

    real_inc_data = set()
    real_dec_data = set()

    estimated_inc_data = set()
    estimated_dec_data = set()

    round_estimated_inc_data = set()
    round_estimated_dec_data = set()

    floor_estimated_inc_data = set()
    floor_estimated_dec_data = set()

    ceil_estimated_inc_data = set()
    ceil_estimated_dec_data = set()

    origin_price_set = set()

    delta = 0.001
    percent1 = 0.03
    percent2 = -0.02
    for index, inst in enumerate(instList, 1):
        print("processing ", inst, "(", index, "/", total_count, ")")
        df = pd.read_csv(inst)

        df['Timestamp'] = pd.to_datetime(df['Timestamp'])
        df = df.sort_values('MyIndex')
        df = df[df.clean == 1].copy()

        # df = df[(df.Timestamp >= pd.to_datetime("12/1/2020 00:00:00 AM"))].copy()
        if not df.empty:
            origin_price_set.update(df.OriginPrice)

            for op, realDelta, missing_data in list(zip(df.OriginPrice, df.NormPriceDelta, df.missing_data)):
                if (missing_data == 0) and (op <= 1):
                    if (0.031 <= realDelta <= 0.033):
                        # estimated_inc_data.add((op, quantir(op, 'RAND', percent1, delta)))
                        # round_estimated_inc_data.add((op, quantir(op, 'ROUND', percent1, delta)))
                        ceil_estimated_inc_data.add((op, quantir(op, 'CEIL', percent1, delta)))
                    elif (0.027 <= realDelta <= 0.029):
                        floor_estimated_inc_data.add((op, quantir(op, 'FLOOR', percent1, delta)))
                    elif (0.029 < realDelta < 0.031):
                        round_estimated_inc_data.add((op, quantir(op, 'ROUND', percent1, delta)))
                    real_inc_data.add((op, realDelta))

                    # elif (realDelta < 0) and (-0.025 <= realDelta <= -0.015):
                    #     # estimated_dec_data.add((op, quantir(op, 'RAND', percent2, delta)))
                    #     # round_estimated_dec_data.add((op, quantir(op, 'ROUND', percent2, delta)))
                    #     floor_estimated_dec_data.add((op, quantir(op, 'FLOOR', percent2, delta)))
                    #     ceil_estimated_dec_data.add((op, quantir(op, 'CEIL', percent2, delta)))
                    #     real_dec_data.add((op, realDelta))

    # inc_check_data = []
    # dec_check_data = []
    # for op in origin_price_set:
    #     if op <= 1.0:
    #         for h in np.arange(0.001, op, 0.001):
    #             inc_check_data.append((op, (h / op)))
    #             dec_check_data.append((op, ((-1 * h) / op)))

    # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    # ax = fig.add_subplot(1, 1, 1)
    # # ax.set_title("Decrease")
    # ax.scatter(*zip(*ceil_estimated_dec_data), s=80, facecolors='none', edgecolors='blue', label="ceil", linewidths=1.0)
    # ax.scatter(*zip(*floor_estimated_dec_data), s=50, facecolors='none', edgecolors='greenyellow', label="floor", linewidths=1.0)
    # ax.scatter(*zip(*real_dec_data), color='black', marker='x', label="trace data", linewidths=1.0)
    # ax.set_ylabel("height")
    # ax.set_xlabel("origin price ($)")
    # ax.set_ylim(-0.024, -0.016)
    # ax.set_xlim(0, 1)
    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    # # file = "../../Alibaba_log_files/paper_figures/dec_after_2020.png"
    # # plt.savefig(file, bbox_inches="tight")

    # print("Not in ceil")
    # print(floor_estimated_inc_data - ceil_estimated_inc_data)
    # print("Not in floor")
    # print(ceil_estimated_inc_data - floor_estimated_inc_data)
    # print("Intersection floor and ceil")
    # print(ceil_estimated_inc_data & floor_estimated_inc_data)
    fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
    ax = fig.add_subplot(1, 1, 1)
    # ax.set_title("Increase")
    ax.scatter(*zip(*ceil_estimated_inc_data), s=80, facecolors='none', edgecolors='blue', label="ceil", linewidths=1.0)
    ax.scatter(*zip(*floor_estimated_inc_data), s=50, facecolors='none', edgecolors='greenyellow', label="floor", linewidths=1.0)
    ax.scatter(*zip(*round_estimated_inc_data), s=30, facecolors='none', edgecolors='red', label="round", linewidths=1.0)
    ax.scatter(*zip(*real_inc_data), color='black', marker='x', label="trace data", linewidths=1.0)
    ax.set_ylabel("height")
    ax.set_xlabel("origin price ($)")
    ax.set_ylim(0.026, 0.034)
    ax.set_xlim(0, 1)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    # file = "../../Alibaba_log_files/paper_figures/inc_after_2020.png"
    # plt.savefig(file, bbox_inches="tight")

    plt.show()


def real_trace_data_divided_by_month():
    regions = ['*']

    for region in regions:
        print("Working on region ", region)
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)

        # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        # instList = ["../../Alibaba_log_files/transition_matrices/cn_beijing_g__ecs_hfr6_2xlarge__optimized__vpc_2.csv"]
        # instList = ["../../Alibaba_log_files/interesting_traces/cn_beijing_a__ecs_e3_3xlarge__optimized__vpc.csv"]

        total_count = len(instList)

        real_inc_data = dict()
        real_dec_data = dict()

        for num, inst in enumerate(instList,1):
            print("Processing ", inst, num, "/", total_count)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('MyIndex')

            df['month_year'] = df['Timestamp'].dt.to_period('M')
            grouped = df.groupby('month_year')
            for month, group in grouped:
                if month not in real_inc_data:
                    real_inc_data[month] = []
                if month not in real_dec_data:
                    real_dec_data[month] = []

                group1 = group.copy()
                group1.reset_index(drop=True, inplace=True)

                for op, realDelta in list(zip(group1.OriginPrice, group1.NormPriceDelta)):
                    if realDelta > 0:
                        real_inc_data[month].append((op, realDelta))
                    elif realDelta < 0:
                        real_dec_data[month].append((op, realDelta))


        fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))

        f1 = 0
        f2 = 0
        for month, real_data in real_dec_data.items():
            if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                # print(price_held_values)
                if f1 == 0:
                    plt.scatter(*zip(*real_data), s=80, facecolors='none', edgecolors='blue', label="Before 01-2020", linewidths=1.0, alpha=0.2)
                    f1 = 1
                else:
                    plt.scatter(*zip(*real_data), s=80, facecolors='none', edgecolors='blue', linewidths=1.0, alpha=0.2)
            else:
                if f2 == 0:
                    plt.scatter(*zip(*real_data), marker='x', color='red', label="After 01-2020", linewidths=1.0, alpha=0.2)
                    f2 = 1
                else:
                    plt.scatter(*zip(*real_data), marker='x', color='red', linewidths=1.0, alpha=0.2)

        plt.xlim(0,1.0)
        plt.ylim(-0.024, -0.016)
        plt.xlabel("Origin price ($)")
        plt.ylabel("Height")
        plt.legend()

        fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))

        f1 = 0
        f2 = 0
        for month, real_data in real_inc_data.items():
            if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                # print(price_held_values)
                if f1 == 0:
                    plt.scatter(*zip(*real_data), s=80, facecolors='none', edgecolors='blue', label="Before 01-2020", linewidths=1.0, alpha=0.2)
                    f1 = 1
                else:
                    plt.scatter(*zip(*real_data), s=80, facecolors='none', edgecolors='blue', linewidths=1.0, alpha=0.2)
            else:
                if f2 == 0:
                    plt.scatter(*zip(*real_data), marker='x', color='red', label="After 01-2020", linewidths=1.0, alpha=0.2)
                    f2 = 1
                else:
                    plt.scatter(*zip(*real_data), marker='x', color='red', linewidths=1.0, alpha=0.2)

        plt.xlim(0, 1.0)
        plt.ylim(0.026, 0.034)
        plt.xlabel("Origin price ($)")
        plt.ylabel("Height")
        plt.legend()

        plt.show()


def draw_normalized_price_delta_ecdf_per_region():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    normPrice_delta_dec_epoch_1 = []
    normPrice_delta_dec_epoch_2 = []

    normPrice_delta_inc_epoch_1 = []
    normPrice_delta_inc_epoch_2 = []

    for region in regions:
        print(region)
        # instList = ['D:/alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)
        total_num = len(instList)

        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_num)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df = df.sort_values('Timestamp')
            # df.reset_index(drop=True, inplace=True)
            df = df.sort_values('MyIndex')
            # df = df[df.clean == 1].copy()
            # df = df[df.missing_data == 0].copy()
            df.reset_index(drop=True, inplace=True)

            # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
            # plt.plot(df.Timestamp, df.NormPrice, '.-', color='black')

            # df1 = df[df['NormPriceDelta'] != 0.0].copy()
            # normPrice_delta_dec_epoch_1.extend(df1.NormPriceDelta[(df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta < 0.0)].dropna().tolist())
            # normPrice_delta_dec_epoch_2.extend(df1.NormPriceDelta[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta < 0.0)].dropna().tolist())
            #
            # normPrice_delta_inc_epoch_1.extend(df1.NormPriceDelta[(df1.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta > 0.0)].dropna().tolist())
            # normPrice_delta_inc_epoch_2.extend(df1.NormPriceDelta[(df1.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (df1.NormPriceDelta > 0.0)].dropna().tolist())

            # df2 = df[df.FixedPeakInfo.notnull()].copy()
            # df2['TimeDeltaHours'] = df2.TimeInSeconds.diff() / 3600
            # df2['PeakNormPriceDelta'] = df2.FixedPeakInfo.diff()

            df2 = df[df.PeakInfo48.notnull()].copy()
            df2['TimeDeltaHours'] = df2.TimeInSeconds.diff() / 3600
            df2['PeakNormPriceDelta'] = df2.PeakInfo48.diff()
            df2 = df2[df2.missing_data == 0].copy()
            df2 = df2[df2['PeakNormPriceDelta'] != 0.0].copy()

            normPrice_delta_dec_epoch_1.extend(df2.PeakNormPriceDelta[
                                                   (df2.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (
                                                           df2.PeakNormPriceDelta < 0.0)].dropna().tolist())
            normPrice_delta_dec_epoch_2.extend(df2.PeakNormPriceDelta[
                                                   (df2.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (
                                                           df2.PeakNormPriceDelta < 0.0)].dropna().tolist())

            normPrice_delta_inc_epoch_1.extend(df2.PeakNormPriceDelta[
                                                   (df2.Timestamp < pd.to_datetime("1/1/2020 00:00:00 AM")) & (
                                                           df2.PeakNormPriceDelta > 0.0)].dropna().tolist())
            normPrice_delta_inc_epoch_2.extend(df2.PeakNormPriceDelta[
                                                   (df2.Timestamp >= pd.to_datetime("1/1/2020 00:00:00 AM")) & (
                                                           df2.PeakNormPriceDelta > 0.0)].dropna().tolist())

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
    ax = fig.add_subplot(1, 1, 1)

    # ax.set_title("Normalized Jumps (0 hours)")
    # ax.set_title("Normalized Steps")

    normPrice_delta_inc_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_1)
    y = ecdf(normPrice_delta_inc_epoch_1)
    ax.step(normPrice_delta_inc_epoch_1, y, color='black', linestyle='--', where='post',
            label="Increase before 01-2020")

    print(
        "step inc epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_1, y)))

    normPrice_delta_inc_epoch_2.sort()
    ecdf = ECDF(normPrice_delta_inc_epoch_2)
    y = ecdf(normPrice_delta_inc_epoch_2)
    ax.step(normPrice_delta_inc_epoch_2, y, color='red', linestyle='--', where='post', label="Increase after 01-2020")

    print(
        "step inc epoch 2 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_inc_epoch_2, y)))

    normPrice_delta_dec_epoch_1 = [x * -1 for x in normPrice_delta_dec_epoch_1]
    normPrice_delta_dec_epoch_1.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_1)
    y = ecdf(normPrice_delta_dec_epoch_1)
    ax.step(normPrice_delta_dec_epoch_1, y, color='gray', linestyle='--', where='post', label="Decrease before 01-2020")

    print(
        "step dec epoch 1 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_1, y)))

    normPrice_delta_dec_epoch_2 = [x * -1 for x in normPrice_delta_dec_epoch_2]
    normPrice_delta_dec_epoch_2.sort()
    ecdf = ECDF(normPrice_delta_dec_epoch_2)
    y = ecdf(normPrice_delta_dec_epoch_2)
    ax.step(normPrice_delta_dec_epoch_2, y, color='orange', linestyle='--', where='post',
            label="Decrease after 01-2020")

    print(
        "step dec epoch 2 " + str(np.interp([0.019, 0.02, 0.021, 0.029, 0.03, 0.031], normPrice_delta_dec_epoch_2, y)))

    ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0., columnspacing=3.0)
    # ax.legend(loc='lower right')
    ax.set_xlabel("Jump height")
    # ax.set_xlabel("Step height")
    # ax.set_xticks(np.arange(0, 0.6, 0.01))
    # ax.set_xticklabels(np.arange(0, 0.6, 0.01), rotation=90)
    ax.set_xlim(0, 0.3)
    # ax.set_yticks(np.arange(0, 1.0, 0.1))
    # ax.set_yticklabels(np.arange(0, 1.0, 0.1), fontsize=8)
    ax.set_ylabel("Probability")

    print("Steps finished")

    # plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/normalized_jumps_ecdf_by_epoch.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_normalized_price_delta_ecdf_per_month():
    # regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
    #            'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
    #            'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    regions = ['*']

    # fig = plt.figure(figsize=set_size(fraction=4))
    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))

    for num, region in enumerate(regions, 1):
        # print(region)
        # instList = ['./alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
        # instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)
        total_count = len(instList)

        data = dict()
        data_dec = dict()
        data_inc = dict()
        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_count)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            # df = df.sort_values('Timestamp')
            df = df.sort_values('MyIndex')
            df = df[df.clean == 1].copy()
            df = df[df.missing_data == 0].copy()
            df.reset_index(drop=True, inplace=True)

            # df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
            df['month_year'] = df['Timestamp'].dt.to_period('M')

            grouped = df.groupby('month_year')
            for month, group in grouped:
                if month in data:
                    data[month].extend(group.NormPriceDelta.dropna().tolist())
                else:
                    data[month] = group.NormPriceDelta.dropna().tolist()

                group_dec = group[group['NormPriceDelta'] < 0.0].copy()
                if month in data_dec:
                    data_dec[month].extend(group_dec.NormPriceDelta.dropna().tolist())
                else:
                    data_dec[month] = group_dec.NormPriceDelta.dropna().tolist()

                group_inc = group[group['NormPriceDelta'] > 0.0].copy()
                if month in data_inc:
                    data_inc[month].extend(group_inc.NormPriceDelta.dropna().tolist())
                else:
                    data_inc[month] = group_inc.NormPriceDelta.dropna().tolist()

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
        data = dict(sorted(data.items()))
        # ax = fig.add_subplot(4, 5, num)
        # ax = fig.add_subplot(1, 1, 1)
        # ax.set_title("Decrease steps per month")

        f1 = 0
        f2 = 0
        for month, values in data.items():
            # values.sort()
            if values:
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 01-2020")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                else:
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="After 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')

        plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Step height")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(-0.2, 0.2, 0.01))
        plt.xlim(-0.2, 0.2)

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
        # # ax = fig.add_subplot(4, 5, num)
        # ax = fig.add_subplot(1, 2, 1)
        # ax.set_title("Decrease steps per month")
        #
        data_dec = dict(sorted(data_dec.items()))
        f1 = 0
        f2 = 0
        for month, values in data_dec.items():
            # values.sort()
            if values:
                values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                print(month, str(np.interp([0.019, 0.0195, 0.02, 0.0205, 0.021], values, y)))
                if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 01-2020")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                else:
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="After 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')

        plt.legend(loc='lower right', ncol=1)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Step height")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0.0, 0.2)

        fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
        # ax = fig.add_subplot(1, 2, 2)
        # ax.set_title("Increase steps per month")
        #
        data_inc = dict(sorted(data_inc.items()))
        f1 = 0
        f2 = 0
        for month, values in data_inc.items():
            # values.sort()
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)
                if month.to_timestamp(freq='M') < pd.to_datetime("1/1/2020 00:00:00 AM"):
                    if f1 == 0:
                        plt.step(values, y, '--', color='black', where='post', label="Before 01-2020")
                        f1 = 1
                    else:
                        plt.step(values, y, '--', color='black', where='post')
                else:
                    if f2 == 0:
                        plt.step(values, y, '-.', color='red', where='post', label="After 01-2020")
                        f2 = 1
                    else:
                        plt.step(values, y, '-.', color='red', where='post')

        plt.legend(loc='lower right', ncol=1)
        # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
        plt.xlabel("Step height")
        plt.ylabel("Probability")
        # ax.set_xticks(np.arange(0, 0.2, 0.01))
        plt.xlim(0.0, 0.2)

    # plt.tight_layout()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/analyze_slopes_by_month_decrease_vs_increase.png"
    # plt.savefig(file, bbox_inches="tight")
    plt.show()


# Draw the normalized price delta cdfs per region for each week separately.
def draw_normalized_price_delta_ecdf_separated_per_week():
    regions = ['ap-northeast-1', 'ap-south-1', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3', 'ap-southeast-5',
               'cn-beijing', 'cn-hangzhou', 'cn-hongkong', 'cn-huhehaote', 'cn-qingdao', 'cn-shanghai', 'cn-shenzhen',
               'cn-zhangjiakou', 'eu-central-1', 'eu-west-1', 'me-east-1', 'us-east-1', 'us-west-1']
    # regions = ['cn-beijing']
    # regions = ['*']

    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))

    # week_list = ['2019-50', '2019-51', '2019-52', '2020-00', '2020-01', '2020-02', '2020-03', '2020-04', '2020-05']
    week_list = ['2019-50', '2019-51', '2020-01']
    # week_list = ['2020-13', '2020-14', '2020-15']

    for num, week in enumerate(week_list, 1):
        print(week)
        data = dict()
        for region in regions:
            # print(region)
            # instList = ['./alibaba_work/cn-beijing_full_traces_updated/cn_beijing_e__ecs_sn1ne_4xlarge__optimized__vpc__clean.csv']
            instList = "F:/alibaba_work/" + region + "_full_traces_updated/*clean_2.csv"
            instList = glob.glob(instList)

            for inst in instList:
                df = pd.read_csv(inst)
                df['Timestamp'] = pd.to_datetime(df['Timestamp'])
                df = df.sort_values('Timestamp')
                df.reset_index(drop=True, inplace=True)

                # df = df[(df.Timestamp >= pd.to_datetime("12/16/2019 00:00:00 AM")) & (df.Timestamp < pd.to_datetime("2/9/2020 00:00:00 AM"))]
                # df['week'] = df['Timestamp'].dt.to_period('W')

                df['Year-Week'] = df['Timestamp'].dt.strftime('%Y-%W')
                df = df[df['Year-Week'] == week]

                df = df[df['NormPriceDelta'] < 0.0].copy()
                if region in data:
                    data[region].extend(df.NormPriceDelta.dropna().tolist())
                    # data[name].extend(group1.FixedNormPriceDelta.dropna().tolist())
                else:
                    data[region] = df.NormPriceDelta.dropna().tolist()
                    # data[name] = group1.FixedNormPriceDelta.dropna().tolist()

        # data = dict(sorted(data.items()))

        # ax = fig.add_subplot(2, 5, num)
        ax = fig.add_subplot(1, 3, num)
        ax.set_title(week)
        for region, values in data.items():
            if values:
                # values = [x * -1 for x in values]
                values.sort()
                ecdf = ECDF(values)
                y = ecdf(values)

                ax.step(values, y, '-', where='post', label=region)

        ax.set_xlim(-0.2, 0.01)

    # Create the legend
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=2)

    # handles, labels = ax.get_legend_handles_labels()
    # fig.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=6, mode="expand", borderaxespad=0.)
    # fig.subplots_adjust(bottom=0.75)
    # lax = fig.add_subplot(2, 2, 4)
    # lax.legend(handles, labels, borderaxespad=0, ncol=2)
    # lax.axis("off")
    # fig.legend(handles, labels, loc="center right", ncol=4, fancybox=True, shadow=True)
    # plt.subplots_adjust(right=0.85)
    # plt.tight_layout()
    plt.show()
    # file = "./cloud_analyzing_repo/alibaba_scripts/graphs/normalized_prices_ecdf_split_by_week.png"
    # plt.savefig(file, bbox_inches="tight")


def draw_jumps_data():
    regions = ['*']

    data = []

    for region in regions:
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        # instList = ["D:/alibaba_work/cn-beijing_full_traces_updated/cn_beijing_a__ecs_n4_4xlarge__optimized__clean_new_2.csv"]
        instList = "../../alibaba_work/" + region + "_full_traces_updated/*_clean_new_2.csv"
        instList = glob.glob(instList)
        total_num = len(instList)

        for num, inst in enumerate(instList, 1):
            print(inst, num, "/", total_num)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('MyIndex')

            if df.SpotPrice.nunique() > 1:
                # fig = plt.figure(tight_layout=True, figsize=set_size(fraction=2))
                # plt.plot(df.Timestamp, df.NormPrice, '.:', color='black')

                # print(df[['Timestamp','missing_data', 'FixedPeakInfo', 'NormPrice']].head(20))

                # df = df[df.clean == 1].copy()
                # df = df[df.FixedPeakInfo.notnull()].copy()
                df = df[df.PeakInfo48.notnull()].copy()
                # df.FixedPeakInfo = np.around(df.FixedPeakInfo, 2)
                df['TimeDeltaHours'] = df.TimeInSeconds.diff() / 3600
                # df['NormPriceJump'] = df.FixedPeakInfo.diff()
                df['NormPriceJump'] = df.PeakInfo48.diff()
                df['Slope'] = df.NormPriceJump / df.TimeDeltaHours

                # df = df[df.NormPriceJump != 0].copy()
                # # df = df[df.TimeDeltaHours != 0].copy()
                # # plt.scatter(df.Timestamp, df.NormPrice, color='blue')
                # # df.reset_index(inplace=True)
                # df['Steps'] = df['MyIndex'].diff() / 2
                # df.loc[(df['Min'].notnull()), 'Steps'] = (-1 * df.Steps)

                # print(df[['Timestamp', 'TimeDeltaHours', 'NormPriceJump', 'FixedPeakInfo', 'Slope']].head(30))
                # print(df[['Timestamp', 'TimeDeltaHours', 'NormPriceJump', 'Slope', 'Steps']])
                df = df[df.missing_data == 0].copy()
                df = df[df.Slope != 0].copy()
                # df.reset_index(drop=True, inplace=True)
                # plt.scatter(df.Timestamp, df.NormPrice, color='red')

                # df = df.dropna(subset=['TimeDeltaHours', 'NormPriceJump', 'Slope'])
                # df.reset_index(drop=True, inplace=True)
                # print(df[['Timestamp', 'missing_data', 'TimeDeltaHours', 'FixedPeakInfo', 'NormPrice']].head(20))

                data.extend(list(zip(df.TimeDeltaHours, df.NormPriceJump, df.Timestamp, df.Slope)))
                # data.extend(list(zip(df.TimeDeltaHours, df.NormPriceJump, df.Timestamp, df.Slope, df.Steps)))

    data = sorted(data, key=lambda x: x[2])
    # x, y, z, sl, st = zip(*data)
    x, y, z, sl = zip(*data)

    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    # plt.scatter(x, y, color='black', marker='+', label='Data')
    # # plt.plot(x, intercept + slope * np.array(x), label=line)
    # plt.title("All data")
    # plt.xlabel("Time interval (h)")
    # plt.ylabel("Normalized jumps ($)")
    # plt.legend(loc='lower right')

    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    # plt.scatter(z, x, color='black', marker='+', label='Data')
    # # plt.plot(x, intercept + slope * np.array(x), label=line)
    # plt.title("All data")
    # plt.xlabel("Timestamp")
    # plt.ylabel("Time interval (h)")
    # plt.legend(loc='lower right')

    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
    # plt.scatter(z, y, color='black', marker='+', label='Data')
    # plt.title("no hour filter")
    # plt.xticks(rotation=45)
    # plt.xlabel("Timestamp")
    # plt.ylabel("Jump height")

    # fig = plt.figure(tight_layout='True', figsize=(5.47807, 2.4651315))
    fig = plt.figure(tight_layout='True', figsize=set_size())
    plt.scatter(z, sl, color='black', marker='.', alpha=0.01)
    # plt.title("no hour filter")

    plt.annotate('A', xy=('2019-09-01', 0.12), xytext=('2019-09-20', 1), color='blue', fontweight='bold',
                 arrowprops=dict(arrowstyle='-|>', color='b'))
    plt.annotate('B', xy=('2019-12-25', -0.01), xytext=('2019-12-01', -1.5), color='orange', fontweight='bold',
                 arrowprops=dict(arrowstyle='-|>', color='orange'))
    plt.annotate('C', xy=('2020-02', -0.35), xytext=('2020-02-10', -1.5), color='magenta', fontweight='bold',
                 arrowprops=dict(arrowstyle='-|>', color='magenta'))
    plt.annotate('D', xy=('2020-12-05', 0.12), xytext=('2020-11-10', 1), color='green', fontweight='bold',
                 arrowprops=dict(arrowstyle='-|>', color='green'))
    plt.xticks(rotation=45)
    plt.xlabel("Timestamp")
    plt.ylabel("Jump slopes")
    plt.ylim(-2, 2)

    # fig = plt.figure(tight_layout='True', figsize=set_size(fraction=1))
    # plt.scatter(z, st, color='black', marker='+', label='Data')
    # plt.xlabel("Timestamp")
    # plt.ylabel("Number of steps")

    plt.show()
    # plt.savefig("C:/Users/user/Documents/RaaS/deconstructing-alibaba-cloud-pricing-paper/sigmetrics_figs/jump_slopes_per_timestamp_new.png")

def draw_step_data():
    regions = ['*']

    data_ceil = []
    data_floor = []
    count_t = 0
    for region in regions:
        print(region)
        # instList = "./alibaba_work/" + region + "_full_traces/*clean2.csv"
        # instList = ['../../alibaba_work/ap-northeast-1_full_traces_updated/ap_northeast_1a__ecs_sn1ne_4xlarge__optimized__clean_new_2.csv']
        # instList = ["../../alibaba_work\cn-beijing_full_traces_updated\cn_beijing_k__ecs_hfc6_3xlarge__optimized__clean_new_2.csv"]
        instList = "../../alibaba_work/*_full_traces_updated/*clean_new_2.csv"
        instList = glob.glob(instList)
        total_num = len(instList)

        for num, inst in enumerate(instList, 1):
            # print(inst, num, "/", total_num)
            df = pd.read_csv(inst)
            df['Timestamp'] = pd.to_datetime(df['Timestamp'])
            df = df.sort_values('MyIndex')
            df = df[df.clean == 1].copy()
            df = df[df.missing_data == 0].copy()
            df = df[df.OriginPrice >= 0.4].copy()

            # print(df.head())
            # df_ceil = df[(df.NormPriceDelta >= 0.031) & (df.NormPriceDelta <= 0.034)].copy()
            # df_floor = df[(df.NormPriceDelta >= 0.026) & (df.NormPriceDelta <= 0.029)].copy()
            df_ceil = df[(df.NormPriceDelta >= -0.019) & (df.NormPriceDelta <= -0.016)].copy()
            df_floor = df[(df.NormPriceDelta >= -0.024) & (df.NormPriceDelta <= -0.021)].copy()

            if not df_ceil.empty and not df_floor.empty:
                count_t += 1
                print(inst, "ceil: ", df_ceil.OriginPrice.unique(), "floor: ", df_floor.OriginPrice.unique())
                data_ceil.extend(list(zip(df_ceil.Timestamp, df_ceil.NormPriceDelta)))
                data_floor.extend(list(zip(df_floor.Timestamp, df_floor.NormPriceDelta)))
            # print(len(data))

    data_ceil = sorted(data_ceil, key=lambda x: x[0])
    data_floor = sorted(data_floor, key=lambda x: x[0])
    print(count_t, len(data_ceil), len(data_floor))
    fig = plt.figure(tight_layout='True', figsize=set_size(fraction=2))
    plt.scatter(*zip(*data_ceil), color='black', marker='x', alpha=0.1, label='ceil')
    plt.scatter(*zip(*data_floor), color='red', marker='x', alpha=0.1, label='floor')
    # plt.title("no hour filter")
    plt.xticks(rotation=45)
    plt.xlabel("Timestamp")
    plt.ylabel("Step height")
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))


    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate ecdf traces based on diff time between traces.')
    parser.add_argument("--region", help='The region name for which we want to calculate the new ECDFs')
    parser.add_argument("--instType", help='The inst type for which we want to calculate the ECDF graph')
    parser.add_argument("--function", help='The function we want to run')
    parser.add_argument("--slope", help='The slope we want to examine')
    parser.add_argument("--ephoc",
                        help='The ephoc we want to examine (1 - until September 2019, 2 - sincse September 2019)')
    parser.add_argument("--type", help='The type of epoch we are trying to find (NormPriceDelta / TimeDelta)')

    args = parser.parse_args()

    if args.region:
        region = args.region
    else:
        region = 'cn-beijing'

    if args.instType:
        instType = args.instType
    else:
        instType = 'r6'

    if args.function:
        func = args.function
        if func == 'add_time_and_price_delta_cols':
            print("Running add_time_and_price_delta_cols function...")
            add_time_and_price_delta_cols()

        elif func == 'draw_normalized_price_delta_ecdf_per_region':
            print('Running draw_normalized_price_delta_ecdf_per_region function...')
            draw_normalized_price_delta_ecdf_per_region()

        elif func == 'draw_normalized_price_delta_ecdf_per_month':
            print('Running draw_normalized_price_delta_ecdf_per_month...')
            draw_normalized_price_delta_ecdf_per_month()

        elif func == 'draw_normalized_price_delta_ecdf_separated_per_week':
            print('Running draw_normalized_price_delta_ecdf_separated_per_week function...')
            draw_normalized_price_delta_ecdf_separated_per_week()

        elif func == 'draw_jumps_data':
            print("Running draw_jumps_data function...")
            draw_jumps_data()

        elif func == 'new_quantization_algo':
            print("Running new_quantization_algo function...")
            new_quantization_algo()

        elif func == 'real_trace_data_divided_by_region':
            print("Running real_trace_data_divided_by_region function...")
            real_trace_data_divided_by_region()

        elif func == 'real_trace_data_divided_by_month':
            print("Running real_trace_data_divided_by_month function...")
            real_trace_data_divided_by_month()

        elif func == 'draw_price_delta_per_base_price':
            print("Running draw_price_delta_per_base_price function...")
            draw_price_delta_per_base_price()

        elif func == 'testing_quantization_algo':
            print("Running testing_quantization_algo function...")
            testing_quantization_algo()

        elif func == 'draw_step_data':
            print("Running draw_step_data function...")
            draw_step_data()

    print("Finish")
